variable "region" {
  type        = "string"
  description = "the aws region"
}

variable "aws_profile" {
  type = "string"
}

variable "unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
}