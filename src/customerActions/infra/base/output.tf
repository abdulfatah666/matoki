output "aws_ecs_task_definition_arn_customer" {
  value = aws_ecs_task_definition.rd_customer.arn
}

output "cluster_arn" {
  value = aws_ecs_cluster.base-cluster.arn
}

output "subnets" {
  value = aws_subnet.public_subnets.*.id
}

output "aws_ecs_task_definition_arn_integration" {
  value = aws_ecs_task_definition.rd_integration.arn
}

output "container_name" {
  value = "bc-ecs-customer-stack-base-${var.unique_tag}"
}