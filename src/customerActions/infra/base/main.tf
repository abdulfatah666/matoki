locals {

  availability_zones                  = ["${var.region}a", "${var.region}b"]
  subnets_cidrs_per_availability_zone = ["192.168.0.0/19", "192.168.32.0/19"]
}

data "aws_caller_identity" "current" {}

resource "aws_ecs_cluster" "base-cluster" {
  name = "bc-es-cluster-${var.unique_tag}"
}

# ---------------------------------------------------------------------------------------------------------------------
# AWS Virtual Private Network
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_vpc" "vpc" {
  cidr_block           = "192.168.0.0/16" # The CIDR block for the VPC.
  enable_dns_support   = true             # A boolean flag to enable/disable DNS support in the VPC.
  enable_dns_hostnames = true             # A boolean flag to enable/disable DNS hostnames in the VPC.
  tags = {
    Name = "bc-ecs-vpc-${var.unique_tag}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# AWS Internet Gateway
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_internet_gateway" "internet_gw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "bc-ecs-internet-gw-${var.unique_tag}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# AWS Subnets - Public
# ---------------------------------------------------------------------------------------------------------------------
# Subnets
resource "aws_subnet" "public_subnets" {
  count                   = length(local.availability_zones)
  vpc_id                  = aws_vpc.vpc.id
  availability_zone       = element(local.availability_zones, count.index)
  cidr_block              = element(local.subnets_cidrs_per_availability_zone, count.index)
  map_public_ip_on_launch = true
  tags = {
    Name = "bc-ecs-net-${element(local.availability_zones, count.index)}-${var.unique_tag}"
  }
}

# Public route table
resource "aws_route_table" "public_subnets_route_table" {
  count  = length(local.availability_zones)
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "bc-ecs-rt-${element(local.availability_zones, count.index)}-${var.unique_tag}"
  }
}

# Public route to access internet
resource "aws_route" "public_internet_route" {
  count = length(local.availability_zones)
  depends_on = [
    aws_internet_gateway.internet_gw,
    aws_route_table.public_subnets_route_table,
  ]
  route_table_id         = element(aws_route_table.public_subnets_route_table.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.internet_gw.id
}

# Association of Route Table to Subnets
resource "aws_route_table_association" "public_internet_route_table_associations" {
  count          = length(local.subnets_cidrs_per_availability_zone)
  subnet_id      = element(aws_subnet.public_subnets.*.id, count.index)
  route_table_id = element(aws_route_table.public_subnets_route_table.*.id, count.index)
}

resource "aws_cloudwatch_log_group" "ecs_lg" {
  name = "/ecs/bc-ecs-customer-stack-base-${var.unique_tag}"
}

module "container_definition" {
  source  = "cloudposse/ecs-container-definition/aws"
  version = "0.21.0"

  container_name   = "bc-ecs-customer-stack-base-${var.unique_tag}"
  container_image  = "${data.aws_caller_identity.current.account_id}.dkr.ecr.${var.region}.amazonaws.com/customer-stack-base-${var.aws_profile}-${var.unique_tag}:latest"
  container_memory = 3072
  container_cpu    = 1024
  essential        = true
  port_mappings    = []
  command          = ["customer", "apply"]
  log_configuration = {
    logDriver = "awslogs"
    "options" : {
      "awslogs-group" : aws_cloudwatch_log_group.ecs_lg.name,
      "awslogs-region" : var.region,
      "awslogs-stream-prefix" : "ecs"
    }
    secretOptions = []
  }
}

// Task Role

resource "aws_iam_role" "authenticated_role" {
  name = "bc-ecs-task-role-${var.unique_tag}"

  assume_role_policy = data.aws_iam_policy_document.authenticated_policy.json
}

data "aws_iam_policy_document" "authenticated_policy" {
  statement {
    effect = "Allow"
    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role_policy" "authenticated" {
  name = "bc-ecs-task-policy-${var.unique_tag}"
  role = aws_iam_role.authenticated_role.id

  policy = data.aws_iam_policy_document.authenticate_policy_document.json
}

data "aws_iam_policy_document" "authenticate_policy_document" {
  statement {
    effect = "Allow"
    actions = [
      "*",
    ]

    resources = ["*"]
  }
}

// Task execution role

resource "aws_iam_role" "authenticated_role_task_execution" {
  name = "bc-ecs-task-execution-role-${var.unique_tag}"

  assume_role_policy = data.aws_iam_policy_document.authenticated_policy.json
}

resource "aws_iam_role_policy" "authenticated_task_execution" {
  name = "bc-ecs-task-execution-policy-${var.unique_tag}"
  role = aws_iam_role.authenticated_role_task_execution.id

  policy = data.aws_iam_policy_document.authenticate_policy_document_task_execution.json
}

data "aws_iam_policy_document" "authenticate_policy_document_task_execution" {
  statement {
    effect = "Allow"
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = ["*"]
  }
}

resource "aws_ecs_task_definition" "rd_customer" {
  family                   = "bc-ecs-customer-stack-base-td-${var.unique_tag}"
  container_definitions    = "[ ${module.container_definition.json_map} ]"
  task_role_arn            = aws_iam_role.authenticated_role.arn
  execution_role_arn       = aws_iam_role.authenticated_role_task_execution.arn
  requires_compatibilities = ["EC2", "FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 3072
}

module "container_definition_integration" {
  source  = "cloudposse/ecs-container-definition/aws"
  version = "0.21.0"

  container_name   = "bc-ecs-customer-stack-base-${var.unique_tag}"
  container_image  = "${data.aws_caller_identity.current.account_id}.dkr.ecr.${var.region}.amazonaws.com/customer-stack-base-${var.aws_profile}-${var.unique_tag}:latest"
  container_memory = 3072
  container_cpu    = 1024
  essential        = true
  port_mappings    = []
  command          = ["integration", "apply"]
  log_configuration = {
    logDriver = "awslogs"
    "options" : {
      "awslogs-group" : "/ecs/bc-ecs-integration-stack-base-${var.unique_tag}",
      "awslogs-region" : var.region,
      "awslogs-stream-prefix" : "ecs"
    }
    secretOptions = []
  }
}

resource "aws_cloudwatch_log_group" "ecs_lg_integration" {
  name = "/ecs/bc-ecs-integration-stack-base-${var.unique_tag}"
}

resource "aws_ecs_task_definition" "rd_integration" {
  family                   = "bc-ecs-integration-stack-base-td-${var.unique_tag}"
  container_definitions    = "[ ${module.container_definition_integration.json_map} ]"
  task_role_arn            = aws_iam_role.authenticated_role.arn
  execution_role_arn       = aws_iam_role.authenticated_role_task_execution.arn
  requires_compatibilities = ["EC2", "FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 3072
}

resource "aws_ssm_parameter" "default_elastic_ebs_volume_size" {
  name  = "/base_stack/default_elastic_ebs_volume_size_${var.unique_tag}"
  type  = "String"
  value = var.aws_profile == "prod" || var.aws_profile == "stage" || var.aws_profile == "demo" ? 150 : 10
}

resource "aws_ssm_parameter" "default_elastic_data_node_type" {
  name  = "/base_stack/default_elastic_data_node_type_${var.unique_tag}"
  type  = "String"
  value = var.aws_profile == "prod" || var.aws_profile == "stage" || var.aws_profile == "demo" ? "m5.xlarge.elasticsearch" : "t2.small.elasticsearch"
}

resource "aws_ssm_parameter" "default_elastic_data_node_count" {
  name  = "/base_stack/default_elastic_data_node_count_${var.unique_tag}"
  type  = "String"
  value = var.aws_profile == "prod" || var.aws_profile == "stage" || var.aws_profile == "demo" ? 2 : 1
}

resource "aws_ssm_parameter" "default_elastic_master_enable" {
  name  = "/base_stack/default_elastic_master_enable_${var.unique_tag}"
  type  = "String"
  value = var.aws_profile == "prod" || var.aws_profile == "stage" || var.aws_profile == "demo" ? true : false
}

resource "aws_ssm_parameter" "default_elastic_master_type" {
  name  = "/base_stack/default_elastic_master_type_${var.unique_tag}"
  type  = "String"
  value = "c5.large.elasticsearch"
}

resource "aws_ssm_parameter" "default_elastic_master_count" {
  name  = "/base_stack/default_elastic_master_count_${var.unique_tag}"
  type  = "String"
  value = 3
}