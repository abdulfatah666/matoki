from builtins import int
from time import time

import boto3
from utilsPython.remote_lambda.invoke import lambda_invoke

from .BaseActivationAction import BaseActivationAction

ssm = boto3.client('ssm')


class SignUpAction(BaseActivationAction):
    def __init__(self):
        super().__init__("CustomerSignUp", ["cloudtrail", "aws-api-access"], )

    def _extract_integration_params(self, event):
        customer_properties = event['ResourceProperties']
        customer_name = customer_properties['CustomerName']
        external_id = customer_properties['ExternalId']
        sqs_queue_url = customer_properties['SqsQueueUrl']
        aws_account_id = customer_properties['AWSAccountId']
        deployment_region = customer_properties.get('DeploymentRegion')
        cross_account_role_arn = customer_properties.get('CrossAccountRoleArn')
        ct_stack_name = customer_properties['IntegrationName']
        template_version = customer_properties['TemplateVersion']

        integration_params = {
            "customer_name": customer_name,
            "external_id": external_id,
            "sqs_queue_url": sqs_queue_url,
            "aws_account_id": aws_account_id,
            "cross_account_role_arn": cross_account_role_arn,
            "deployment_region": deployment_region,
            "ct_stack_name": ct_stack_name,
            "template_version": template_version
        }
        if 'SecurityAccountId' in customer_properties:
            integration_params['security_account_id'] = customer_properties['SecurityAccountId']
        return integration_params

    def _is_really_delete(self, integration_params, tenant_env_details) -> bool:
        current_stack_version = tenant_env_details["cloud_trail_cf_details"].get("template_version", None)

        # From now on every create / update will save a version into the DB
        # If the DB has no version - no update occurred, and this is a real deletion event
        # If the DB has a version - verify it is the version saved in the DB currently
        return current_stack_version is None or integration_params["template_version"] == current_stack_version
