import json
import os
from abc import abstractmethod
from time import time

import boto3
from utilsPython.remote_lambda.invoke import lambda_invoke

from .baseCustomerAction import BaseCustomerAction


class BaseActivationAction(BaseCustomerAction):
    def __init__(self, activation_name: str, default_integrations: list):
        super().__init__()
        self.activation_name = activation_name
        self._default_integrations = default_integrations
        self.scanners_sf_arn = os.environ.get('SCANNERS_SF_ARN')

    def get_account_alias(self, cross_account_role_arn, customer_name, external_id):
        sts_client = boto3.client('sts')

        alias = "default"

        assumed_role_object = sts_client.assume_role(
            RoleArn=cross_account_role_arn,
            ExternalId=external_id,
            RoleSessionName="bridgecrew_{}_alias_session".format(customer_name)
        )
        credentials = assumed_role_object['Credentials']
        response = boto3.client('iam', aws_access_key_id=credentials['AccessKeyId'],
                                aws_secret_access_key=credentials['SecretAccessKey'],
                                aws_session_token=credentials['SessionToken']).list_account_aliases()['AccountAliases']
        if response:
            alias = response[0]
            self.logger.info(f"Account alias is {alias}")
        return alias

    @abstractmethod
    def _extract_integration_params(self, event: dict) -> dict:
        raise NotImplementedError()

    def on_create(self, customer_name, message, context):
        integration_params = self._extract_integration_params(message)
        aws_account_id = integration_params['aws_account_id']

        # get alias
        alias = self.get_account_alias(integration_params['cross_account_role_arn'], customer_name, integration_params['external_id'])
        self.logger.info(f"Connecting account {aws_account_id} ({alias}) for customer {customer_name}")

        # todo: check if customer exists

        # check if tenant env details exist
        env_details = self.get_tenant_env_details(aws_account_id)
        if isinstance(env_details, list) and env_details[0]["customer_name"] == customer_name:
            raise Exception('CloudFormation details already exist for current customer with this aws account id')

        lambda_invoke(lambda_function_name=self.customers_api_lambda_name, name="saveTenantEnvironmentDetails",
                      body=json.dumps({**integration_params, "activation_name": self.activation_name}))

        for integration_type in self._default_integrations:
            current_time = int(time() * 1000)
            payload = {
                "customerName": integration_params['customer_name'],
                "type": integration_type,
                "dateCreated": current_time,
                "updatedBy": 'Sign up Process',
                "lastStatusUpdate": current_time,
                "alias": alias,
                "params": {'customer_aws_account_id': integration_params['aws_account_id']}
            }
            self._trigger_integration_stack(payload, 'create')
            self.logger.info(f"Created {integration_type} for account {aws_account_id} ({alias}), for customer {customer_name}, has been triggered")

        lambda_invoke(lambda_function_name=self.integrations_api_lambda_name, name='triggerStepFunction', body={
            "name": f"bc-{customer_name}-{aws_account_id}-{int(time() * 1000)}",
            "params": {"customerName": customer_name},
            "stateMachineArn": self.scanners_sf_arn
        }, format="raw")

        return "Resource creation successful!"

    def on_update(self, customer_name, message, context):
        integration_params = self._extract_integration_params(message)
        aws_account_id = integration_params['aws_account_id']
        lambda_invoke(lambda_function_name=self.customers_api_lambda_name, name="saveTenantEnvironmentDetails",
                      body=json.dumps({**integration_params, "activation_name": self.activation_name}))

        for integration_type in self._default_integrations:
            existing_integrations = self._get_existing_integrations(customer_name, aws_account_id)
            for existing_integration in existing_integrations:
                if existing_integration['status'] == 'IN_PROGRESS':
                    self.logger.info(f"Skipping update for {integration_type} on account {aws_account_id} ({customer_name}) - it is now updating")
                    continue
                self._trigger_integration_stack({"customerName": integration_params['customer_name'], "id": existing_integration['id']}, "update")
                self.logger.info(f"Updated {integration_type} for account {aws_account_id}, for customer {customer_name}")
        return "Resource updated successful!"

    def on_delete(self, customer_name, message, context):
        integration_params = self._extract_integration_params(message)
        aws_account_id = integration_params['aws_account_id']
        customer_name = integration_params['customer_name']
        tenant_env_details = self.get_tenant_env_details(aws_account_id)

        is_delete = self._is_really_delete(integration_params, tenant_env_details)
        if not is_delete:
            self.logger.info(f"Skipping as this is an update operation")
            return

        self.logger.info(f"Disconnecting account {aws_account_id} (customer {customer_name})")
        existing_integrations = self._get_existing_integrations(customer_name, aws_account_id)
        for existing_integration in existing_integrations:
            self._trigger_integration_stack({"customerName": customer_name, "id": existing_integration['id']}, 'delete')
        lambda_invoke(lambda_function_name=self.customers_api_lambda_name, name="removeCustomerFromTenantEnvironmentDetails",
                      body={"awsAccountId": aws_account_id}, format="raw")
        self.logger.info(f"Disconnected account {aws_account_id} (customer {customer_name})")
        return "Resource deletion successful!"

    @abstractmethod
    def _is_really_delete(self, integration_params: dict, tenant_env_details: dict) -> bool:
        raise NotImplementedError()

    def _trigger_integration_stack(self, body: dict, action_name: str):
        lambda_invoke(lambda_function_name=self.integrations_api_lambda_name, name=action_name, body=body, format="raw")

    def _get_existing_integrations(self, customer_name: str, aws_account_id: str) -> list:
        result_integrations = []
        for integration_type in self._default_integrations:
            existing_integrations = lambda_invoke(lambda_function_name=self.integrations_api_lambda_name, name="getByType",
                                                  body={"customerName": customer_name, "type": integration_type},
                                                  format="raw")
            for existing_integration in existing_integrations:
                customer_account_id = existing_integration['params']['customer_aws_account_id']
                if customer_account_id == aws_account_id:
                    result_integrations.append(existing_integration)
        return result_integrations
