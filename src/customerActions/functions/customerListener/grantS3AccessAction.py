import json
import os

import boto3

from .baseCustomerAction import BaseCustomerAction

customers_api_lambda_name = os.environ.get('CUSTOMERS_API_LAMBDA_NAME', "")
client = boto3.client('lambda')


class GrantS3AccessAction(BaseCustomerAction):
    def action_name(self):
        return 'GrantS3Access'

    def on_create(self, customer_name, message, context):
        self.logger.info('Setting s3 access role details for customer {}'.format(customer_name))
        self.__set_s3_access(message)
        response_message = "S3 access role details has been set successfully for customer: {}".format(customer_name)
        return response_message

    def on_update(self, customer_name, message, context):
        self.on_create(customer_name, message, context)

    def on_delete(self, customer_name, message, context):
        self.logger.info('Removing S3 access role for customer {}'.format(customer_name))
        self.__delete_s3_access(message)
        self.logger.info('Removed S3 access role for customer {}'.format(customer_name))

    def __set_s3_access(self, message):
        props = message['ResourceProperties']
        aws_account_id = props['AWSAccountId']

        body_object = {
            "awsAccountId": aws_account_id,
            "s3AccessObject": {
                "role_arn": props['CrossAccountRoleArn'],
                "external_id": props['ExternalId'],
                "stack_id": message['StackId'],
                "bucket_name": props['BucketName']
            }
        }

        payload = json.dumps({
            "body": body_object,
            "headers": {
                'Content-Type': 'application/json'
            },
            "path": "/invoke/setS3AccessDetails"
        })
        self.update_tenant_env_details(payload)

    def __delete_s3_access(self, message):
        props = message['ResourceProperties']

        payload = json.dumps({
            "body": {
                "awsAccountId": props['AWSAccountId']
            },
            "headers": {
                "Content-Type": "application/json"
            },
            "path": "/invoke/removeS3AccessDetails"
        })

        self.update_tenant_env_details(payload)
