import json
import logging
import os
from .allowRemoteRemediationAction import AllowRemoteRemediationAction
from .signUpAction import SignUpAction
from .grantS3AccessAction import GrantS3AccessAction
from .guardrailAction import GuardrailAction
from .AWSConfigActivationAction import AWSConfigActivationAction
from urllib.request import build_opener, HTTPHandler, Request
# do not remove this import, used in tests
import urllib

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))

handlers_dictionary = {
    'AllowRemoteRemediation': AllowRemoteRemediationAction(),
    'CustomerSignUp': SignUpAction(),
    'GrantS3Access': GrantS3AccessAction(),
    'Guardrail': GuardrailAction(),
    'AWSConfigActivation': AWSConfigActivationAction()
}


def handle(event, context):
    message = "Message failed to parse"
    logger.info('REQUEST RECEIVED:\n %s', json.dumps(event))
    try:
        '''Handle Lambda event from AWS'''
        message = json.loads(event['Records'][0]['Sns']['Message'])
        if isinstance(message, str):
            message = json.loads(message)

        action_type = message['ResourceProperties']['ActionType']
        request_type = message['RequestType']
        action_handler = handlers_dictionary.get(action_type)

        customer_name = action_handler.get_customer_name(message)

        response_status = "SUCCESS"

        if request_type == 'Create':
            response_message = action_handler.on_create(customer_name, message, context)
        elif request_type == 'Update':
            response_message = action_handler.on_update(customer_name, message, context)
        elif request_type == 'Delete':
            response_message = action_handler.on_delete(customer_name, message, context)
        else:
            logger.error('FAILED!')
            response_status = "FAILED"
            response_message = "Unexpected event received from CloudFormation"

        if message.get('Source', None) != "Terraform":
            send_response(message, context, response_status, {"Message": response_message})

    except Exception as e:
        logger.error('FAILED!')
        logger.error(e, exc_info=True)
        send_response(message, context, "FAILED", {"Message": str(e)})
        raise e


def send_response(message, context, response_status, response_data):
    '''Send a resource manipulation status response to CloudFormation'''
    response_body = json.dumps({
        "Status": response_status,
        "Reason": "See the details in CloudWatch Log Stream: " + context.log_stream_name,
        "PhysicalResourceId": context.log_stream_name,
        "StackId": message.get('StackId', ''),
        "RequestId": message['RequestId'],
        "LogicalResourceId": message['LogicalResourceId'],
        "Data": response_data
    })

    logger.info('ResponseURL: %s', message['ResponseURL'])
    logger.info('ResponseBody: %s', response_body)

    opener = build_opener(HTTPHandler)
    response_body_encode = response_body.encode()
    request = Request(message['ResponseURL'], data=response_body_encode)
    request.add_header('Content-Type', '')
    request.add_header('Content-Length', str(len(response_body_encode)))
    request.get_method = lambda: 'PUT'
    response = opener.open(request)
    logger.info("Status code: %s", response.getcode())
    logger.info("Status message: %s", response.msg)
