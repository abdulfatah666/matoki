import json
import os
from time import time

import boto3
from packaging import version
from utilsPython.remote_lambda.invoke import lambda_invoke

from .baseCustomerAction import BaseCustomerAction

integrations_api_lambda_name = os.environ.get('INTEGRATIONS_API_LAMBDA_NAME')

client = boto3.client('lambda')


class AllowRemoteRemediationAction(BaseCustomerAction):
    def action_name(self):
        return "AllowRemoteRemediation"

    def on_create(self, customer_name, message, context):
        account_id = message['ResourceProperties']['AWSAccountId']
        print('set remote remediation role details for customer ', customer_name, "with account id ", account_id)
        self.__set_remote_remediation_role_details(message, context, customer_name)
        response_message = "Remote remediation role details has been set successfully for customer: " + customer_name
        return response_message

    def on_update(self, customer_name, message, context):
        return self.on_create(customer_name, message, context)

    def on_delete(self, customer_name, message, context):
        account_id = message['ResourceProperties']['AWSAccountId']
        print('delete remote remediation role details for customer ', customer_name, "with account id ", account_id)
        self.__remove_remote_remediation_role_details(message, context, customer_name)
        response_message = "Remote remediation role details has been set successfully for customer: {}".format(customer_name)
        return response_message

    # internal methods
    def __set_remote_remediation_role_details(self, event, context, customer_name):
        stackId = event['StackId']
        remote_remediation_configuration_props = event['ResourceProperties']
        customer_account = remote_remediation_configuration_props["AWSAccountId"]
        remote_remediation_role_arn = remote_remediation_configuration_props["CrossAccountRoleArn"]
        cf_installed_version = remote_remediation_configuration_props["TemplateVersion"]
        event_external_id = remote_remediation_configuration_props["ExternalId"]
        physical_resource_id = event.get("PhysicalResourceId")

        self.logger.info("Customer account: {}".format(customer_account))
        self.logger.info("Customer role arn: {}".format(remote_remediation_role_arn))
        self.logger.info("Customer cloud formation installed version: {}".format(cf_installed_version))
        self.logger.info("Customer cloud formation external id: {}".format(event_external_id))

        tenant_env_details = self.get_tenant_env_details(aws_account_id=customer_account)

        body_object = {
            "awsAccountId": customer_account,
            "remoteRemediationDetailsObject": {
                "role_arn": remote_remediation_role_arn,
                "installed_version": cf_installed_version,
                "stack_id": stackId,
                "external_id": event_external_id
            }
        }

        if physical_resource_id is not None:
            body_object["remoteRemediationDetailsObject"]["physical_resource_id"] = physical_resource_id

        payload = json.dumps({
            "body": body_object,
            "headers": {
                'Content-Type': 'application/json'
            },
            "path": "/invoke/setRemoteRemediationDetails"
        })
        response = self.update_tenant_env_details(payload)

        self.logger.info("Set details action lambda response: {}".format(response))
        current_time = int(time() * 1000)
        integration_type = event['ResourceProperties']['ActionType']
        req_body = {
            "customerName": customer_name,
            "type": integration_type,
            "dateCreated": current_time,
            "updatedBy": 'user',
            "lastStatusUpdate": current_time,
            "alias": 'default',
            "params": {
                "customer_aws_account_id": body_object['awsAccountId'],
            }
        }
        integration_payload = json.dumps({
            "body": req_body,
            "headers": {
                'Content-Type': 'application/json'
            },
            "path": "/invoke/create"
        })
        print("create new remediation integration in integrations table with payload", payload)
        response = self.update_integrations(integration_payload)
        return response

    def __remove_remote_remediation_role_details(self, event, context, customer_name):
        remote_remediation_configuration_props = event['ResourceProperties']
        customer_account = remote_remediation_configuration_props["AWSAccountId"]
        event_cf_version = remote_remediation_configuration_props["TemplateVersion"]
        event_physical_resource_id = event["PhysicalResourceId"]

        tenant_env_details = self.get_tenant_env_details(aws_account_id=customer_account)
        dynamo_physical_resource_id = (tenant_env_details['remote_remediation_cf_details']).get('physical_resource_id')
        dynamo_installed_version = (tenant_env_details['remote_remediation_cf_details']).get('installed_version')

        # In case the dynamo version in tenants_env_details table is bigger than the version in the event,
        # it means that this delete that was triggered as a result of upgrade action, and not triggered by the customer.
        # In that case, the remote remediation details should not be deleted
        if (dynamo_installed_version is not None) and (version.parse(dynamo_installed_version) > version.parse(event_cf_version)):
            self.logger.info("This delete was triggered as a result of stack update")
            return

        # In case there was a failure in upgrading the stack, we should prevent the triggered delete action from removing the customer
        # cloud formation details. Our indication for failure in upgrading the stack is if the delete event's physicalResourceId is
        # different than the upgrade event's physicalResourceId
        elif (event_physical_resource_id is not None) and (dynamo_physical_resource_id is not None) and (
                event_physical_resource_id != dynamo_physical_resource_id):
            payload = json.dumps({
                "body": {
                    "awsAccountId": customer_account,
                    "remoteRemediationDetailsObject": {
                        "role_arn": tenant_env_details['remote_remediation_cf_details']["remote_remediation_role_arn"],
                        "installed_version": dynamo_installed_version,
                        "stack_id": tenant_env_details['remote_remediation_cf_details']["stack_id"],
                        "external_id": tenant_env_details['remote_remediation_cf_details']["external_id"],
                        "physical_resource_id": context.log_stream_name
                    }
                },
                "headers": {
                    'Content-Type': 'application/json'
                },
                "path": "/invoke/setRemoteRemediationDetails"
            })

            self.update_tenant_env_details(payload)

        # In case of delete action that was triggered by the customer
        else:
            payload = json.dumps({
                "body": {
                    "awsAccountId": customer_account
                },
                "headers": {
                    'Content-Type': 'application/json'
                },
                "path": "/invoke/removeRemoteRemediationDetails"
            })

            response = self.update_tenant_env_details(payload)

            self.logger.info("removed details action lambda response: {}".format(response))
            remote_remediation_integration = lambda_invoke(lambda_function_name=integrations_api_lambda_name, name="getByType", body={"customerName": customer_name, "type": 'AllowRemoteRemediation'}, format="raw")
            integration_payload = json.dumps({
                "body": {"customerName": customer_name, "id": remote_remediation_integration[0]['id']},
                "headers": {
                    'Content-Type': 'application/json'
                },
                "path": "/invoke/delete"
            })
            print("remove remote remediation from integration details", integration_payload)
            response = self.delete_remediation_integration(integration_payload)
            return response

    def get_customer_name(self, message: dict) -> str:
        account_id = message['ResourceProperties']['AWSAccountId']
        payload = json.dumps({
            "body": {"awsAccountId": account_id},
            "headers": {
                'Content-Type': 'application/json'
            },
            "path": "/invoke/getCustomerName"
        })
        res = client.invoke(FunctionName=self.customers_api_lambda_name, Payload=payload)
        return json.loads(json.loads(res['Payload'].read())['body'])
