from .BaseActivationAction import BaseActivationAction


class AWSConfigActivationAction(BaseActivationAction):
    def __init__(self):
        super().__init__("AWSConfigActivation", ["aws-api-access"])

    def _extract_integration_params(self, event: dict):
        customer_properties = event['ResourceProperties']
        template_version = customer_properties['TemplateVersion']
        customer_name = customer_properties['CustomerName']
        external_id = customer_properties['ExternalId']
        aws_account_id = customer_properties['AWSAccountId']
        cross_account_role_arn = customer_properties['CrossAccountRoleArn']
        stack_name = customer_properties['IntegrationName']
        return {
            "customer_name": customer_name,
            "external_id": external_id,
            "aws_account_id": aws_account_id,
            "cross_account_role_arn": cross_account_role_arn,
            "stack_name": stack_name,
            "template_version": template_version
        }

    def _is_really_delete(self, integration_params: dict, tenant_env_details: dict) -> bool:
        return tenant_env_details["cross_account_cf_details"]["template_version"] == integration_params["template_version"]
