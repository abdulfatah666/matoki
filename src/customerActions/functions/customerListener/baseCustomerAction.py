import json
import logging
import os
from abc import ABC

import boto3

client = boto3.client('lambda')


class BaseCustomerAction(ABC):
    def __init__(self):
        self.logger = logging.getLogger()
        self.logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))
        self.customers_api_lambda_name = os.environ.get('CUSTOMERS_API_LAMBDA_NAME')
        self.integrations_api_lambda_name = os.environ.get('INTEGRATIONS_API_LAMBDA_NAME')
        self.base_stack_unique_tag = os.environ['BASE_STACK_UNIQUE_TAG']

    def action_name(self):
        pass

    def on_create(self, customer_name: str, message: dict, context) -> str:
        pass

    def on_update(self, customer_name: str, message: dict, context) -> str:
        pass

    def on_delete(self, customer_name: str, message: dict, context) -> str:
        pass

    def get_tenant_env_details(self, aws_account_id) -> dict:
        # Getting the customer dedicated externalId from tenant_env_details table
        tenant_env_details_params = json.dumps({
            "body": {
                "awsAccountId": aws_account_id
            },
            "headers": {
                'Content-Type': 'application/json'
            },
            "path": "/invoke/getTenantEnvironmentDetails",
        })

        tenant_env_details_res = client.invoke(FunctionName=self.customers_api_lambda_name, Payload=tenant_env_details_params)

        return json.loads(json.loads(tenant_env_details_res['Payload'].read())['body'])

    def update_tenant_env_details(self, payload):
        return client.invoke(FunctionName=self.customers_api_lambda_name, Payload=payload)

    def update_integrations(self, payload):
        return client.invoke(FunctionName=self.integrations_api_lambda_name, Payload=payload)

    def delete_remediation_integration(self, payload):
        return client.invoke(FunctionName=self.integrations_api_lambda_name, Payload=payload)

    def get_customer_name(self, message: dict) -> str:
        return message['ResourceProperties']['CustomerName']
