import json
import os

import boto3

from .baseCustomerAction import BaseCustomerAction

guardrail_api_lambda_name = os.environ.get('GUARDRAIL_API_LAMBDA_NAME', "")
client = boto3.client('lambda')


class GuardrailAction(BaseCustomerAction):
    def action_name(self):
        return 'Guardrail'

    def on_create(self, customer_name, message, context):
        self.logger.info('Setting Guardrail details for customer {}'.format(customer_name))
        self.__set_guardrail_details(message)
        response_message = "Guardrail details has been set successfully for customer: {}".format(customer_name)
        return response_message

    def on_update(self, customer_name, message, context):
        self.on_create(customer_name, message, context)

    def on_delete(self, customer_name, message, context):
        props = message['ResourceProperties']

        complete_activation_params = json.dumps({
            "body": {
                "actionType": "deactivate",
                "params": {
                    "aws_account_id": props['AWSAccountId'],
                    "violation_id": props['ViolationId'],
                    "stack_id": message.get("StackId"),
                }
            },
            "headers": {
                'Content-Type': 'application/json'
            },
            "path": "/invoke/completeAction"
        })

        response = client.invoke(FunctionName=guardrail_api_lambda_name, Payload=complete_activation_params)
        return json.loads(json.loads(response['Payload'].read())['body'])

    def __set_guardrail_details(self, message):
        props = message['ResourceProperties']

        complete_activation_params = json.dumps({
            "body": {
                "actionType": "activate",
                "params": {
                    "aws_account_id": props['AWSAccountId'],
                    "violation_id": props['ViolationId'],
                    "template_version": props['TemplateVersion'],
                    "stack_id": message.get("StackId"),
                    "cf_physical_id": message.get("PhysicalResourceId")
                }
            },
            "headers": {
                'Content-Type': 'application/json'
            },
            "path": "/invoke/completeAction"
        })

        response = client.invoke(FunctionName=guardrail_api_lambda_name, Payload=complete_activation_params)
        self.logger.info('Updated guardrail details')
        return json.loads(json.loads(response['Payload'].read())['body'])
