output "user_guid" {
  value = "${local.username}"
}

output "user_email" {
  value = "${var.user_email}"
}
