// Required parameters
//--------------------
variable "aws_profile" {
  type        = "string"
  description = "AWS provider profile"
}

variable "region" {
  type        = "string"
  description = "AWS provider region"
}

variable "user_pool_id" {
  type        = "string"
  description = "The user pool id on which we shpuld create the user"
}

variable "user_first_name" {
  description = "The first name of the cognito user to be created"
}

variable "user_family_name" {
  description = "The last name of the cognito user to be created"
}

variable "user_email" {
  description = "The email of the cognito user to be created"
}

variable "user_phone" {
  description = "The phone of the cognito user to be created"
}

variable "user_exists" {
  description = "Indicates whether the user exists already or not"
  default     = false
}

// Optional parameters
//--------------------
variable "desired_delivery_medium" {
  type        = "list"
  description = "Specify delivery medium to send the welcome message. Supported values: EMAIL, SMS. More than one value can be specified. The default is EMAIL"
  default     = ["EMAIL"]
}

variable "enable_mfa" {
  type        = "string"
  description = "Enable/Disable MFA. The default value is true."
  default     = "true"
}

variable "temp_password_length" {
  description = "The length of the generated temporary password"
  default     = 8
}

variable "password_contains_lowercase" {
  description = "Should generated temporary password contain lowercase characters"
  default     = true
}

variable "password_contains_uppercase" {
  description = "Should generated temporary password contain uppercase characters"
  default     = true
}

variable "password_contains_numbers" {
  description = "Should generated temporary password contain numeric characters"
  default     = true
}

variable "password_contains_special" {
  description = "Should generated temporary password contain special characters"
  default     = true
}

variable "special_characters_list" {
  description = "The special charachers set to use in generating password"
  default     = "!@#$%^*"
}