locals {
  user_attributes          = "[{\"Name\": \"name\", \"Value\": \"${var.user_first_name}\"},{\"Name\": \"family_name\", \"Value\": \"${var.user_family_name}\"} , {\"Name\": \"phone_number\", \"Value\": \"${var.user_phone}\"}]"
  desired_delivery_mediums = join(" ", var.desired_delivery_medium)
}

// Define policy for the temporary generated password
resource "random_string" "password" {
  count            = var.user_exists ? 0 : 1
  length           = var.temp_password_length
  upper            = var.password_contains_uppercase
  min_upper        = var.password_contains_uppercase ? 1 : 0
  lower            = var.password_contains_lowercase
  min_lower        = var.password_contains_lowercase ? 1 : 0
  number           = var.password_contains_numbers
  min_numeric      = var.password_contains_numbers ? 1 : 0
  special          = var.password_contains_special
  override_special = var.password_contains_special ? var.special_characters_list : ""
  min_special      = var.password_contains_special ? 1 : 0
}

// Creaing cognito user
resource "null_resource" "create_cognito_user" {
  count = var.user_exists ? 0 : 1
  triggers = {
    build_number = var.user_email
  }

  provisioner "local-exec" {
    command = "aws --profile=${var.aws_profile} --region=${var.region} cognito-idp admin-create-user --user-pool-id ${var.user_pool_id} --username ${var.user_email} --user-attributes='${local.user_attributes}' --desired-delivery-mediums ${local.desired_delivery_mediums} --temporary-password='${concat(random_string.password.*.result, list(""))[0]}'"
  }
}

resource "null_resource" "set_cognito_user_phone_mfa" {
  // This command will set the SMS MFA to true, so it should run only if the user is new and has a phone number.
  count = !var.enable_mfa ? 0 : (var.user_phone == "" && ! var.user_exists ? 0 : 1)

  triggers = {
    build_number = var.user_email
  }

  provisioner "local-exec" {
    command = "aws --profile=${var.aws_profile} --region=${var.region} cognito-idp admin-set-user-settings --user-pool-id ${var.user_pool_id}  --username ${var.user_email} --mfa-options DeliveryMedium=SMS,AttributeName=phone_number"
  }

  depends_on = [
    "null_resource.create_cognito_user",
  ]
}



resource "null_resource" "get_user_guid" {
  triggers = {
    build = timestamp()
  }

  provisioner "local-exec" {
    command = "aws cognito-idp admin-get-user --user-pool-id ${var.user_pool_id} --username ${var.user_email} --profile ${var.aws_profile} --region ${var.region} | printf $(jq .Username) > output.txt"
  }

  depends_on = [
    "null_resource.create_cognito_user",
  ]
}

// Reaing the Username (GUID generated by AWS) from the output file
data "local_file" "template_file_content" {
  filename = "./output.txt"

  depends_on = [
    "null_resource.get_user_guid",
  ]
}

locals {
  username = replace(data.local_file.template_file_content.content, "\"", "")
}

// Enable mfa for the user
resource "null_resource" "enable_mfa" {
  count = !var.enable_mfa ? 0 : (var.user_phone == "" && ! var.user_exists ? 0 : 1)

  triggers = {
    build_number = var.user_email
  }

  provisioner "local-exec" {
    command = "aws --profile=${var.aws_profile} --region=${var.region} cognito-idp admin-set-user-mfa-preference --user-pool-id ${var.user_pool_id} --username ${var.user_email} --sms-mfa-settings Enabled=${var.enable_mfa},PreferredMfa=${var.enable_mfa}"
  }

  depends_on = [
    "null_resource.create_cognito_user",
  ]
}

// Will be called only in *Destroy*. Will delete the user we created with the cli command
resource "null_resource" "delete_cognito_user" {
  provisioner "local-exec" {
    when    = "destroy"
    command = "aws --profile=${var.aws_profile} --region=${var.region} cognito-idp admin-delete-user --user-pool-id ${var.user_pool_id} --username ${var.user_email}"
  }
}
