resource "aws_dynamodb_table" "table" {
  count        = length(var.tables)
  name         = "${lookup(var.tables[count.index], "name")}${var.unique_tag}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = lookup(var.tables[count.index], "index_name", "customer_name")

  attribute {
    name = lookup(var.tables[count.index], "index_name", "customer_name")
    type = lookup(var.tables[count.index], "index_type", "S")
  }

  server_side_encryption {
    enabled = var.server_side_encryption
  }

  # When restoring a table via point_in_time_recovery you must manually set up the following on the restored table:
  # - Auto scaling policies
  # - AWS Identity and Access Management (IAM) policies
  # - Amazon CloudWatch metrics and alarms
  # - Tags
  # - Stream settings
  # - Time to Live (TTL) settings

  # Currently we dont use any of these settings
  point_in_time_recovery {
    enabled = true
  }
}
