output "table_names" {
  value = aws_dynamodb_table.table.*.name
}

output "table_indices" {
  value = aws_dynamodb_table.table.*.hash_key
}

output "table_arns" {
  value = aws_dynamodb_table.table.*.arn
}
