variable "tables" {
  type        = "list"
  description = "A list of the table objects to be created"

  // Full example:
  // {
  //   name           = "loggly-handler-metadata-nimtest"
  //   index_name     = "customer-name"
  //   index_type     = "S"
  // }
  // DO NOT ADD MORE TABLES HERE - this module is broken and will delete existing tables if index of table in list changes!!
  default = [
    {
      name       = "loggly-handler-metadata"
      index_name = "customer-name"
    },
    {
      name = "tenants"
    },
    {
      name       = "integrations"
      index_name = "customer-name"
    },
    {
      name       = "tenants_lacework"
      index_name = "aws_account_id"
    },
    {
      name       = "violations_summary"
      index_name = "id"
    },
    {
      name       = "active_guardrails"
      index_name = "aws_account_id"
    },
    {
      name       = "api_tokens"
      index_name = "token"
    }
  ]
  // DO NOT ADD MORE TABLES HERE - this module is broken and will delete existing tables if index of table in list changes!!
}

variable "server_side_encryption" {
  description = "Select whether the tables created will have sserver side encryption or not"
  default     = false
}

variable "unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
}
