output "sns_topic_arn" {
  value       = concat(aws_sns_topic.sns_topic.*.arn, list(""))[0]
  description = "The created sns arn."
}

output "sns_topic_name" {
  value = aws_sns_topic.sns_topic.name
}
