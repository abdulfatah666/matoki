variable "region" {
  type        = "string"
  description = "AWS provider region"
}

variable "aws_profile" {
  type        = "string"
  description = "AWS provider profile"
}

variable "sns_topic_name" {
  description = "SNS name"
  type        = "string"
}

variable "sns_topic_display_name" {
  description = "SNS display name"
  type        = "string"
  default     = ""
}

variable delivery_policy {
  description = "This policy defines how Amazon SNS retries failed deliveries to HTTP/S endpoints"
  type        = "string"

  default = <<EOF
{
  "http": {
    "defaultHealthyRetryPolicy": {
      "minDelayTarget": 20,
      "maxDelayTarget": 20,
      "numRetries": 3,
      "numMaxDelayRetries": 0,
      "numNoDelayRetries": 0,
      "numMinDelayRetries": 0,
      "backoffFunction": "linear"
    },
    "disableSubscriptionOverrides": false,
    "defaultThrottlePolicy": {
      "maxReceivesPerSecond": 1
    }
  }
}
EOF
}

variable "subscribed_lambda_functions_arn_list" {
  description = "subscribed lambda functions arns list"
  type = "list"
  default = []
}

variable "unique_tag" {
  type = "string"
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
}

variable "monitor" {
  type = "map"

  default = {
    enable = false
    sns_topic_error_name = ""
  }
}
