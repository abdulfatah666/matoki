provider "aws" {
  profile = var.aws_profile
  region  = var.region
}

locals {
  topic_name = "${var.sns_topic_name}${var.unique_tag}"
}

resource "aws_sns_topic" "sns_topic" {
  name            = local.topic_name
  display_name    = var.sns_topic_display_name == "" ? var.sns_topic_name : var.sns_topic_display_name
  delivery_policy = var.delivery_policy
}

resource "aws_sns_topic_policy" "sns_topic_policy_default" {
  arn = aws_sns_topic.sns_topic.arn

  policy = data.aws_iam_policy_document.sns_topic_policy.json
}

data "aws_iam_policy_document" "sns_topic_policy" {
  statement {
    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:RemovePermission",
      "SNS:Receive",
      "SNS:Publish",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:AddPermission",
    ]

    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = [
      aws_sns_topic.sns_topic.arn,
    ]
  }
}

resource "aws_sns_topic_subscription" "lambda_subscription" {
  count = length(var.subscribed_lambda_functions_arn_list)

  topic_arn = aws_sns_topic.sns_topic.arn
  protocol  = "lambda"
  endpoint  = var.subscribed_lambda_functions_arn_list[count.index]
}

// ------ Monitor ------

data "aws_sns_topic" "sns_topic_monitor_error" {
  count = var.monitor["enable"] ? 1 : 0
  name  = var.monitor["sns_topic_error_name"]
}

resource "aws_cloudwatch_metric_alarm" "create_alarm_lambda" {
  count               = var.monitor["enable"] ? 1 : 0
  alarm_name          = "sns_errors_alarm_${local.topic_name}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "10"
  metric_name         = "NumberOfNotificationsFailed"
  namespace           = "AWS/SNS"

  dimensions = {
    TopicName = local.topic_name
  }

  treat_missing_data = "notBreaching"
  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors sns failed"

  alarm_actions = data.aws_sns_topic.sns_topic_monitor_error.*.arn
}

// ------------------
