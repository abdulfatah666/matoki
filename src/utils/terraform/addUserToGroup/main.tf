// Add the user to group
resource "null_resource" "add_user_to_group" {
  triggers = {
    "timestamp" = timestamp()
  }

  provisioner "local-exec" {
    command = "aws cognito-idp admin-add-user-to-group --user-pool-id ${var.user_pool_id} --group-name ${var.group_name} --username ${var.username} --profile=${var.aws_profile} --region=${var.region}"
  }
}
