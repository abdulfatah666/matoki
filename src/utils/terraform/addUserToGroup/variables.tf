variable "aws_profile" {
  type        = "string"
  description = "AWS provider profile"
}

variable "region" {
  type        = "string"
  description = "AWS provider region"
}

variable "group_name" {
  type        = "string"
  description = "The name of the group to add the user to"
}

variable "user_pool_id" {
  type        = "string"
  description = "The user pool id on which we shpuld create the user"
}

variable "username" {
  type        = "string"
  description = "The user's username"
}
