data "aws_iam_policy_document" "policy_document" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      var.customer_assume_role_arn,
    ]
  }
}

data "aws_lambda_function" "lambda_function" {
  function_name = var.function_name
}

data "aws_iam_role" "lambda_role" {
  name = substr(data.aws_lambda_function.lambda_function.role, 31, length(data.aws_lambda_function.lambda_function.role) - 31)
}

resource "aws_iam_role_policy" "specific_policy" {
  name   = "bc-ap-${var.prefix_name}-${var.customer_name}-${var.customer_aws_account_id}"
  policy = data.aws_iam_policy_document.policy_document.json
  role   = data.aws_iam_role.lambda_role.id
}

resource "aws_cloudwatch_event_rule" "console" {
  name                = "bc-er-${var.prefix_name}-${var.customer_name}-${var.customer_aws_account_id}"
  description         = "${var.function_name} event rule"
  schedule_expression = var.schedule_expression
}

resource "aws_cloudwatch_event_target" "event_target" {
  rule      = aws_cloudwatch_event_rule.console.name
  target_id = "SendToLambdaToCollectCloudFormation"
  arn       = data.aws_lambda_function.lambda_function.arn
  input     = "{\"accountId\":\"${var.customer_aws_account_id}\"}"
}


resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "bc-ae-${var.prefix_name}-${var.customer_name}-${var.customer_aws_account_id}"
  action        = "lambda:InvokeFunction"
  function_name = data.aws_lambda_function.lambda_function.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.console.arn
}
