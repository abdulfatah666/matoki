variable "prefix_name" {
  type = "string"
}
variable "function_name" {
  type = "string"
}
variable "customer_name" {
  type = "string"
}
variable "customer_aws_account_id" {
  type = "string"
}
variable "customer_assume_role_arn" {
  type = "string"
}
variable "schedule_expression" {
  description = "schedule expression"
  default     = "rate(30 minutes)"
  type        = "string"
}