variable "region" {
  type        = "string"
  description = "Aws region"
}

variable "aws_profile" {
  type        = "string"
  description = "AWS profile"
}

variable "unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
}

variable "name" {
  description = "A name of the VPC you want to create"
  type        = "string"
}

variable "cidr_block" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  type        = "string"
}

variable "subnets_cidrs_per_availability_zone" {
  description = "List of CIDR blocks to associate with the VPC for each availability zone"
  type        = list(string)
}

variable "module_depends_on" {
  type    = any
  default = null
}
