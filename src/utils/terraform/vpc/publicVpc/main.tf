
module "consts" {
  source = "../../../../utils/terraform/consts"
}

locals {
  availability_zones                  = ["${var.region}a", "${var.region}b"]
  subnets_cidrs_per_availability_zone = var.subnets_cidrs_per_availability_zone

}


# ---------------------------------------------------------------------------------------------------------------------
# AWS Virtual Private Network
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_vpc" "vpc" {
  cidr_block           = var.cidr_block # The CIDR block for the VPC.
  enable_dns_support   = true           # A boolean flag to enable/disable DNS support in the VPC.
  enable_dns_hostnames = true           # A boolean flag to enable/disable DNS hostnames in the VPC.
  tags = {
    Name = "bc-${var.name}-vpc-${var.unique_tag}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# AWS Internet Gateway
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_internet_gateway" "internet_gw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "bc-${var.name}-internet-gw-${var.unique_tag}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# AWS Subnets - Public
# ---------------------------------------------------------------------------------------------------------------------
# Subnets
resource "aws_subnet" "public_subnets" {
  count                   = length(local.availability_zones)
  vpc_id                  = aws_vpc.vpc.id
  availability_zone       = element(local.availability_zones, count.index)
  cidr_block              = element(local.subnets_cidrs_per_availability_zone, count.index)
  map_public_ip_on_launch = true
  tags = {
    Name = "bc-${var.name}-pub-subnet-${element(local.availability_zones, count.index)}-${var.unique_tag}"
  }
  depends_on = [var.module_depends_on]
}

# Public route table
resource "aws_route_table" "public_subnets_route_table" {
  count  = length(local.availability_zones)
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "bc-${var.name}-rt-${element(local.availability_zones, count.index)}-${var.unique_tag}"
  }
}

# Public route to access internet
resource "aws_route" "public_internet_route" {
  count = length(local.availability_zones)
  depends_on = [
    aws_internet_gateway.internet_gw,
    aws_route_table.public_subnets_route_table,
  ]
  route_table_id         = element(aws_route_table.public_subnets_route_table.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.internet_gw.id
}

# Association of Route Table to Subnets
resource "aws_route_table_association" "public_internet_route_table_associations" {
  count          = length(local.subnets_cidrs_per_availability_zone)
  subnet_id      = element(aws_subnet.public_subnets.*.id, count.index)
  route_table_id = element(aws_route_table.public_subnets_route_table.*.id, count.index)
}