locals {
  variables_string = format("%s %s %s", var.variable_string, "--tag", var.unique_tag)
}

data "external" "hash" {
  program     = ["node", "index.js", "${abspath(path.root)}/${var.path_to_serverless_yml}"]
  working_dir = "../../../devTools/hash-dir"
}

resource "null_resource" "sls-deploy" {
  triggers = {
    //    The timestamp always changes, which causes the script to run every terraform apply
    build_number = data.external.hash.result.hash
  }

  provisioner "local-exec" {
    command = "cd ${var.path_to_serverless_yml}\nnpm i\nsls deploy --deploymentBucket ${var.sls_bucket_name} --aws-profile ${var.aws_profile} ${local.variables_string} -r ${var.region}"
  }
}

resource "null_resource" "sls-remove" {
  provisioner "local-exec" {
    when    = "destroy"
    command = "cd ${var.path_to_serverless_yml}\nnpm i\nsls remove --force --deploymentBucket ${var.sls_bucket_name} --aws-profile ${var.aws_profile} -r ${var.region} ${local.variables_string}"
  }
}

// ------ Monitor ------

data "aws_sns_topic" "sns_topic_monitor_error" {
  count = var.monitor["enable"] ? 1 : 0
  name  = var.monitor["sns_topic_error_name"]
}

resource "aws_cloudwatch_metric_alarm" "create_alarm_lambda" {
  count               = var.monitor["enable"] ? length(var.lambda_names) : 0
  alarm_name          = "lambda-errors-alarm-${var.lambda_names[count.index]}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "Errors"
  namespace           = "AWS/Lambda"

  dimensions = {
    FunctionName = var.lambda_names[count.index]
  }

  treat_missing_data = "notBreaching"
  period             = "300"
  statistic          = "Sum"
  threshold          = "1"
  alarm_description  = "This metric monitors lambda errors"

  alarm_actions = data.aws_sns_topic.sns_topic_monitor_error.*.arn
}
