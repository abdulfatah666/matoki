output "arn" {
  value = local.output_as_json["DomainStatus"]["ARN"]
}

output "es_endpoint" {
  value = local.output_as_json["DomainStatus"]["Endpoint"]
}

output "kibana_endpoint" {
  value = "${local.output_as_json["DomainStatus"]["Endpoint"]}/_plugin/kibana/"
}