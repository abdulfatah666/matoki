variable "profile" {
  type        = "string"
  description = "AWS profile"
}

variable "region" {
  type        = "string"
  description = "Aws region"
}

variable "es_domain_name" {
  type        = "string"
  description = "The es domain name"
}
