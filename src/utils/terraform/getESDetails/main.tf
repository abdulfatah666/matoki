data "template_file" "output_name" {
  template = "${path.module}/output.log"
}

data "local_file" "local_output_file" {
  filename   = data.template_file.output_name.rendered
  depends_on = [null_resource.get-es-details]
}

resource "null_resource" "get-es-details" {
  triggers = {
    timestamp = timestamp()
  }
  provisioner "local-exec" {
    command = "for i in 1 2 3 4 5; do aws es describe-elasticsearch-domain --domain-name ${var.es_domain_name} --region ${var.region} --profile ${var.profile} > ${data.template_file.output_name.rendered}  || echo '{\"DomainStatus\": {\"ARN\": \"\", \"Endpoint\": \"\"}}' > ${data.template_file.output_name.rendered} && break || sleep 30; done"
  }
}

locals {
  output_as_json = jsondecode(data.local_file.local_output_file.content)
}
