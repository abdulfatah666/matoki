provider "aws" {
  region  = var.region
  profile = var.aws_profile
}

locals {
  variables_string = var.unique_tag == "" ? var.variable_string : format("%s %s %s", var.variable_string, "--tag", var.unique_tag)
}

data "external" "hash" {
  program     = ["node", "index.js", "${abspath(path.root)}/${var.path_to_serverless_yml}"]
  working_dir = "../../../devTools/hash-dir"
}

resource "null_resource" "sls-deploy" {
  triggers = {
    //    The timestamp always changes, which causes the script to run every terraform apply
    build_hash = data.external.hash.result.hash
  }

  provisioner "local-exec" {
    command = <<EOT
      path_to_severless=$(echo ${var.path_to_serverless_yml} | sed 's:/*$::')
      export SLS_PATH=$path_to_severless-${var.region}-$(date +%s)
      cp -r ${var.path_to_serverless_yml} $SLS_PATH
      base_path=$(pwd)
      cd $SLS_PATH
      npm i
      sls deploy --deploymentBucket ${var.sls_bucket_name} --aws-profile ${var.aws_profile} ${local.variables_string} -r ${var.region}
      code=$?
      cd $base_path
      rm -rf $SLS_PATH
      exit $code
    EOT
  }
}

// This gets the lambda that was created in the sls-deploy resource
data "aws_lambda_function" "existing_lambdas" {
  count = length(var.lambda_names)
  function_name = var.lambda_names[count.index]
  depends_on = ["null_resource.sls-deploy"]
}

resource "null_resource" "sls-remove" {
  provisioner "local-exec" {
    when = "destroy"
    command = <<EOT
      path_to_severless=$(echo ${var.path_to_serverless_yml} | sed 's:/*$::')
      export SLS_PATH=$path_to_severless-${var.region}-$(date +%s)
      cp -r ${var.path_to_serverless_yml} $SLS_PATH
      base_path=$(pwd)
      cd $SLS_PATH
      npm i
      sls remove --force --deploymentBucket ${var.sls_bucket_name} --aws-profile ${var.aws_profile} -r ${var.region} ${local.variables_string}
      code=$?
      cd $base_path
      rm -rf $SLS_PATH
      exit $code
    EOT
  }
}

// ------ Monitor ------

data "aws_sns_topic" "sns_topic_monitor_error" {
  count = var.monitor["enable"] ? 1 : 0
  name  = var.monitor["sns_topic_error_name"]
}

resource "aws_cloudwatch_metric_alarm" "create_alarm_lambda" {
  count               = var.monitor["enable"] ? length(var.lambda_names) : 0
  alarm_name          = "lambda-errors-alarm-${var.lambda_names[count.index]}"
  comparison_operator = lookup(var.monitor, "comparison_operator", "GreaterThanOrEqualToThreshold")
  evaluation_periods  = lookup(var.monitor, "evaluation_periods", "1")
  metric_name         = lookup(var.monitor, "metric_name", "Errors")
  namespace           = lookup(var.monitor, "namespace", "AWS/Lambda")

  dimensions = {
    FunctionName = var.lambda_names[count.index]
  }

  treat_missing_data = lookup(var.monitor, "treat_missing_data", "notBreaching")
  period             = lookup(var.monitor, "period", "300")
  statistic          = lookup(var.monitor, "statistic", "Sum")
  threshold          = lookup(var.monitor, "threshold", "1")
  alarm_description  = lookup(var.monitor, "alarm_description", "This metric monitors lambda errors")

  alarm_actions = data.aws_sns_topic.sns_topic_monitor_error.*.arn
}
