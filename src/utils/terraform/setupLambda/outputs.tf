output "function_arn" {
  value = data.aws_lambda_function.existing_lambdas.*.arn
}

output "function_name" {
  value = data.aws_lambda_function.existing_lambdas.*.function_name
}

output "invoke_arn" {
  value = data.aws_lambda_function.existing_lambdas.*.invoke_arn
}
