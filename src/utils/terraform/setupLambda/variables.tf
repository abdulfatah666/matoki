variable "region" {
  type        = "string"
  description = "Aws region"
}

variable "aws_profile" {
  type        = "string"
  description = "AWS profile"
}

variable "path_to_serverless_yml" {
  description = "The path to the serverless deployment file, serverless.yml"
  type        = "string"
}

variable "variable_string" {
  description = "The string that describes the variables formatted for sls deploy"
  default     = ""
}

variable "plugins" {
  type = "list"

  default = [
    "serverless-python-requirements",
    "serverless-package-external",
  ]
}

variable "lambda_names" {
  description = "The list of names of the lambdas that are created"
  type        = "list"
}

variable "monitor" {
  type = "map"

  default = {
    enable               = false
    sns_topic_error_name = ""
  }
}

variable "unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
  default     = ""
}

variable "sls_bucket_name" {
  description = "A name for deployment bucket that all serverless function deployment state is saved on"
  type        = "string"
}
