
output "ecs_cluster_id" {
  value = aws_ecs_cluster.base-cluster.id
}

output "ecs_cluster_name" {
  value = aws_ecs_cluster.base-cluster.name
}

output "ecs_log_name" {
  value = aws_cloudwatch_log_group.ecs_log.name
}

output "authenticated_role_arn" {
  value = aws_iam_role.authenticated_role.arn
}

output "authenticated_role_task_execution_arn" {
  value = aws_iam_role.authenticated_role_task_execution.arn
}