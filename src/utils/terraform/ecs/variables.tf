variable "region" {
  type        = "string"
  description = "Aws region"
}

variable "aws_profile" {
  type        = "string"
  description = "AWS profile"
}

variable "unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
}

variable "name" {
  description = "A name of the ECS you want to create"
  type        = "string"
}