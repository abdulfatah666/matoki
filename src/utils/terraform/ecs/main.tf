
module "consts" {
  source = "../../../utils/terraform/consts"
}

data "aws_caller_identity" "current" {}

resource "aws_ecs_cluster" "base-cluster" {
  name = "bc-${var.name}-cluster-${var.unique_tag}"
}
resource "aws_cloudwatch_log_group" "ecs_log" {
  name = "/ecs/bc-${var.name}-ecs-log-${var.unique_tag}"
}

// Task Role
resource "aws_iam_role" "authenticated_role" {
  name               = "bc-${var.name}-ecs-task-role-${var.unique_tag}"
  assume_role_policy = data.aws_iam_policy_document.authenticated_policy.json
}

data "aws_iam_policy_document" "authenticated_policy" {
  statement {
    effect = "Allow"
    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role_policy" "authenticated" {
  name   = "bc-${var.name}-ecs-task-policy-${var.unique_tag}"
  role   = aws_iam_role.authenticated_role.id
  policy = data.aws_iam_policy_document.authenticate_policy_document.json
}

data "aws_iam_policy_document" "authenticate_policy_document" {
  statement {
    effect = "Allow"
    actions = [
      "*",
    ]

    resources = ["*"]
  }
}

// Task execution role

resource "aws_iam_role" "authenticated_role_task_execution" {
  name               = "bc-${var.name}-ecs-task-execution-role-${var.unique_tag}"
  assume_role_policy = data.aws_iam_policy_document.authenticated_policy.json
}

resource "aws_iam_role_policy" "authenticated_task_execution" {
  name   = "bc-${var.name}-ecs-task-execution-policy-${var.unique_tag}"
  role   = aws_iam_role.authenticated_role_task_execution.id
  policy = data.aws_iam_policy_document.authenticate_policy_document_task_execution.json
}

data "aws_iam_policy_document" "authenticate_policy_document_task_execution" {
  statement {
    effect = "Allow"
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = ["*"]
  }
}


