output "monitor_sns_topic_error" {
  value = "bc-cloudwatch-alarm-error"
}

output "remediations_sns_topic_name" {
  value = "remediations"
}

output "root_cause_analysis_function_name" {
  value = "root-cause-analysis"
}

output "ec2_tags_function_name" {
  value = "ec2-tags"
}

output "kms_alias_function_name" {
  value = "bc-enrich-kms-alias"
}
output "vpc_scanner_function_name" {
  value = "bc-vpc-scanner"
}

output "cloudmapper_api_function_name" {
  value = "bc-cloudmapper-api"
}

output "enrichment_account_admins_function_name" {
  value = "enrich-account-admins"
}

output "pull_from_api_producer_function_name" {
  value = "bc-handle-pull-logs-from-api"
}

output "scanners_cloudwatch_stepfunction_trigger_role_name" {
  value = "bc-scanners-trigger"
}

output "bc_scanners_step_function_name" {
  value = "bc-scanners-sfn"
}

output "checkov_step_function_name" {
  value = "bc-checkov-jobs"
}

output "checkov_cloudwatch_stepfucntion_trigger_role_name" {
  value = "bc-checkov"
}

output "api_name" {
  value = "BridgeCrew API"
}

output "compliances_module_name" {
  value = "bc-compliances"
}

output "scanners_module_name" {
  value = "bc-scanners"
}

output "github_module_name" {
  value = "bc-github"
}

output "authorization_module_name" {
  value = "bc-authorization"
}

output "cloudformation_customer_config_template_name" {
  value = "config_template.yml"
}

output "cloudformation_customer_s3_access_template_name" {
  value = "s3_access_template.json"
}

output "latest_remediation_cf_version" {
  value = "1.4.0"
}

output "latest_guardrail_cf_version" {
  value = "1.0.0"
}

output prod_new_customer_slack_hook_paths {
  value = "TGMGNK6LT/BSARFR11T/p5Pjon1WsTgJN7Mb0ikNem9M"
}

output default_new_customer_default_slack_hook_path {
  value = "TGMGNK6LT/BJ9U6P5B3/Lp3LtHbWoMZxRQCTGkYBymm6"
}

output "bridgecrew_zone_id" {
  value = "Z2CCHMURZOJZR6"
}

output "accelerator_zone_id" {
  value = "Z2BJ6XQ5FK7U4H"
}

output "checkov_scanner_prefix_name" {
  value = "checkov/"
}

output "cloudtrail_template_version" {
  value = "1.4"
}

output "user_pool_name" {
  description = "The name of the BridgeCrew User Pool to be created"
  value       = "bridgecrew-users"
}

output "openid_provider_name" {
  description = "OpenID Connect - the name of the identity provider"
  value       = "GitHub"
}

output "es_index_retention_cw_rule" {
  description = "The name of the CloudWatch event for ES index retention policy "
  value       = "es-index-retention"
}

output "es_index_retention_age" {
  description = "default ES index retention age, in days"
  value       = 365
}

//google provider variables:
output "google_provider_name" {
  description = "Google Provider - the name of the identity provider"
  value       = "Google"
}

output "tag_manager_id" {
  description = "Google tag manager id"
  value       = "GTM-WN4GQLX"
}