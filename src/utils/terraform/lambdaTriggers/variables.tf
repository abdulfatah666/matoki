variable region {
  type        = "string"
  description = "AWS region"
}

variable "aws_profile" {
  type        = "string"
  description = "AWS profile"
}

variable "lambda_arn_list" {
  description = "A list of the lambda ARNs tht should have this sqs as a trigger"
  type        = "list"
}

variable "sqs_arn" {
  description = "The ARN of the SQS that should trigger the lambdas"
}

variable "customer_name" {
  type = "string"
}

variable "customer_assume_role_arn" {
  type        = "string"
  description = "The ARN of the role which lets us access the bucket in the client's account"
}

variable "customer_aws_account_id" {
  type = "string"
}