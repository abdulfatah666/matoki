// Don't remove that provider initialization. It is necessary to multi region feature
provider "aws" {
  region  = var.region
  profile = var.aws_profile
}

resource "aws_lambda_event_source_mapping" "trigger_attachments" {
  count            = length(var.lambda_arn_list)
  event_source_arn = var.sqs_arn
  function_name    = element(var.lambda_arn_list, count.index)
}
