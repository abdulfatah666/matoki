from setuptools import setup, find_packages

requires = [
    'circleci',
    'checkov'
]

setup(name='utilsPython',
      version='1.4',
      author="Bridgecrew",
      description="Bridgecrew utils",
      packages=find_packages(),
      include_package_data=True,
      install_requires=requires
      )
