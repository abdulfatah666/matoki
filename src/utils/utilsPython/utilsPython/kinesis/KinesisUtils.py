import json
from utilsPython.json.flattner import flatten_json
import uuid
import logging
import os
import sys
import boto3

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))


class KinesisUtils:
    def __init__(self):
        session = boto3.session.Session()
        region = session.region_name
        self.kinesis = boto3.client('kinesis', region_name=region)

    def send_to_kinesis(self, data, kinesis_name):
        converted_data_format = self._convert_to_kinesis_records_format(data, flatten=False)
        self._safe_kinesis_put_records(converted_data_format, kinesis_name)

    def _convert_to_kinesis_records_format(self, data, flatten):
        formatted_records = []
        for currRecord in data:
            new_rec = {}
            if flatten:
                new_rec["Data"] = json.dumps(flatten_json(currRecord))
            else:
                new_rec["Data"] = json.dumps(currRecord)
            new_rec["PartitionKey"] = str(uuid.uuid4())
            formatted_records.append(new_rec)
        return formatted_records

    def _safe_kinesis_put_records(self, data, stream_name):
        try:
            self.kinesis.put_records(Records=data, StreamName=stream_name)
        except Exception as e:
            logger.error(
                "Failed to write records: {} \n "
                "The length of the data: {} \n"
                "The kinesis data stream name: {}.\n"
                "The size of the failed records is: {} KB.".format(
                    data, len(data), stream_name, (sys.getsizeof(data) / 1024)))
            raise e
