import boto3
import logging

client = boto3.client('es')
logger = logging.getLogger(__name__)


def get_elastic_endpoint(customer_name):
    customer_domain = "bc-es-{}".format(customer_name)
    try:
        domain = client.describe_elasticsearch_domain(DomainName=customer_domain)
        return domain['DomainStatus']['Endpoint']
    except Exception:
        logger.warning("could not find elasticsearch domain for customer={}".format(customer_name))
        return None


def get_kibana_endpoint(customer_name):
    return get_elastic_endpoint(customer_name) + "/_plugin/kibana/"


def get_kibana_api_endpoint(customer_name):
    return get_kibana_endpoint(customer_name) + "api/"
