import boto3
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth

session = boto3.Session()


class ElasticsearchUtilities:
    def __init__(self, domain_name: str, region: str, port: int = 443,
                 use_ssl: bool = True, verify_cert: bool = True, profile: str = None):
        self.domain_name = domain_name
        self.port = port
        service = 'es'
        es_client = session.client(service)
        credentials = boto3.Session(profile_name=profile).get_credentials()
        # Creating temp credentials for the ElasticSearch instance
        aws_auth = AWS4Auth(credentials.access_key, credentials.secret_key, region, service,
                            session_token=credentials.token)
        describe_domain = es_client.describe_elasticsearch_domain(DomainName=domain_name)
        self.es_host = describe_domain["DomainStatus"]["Endpoint"]

        # Initiate an elasticsearch instance
        self.es_instance = Elasticsearch(
            hosts=[{'host': self.es_host, 'port': port}],
            http_auth=aws_auth,
            use_ssl=use_ssl,
            verify_certs=verify_cert,
            connection_class=RequestsHttpConnection,
            retry_on_timeout=True, max_retries=20

        )

    # Reindex single source index to destination index
    def reindex(self, source_index, destination_index):
        self.es_instance.reindex({
            "source": {"index": source_index},
            "dest": {"index": destination_index}
        }, wait_for_completion=True, request_timeout=2000)

    # Gets all indices that stands the index_pattern
    def get_all_indices(self, index_pattern):
        indices_dic = self.es_instance.indices.get(index_pattern)
        return indices_dic

    # Gets the existing indices that stands the source_indices_pattern index pattern.
    # For each source index, the function:
    # 1. create the destination index with the mappings that defined in mapping_path file
    # 2. reindex the source index to the new destination index
    # 3. remove the source index
    def reindex_with_mapping(self, source_indices_pattern, dest_indices_suffix, mapping):
        source_indices = self.get_all_indices(source_indices_pattern)
        try:
            for source_index in source_indices.keys():
                dest_index_name = "{}_{}".format(source_index, dest_indices_suffix)
                # If new index exists, delete it first
                if self.es_instance.indices.exists(index=dest_index_name):
                    self.es_instance.indices.delete(dest_index_name)

                # Create the destination index with the correct mapping
                self.es_instance.indices.create(index=dest_index_name, body=mapping)
                self.reindex(source_index, dest_index_name)
                # self.es_instance.indices.delete(source_index)
        except Exception as ex:
            print(ex)

    # Creates index template
    def create_index_template(self, template_name, template_body):
        self.es_instance.indices.put_template(template_name, template_body)

    def increase_domain_indexes_fields_limit(self, fields_limit):
        settings = {}
        settings["index.mapping.total_fields.limit"] = fields_limit
        self.es_instance.indices.put_settings(index="_all", body=settings)
