import os
import sys
import json
import configparser
from argparse import ArgumentParser
from os import path

sys.path.append(path.abspath('./'))
from es_utilities import ElasticsearchUtilities

# Parsing the command-line parameters
parser = ArgumentParser()
parser.add_argument('--domain_name', required=True)
parser.add_argument('--aws_profile', required=True)
parser.add_argument('--index_pattern', required=True)
parser.add_argument('--mapping_path', required=True)
parser.add_argument('--index_type', required=True)

# Extract the command line variables.tf
args = vars(parser.parse_args())
domain_name = args["domain_name"]
profile = args["aws_profile"]
index_pattern = args["index_pattern"]
mapping_path = args["mapping_path"]
index_type = args["index_type"]

config = configparser.ConfigParser()
config.read('{0}/.aws/config'.format(os.environ["HOME"]))
if profile == 'default':
    region = config["{0}".format(profile)]["region"]
else:
    region = config["profile {0}".format(profile)]["region"]

# Initiate elasticsearch instance
es_instance = ElasticsearchUtilities(domain_name, profile, region)
with open(mapping_path) as json_file:
    mapping = json.load(json_file)

mapping_properties = mapping["mappings"]["type"]
del mapping["mappings"]["type"]
mapping["mappings"][index_type] = mapping_properties


es_instance.reindex_with_mapping(index_pattern, "reindex", mapping)



# python3.7 ./reindex-existing-customer.py --domain_name search-es-putzi.us-west-2.es.amazonaws.com --aws_profile dev --index_pattern *lacework-events* --mapping_path ./lacework-index-mapping.json --index_type putzi_lacework-events_index
