import configparser
import json
import os
from argparse import ArgumentParser

from es_utilities import ElasticsearchUtilities

# Parsing the command-line parameters
parser = ArgumentParser()
parser.add_argument('--domain_name', required=True)
parser.add_argument('--aws_profile', required=True)
parser.add_argument('--template_name', required=True)
parser.add_argument('--index_pattern', required=True)
parser.add_argument('--mapping_path', required=True)
parser.add_argument('--index_type_name', required=True)

# Extract the command line variables
args = vars(parser.parse_args())
domain_name = args["domain_name"]
profile = args["aws_profile"]
template_name = args["template_name"]
index_pattern = args["index_pattern"]
mapping_path = args["mapping_path"]
index_type_name = args["index_type_name"]

# Retrieve the aws credentials from credentials file
config = configparser.ConfigParser()
config.read('{0}/.aws/config'.format(os.environ["HOME"]))
if profile == 'default':
    region = config["{0}".format(profile)]["region"]
else:
    region = config["profile {0}".format(profile)]["region"]

# Initiate elasticsearch instance
es_instance = ElasticsearchUtilities(domain_name=domain_name, profile=profile, region=region)

# Reading the index mapping
with open(mapping_path) as json_file:
    mapping = json.load(json_file)

mapping_properties = mapping["mappings"]["type"]

# Create the index template body object
template_body = {}
template_body["index_patterns"] = [index_pattern]
template_body["mappings"] = {}
template_body["mappings"][index_type_name] = mapping_properties

# Creating the index template
es_instance.create_index_template(template_name, template_body)
