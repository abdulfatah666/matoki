import os
import sys
import configparser
from argparse import ArgumentParser

from os import path

sys.path.append(path.abspath('./'))
from es_utilities import ElasticsearchUtilities

# Parsing the command-line parameters
parser = ArgumentParser()
parser.add_argument('--domain_name', required=True)
parser.add_argument('--aws_profile', required=True)
parser.add_argument('--indices_file_path', default="./existing_indices_file.txt")

# Extract the command line variables.tf
args = vars(parser.parse_args())
domain_name = args["domain_name"]
profile = args["aws_profile"]
indices_file_path = args["indices_file_path"]

# Retrieve the aws credentials from credentials file
config = configparser.ConfigParser()

config.read('{0}/.aws/config'.format(os.environ["HOME"]))
if profile == 'default':
    region = config["{0}".format(profile)]["region"]
else:
    region = config["profile {0}".format(profile)]["region"]

# Initiate elasticsearch instance
es_instance = ElasticsearchUtilities(domain_name=domain_name, profile=profile, region=region)

with open(indices_file_path) as f:
    content = f.readlines()

# you may also want to remove whitespace characters like `\n` at the end of each line
index_list = [x.strip() for x in content]

es_instance.delete_indices(index_list)

# python3.7 ./delete-customer-indices.py --domain_name search-es-lital2352-v6aa6m4tjwa4ped5husbkhwxfm.us-west-2.es.amazonaws.com --aws_profile dev



