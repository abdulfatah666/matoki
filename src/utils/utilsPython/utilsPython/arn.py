from __future__ import absolute_import

def empty_str_to_none(str_):
    if str_ == '':
        return None

    return str_


class MalformedArnError(Exception):
    def __init__(self, arn_str):
        self.arn_str = arn_str

    def __str__(self):
        return 'arn_str: {arn_str}'.format(arn_str=self.arn_str)


class Arn(object):
    def __init__(self, partition, service, region, account_id, resource_type, resource):
        self.partition = partition
        self.service = service
        self.region = region
        self.account = account_id
        self.resource_type = resource_type
        self.resource = resource

    @staticmethod
    def convert_sqs_url_to_arn(url):
        """
        Example: https://sqs.us-west-2.amazonaws.com/890234264427/lacework-cloudwatch-events turns into:
        arn:aws:sqs:us-west-2:890234264427:lacework-cloudwatch-events

        :param url: The URL to be converted
        :return: The ARN of the resource
        """
        # The region can be extracted from the first parts, separated by "."
        split_by_dot = url.split(".")
        # The Account ID and the queue name can be extracted from the last part of the url, separated by "/"
        split_by_backslash = split_by_dot[len(split_by_dot) - 1].split("/")

        return "arn:aws:sqs:{}:{}:{}".format(split_by_dot[1], split_by_backslash[1], split_by_backslash[2])

# todo: remove this once https://github.com/PokaInc/arnparse/issues/8 is fixed
def arnparse(arn_str):
    if not arn_str.startswith('arn:'):
        raise MalformedArnError(arn_str)

    elements = arn_str.split(':', 5)

    if "volume/vol-" in arn_str:
        elements = elements[:2] + ["volume"] + elements[2:]

    service = elements[2]
    resource = elements[5]

    if service in ['s3', 'sns', 'apigateway', 'execute-api']:
        resource_type = None
    else:
        resource_type, resource = _parse_resource(resource)

    return Arn(
        partition=elements[1],
        service=service,
        region=empty_str_to_none(elements[3]),
        account_id=empty_str_to_none(elements[4]),
        resource_type=resource_type,
        resource=resource,
    )


def _parse_resource(resource):
    first_separator_index = -1
    for idx, c in enumerate(resource):
        if c in (':', '/'):
            first_separator_index = idx
            break

    if first_separator_index != -1:
        resource_type = resource[:first_separator_index]
        resource = resource[first_separator_index + 1:]
    else:
        resource_type = None

    return resource_type, resource

