import jwt


class ToeknUtils():

    def extractCallingUserDataFromToken(self, token):
        sessionData = jwt.decode(token, verify=False)
        user = sessionData['email']
        groups = sessionData['cognito:groups']
        if (user == 'support@bridgecrew.io'):
            customers_names = ['BridgeCrew']
        else:
            customers_names = list(map(lambda s: str(s).split("_")[0], groups))

        return user, customers_names
