import hashlib
import os

import boto3

table_name = os.environ["TENANTS_CLOUD_FORMATION_TABLE"]
dynamodb = boto3.client('dynamodb')
# in memory dictionary that matches resource to it's stack.
resource_stack_cache = {}


def get_stack_for_resource(aws_account, resource_arn):
    """

    :param aws_account: aws account id to fetch resource stack id for
    :param resource_arn: PhysicalResourceId of the resource we are searching a stack for
    :return: the stack that created the resource
    """
    resource_hash = hashlib.md5(resource_arn.encode('utf-8')).hexdigest()

    # return stack name from cache, if exists
    if resource_hash in resource_stack_cache.keys():
        return resource_stack_cache[resource_hash]

    # populate cache with db value
    key = "{}:{}".format(aws_account, resource_hash)
    response = dynamodb.get_item(TableName=table_name,
                                 Key={'aws_account_id': {'S': key}})
    if 'Item' in response.keys():
        if 'stack' in response['Item'].keys():
             stack_name = response['Item']['stack']['M']['StackId']['S']
        else:
             stack_name = " "
    else:
        stack_name = " "
    resource_stack_cache[resource_hash] = stack_name

    return resource_stack_cache[resource_hash]
