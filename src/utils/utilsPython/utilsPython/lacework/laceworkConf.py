import boto3
import os

ssm = boto3.client('ssm')
uniquetag = os.environ.get("UNIQUE_TAG","")

class LaceworkConf:

    def __init__(self, customer_name: str, lacework_account: str = "", access_key_id: str = "",
                 secret_access_key: str = "", lacework_url: str = ""):
        self.customer_name = customer_name
        self.lacework_account = lacework_account
        self.access_key_id = access_key_id
        self.secret_access_key = secret_access_key
        if not lacework_url:
            self.lacework_url = "https://{}.lacework.net".format(lacework_account)
        else:
            self.lacework_url = lacework_url


class MissingLaceworkTenantConfError(Exception):
    pass


class LaceworkConfManager(object):
    def __init__(self):
        # dictionary <lacework_account,ssm>. used as cache for lacework parameter stores
        self.parameter_stores = {}

    def get_conf_by_lacework_account(self, lacework_account: str):
        if lacework_account in self.parameter_stores:
            customer_name = self.parameter_stores[lacework_account]['customer_name']
            access_key_id = self.parameter_stores[lacework_account]['access_key_id']
            secret_access_key = self.parameter_stores[lacework_account]['secret_access_key']
            lacework_url = self.parameter_stores[lacework_account]['lacework_url']
            conf = LaceworkConf(lacework_account=lacework_account, customer_name=customer_name,
                                access_key_id=access_key_id, secret_access_key=secret_access_key,
                                lacework_url=lacework_url)
            return conf
        else:
            try:
                return self.get_lacework_conf_directly_from_ssm(lacework_account)
            except Exception as e:
                error = MissingLaceworkTenantConfError(
                    "could not find item for lacework_account={} in ssm".format(lacework_account))
                print(e)
                raise error

    def get_lacework_conf_directly_from_ssm(self, lacework_account):
        customer_name = self.get_lacework_param_from_ssm(lacework_account, 'customer_name')
        access_key_id = self.get_lacework_param_from_ssm(lacework_account, 'access_key_id')
        secret_access_key = self.get_lacework_param_from_ssm(lacework_account, 'secret_access_key')
        lacework_url = self.get_lacework_param_from_ssm(lacework_account, 'lacework_url')

        self.parameter_stores[lacework_account] = {
            'customer_name': customer_name,
            'access_key_id': access_key_id,
            'secret_access_key': secret_access_key,
            'lacework_url': lacework_url
        }

        conf = LaceworkConf(lacework_account=lacework_account, customer_name=customer_name,
                            access_key_id=access_key_id, secret_access_key=secret_access_key,
                            lacework_url=lacework_url)
        return conf

    def get_lacework_param_from_ssm(self, lacework_account, key):
        return ssm.get_parameter(Name='/lacework_stack_{}/{}/{}'.format(uniquetag,lacework_account, key), WithDecryption=True)['Parameter']['Value']
