import boto3
import logging
import os
from boto3.dynamodb.conditions import Attr

tenants_lacework = os.environ["TENANTS_LACEWORK_TABLE"]

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))


class LaceworkReportDbMgr(object):
    def __init__(self):
        self.tenants_lacework_table = boto3.resource("dynamodb").Table(tenants_lacework)

    def update_tenants_lacework(self, aws_account_id, customer_name, lacework_account):
        item = {
            'aws_account_id': aws_account_id,
            'customer_name': customer_name,
            'is_lacework_report_exist': True,
            'lacework_account': lacework_account
        }
        logger.debug("Trying to put object {} to tenants_lacework table".format(item))
        self.tenants_lacework_table.put_item(Item=item)

    def get_accounts_with_reports(self):
        accounts_with_reports = set()
        response = self.tenants_lacework_table.scan(
            ProjectionExpression='aws_account_id',
            FilterExpression=Attr('is_lacework_report_exist').eq(True)
        )
        items = response['Items']
        if items:
            for item in items:
                accounts_with_reports.add(item["aws_account_id"])

        return accounts_with_reports
