import logging
import os
from utilsPython.lacework.db.LaceworkReportDbMgr import LaceworkReportDbMgr
from utilsPython.lacework.db.ViolationDbMgr import ViolationDbMgr

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))

lacework_report_db_mgr = LaceworkReportDbMgr()
violation_db_mgr = ViolationDbMgr()


class LaceworkCache(object):
    def __init__(self):
        self.bridgecrew_violations_id = violation_db_mgr.get_bridgecrew_violations_id()

    def get_bridgecrew_violations_id(self):
        return self.bridgecrew_violations_id

    def get_bridgecrew_violation_id(self, violation):
        if violation in self.bridgecrew_violations_id:
            return self.bridgecrew_violations_id[violation]
        else:
            logger.warning("Missing Bridgecrew violation id for {}".format(violation))
            return violation_db_mgr.set_unknown_violation(violation)
