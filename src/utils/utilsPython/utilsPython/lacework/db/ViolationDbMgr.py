import json
import logging
import os
import datetime
import dateutil.parser
import boto3
from boto3.dynamodb.conditions import Attr, Key
from utilsPython.rootCauseAnalysis.data import get_stack_for_resource
from utilsPython.remote_lambda.invoke import lambda_invoke


logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))

violations_table = os.environ["VIOLATIONS_FOR_RESOURCE_TABLE"]
violations_summary_table = os.environ["VIOLATIONS_SUMMARY_TABLE"]
violations_history_table = os.environ["VIOLATIONS_HISTORY_TABLE"]
conf_violations_table_name = os.environ["CONF_VIOLATION_TABLE"]
customers_api_lambda_name = os.environ.get('CUSTOMERS_API_LAMBDA_NAME', "")



class ViolationDbMgr(object):
    def __init__(self):
        self.dynamodb = boto3.resource('dynamodb')

    def get_violation_resources_by_status(self, violation_id, aws_account_id, resource_status):
        table = self.dynamodb.Table(violations_table)
        response = table.query(IndexName='violation_id-aws_account_id-index',
                               KeyConditionExpression=Key('violation_id').eq(violation_id) & Key('aws_account_id').eq(aws_account_id),
                               FilterExpression=Attr('status').eq(resource_status),
                               Limit=1000)['Items']
        return response

    def get_violation_resource(self, id):
        table = self.dynamodb.Table(violations_table)
        response = table.query(KeyConditionExpression=Key('id').eq(id))['Items']
        return response


    def get_violation_summary_object(self, key):
        table = self.dynamodb.Table(violations_summary_table)
        response = table.query(KeyConditionExpression=Key('id').eq(key))
        items = response['Items']
        if items:
            return items[0]
        else:
            return None

    def save_to_violations_summary(self, aws_account_id, violation_id, resources, last_update_date):
        key = self.violation_summary_key(aws_account_id, violation_id)
        violation_summary_table = self.dynamodb.Table(violations_summary_table)
        old_item = self.get_violation_summary_object(key)
        if resources:
            if old_item is not None: # this resource already exist and change to suppress/remediate
                item = {'id': key, 'aws_account_id': aws_account_id, 'violation_id': violation_id,
                        'amount_close': 0, 'amount_open': len(resources), 'amount_remediate': old_item['amount_remediate'],
                        'amount_suppress': old_item['amount_suppress'], 'last_update_date': last_update_date}
                logger.debug("object {} non-compliant, saving to violations summary table".format(item))
            else: # this is new resource
                item = {'id': key, 'aws_account_id': aws_account_id, 'violation_id': violation_id,
                        'amount_close': 0, 'amount_open': len(resources), 'amount_remediate': 0,
                        'amount_suppress': 0, 'last_update_date': last_update_date}
                logger.debug("object {} non-compliant, saving to violations summary table".format(item))
            violation_summary_table.put_item(Item=item)
        else:
            if old_item is not None and (old_item['amount_remediate'] != 0 or old_item['amount_suppress'] != 0):
                item = {'id': key, 'aws_account_id': aws_account_id, 'violation_id': violation_id,
                        'amount_close': 0, 'amount_open': 0, 'amount_remediate': old_item['amount_remediate'],
                        'amount_suppress': old_item['amount_suppress'], 'last_update_date': last_update_date}
                logger.debug("object {} compliant, saving to violations summary table".format(item))
            else:
                item = {'id': key, 'aws_account_id': aws_account_id, 'violation_id': violation_id,
                        'amount_close': 1, 'amount_open': 0, 'amount_remediate': 0, 'amount_suppress': 0,
                        'last_update_date': last_update_date}
                logger.debug("object {} compliant, saving to violations summary table".format(item))
            violation_summary_table.put_item(Item=item)

    def violation_summary_key(self, aws_account_id, violation_id):
        key = "{}::{}".format(aws_account_id, violation_id)
        return key

    def _resources_to_resource_ids(self, resources):
        result = []
        for resource in resources:
            result.append(resource._id)
        return result

    def remove_deleted_resources(self, aws_account_id, violation_id, resources):
        table = self.dynamodb.Table(violations_table)

        current_violation_resources = self.get_violation_resources_by_status(violation_id, aws_account_id, 'OPEN')
        resources_ids = self._resources_to_resource_ids(resources)

        for item in current_violation_resources:
            if item['resource'] not in resources_ids and item['status'] != 'REMEDIATED':
                table.delete_item(Key={'id': item['id'], 'violation_id': item['violation_id']})

    def save_to_violations_resource(self, customer_name, aws_account_id, violation_id, resources, last_update):
        self.remove_deleted_resources(aws_account_id=aws_account_id, violation_id=violation_id, resources=resources)
        table = self.dynamodb.Table(violations_table)
        for resource in resources:
            if resource.get_id() == aws_account_id:
                key = "{}::{}".format(violation_id, aws_account_id)
            else:
                key = "{}::{}::{}".format(violation_id, aws_account_id, resource.get_id())

            violation_resource = self.get_violation_resource(key)
            if len(violation_resource) > 0:
                old_resource = violation_resource[0]
                old_resource_date = dateutil.parser.parse(old_resource['last_update_date'])
                report_date = dateutil.parser.parse(last_update)
            if len(violation_resource) == 0 or old_resource_date < report_date:
                item = {
                    'id': key,
                    'violation_id': violation_id,
                    'aws_account_id': aws_account_id,
                    'resource': resource.get_id(),
                    'status': resource.violation_status,
                    'region': resource.region,
                    'created_by': get_stack_for_resource(aws_account_id, resource.get_id()),
                    'last_update_date': last_update
                }
                logger.debug("Trying to save object {} to violation resource table".format(item))
                table.put_item(Item=item)

    def save_to_violations_history(self, customer_name, aws_account_id, violation_id, resources, last_update):
        table = self.dynamodb.Table(violations_history_table)
        for resource in resources:
            if resource.get_id() == aws_account_id:
                key = "{}::{}::{}".format(violation_id, aws_account_id, last_update)
            else:
                key = "{}::{}::{}::{}".format(violation_id, aws_account_id, resource.get_id(), last_update)

            item = {
                'id': key,
                'violation_id': violation_id,
                'aws_account_id': aws_account_id,
                'resource': resource.get_id(),
                'status': resource.violation_status,
                'region': resource.region,
                'created_by': get_stack_for_resource(aws_account_id, resource.get_id()),
                'last_update_date': last_update

            }
            logger.debug("Trying to save object {} to violation history table".format(item))
            table.put_item(Item=item)

    def get_bridgecrew_violations_id(self):
        bridgecrew_violations_id = {}
        conf_violations_table = self.dynamodb.Table(conf_violations_table_name)
        response = conf_violations_table.scan(ProjectionExpression='violation_id, lacework_violation_id')
        items = response['Items']
        if items:
            for item in items:
                bridgecrew_violation_id = item["violation_id"]
                lacework_violation_id = item.get("lacework_violation_id", "")
                bridgecrew_violations_id[lacework_violation_id] = bridgecrew_violation_id

        return bridgecrew_violations_id

    def set_unknown_violation(self, lacework_violation_id, bridgecrew_violation_id=""):
        conf_violations_table = self.dynamodb.Table(conf_violations_table_name)
        logger.info(
            "Trying to put unknown lacework violation {} to violation conf table".format(lacework_violation_id))
        if not bridgecrew_violation_id:
            bridgecrew_violation_id = "BC_UNKNOWN_{}".format(lacework_violation_id)

        conf_violations_table.put_item(
            Item={
                'violation_id': bridgecrew_violation_id,
                'lacework_violation_id': lacework_violation_id
            })

        return bridgecrew_violation_id
