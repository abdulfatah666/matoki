import json
import logging

import requests
import os
from utilsPython.lacework.laceworkConf import LaceworkConf

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))


class LaceworkMgr:
    def get_access_token(self, lacework_conf: LaceworkConf):
        data = {"keyId": lacework_conf.access_key_id, "expiryTime": 36000}
        headers = {"X-LW-UAKS": lacework_conf.secret_access_key, "Content-Type": "application/json"}
        url = "{}/api/v1/access/tokens".format(lacework_conf.lacework_url)
        resp = requests.post(url=url, data=json.dumps(data), headers=headers)
        logger.debug("response.txt=%s" % resp.text)
        return json.loads(resp.text)['data'][0]['token']

    def get_detailed_event(self, access_token, event_id, lacework_conf):
        header = self.lacework_header(access_token, lacework_conf)
        url = "{}/api/v1/external/events/GetEventDetails?EVENT_ID={}".format(lacework_conf.lacework_url, event_id)
        resp = requests.get(url=url, headers=header)
        return json.loads(resp.text)['data'][0]

    def get_aws_violation_report(self, access_token, lacework_conf, aws_account_id):
        logger.debug("The access token is {0} and the secret access key is {1}".format(access_token,
                                                                                       lacework_conf.secret_access_key))
        headers = self.lacework_header(access_token, lacework_conf)
        url = "{}/api/v1/external/compliance/aws/GetLatestComplianceReport?AWS_ACCOUNT_ID={}&FILE_FORMAT=json" \
            .format(lacework_conf.lacework_url, aws_account_id)
        logger.debug("The url to get violation reports {0}".format(url))

        resp = requests.get(url=url, headers=headers)
        # todo: manage a response only for 200OK send the response otherwise send error
        return json.loads(resp.text)

    def get_gcp_violation_report(self, access_token, lacework_conf, gcp_project_id):
        logger.debug("The access token is {0} and the secret access key is {1}".format(access_token,
                                                                                       lacework_conf.secret_access_key))
        headers = self.lacework_header(access_token, lacework_conf)
        url = "{}/api/v1/external/compliance/gcp/GetLatestComplianceReport?GCP_ORG_ID={}&FILE_FORMAT=json" \
            .format(lacework_conf.lacework_url, gcp_project_id)
        logger.debug("The url to get violation reports {0}".format(url))

        resp = requests.get(url=url, headers=headers)
        # todo: manage a response only for 200OK send the response otherwise send error
        return json.loads(resp.text)

    def lacework_header(self, access_token, lacework_conf):
        headers = {"X-LW-UAKS": lacework_conf.secret_access_key, "Content-Type": "application/json",
                   "Authorization": "Bearer {}".format(access_token)}
        return headers

    def get_cloudformation_file(self, lacework_conf: LaceworkConf, file_type):
        access_token = self.get_access_token(lacework_conf)
        url = "{}/api/v1/external/files/templates/{}" \
            .format(lacework_conf.lacework_url, file_type)
        header = self.lacework_header(access_token, lacework_conf)
        resp = requests.get(url=url, headers=header)
        return json.loads(resp.text)

    def get_cloudformation_config_file(self, lacework_conf: LaceworkConf):
        return self.get_cloudformation_file(lacework_conf, "aws-config")

    def get_cloudformation_cloudtrail_file(self, lacework_conf: LaceworkConf):
        return self.get_cloudformation_file(lacework_conf, "aws-cloudtrail")

    def run_aws_report(self, lacework_conf: LaceworkConf, aws_account_id):
        access_token = self.get_access_token(lacework_conf)
        url = "{}/api/v1/external/runReport/aws/{}" \
            .format(lacework_conf.lacework_url, aws_account_id)
        resp = requests.post(url=url, headers=self.lacework_header(access_token=access_token, lacework_conf=lacework_conf))
        return json.loads(resp.text)

    def run_gcp_report(self, lacework_conf: LaceworkConf, gcp_project_id):
        access_token = self.get_access_token(lacework_conf)
        url = "{}/api/v1/external/runReport/gcp/{}" \
            .format(lacework_conf.lacework_url, gcp_project_id)
        resp = requests.post(url=url,
                             headers=self.lacework_header(access_token=access_token, lacework_conf=lacework_conf))
        return json.loads(resp.text)

    def configure_integration(self, lacework_conf: LaceworkConf, name, type, integration_data, enabled=1):
        access_token = self.get_access_token(lacework_conf)
        header = self.lacework_header(access_token, lacework_conf)
        url = "{}/api/v1/external/integrations".format(lacework_conf.lacework_url)
        data = {
            "Name": name,
            "TYPE": type,
            "ENABLED": enabled,
            "DATA": integration_data
        }
        resp = requests.post(url=url, data=json.dumps(data), headers=header)
        return json.loads(resp.text)
