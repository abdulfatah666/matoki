from utilsPython.arn import arnparse


class Resource(object):
    def __init__(self, _id, aws_account_id, violation_status):
        self.violation_status = violation_status
        self.aws_account_id = aws_account_id
        self._id = _id

        if _id != aws_account_id:
            if "security-group" in _id:
                self.region = _id.split(":")[2]
            elif "route-table" in _id:
                self.region =  _id.split(":")[2]
            elif "@" in _id and "arn" not in _id:
                self.region = "global"
            #     vpc endpoints (AKA AWS PrivateLink) are not ARNable
            elif "vpce" in _id and "arn" not in _id:
                self.region = "global"
            else:
                arn = arnparse(_id)
                if arn.region is None:
                    self.region = "global"
                else:
                    self.region = arn.region
        else:
            self.region = "global"

    def __str__(self):
        return "resource: id={}, violation_status={},region={}".format(
            self._id, self.violation_status, self.region)

    def get_id(self):
        return self._id
