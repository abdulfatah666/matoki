import json
from utilsPython.lacework.models.Violation import Violation
from utilsPython.lacework.models.Resource import Resource


class LaceworkEvent(object):
    def __init__(self, start_time, end_time, lacework_event_type, lacework_event_id, lacework_event_actor,
                 lacework_event_model, lacework_entity_map, lacework_event_name, lacework_severity, lacework_summary,
                 lacework_link, lacework_source, lacework_threat_tags, lacework_account, lacework_event_category,
                 aws_account_id, is_initial_report=False):
        self.lacework_summary = lacework_summary
        self.lacework_event_category = lacework_event_category
        self.lacework_account = lacework_account
        self.lacework_severity = lacework_severity
        self.lacework_event_actor = lacework_event_actor
        self.lacework_event_model = lacework_event_model
        self.lacework_link = lacework_link
        self.lacework_source = lacework_source
        self.lacework_event_name = lacework_event_name
        self.lacework_entity_map = lacework_entity_map
        self.lacework_threat_tags = lacework_threat_tags
        self.lacework_event_id = lacework_event_id
        self.lacework_event_type = lacework_event_type
        self.end_time = end_time
        self.start_time = start_time
        self.is_initial_report = is_initial_report
        self.aws_account_id = aws_account_id

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def to_violation(self):
        resources = []
        if "Resource" in self.lacework_entity_map:
            for lacework_resource in self.lacework_entity_map["Resource"]:
                resource = Resource(lacework_resource["VALUE"], self.aws_account_id, "OPEN")
                resources.append(resource)

        violation = Violation(aws_account_id=self.aws_account_id,
                              lacework_violation=self.lacework_entity_map["ViolationReason"][0]["REC_ID"],
                              severity=self.lacework_severity,
                              last_update=self.start_time,
                              title=self.lacework_entity_map["RecId"][0]["TITLE"],
                              resources=resources)
        return violation
