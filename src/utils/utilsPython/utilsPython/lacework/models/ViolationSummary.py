import logging
import os

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))

class ViolationSummary(object):
    def __init__(self, aws_account_id, violation_id, amount_open, amount_close, amount_resolved, amount_suppressed,
                 amount_remeidate, severity, last_update, is_initial_report=False):
        self.aws_account_id = aws_account_id
        self.violation_id = violation_id
        self.amount_open = amount_open
        self.amount_closed = amount_close
        self.amount_resolved = amount_resolved
        self.amount_suppressed = amount_suppressed
        self.amount_remediate = amount_remeidate
        self.severity = severity
        self.last_update = last_update
        self.is_initial_report = is_initial_report
        self._id = "{0}::{1}".format(aws_account_id, violation_id)

    def __str__(self):
        return "Violation summary: id={}, violation_id:{}," \
               " aws_account_id={},severity={},last_update={}," \
               "is_initial_report={}".format(self._id, self.violation_id, self.aws_account_id,
                                             self.severity, self.last_update, self.is_initial_report)
