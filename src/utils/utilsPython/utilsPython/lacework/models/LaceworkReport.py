import logging
import os
from utilsPython.lacework.models.Resource import Resource
from utilsPython.lacework.models.Violation import Violation
from utilsPython.lacework.db.LaceworkReportDbMgr import LaceworkReportDbMgr

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))
lacework_report_db_mgr = LaceworkReportDbMgr()


class LaceworkReport(object):
    def __init__(self, title, time, type, aws_account_id, customer_name, recommendations, lacework_account):
        self.title = title
        self.time = time
        self.type = type
        self.aws_account_id = aws_account_id
        self.customer_name = customer_name
        self.recommendations = recommendations
        self.lacework_account = lacework_account
        self._id = "{0}::{1}::{2}".format(customer_name, aws_account_id, time)

    def update_tenants_lacework(self):
        lacework_report_db_mgr.update_tenants_lacework(aws_account_id=self.aws_account_id,
                                                       customer_name=self.customer_name,
                                                       lacework_account=self.lacework_account)

    def to_violations(self):
        violations = []
        for lacework_recommendation in self.recommendations:
            resources = []
            if lacework_recommendation["VIOLATIONS"] is not None and lacework_recommendation["VIOLATIONS"]:
                for violation in lacework_recommendation["VIOLATIONS"]:
                    if "resource" in violation:
                        resource_id = violation["resource"]
                    else:
                        resource_id = self.aws_account_id
                    resource = Resource(resource_id, self.aws_account_id, "OPEN")
                    resources.append(resource)

            violation = Violation(aws_account_id=self.aws_account_id,
                                  severity=lacework_recommendation["SEVERITY"],
                                  title=lacework_recommendation["TITLE"],
                                  last_update=self.time,
                                  resources=resources,
                                  lacework_violation=lacework_recommendation["REC_ID"],
                                  is_initial_report=True)
            violations.append(violation)
        return violations
