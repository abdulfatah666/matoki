import logging
import os

from utilsPython.lacework.db.LaceworkCache import LaceworkCache
from utilsPython.lacework.db.ViolationDbMgr import ViolationDbMgr

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))

violation_db_mgr = ViolationDbMgr()
lacework_cache = LaceworkCache()


class Violation(object):
    def __init__(self, aws_account_id, title, severity, last_update, resources, lacework_violation,
                 is_initial_report=False):
        self.aws_account_id = aws_account_id
        self.title = title
        self.severity = severity
        self.resources = resources
        self.last_update = last_update
        self.is_initial_report = is_initial_report
        self.lacework_violation = lacework_violation
        self.violation_db_mgr = ViolationDbMgr()
        self.violation_id = lacework_cache.get_bridgecrew_violation_id(lacework_violation)
        self._id = "{0}::{1}".format(aws_account_id, self.violation_id)

    def __str__(self):
        return "Violation: id={}, violation_id:{}, " \
               "aws_account_id={},title={},severity={}," \
               "resources={},last_update={},is_initial_report={}".format(
                self._id, self.violation_id, self.aws_account_id,
                self.title, self.severity, self.resources,
                self.last_update, self.is_initial_report)

    def save(self, customer_name):
        suppressed_violation_resources = violation_db_mgr.get_violation_resources_by_status(self.violation_id, self.aws_account_id,
                                                                                            'SUPPRESSED')
        suppressed_violation_resource_ids = list(map(lambda res: res['resource'], suppressed_violation_resources))

        resources_to_update = list(filter(lambda res: not (res.get_id() in suppressed_violation_resource_ids), self.resources))

        violation_db_mgr.save_to_violations_summary(self.aws_account_id, self.violation_id,
                                                    resources_to_update, self.last_update)
        violation_db_mgr.save_to_violations_resource(customer_name, self.aws_account_id, self.violation_id,
                                                     resources_to_update, self.last_update)
        violation_db_mgr.save_to_violations_history(customer_name, self.aws_account_id, self.violation_id,
                                                    resources_to_update, self.last_update)
