import boto3

from utilsPython.ssm.ssm_parameter_store import SSMParameterStore

PARAMETER_PREFIX = "lacework_stack"


class LaceworkStackParameterStore(SSMParameterStore):
    def __init__(self, lacework_account):
        self.ssm_client = boto3.client('ssm')
        self.lacework_account = lacework_account

        self.prefix = "/{}/{}".format(PARAMETER_PREFIX, lacework_account)
        super(LaceworkStackParameterStore, self).__init__(prefix=self.prefix, ssm_client=None)

    def put_parameter(self, name, value, type, customer_name, description="", overwrite=True):
        self.ssm_client.put_parameter(Name="{}/{}".format(self.prefix, name), Description=description, Value=value,
                                      Type=type, Overwrite=overwrite)
