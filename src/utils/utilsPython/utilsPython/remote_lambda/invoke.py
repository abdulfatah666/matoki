import logging
import json
import boto3

logging.basicConfig(level=logging.INFO)
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
# tell the handler to use this format
console.setFormatter(formatter)
"""
Getting 3 params:
lambdaFunctionName  (json)   : the name of the lambda function that will be invoked. eg: "customers-api-TAGNAME"
name                (string) : the name of the inner function to execute. eg: "getTenantEnvironmentDetails"
body                (string) : the function body as json. eg: json.dumps({"awsAccountId": "1111111111111"})
"""


def lambda_invoke(lambda_function_name, name, body, region="us-west-2", format="body", new_invoke=False):
    client = boto3.client('lambda', region_name=region)
    try:
        payload = json.dumps({
            "body": body,
            "headers": {
                'Content-Type': 'application/json'
            },
            "path": f"/invoke/{name}",
            "newInvoke": new_invoke
        })
        lambda_response = client.invoke(FunctionName=lambda_function_name, InvocationType="RequestResponse",
                                        Payload=payload,LogType="Tail")
        if format == "body":
            return json.loads(json.loads(lambda_response['Payload'].read())['body'])
        else:
            return json.loads(lambda_response['Payload'].read())
    except Exception as e:
        logging.error(e)
