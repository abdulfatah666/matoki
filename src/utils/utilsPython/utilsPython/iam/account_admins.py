import boto3


# I hope AWS will never add another capability to grant admin rights to a user...
# If they did... well... good luck.


def user_has_admin_policy(user, admin_access):
    if "UserPolicyList" in user:
        policies = user["UserPolicyList"]

        for policy in policies:
            if policy["PolicyName"] == admin_access:
                return True
    return False


def user_has_attached_policy(user, admin_access):
    if "AttachedManagedPolicies" in user:
        policies = user["AttachedManagedPolicies"]
        for policy in policies:
            if policy["PolicyName"] == admin_access:
                return True
    return False


def group_has_admin_policy(client, group, admin_access):
    resp = client.list_group_policies(
        GroupName=group["GroupName"]
    )

    for policies in resp["PolicyNames"]:
        for name in policies:
            if name == admin_access:
                return True
    return False


def group_has_attached_policy(client, group, admin_access):
    resp = client.list_attached_group_policies(
        GroupName=group["GroupName"]
    )

    for policy in resp["AttachedPolicies"]:
        if policy["PolicyName"] == admin_access:
            return True
    return False


def user_has_admin_from_group(client, user, admin_access):
    resp = client.list_groups_for_user(
        UserName=user["UserName"]
    )

    for group in resp["Groups"]:
        has_admin_policy = group_has_admin_policy(client, group, admin_access)
        if has_admin_policy:
            return True
        has_attached_policy = group_has_attached_policy(client, group, admin_access)
        if has_attached_policy:
            return True
    return False


def is_user_admin(client, user, admin_access):
    has_admin_policy = user_has_admin_policy(user, admin_access)
    if has_admin_policy:
        return True

    has_attached_admin_policy = user_has_attached_policy(user, admin_access)
    if has_attached_admin_policy:
        return True

    has_admin_from_group = user_has_admin_from_group(client, user, admin_access)
    if has_admin_from_group:
        return True
    return False


def get_admins(client, users, admin_access):
    admins = []
    for user in users:
        is_admin = is_user_admin(client, user, admin_access)
        if is_admin:
            admins.append(user["Arn"])

    return admins


def get_all_account_admins(account_id, client=boto3.client('iam')):
    access_admin = 'AdministratorAccess'
    details = client.get_account_authorization_details(
        Filter=['User']
    )
    users = details['UserDetailList']
    admin_list = get_admins(client, users, access_admin)
    more_users = details["IsTruncated"]
    while more_users:
        details = client.get_account_authorization_details(
            Filter=['User'],
            Marker=details["Marker"]
        )
        users = details['UserDetailList']
        admin_list.extend((get_admins(client, users, access_admin)))
        more_users = details["IsTruncated"]

    # add root account to admin list
    admin_list.append("arn:aws:iam::{}:root".format(account_id))

    return admin_list
