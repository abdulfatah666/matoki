import time
import boto3
from circleci.api import Api
import botocore
import re
import json
import logging
import os

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))


class CircleCI:
    def __init__(self, api_token):
        self.circleci = Api(api_token)
        self.token = api_token
        self.username = "bridgecrewio"

    def run_job(self, job_name, project="platform", branch="master", **additional_params):
        build_parameters = {'CIRCLE_JOB': job_name}
        build_parameters.update(additional_params)
        build_params = {
            "job": job_name,
            "build_parameters": build_parameters
        }
        resp = self.circleci.trigger_build(username=self.username, project=project, branch=branch, vcs_type="github",
                                           params=build_params)
        return resp['build_num']

    def job_busy_wait(self, build_num, project="platform"):
        """
        :param build_num: build number
        :param project: circle ci project (git repo name)
        :return: build info once job is finished
        """
        build_info = self.circleci.get_build_info(username=self.username, project=project, build_num=build_num)
        self.circleci.get_recent_builds()
        while build_info['lifecycle'] != 'finished':
            build_info = self.circleci.get_build_info(username=self.username, project=project, build_num=build_num)
            time.sleep(5)
        return build_info

    def get_recent_api_build(self, **build_params_filter):
        """
        :param build_params_filter:  dictionary to look for in recent build build paramters. if all filters are met,
            build is returned
        :return: build  response
        """
        for build in self.get_recent_builds():
            if build["why"] == "api":
                non_found_params = len(build_params_filter)
                for param_filter_key in build_params_filter.keys():
                    if build['build_parameters'][param_filter_key] == build_params_filter[param_filter_key]:
                        non_found_params = non_found_params - 1
                    if non_found_params == 0:
                        return build

    def busy_wait_last_build_by_filter(self, job_name, **build_params_filter):
        """

        :param job_name: circle ci job name
        :param build_params_filter:
        :return: build info once job is finieshed
        """
        build = self.get_recent_api_build(job_name=job_name, **build_params_filter)
        return self.job_busy_wait(build_num=build["build_num"])

    # circleci python api recent_builds has a bug. it does not call the correct endpoint. this is a workaround.
    def get_recent_builds(self):
        return Api(token=self.token)._request(verb="GET",
                                              endpoint="project/github/bridgecrewio/platform")

    def update_customer_stacks(self, terraform_remote_state_bucket, access_key, access_key_secret, region, branch):
        customer_names_list = set()
        s3_client = boto3.client(
            's3',
            aws_access_key_id=access_key,
            aws_secret_access_key=access_key_secret,
            config=boto3.session.Config(signature_version='s3v4'),
            region_name=region
        )
        response = s3_client.list_objects(Bucket=terraform_remote_state_bucket, Prefix="customer_stacks")
        if 'Contents' in response:
            for object_key in response["Contents"]:
                if "params.tfbackend" in object_key["Key"]:
                    customer_name = object_key["Key"].split("/")[1]
                    customer_names_list.add(customer_name)
                    try:
                        data = s3_client.get_object(Bucket=terraform_remote_state_bucket, Key=object_key["Key"])
                        contents = data['Body'].read()
                        params = dict()
                        for param in contents.decode("utf-8").splitlines():
                            key = re.search("(.*)=", param).group(1).strip()
                            value = re.search('"(.*)"', param).group(1)
                            params[key] = value

                        with open('../../../../../account_mapping.json') as json_file:
                            mapping_accounts_file = json.load(json_file)
                        profile = params.get("profile")
                        for i in mapping_accounts_file:
                            if mapping_accounts_file[i] == profile:
                                base_aws_account_id = i
                                break
                        cci.run_job(job_name="customer-stack-upgrade-simple", branch=branch,
                                    project="platform", REGION=params.get("region"),
                                    BASE_AWS_ACCOUNT_ID=base_aws_account_id,
                                    CUSTOMER_NAME=params.get("customer_name"))
                    except botocore.exceptions.ClientError as e:
                        if e.response['Error']['Code'] == "404":
                            logger.error("The object does not exist.")
                        else:
                            raise


"""
A cool way to launch CircleCI jobs with parameters.
Set the CircleCI key, configure the job, branch, and the other parameters - and just go!

DO NOT commit the CircleCI key!!
"""
if __name__ == '__main__':
    cci = CircleCI("circlecikey")
    # cci.update_customer_stacks("bridgecrew-terraform-remote-state-372188014275", "key",
    #                           "secret", "us-west-2",branch="release/0.1.2")
    # Base stack job example
    # cci.run_job("base-stack-apply", project="platform", branch="release/0.1.2", TAG="", TURBO_MODE=False,
    #            REGION="us-west-2", DOMAIN="stage.bridgecrew.cloud", BASE_AWS_ACCOUNT_ID="372188014275",
    #            SLACK_HOOK_URL="https://hooks.slack.com/services/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")

    # Customer stack job example
    # cci.run_job(job_name="customer-stack-apply", branch="feature/BC-188/Lacework_compliance",
    #             project="platform", REGION="us-west-2", BASE_AWS_ACCOUNT_ID="090772183824",
    #             CUSTOMER_ASSUME_CROSS_ACCOUNT_ROLE_ARN="arn:aws:iam::090772183824:role/sulilace-bc-bridgecrewcwssarole",
    #             CUSTOMER_ASSUME_ROLE_ARN="arn:aws:iam::090772183824:role/sulilace-bc-CloudTrailAccessForBrigecrew",
    #             CUSTOMER_NAME="suli", EXTERNAL_ID="JEQF0N",
    #             CLIENT_SQS_ARN="arn:aws:sqs:us-west-2:090772183824:sulilace-bc-bridgecrewcws",
    #             OWNER_NAME="Guy", OWNER_LAST_NAME="Sulema", OWNER_PHONE_NUMBER="+972526108705",
    #             BASE_STACK_UNIQUE_TAG="newsuli2",
    #             OWNER_EMAIL="guysul.spam@gmail.com", USER_POOL_ID="us-west-2_mkJpAFNvX")
