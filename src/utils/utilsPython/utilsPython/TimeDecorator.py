import time
import logging

logger = logging.getLogger()


def timeit(method, loggerLevel=logging.INFO):
    logger.setLevel(loggerLevel)

    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        logger.log(loggerLevel, 'The function %r execution time is: %2.2f sec' % \
              (method.__name__, te-ts))
        return result
    return timed
