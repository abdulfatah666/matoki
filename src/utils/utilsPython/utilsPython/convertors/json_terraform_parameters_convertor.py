import json
TERRAFORM_STRING_ATTRIBUTE_FORMAT = '{}="{}"'
TERRAFORM_NON_STRING_ATTRIBUTE_FORMAT = '{}={}'


def convert_to_terraform(attributes):
    properties = []
    for attribute in attributes:
        attribute_to_concat = None
        concat_format = None

        if type(attributes[attribute]) is str:
            attribute_to_concat = attributes[attribute]
            concat_format = TERRAFORM_STRING_ATTRIBUTE_FORMAT
        elif type(attributes[attribute]) is bool:
            attribute_to_concat = str(attributes[attribute]).lower()
            concat_format = TERRAFORM_NON_STRING_ATTRIBUTE_FORMAT
        elif type(attributes[attribute]) is list:
            attribute_to_concat = json.dumps(attributes[attribute])
            concat_format = TERRAFORM_NON_STRING_ATTRIBUTE_FORMAT
        else:
            attribute_to_concat = attributes[attribute]
            concat_format = TERRAFORM_NON_STRING_ATTRIBUTE_FORMAT

        properties.append(concat_format.format(attribute, attribute_to_concat))

    result = "\n".join(properties)
    return result

