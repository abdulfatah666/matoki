const AWS = require('aws-sdk');

const cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider({ region: 'us-west-2' });

class UsersUtils {
    static createCognitoUser({
        userPoolId, email, desiredDeliveryMediums, forceAliasCreation, temporaryPassword, userAttributes, validationData
    }) {
        const createUserParams = {
            UserPoolId: userPoolId, /* required */
            Username: email, /* required */
            DesiredDeliveryMediums: desiredDeliveryMediums,
            ForceAliasCreation: forceAliasCreation,
            MessageAction: 'SUPPRESS',
            TemporaryPassword: temporaryPassword,
            UserAttributes: userAttributes,
            ValidationData: validationData

        };

        return cognitoIdentityServiceProvider.adminCreateUser(createUserParams)
            .promise()
            .then((data) => data.User)
            .catch((err) => {
                console.log(err);
                throw new Error('failed to create cognito user');
            });
    }

    static deleteCognitoUser(userpoolId, userId) {
        const params = {
            UserPoolId: userpoolId, /* required */
            Username: userId /* required */
        };

        return cognitoIdentityServiceProvider.adminDeleteUser(params).promise()
            .then(() => console.log('user was deleted successfully. ', userId))
            .catch((err) => {
                console.log(err);
                throw new Error('failed to delete cognito user ', userId);
            });
    }

    static addCognitoUserToGroups({ userPoolId, username, groups }) {
        return Promise.all(groups.map((group) => {
            const addToGroupParam = {
                GroupName: group,
                UserPoolId: userPoolId,
                Username: username
            };

            return cognitoIdentityServiceProvider
                .adminAddUserToGroup(addToGroupParam).promise().then((data) => data).catch((err) => {
                    console.log(err);
                    throw new Error('failed to add user to group');
                });
        }));
    }
}

module.exports = UsersUtils;
