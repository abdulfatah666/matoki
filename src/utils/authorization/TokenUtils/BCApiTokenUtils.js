const AWS = require('aws-sdk');
const jwt = require('jsonwebtoken');
const BaseTokenUtils = require('./BaseTokenUtils');

const ssm = new AWS.SSM({ region: process.env.REGION });
const dynamoDB = new AWS.DynamoDB.DocumentClient({ region: process.env.REGION });

class BCApiTokenUtils extends BaseTokenUtils {
    static async generate(params) {
        const claims = {
            iss: 'BRIDGECREW',
            userId: params.userId
        };

        const secret = await this.getSecret();
        const signingAlgorithm = await this.getSigningAlgorithm();

        const token = await jwt.sign(claims, secret, { algorithm: signingAlgorithm });

        const itemParams = {
            TableName: process.env.API_TOKENS_TABLE,
            Item: {
                token,
                user_id: params.userId
            }
        };

        // Saving the token to dedicated dynamo table.
        // This will give us the ability to "disable" generated API token
        return dynamoDB.put(itemParams).promise().then(() => {
            console.log('token was saved successfully');
            return token;
        }).catch((err) => {
            console.error(err);
            throw new Error('failed to generate token');
        });
    }

    static async disable({ token }) {
        const itemParams = {
            TableName: process.env.API_TOKENS_TABLE,
            Key: {
                token
            }
        };

        // Saving the token to dedicated dynamo table.
        // This will give us the ability to "disable" generated API token
        return dynamoDB.delete(itemParams).promise().then((data) => {
            console.log('token was disables successfully');
            return data;
        }).catch((err) => {
            console.error(err);
            throw new Error('failed to disable token');
        });
    }

    static async getSecret() {
        const secretParameterName = process.env.SECRET_PARAMETER_NAME;

        const params = {
            Name: secretParameterName,
            WithDecryption: true
        };

        try {
            const result = await ssm.getParameter(params).promise();
            return result.Parameter.Value;
        } catch (err) {
            console.error(err);
            throw new Error('failed to fetch secret');
        }
    }

    static async getSigningAlgorithm() {
        const algorithmParameterName = process.env.ALGORITHM_PARAMETER_NAME;

        const params = {
            Name: algorithmParameterName,
            WithDecryption: true
        };

        try {
            const result = await ssm.getParameter(params).promise();
            return result.Parameter.Value;
        } catch (err) {
            console.error(err);
            throw new Error('failed to fetch algorithm');
        }
    }
}

module.exports = BCApiTokenUtils;
