/* eslint-disable class-methods-use-this */
class BaseTokenUtils {
    generate() {
        throw new Error('Missing implementation to validate function');
    }
}

module.exports = BaseTokenUtils;
