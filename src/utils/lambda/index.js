/* eslint-disable no-case-declarations */
const AWS = require('aws-sdk/index');

module.exports.handler = async (event) => {
    console.debug(`Received a request for lambda utils:\n${JSON.stringify(event)}`);

    switch (event.type) {
        case 'TOKEN.GET_USER':
            const { cognitoAuthenticationProvider } = event;
            const userPoolId = cognitoAuthenticationProvider.split(',')[0].split('/')[1];
            const userId = cognitoAuthenticationProvider.split('CognitoSignIn:')[1];
            const cognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider();

            const params = {
                UserPoolId: userPoolId,
                Username: userId
            };
            const user = await cognitoIdentityServiceProvider.adminGetUser(params).promise();
            const userGroups = await cognitoIdentityServiceProvider.adminListGroupsForUser(params).promise();
            const userDetails = {};
            user.UserAttributes.forEach((att) => { userDetails[att.Name] = att.Value; });
            userDetails.groups = userGroups.Groups;
            userDetails.customers = userDetails.groups.filter((group) => group.GroupName.includes('_'))
                .map((group) => group.GroupName.split('_')[0]);

            console.debug(`user details is :${JSON.stringify(userDetails)}`);
            return userDetails;
        case 'SF.MERGE_PARALLEL_OUTPUTS':
            const { parallelOutputs } = event;
            return parallelOutputs.flat();
        default:
            return 'Utils type not exist';
    }
};
