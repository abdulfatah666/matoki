output "function_name" {
  value = module.bridgecrew_utils_lambda.function_name
}

output "function_arn" {
  value = module.bridgecrew_utils_lambda.function_arn
}
