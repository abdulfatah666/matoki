locals {

  function_name = "bridgecrew-utils-${var.unique_tag}"
}

module "bridgecrew_utils_lambda" {
  source                 = "../../../terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../utils/lambda"
  variable_string        = "--lambdaName ${local.function_name}"
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.sls_bucket_name

  lambda_names = [
    local.function_name,
  ]
}