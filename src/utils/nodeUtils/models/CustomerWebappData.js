const GettingStarted = require('./GettingStarted');

// When you add new category you should:
// 1. Create model for that category.
// 2. Import that model and add it to:
//      * mapCategoryToClass map
//      * constructor
//      * toDynamoDbItem function

const mapCategoryToClass = {
    getting_started: { class: GettingStarted, classPropertyName: 'gettingStarted' }
};

class CustomerWebappData {
    constructor({ customerName, gettingStarted }) {
        this.customer = customerName;
        this.gettingStarted = gettingStarted ? new GettingStarted(gettingStarted) : undefined;
    }

    toDynamoDbItem() {
        const item = {
            customer_name: this.customer,
            getting_started: this.gettingStarted.toDynamoDbItem()
        };

        // remove from dynamoItem all undefined categories
        // (in order to support partial update of the CustomerWebappData object)
        const itemKeys = Object.keys(item);
        itemKeys.forEach((key) => {
            if (!item[key]) {
                delete item[key];
            }
        });

        return item;
    }

    static createFromDynamoDbItem(item) {
        const result = {
            customerName: item.customer_name
        };

        // Loop over the dynamo categories result (not necessarily all categories)
        const itemKeys = Object.keys(item);
        itemKeys.forEach((key) => {
            if (key !== 'customer_name') {
                // Create instance of the suitable category class
                const classProp = mapCategoryToClass[key].classPropertyName;
                const classInstance = mapCategoryToClass[key].class;
                const dynamoCategoryItem = item[key];
                result[classProp] = classInstance.createFromDynamoDbItem(dynamoCategoryItem);
            }
        });

        return new CustomerWebappData(result);
    }

    static getWebappCategoryDynamoPropertyName(classCategoryName) {
        const result = Object.keys(mapCategoryToClass).filter((key) => mapCategoryToClass[key].classPropertyName === classCategoryName);
        return result.length > 0 ? result[0] : undefined;
    }
}

module.exports = CustomerWebappData;
