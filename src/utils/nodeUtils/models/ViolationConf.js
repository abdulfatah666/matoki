class ViolationConf {
    constructor({ category, guidelineType, laceworkViolationId, severity, title, violationId,
        guideline, remediationIdSet, regulationsIdSet, isManual, checkovCheckId }) {
        this.category = category;
        this.guidelineType = guidelineType;
        this.laceworkViolationId = laceworkViolationId;
        this.severity = severity;
        this.title = title;
        this.violationId = violationId;
        this.guideline = guideline;
        this.remediationIdSet = remediationIdSet;
        this.regulationIdSet = regulationsIdSet;
        this.isManual = isManual;
        this.checkovCheckId = checkovCheckId;
    }

    toDynamoDbItem() {
        const item = {
            category: this.category,
            guideline_type: this.guidelineType,
            guideline: this.guideline,
            severity: this.severity,
            title: this.title,
            lacework_violation_id: this.laceworkViolationId,
            violation_id: this.violationId,
            remediation_id_list: this.remediationIdSet,
            regulation_id_list: this.regulationIdSet,
            checkov_check_id: this.checkovCheckId
        };

        if (this.isManual) {
            item.is_manual = true;
        }
        return item;
    }

    static createFromDynamoDbItem(item) {
        return new ViolationConf({
            category: item.category,
            guidelineType: item.guideline_type,
            severity: item.severity,
            title: item.title,
            laceworkViolationId: item.lacework_violation_id,
            violationId: item.violation_id,
            guideline: item.guideline,
            remediationIdSet: item.remediation_id_list ? item.remediation_id_list.values : null,
            regulationsIdSet: item.regulation_id_list,
            isManual: item.is_manual,
            checkovCheckId: item.checkov_check_id
        });
    }
}

module.exports = ViolationConf;
