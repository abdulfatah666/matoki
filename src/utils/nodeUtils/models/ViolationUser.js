class ViolationUser {
    constructor({ user, violationsMap }) {
        this.user = user;
        this.violationsMap = violationsMap;
    }

    toDynamoDbItem() {
        const item = {
            id: this.user,
            violations_map: this.violationsMap
        };
        return item;
    }

    updateViolation({ violationId, data }) {
        this.violationsMap[violationId] = data;
    }

    static createFromDynamoDbItem(item) {
        if (!item) return null;
        return new ViolationUser({
            user: item.id,
            violationsMap: item.violations_map
        });
    }
}

module.exports = ViolationUser;
