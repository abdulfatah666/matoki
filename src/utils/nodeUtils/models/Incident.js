class Incident {
    constructor({
        type, title, id, awsAccountIds, lastUpdateDate, severity, status, awsAccountIdsToStatus
    }) {
        this.type = type;
        this.title = title;
        this.id = id;
        this.awsAccountIds = awsAccountIds;
        this.lastUpdateDate = lastUpdateDate;
        this.severity = severity;
        this.status = status;
        this.awsAccountIdsToStatus = awsAccountIdsToStatus;
    }

    /**
     * This method merges {@link ViolationSummary} and a {@link ViolationConf}
     * @param {ViolationSummary[]} violationSummaryList - A list of all of the instances (as a ViolationSummary) of the violation across all the accounts, from all the accounts
     * @param {ViolationConf} violationConf - The violation conf
     * @return {Incident}
     */
    static createFromViolationAndConfig(violationSummaryList, violationConf) {
        const awsAccountIds = violationSummaryList.map((summary) => summary.awsAccountId);
        const lastUpdateDate = new Date(Math.max.apply(null, violationSummaryList.map((summary) => new Date(summary.lastUpdateDate)))).toISOString();
        const statuses = [...new Set([].concat(...violationSummaryList.map((summary) => summary.getStatusList())))];

        const awsAccountIdsToStatus = violationSummaryList.reduce((accumulator, summary) => {
            const { awsAccountId } = summary;
            accumulator[awsAccountId] = summary.getStatusList();
            return accumulator;
        }, {});

        return new Incident({
            type: 'violation',
            title: violationConf.title,
            id: violationConf.violationId,
            awsAccountIds,
            lastUpdateDate,
            severity: violationConf.severity,
            status: statuses,
            awsAccountIdsToStatus
        });
    }
}

module.exports = Incident;
