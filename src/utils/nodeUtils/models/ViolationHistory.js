class ViolationHistory {
    constructor({
        id, awsAccountId, createdBy, lastUpdateDate, owner, region, resource, status, violationId, updatedBy
    }) {
        this.id = id;
        this.violationId = violationId;
        this.awsAccountId = awsAccountId;
        this.resource = resource;
        this.status = status;
        this.owner = owner;
        this.region = region;
        this.createdBy = createdBy;
        this.lastUpdateDate = lastUpdateDate;
        this.updatedBy = updatedBy;
    }

    static fromDynamoDbItem(item) {
        return new ViolationHistory({
            id: item.id,
            violationId: item.violation_id,
            awsAccountId: item.aws_account_id,
            resource: item.resource,
            status: item.status,
            owner: item.owner,
            region: item.region,
            createdBy: item.created_by,
            lastUpdateDate: item.last_update_date,
            updatedBy: item.updated_by
        });
    }

    toDynamoDbItem() {
        return {
            id: this.id,
            violation_id: this.violationId,
            aws_account_id: this.awsAccountId,
            resource: this.resource,
            status: this.status,
            owner: this.owner,
            region: this.region,
            created_by: this.createdBy,
            last_update_date: this.lastUpdateDate,
            updated_by: this.updatedBy
        };
    }
}

module.exports = ViolationHistory;
