class GettingStarted {
    constructor({ dashboardVisited, graphsVisited, incidentsVisited }) {
        this.dashboardVisited = dashboardVisited;
        this.graphsVisited = graphsVisited;
        this.incidentsVisited = incidentsVisited;
    }

    toDynamoDbItem() {
        const item = {
            dashboard_visited: this.dashboardVisited,
            graphs_visited: this.graphsVisited,
            incidents_visited: this.incidentsVisited
        };

        return item;
    }

    static createFromDynamoDbItem(item) {
        return new GettingStarted({
            dashboardVisited: item.dashboard_visited,
            graphsVisited: item.graphs_visited,
            incidentsVisited: item.incidents_visited
        });
    }
}

GettingStarted.DynamoProperyName = 'getting_started';

module.exports = GettingStarted;
