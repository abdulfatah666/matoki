const { VIOLATION_STATUSES } = require('./Enums');

class ViolationSummary {
    constructor({
        awsAccountId, id, amountOpen, amountClose, amountSuppress, amountRemediate, amountDelete, lastUpdateDate,
        violationId, isInitialReport, isSuppressed
    }) {
        this.awsAccountId = awsAccountId;
        this.id = id;
        this.violationId = violationId;
        this.amounts = {};
        this.amounts[VIOLATION_STATUSES.OPEN] = amountOpen;
        this.amounts[VIOLATION_STATUSES.CLOSED] = amountClose;
        this.amounts[VIOLATION_STATUSES.REMEDIATED] = amountRemediate;
        this.amounts[VIOLATION_STATUSES.SUPPRESSED] = amountSuppress;
        this.amounts[VIOLATION_STATUSES.DELETED] = amountDelete;
        // lastUpdateDate is updated when new resource in status 'OPEN' was detected
        this.lastUpdateDate = lastUpdateDate;
        this.isInitialReport = isInitialReport || false;
        this.isSuppressed = isSuppressed;
    }

    static createFromDynamoDbItem(item) {
        return new ViolationSummary({
            awsAccountId: item.aws_account_id,
            id: item.id,
            violationId: item.violation_id,
            amountOpen: item.amount_open,
            amountClose: item.amount_close,
            amountSuppress: item.amount_suppress,
            amountRemediate: item.amount_remediate,
            amountDelete: item.amount_delete,
            lastUpdateDate: item.last_update_date,
            isInitialReport: item.is_initial_report,
            isSuppressed: item.is_suppressed
        });
    }

    toDynamoDbItem() {
        const item = {
            id: this.id,
            violation_id: this.violationId,
            aws_account_id: this.awsAccountId,
            amount_open: this.amounts[VIOLATION_STATUSES.OPEN],
            amount_close: this.amounts[VIOLATION_STATUSES.CLOSED],
            amount_suppress: this.amounts[VIOLATION_STATUSES.SUPPRESSED],
            amount_remediate: this.amounts[VIOLATION_STATUSES.REMEDIATED],
            amount_delete: this.amounts[VIOLATION_STATUSES.DELETED],
            last_update_date: this.lastUpdateDate,
            is_initial_report: this.isInitialReport,
            is_suppressed: this.isSuppressed
        };

        if (this.isSuppressed) {
            item.is_suppressed = this.isSuppressed;
        }

        return item;
    }

    getStatusList() {
        const statuses = [];
        Object.keys(this.amounts)
            .filter((status) => this.amounts[status] && this.amounts[status] > 0)
            .forEach((status) => statuses.push(status));

        return statuses;
    }

    /**
     * @param {Number} numOfNewResources     - Number of new resources in violation
     * @param {string} timeOfUpdate          - String representing the time of update
     * @return {ViolationSummary}   - The updated Violation Summary
     */
    addNewResources(numOfNewResources, timeOfUpdate) {
        this.amounts[VIOLATION_STATUSES.OPEN] += numOfNewResources;
        this.lastUpdateDate = timeOfUpdate;
        return this;
    }

    unremediateResources(numOfResources, timeOfUpdate) {
        this.amounts[VIOLATION_STATUSES.REMEDIATED] -= numOfResources;
        return this.addNewResources(numOfResources, timeOfUpdate);
    }

    updateResourceStatus(fromStatus, toStatus) {
        // todo: this is the faster solution, better solution is to add new COMPLIANT row
        // eslint-disable-next-line no-param-reassign
        if (fromStatus === VIOLATION_STATUSES.COMPLIANT) fromStatus = VIOLATION_STATUSES.CLOSED;
        // eslint-disable-next-line no-param-reassign
        if (toStatus === VIOLATION_STATUSES.COMPLIANT) toStatus = VIOLATION_STATUSES.CLOSED;
        // eslint-disable-next-line no-param-reassign
        if (toStatus === VIOLATION_STATUSES.DELETED) toStatus = VIOLATION_STATUSES.DELETED;

        // updatedDate should be update only when new resource is detected
        if (fromStatus !== VIOLATION_STATUSES.OPEN && toStatus === VIOLATION_STATUSES.OPEN) {
            this.lastUpdateDate = new Date().toISOString();
        }
        if (fromStatus) {
            if (this.amounts[fromStatus] > 0) {
                this.amounts[fromStatus] -= 1;
            }
        }

        this.amounts[toStatus] += 1;
        return this;
    }
}

module.exports = ViolationSummary;
