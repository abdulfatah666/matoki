class ViolationResource {
    constructor({
        id, violationId, awsAccountId, createdBy, lastUpdateDate, owner, region, resource, status,
        checksMetadataPath, fixId, firstDetectionDate }) {
        this.id = id;
        this.violationId = violationId;
        this.awsAccountId = awsAccountId;
        this.createdBy = createdBy;
        this.lastUpdateDate = lastUpdateDate;
        this.owner = owner;
        this.region = region;
        this.resource = resource;
        this.status = status;
        this.checksMetadataPath = checksMetadataPath;
        this.fixId = fixId;
        this.firstDetectionDate = firstDetectionDate;
    }

    static fromDynamoDbItem(item) {
        const violationResource = new ViolationResource({
            id: item.id,
            violationId: item.violation_id,
            createdBy: item.created_by,
            lastUpdateDate: item.last_update_date,
            owner: item.owner,
            region: item.region,
            resource: item.resource,
            status: item.status,
            awsAccountId: item.aws_account_id,
            fixId: item.fix_id,
            firstDetectionDate: item.first_detection_date
        });
        if (item.checks_metadata_path) {
            violationResource.checksMetadataPath = item.checks_metadata_path;
        }
        return violationResource;
    }

    toDynamoDbItem() {
        return {
            id: this.id,
            violation_id: this.violationId,
            created_by: this.createdBy,
            resource: this.resource,
            last_update_date: this.lastUpdateDate,
            owner: this.owner,
            region: this.region,
            status: this.status,
            aws_account_id: this.awsAccountId,
            checks_metadata_path: this.checksMetadataPath,
            fix_id: this.fixId,
            first_detection_date: this.firstDetectionDate
        };
    }
}

module.exports = ViolationResource;
