const crypto = require('crypto');

const ALGORITHM = 'sha1';
const ENCODING_TYPE_UTF = 'utf8';
const ENCODING_TYPE_HEX = 'hex';

/**
 * compare 2 strings by rendering it safe from certain timing attacks
 * @param str1
 * @param str2
 * @returns {boolean}
 * @private
 */
const _secureCompare = (str1, str2) => {
    try {
        const bufferStr1 = Buffer.from(str1, ENCODING_TYPE_UTF);
        const bufferStr2 = Buffer.from(str2, ENCODING_TYPE_UTF);
        return crypto.timingSafeEqual(bufferStr1, bufferStr2);
    } catch (e) {
        console.error('got error while calculating secure comparison: ', e);
        return false;
    }
};

/**
 * Validating payloads from GitHub Server
 * Using HMAC authentication code (using cryptographic hash function and a secret cryptographic key)
 * @param payload: object - the body of the request
 * @param digest: string - the hash
 * @param secretToken: string - the token that used to create the HMAC
 * @returns {boolean}
 */
const verifySignature = ({ payload, digest, secretToken }) => {
    if (!digest) throw new Error('payload is not secure! digest does not exist');
    const hmac = crypto.createHmac(ALGORITHM, secretToken);
    hmac.update(JSON.stringify(payload));
    const calculatedDigest = hmac.digest(ENCODING_TYPE_HEX);
    if (!_secureCompare(digest, calculatedDigest)) throw new Error('payload is not secure! digest does not correct');
    console.info(`signature ${digest} verified`);
    return true;
};

module.exports = { verifySignature, _secureCompare };