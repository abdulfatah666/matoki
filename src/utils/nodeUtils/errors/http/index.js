/* eslint-disable max-classes-per-file */
class HttpError extends Error {
    constructor(message) {
        super();
        this.name = HttpError.name;
        this.message = message;
        this.format = {
            statusCode: this.statusCode,
            ok: false,
            message: this.message
        };
    }
}

class UnauthorizedError extends HttpError {
    constructor(message) {
        super(message);
        this.name = UnauthorizedError.name;
        this.statusCode = 401;
    }
}

class InternalServerError extends HttpError {
    constructor(message) {
        super(message);
        this.name = InternalServerError.name;
        this.statusCode = 500;
    }
}

class BadRequestError extends HttpError {
    constructor(message) {
        super(message);
        this.name = BadRequestError.name;
        this.statusCode = 400;
    }
}

class NotFoundError extends HttpError {
    constructor(message) {
        super(message);
        this.name = NotFoundError.name;
        this.statusCode = 404;
    }
}

class BadParamsError extends HttpError {
    constructor(message) {
        super(message);
        this.name = BadParamsError.name;
        this.statusCode = 422;
    }
}

module.exports = {
    HttpError,
    UnauthorizedError,
    InternalServerError,
    NotFoundError,
    BadRequestError,
    BadParamsError
};
