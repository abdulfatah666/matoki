const AWS = require('aws-sdk');

class LambdaInvokeWrapper {
    constructor(lambdaFunctionName, options = {}) {
        if (options.profile) {
            AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: options.profile });
        }
        if (options.region) {
            AWS.config.update({ region: options.region });
        }
        if (!lambdaFunctionName) throw new Error('Can\'t create new invoke lambda instance, lambdaFunctionName does\'t exist!');
        this.lambdaFunctionName = lambdaFunctionName;
    }

    async invoke(functionName, body, region = 'us-west-2', opt) {
        const params = {
            FunctionName: this.lambdaFunctionName,
            Payload: JSON.stringify({
                body,
                headers: {
                    'Content-Type': 'application/json'
                },
                path: `/invoke/${functionName}`,
                httpMethod: (opt && opt.httpMethod) ? opt.httpMethod : 'GET',
                newInvoke: (opt && opt.newInvoke)
            })
        };

        let result;

        try {
            result = await new AWS.Lambda({ region }).invoke(params).promise();
            const payload = JSON.parse(result.Payload);
            if (opt && opt.noBody) {
                return payload;
            }
            return payload.body ? JSON.parse(payload.body) : payload;
        } catch (e) {
            console.error(`Got error while running : ${functionName}\n ${result ? `Result: ${JSON.stringify(result)}` : `Error: ${e}`}`);
            throw new Error(e);
        }
    }
}

module.exports = LambdaInvokeWrapper;
