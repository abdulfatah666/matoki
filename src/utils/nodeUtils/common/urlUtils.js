class UrlUtils {
    static encodeGetParams(p) {
        return Object.entries(p)
            .map((kv) => kv.map(encodeURIComponent)
                .join('='))
            .join('&');
    }
}

module.exports = {
    UrlUtils
};
