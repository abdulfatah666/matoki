const AWS = require('aws-sdk');
const ViolationConf = require('../models/ViolationConf');

class ViolationConfDbMgr {
    constructor() {
        this.tableName = process.env.CONF_VIOLATIONS_TABLE_NAME;
        this.dynamoDb = new AWS.DynamoDB.DocumentClient();
        this.violationsCache = null;
    }

    async init() {
        if (this.violationsCache === null) {
            this.violationsCache = {};
            await this.updateViolationsCache();
        }
    }

    async updateViolationsCache() {
        await this.dynamoDb.scan({ TableName: this.tableName }).promise()
            .then((response) => response.Items.forEach((item) => {
                const violationConf = ViolationConf.createFromDynamoDbItem(item);
                this.violationsCache[violationConf.violationId] = violationConf;
            }))
            .catch((e) => {
                console.error(`Failed to update cache from table ${this.tableName}, error message: ${e.message}`);
                throw new Error('failed to retrieve data');
            });
    }

    /**
     * @param {string} violationId
     * @return {ViolationConf} ViolationConf
     */
    async getViolationConf(violationId) {
        await this.init();
        return this.violationsCache[violationId];
    }

    /**
     * Get all the violation configurations as a list
     * @return {Promise<ViolationConf[]>}
     */
    async getViolationConfs() {
        await this.init();
        return Object.values(this.violationsCache);
    }

    /**
     * @param {ViolationConf} violationConf
     */
    async saveViolationConf(violationConf) {
        this.violationsCache[violationConf.violationId] = violationConf;
        await this.dynamoDb.put({ TableName: this.tableName, Item: violationConf.toDynamoDbItem() });
    }

    async getRemediations(violationId) {
        await this.init();
        return this.violationsCache[violationId].remediationIdSet;
    }

    async getCategorytoViolationsMap() {
        await this.init();
        return Object.keys(this.violationsCache).reduce((accu, violationKey) => {
            const { category } = this.violationsCache[violationKey];
            const newAccu = { ...accu };
            if (!newAccu[category]) newAccu[category] = [];
            newAccu[category].push(violationKey);
            return newAccu;
        }, {});
    }
}

module.exports = new ViolationConfDbMgr();
