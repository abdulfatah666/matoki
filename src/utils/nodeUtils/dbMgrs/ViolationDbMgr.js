/* eslint-disable no-param-reassign */
const AWS = require('aws-sdk');

const VIOLATIONS_SUMMARY_TABLE_NAME = process.env.VIOLATIONS_SUMMARY_TABLE;
const ViolationSummary = require('../models/ViolationSummary');
const { VIOLATION_STATUSES } = require('../models/Enums');

class ViolationDbMgr {
    constructor() {
        this.dynamoDB = new AWS.DynamoDB.DocumentClient();
    }

    async getViolationsList(awsAccountIds) {
        const filterParams = {};
        awsAccountIds.forEach((accountId) => {
            filterParams[`:accountId${awsAccountIds.indexOf(accountId)}`] = accountId;
        });

        const params = {
            TableName: VIOLATIONS_SUMMARY_TABLE_NAME,
            FilterExpression: `aws_account_id IN (${Object.keys(filterParams).toString()})`,
            ExpressionAttributeValues: filterParams
        };
        const violations = [];
        let response;
        do {
            response = await this.dynamoDB.scan(params).promise();
            violations.push(...response.Items.map(ViolationSummary.createFromDynamoDbItem));
            params.ExclusiveStartKey = response.LastEvaluatedKey;
        } while (typeof response.LastEvaluatedKey != 'undefined');

        return violations;
    }

    async getViolation(accountId, violationId) {
        return this.dynamoDB.get({ TableName: VIOLATIONS_SUMMARY_TABLE_NAME, Key: { id: `${accountId}::${violationId}` } }).promise()
            .then((response) => {
                if (response.Item) {
                    return ViolationSummary.createFromDynamoDbItem(response.Item);
                }
                return null;
            })
            .catch((e) => {
                console.error(e);
                return null;
            });
    }

    /**
     * @param {ViolationSummary} violationSummary
     * @return {Promise<PromiseResult<DocumentClient.PutItemOutput, AWSError>>} Operartion's result
     */
    async saveViolation(violationSummary) {
        return this.dynamoDB.put({
            TableName: VIOLATIONS_SUMMARY_TABLE_NAME,
            Item: violationSummary.toDynamoDbItem()
        }).promise();
    }

    // eslint-disable-next-line class-methods-use-this
    groupBy(array, key) {
        return array.reduce((groupsMap, currentValue) => {
            (groupsMap[currentValue[key]] = groupsMap[currentValue[key]] || []).push(currentValue);
            return groupsMap;
        }, {});
    }

    async updateViolationSummary(violationId, resources, fromStatus, toStatus) {
        if (resources.length === 0) {
            // Nothing to do
            return {
                count: 0,
                fromStatus,
                toStatus
            };
        }
        console.log('resources before map: ', JSON.stringify(resources));
        const accountToResourcesMap = this.groupBy(resources, 'awsAccountId');
        console.log('Account to resources map: ', accountToResourcesMap);

        const result = [];
        const promises = [];
        // Loop over the account and update these account violation's selected resources
        // add the update result into array

        Object.keys(accountToResourcesMap).forEach(async (accountId) => {
            promises.push(
                this.getViolation(accountId, violationId)
                    .then(async (violationSummary) => {
                        if (violationSummary.amounts[fromStatus] >= accountToResourcesMap[accountId].length) {
                            violationSummary.amounts[fromStatus] -= accountToResourcesMap[accountId].length;
                            violationSummary.amounts[toStatus] += accountToResourcesMap[accountId].length;
                        }

                        await this.saveViolation(violationSummary);

                        result.push({
                            violationId: violationSummary.violationId,
                            count: resources.length,
                            from: fromStatus,
                            to: toStatus
                        });
                    })
            );
        });

        await Promise.all(promises);

        console.log('result of update violation summary: ', result);

        return result;
    }

    async setIsSuppressed(accountId, violationId, isSuppressed) {
        return this.getViolation(accountId, violationId)
            .then((violationSummary) => {
                violationSummary.isSuppressed = isSuppressed;
                const toMove = violationSummary.amounts[isSuppressed ? VIOLATION_STATUSES.OPEN : VIOLATION_STATUSES.SUPPRESSED];
                violationSummary.amounts[isSuppressed ? VIOLATION_STATUSES.OPEN : VIOLATION_STATUSES.SUPPRESSED] = 0;
                violationSummary.amounts[isSuppressed ? VIOLATION_STATUSES.SUPPRESSED : VIOLATION_STATUSES.OPEN] += toMove;
                return this.saveViolation(violationSummary).then(() => violationSummary);
            });
    }
}

module.exports = new ViolationDbMgr();
