const AWS = require('aws-sdk');

const { VIOLATIONS_RESOURCE_HISTORY_TABLE } = process.env;
const ViolationHistory = require('../models/ViolationHistory');

const splitToChunks = (arr, chunkSize) => {
    const ans = [];
    for (let i = 0; i < arr.length; i += chunkSize) {
        ans.push(arr.slice(i, i + chunkSize));
    }
    return ans;
};

class ViolationHistoryDbMgr {
    constructor() {
        this.dynamoDb = new AWS.DynamoDB.DocumentClient();
    }

    /**
     * Saves the status of the resources to the history table
     * @param {ViolationResource[]} resources - the resources to be saved to history
     * @param {String} user - the name of the entity who updated these resources
     * @return {Promise<ViolationResource[]>} The resources that were saved to the history table
     */
    async saveToHistory(resources, user) {
        const events = resources.map((res) => {
            const currentTime = new Date().toISOString();
            const key = `${res.id}::${currentTime}`;
            return new ViolationHistory({
                id: key,
                violationId: res.violationId,
                awsAccountId: res.awsAccountId,
                resource: res.resource,
                updatedBy: user,
                createdBy: res.createdBy,
                lastUpdateDate: currentTime,
                owner: res.owner,
                region: res.region,
                status: res.status
            }).toDynamoDbItem();
        });

        const eventsChunks = splitToChunks(events, 25); // Max batch size is 25
        await eventsChunks.forEach(async (chunk) => {
            const itemsArray = chunk.map((event) => ({ PutRequest: { Item: event } }));
            const params = { RequestItems: {} };
            params.RequestItems[VIOLATIONS_RESOURCE_HISTORY_TABLE] = itemsArray;
            await this.dynamoDb.batchWrite(params).promise().then(console.log);
            console.log('Updated history');
        });
        return resources;
    }

    getViolationHistoryByViolationId(awsAccountId, violationId) {
        const params = {
            TableName: VIOLATIONS_RESOURCE_HISTORY_TABLE,
            IndexName: 'violation_id-aws_account_id-index',
            KeyConditionExpression: 'violation_id = :violationId and aws_account_id=:awsAccountId',
            ExpressionAttributeValues: {
                ':violationId': violationId,
                ':awsAccountId': awsAccountId
            }
        };
        return this.dynamoDb.query(params).promise().then((res) => res.Items.map((item) => ViolationHistory.fromDynamoDbItem(item)));
    }
}
module.exports = new ViolationHistoryDbMgr();
