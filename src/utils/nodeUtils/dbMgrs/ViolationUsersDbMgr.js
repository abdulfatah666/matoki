const AWS = require('aws-sdk');
const ViolationUser = require('../models/ViolationUser');

const { VIOLATIONS_USERS_TABLE } = process.env;

class ViolationUsersDbMgr {
    constructor() {
        this.dynamoDB = new AWS.DynamoDB.DocumentClient();
    }

    /**
     * @param {String} user
     * @return {Promise<ViolationUser>}
     */
    async getViolationsMapByUser(user) {
        const params = {
            TableName: VIOLATIONS_USERS_TABLE,
            Key: { id: user }
        };
        const data = await this.dynamoDB.get(params).promise();
        return ViolationUser.createFromDynamoDbItem(data.Item);
    }

    /**
     * @param {ViolationUser} violationUser
     * @return {Promise<ViolationUser>} The violation user that was saved
     */
    async saveViolationUser(violationUser) {
        const item = violationUser.toDynamoDbItem();
        await this.dynamoDB.put({ TableName: VIOLATIONS_USERS_TABLE, Item: item }).promise();
        return violationUser;
    }
}

module.exports = new ViolationUsersDbMgr();