const AWS = require('aws-sdk');

class CustomersWebappDataDBMgr {
    constructor() {
        this.tableName = process.env.CUSTOMERS_WEBAPP_DATA_TABLE_NAME;
        this.dynamoDb = new AWS.DynamoDB.DocumentClient();
        this.categoriesDefaultValues = {
            getting_started: {
                dashboard_visited: false,
                incidents_visited: false,
                graphs_visited: false
            }
        };

        this.categories = Object.keys(this.categoriesDefaultValues);
    }

    async getByCustomerName(customerName) {
        console.log(customerName);
        return this.dynamoDb.get({
            TableName: this.tableName,
            Key: { customer_name: customerName }
        }).promise().then((data) => {
            console.log(data);
            return data.Item ? data.Item : null;
        }).catch((e) => {
            console.error(e);
            return null;
        });
    }

    async getByCategory(customerName, categoryName) {
        if (this.categories.includes(categoryName)) {
            const customerData = await this.getByCustomerName(customerName);
            if (customerData) {
                const categoryData = customerData[categoryName];

                if (categoryData) {
                    const newCategoryValues = {};
                    newCategoryValues[categoryName] = categoryData;
                    return newCategoryValues;
                }

                console.error('Invalid category - ', categoryName);
                return null;
            }

            console.error('No details for customer ', customerName);
            return null;
        }

        console.error('Invalid category - ', categoryName);
        return null;
    }

    async update(updatedCategoriesObject) {
        const currentItem = await this.getByCustomerName(updatedCategoriesObject.customer_name);
        if (currentItem) {
            const newCustomerDetailsItem = Object.assign(currentItem, updatedCategoriesObject);

            // Save the updated item to dynamo
            return this.dynamoDb.put({
                TableName: this.tableName,
                Item: newCustomerDetailsItem
            })
                .promise()
                .then(() => updatedCategoriesObject)
                .catch((e) => {
                    console.error(e);
                    return null;
                });
        }

        console.error('No details for customer ', updatedCategoriesObject.customer_name);
        return null;
    }

    async create(customerName, object) {
        const inputObjectCategories = Object.keys(object);
        const filteredCategories = inputObjectCategories.filter((category) => this.categories.includes(category));

        // Check if the input object contains all the required categories
        if ((filteredCategories.length === this.categories.length)
            && (inputObjectCategories.length === this.categories.length)) {
            const item = object;
            item.customer_name = customerName;

            // Save the updated item to dynamo
            return this.dynamoDb.put({
                TableName: this.tableName,
                Item: object
            })
                .promise()
                .then(() => object)
                .catch((e) => {
                    console.error(e);
                    console.error(`failed create for customer ${customerName} with the following data: ${object}`);
                    return null;
                });
        }

        console.error(`Invalid input object for customer ${customerName}: `, object);
        return null;
    }
}

let instance;

function getInstance() {
    if (!instance) {
        instance = new CustomersWebappDataDBMgr();
    }

    return instance;
}

module.exports = { getInstance };