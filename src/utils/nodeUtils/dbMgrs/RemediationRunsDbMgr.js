const AWS = require('aws-sdk');

const { RUNS_TABLE } = process.env;

class RemediationRunsDbMgr {
    constructor() {
        this.dynamoDb = new AWS.DynamoDB.DocumentClient();
    }

    async updateRunTable(runId, updateExpression, expressionValues) {
        return this.dynamoDb.update({
            TableName: RUNS_TABLE,
            Key: { run_id: runId },
            UpdateExpression: updateExpression,
            ExpressionAttributeValues: expressionValues,
            ReturnValues: 'UPDATED_NEW'
        }).promise();
    }

    async saveResultsToDb(runId, results) {
        return this.dynamoDb.update({
            TableName: RUNS_TABLE,
            Key: { run_id: runId },
            UpdateExpression: 'set results = :res, run_status = :sta',
            ExpressionAttributeValues: {
                ':res': JSON.stringify(results),
                ':sta': 'done'
            },
            ReturnValues: 'UPDATED_NEW'
        }).promise().then(() => results);
    }

    async saveFailureToDb(runId, failureMessage) {
        return this.dynamoDb.update({
            TableName: RUNS_TABLE,
            Key: { run_id: runId },
            UpdateExpression: 'set failure_message = :mes, run_status = :sta',
            ExpressionAttributeValues: {
                ':mes': failureMessage,
                ':sta': 'failed'
            },
            ReturnValues: 'UPDATED_NEW'
        }).promise();
    }

    async getRun(runId) {
        return this.dynamoDb.get({
            TableName: RUNS_TABLE,
            Key: { run_id: runId }
        }).promise();
    }

    async getViolationRuns(violationId, customer, limit = 10) {
        return this.dynamoDb.query({
            TableName: RUNS_TABLE,
            IndexName: 'violation_id-customer-index',
            KeyConditionExpression: 'violation_id = :key AND customer = :customer',
            ExpressionAttributeValues: {
                ':key': violationId,
                ':customer': customer
            },
            ScanIndexForward: false,
            Limit: limit
        }).promise();
    }

    async createNewRun(runObject) {
        return this.dynamoDb.put({ TableName: RUNS_TABLE, Item: runObject }).promise();
    }
}

module.exports = new RemediationRunsDbMgr();
