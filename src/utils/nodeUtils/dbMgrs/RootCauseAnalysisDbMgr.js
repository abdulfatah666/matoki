const AWS = require('aws-sdk');
const crypto = require('crypto');

class RootCauseAnalysisDbMgr {
    constructor(tableName) {
        this.dynamoDB = new AWS.DynamoDB.DocumentClient();
        this.tableName = tableName;
    }

    getRootCause(awsAccountId, resourceId) {
        const md5ResourceId = crypto.createHash('md5')
            .update(resourceId)
            .digest('hex');
        return this.dynamoDB.get({ TableName: this.tableName, Key: { aws_account_id: `${awsAccountId}:${md5ResourceId}` } }).promise().then((response) => {
            if (response.Item) {
                return {
                    stackId: response.Item.stack ? response.Item.stack.StackId : null,
                    stackName: response.Item.stack ? response.Item.stack.StackName : null,
                    tags: response.Item.tags ? response.Item.tags : [],
                    lastUpdate: response.Item.lastUpdate
                };
            }
            return {
                stackId: null,
                stackName: null,
                tags: [],
                lastUpdate: null
            };
        });
    }
}

let instance;

function getInstance(tableName) {
    if (!instance) {
        instance = new RootCauseAnalysisDbMgr(tableName);
    }

    return instance;
}

module.exports = { getInstance };
