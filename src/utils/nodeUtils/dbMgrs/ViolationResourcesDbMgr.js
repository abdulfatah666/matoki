const AWS = require('aws-sdk');
const ViolationResource = require('../models/ViolationResource');

const VIOLATIONS_RESOURCES_TABLE_NAME = process.env.VIOLATIONS_FOR_RESOURCES_TABLE_NAME;
const EXPRESSIONS_FIELDS = {
    status: 'status',
    region: 'region',
    last_update_date: 'last_update_date',
    id: 'id',
    violationId: 'violation_id',
    awsAccountId: 'aws_account_id',
    createdBy: 'created_by',
    LastUpdateDate: 'last_update_date',
    resource: 'resource',
    checkMetadataPath: 'check_metadata_path',
    fixId: 'fix_id'
};

class ViolationResourcesDbMgr {
    constructor() {
        this.dynamoDB = new AWS.DynamoDB.DocumentClient();
    }

    /**
     * @param {String} violationId
     * @param {Object<{awsAccountId: String, resourceId: String}>} resource
     * @return {Promise<ViolationResource>}
     */
    async getViolationResource(violationId, resource) {
        const key = `${violationId}::${resource.awsAccountId}::${resource.id}`;
        return this.dynamoDB.get({
            TableName: VIOLATIONS_RESOURCES_TABLE_NAME,
            Key: { id: key, violation_id: violationId }
        }).promise()
            .then((response) => ViolationResource.fromDynamoDbItem(response.Item))
            .catch((e) => {
                console.error(`Bad resource: ${JSON.stringify(resource)}. Error: ${e.message}`);
                return null;
            });
    }

    /**
     * @param {ViolationResource} violationResource
     * @return {Promise<ViolationResource>} The violation resource that was saved
     */
    async saveViolationResource(violationResource) {
        const item = violationResource.toDynamoDbItem();
        return this.dynamoDB.put({ TableName: VIOLATIONS_RESOURCES_TABLE_NAME, Item: item }).promise()
            .then(() => violationResource);
    }

    /**
     * Update the resources to the new status, filtered by current status if required.
     * @param {ViolationResource[]} resources - The resources to update
     * @param {VIOLATION_STATUSES} currentStatus - The current resource status to filter by. If null - will apply to all statuses
     * @param {VIOLATION_STATUSES} newStatus - The new status that will be saved to the DB.
     * @return {Promise<[ViolationResource, String, String]>} - The results, in an array
     */
    async updateViolationResources(resources, currentStatus, newStatus) {
        return Promise.all(resources.filter((res) => currentStatus != null && res.status === currentStatus).map(async (violationResource) => {
            /* eslint-disable no-param-reassign */
            violationResource.status = newStatus;
            violationResource.lastUpdateDate = new Date().toISOString();
            await this.saveViolationResource(violationResource);
            return violationResource;
        }));
    }

    /**
     * Gets all the resources that are of the relevant violationId, and have the correct resource ID
     * @param violationId
     * @param resourceId
     * @return {Promise<ViolationResource[]>}
     */
    async updateViolationResource({ id, violationId, resource }) {
        let updateExpression = 'set ';
        const expressionAttributeNames = {};
        const expressionAttributeValues = {};
        const dynamoQuery = {
            TableName: VIOLATIONS_RESOURCES_TABLE_NAME,
            Key: {
                id,
                violation_id: violationId
            }
        };
        if (Object.keys(resource).length > 0) {
            const queryKeys = Object.keys(resource).filter(key => (EXPRESSIONS_FIELDS[key] !== 'id' && EXPRESSIONS_FIELDS[key] !== 'violation_id'));
            updateExpression += queryKeys.map((key) => (EXPRESSIONS_FIELDS[key] ? `#${EXPRESSIONS_FIELDS[key]} = :value_${EXPRESSIONS_FIELDS[key]}` : null))
                // Filter out any key which is not in EXPRESSIONS_FIELDS
                .filter((exp) => exp)
                .join(' , ');

            queryKeys.forEach((key) => {
                expressionAttributeNames[`#${EXPRESSIONS_FIELDS[key]}`] = EXPRESSIONS_FIELDS[key];
                expressionAttributeValues[`:value_${EXPRESSIONS_FIELDS[key]}`] = resource[key];
            });
            dynamoQuery.UpdateExpression = updateExpression;
            dynamoQuery.ExpressionAttributeValues = expressionAttributeValues;
            dynamoQuery.ExpressionAttributeNames = expressionAttributeNames;
        }
        try {
            const result = await this.dynamoDB.update(dynamoQuery).promise();
            console.log(`Success to update query: ${JSON.stringify(dynamoQuery)}`);
            return result;
        } catch (e) {
            console.error(`Failed to execute query ${JSON.stringify(dynamoQuery)} for ${JSON.stringify(resource)} with error ${e.message}`);
            throw e;
        }
    }

    async getResourceDetailsByResourceId(violationId, resourceId) {
        const dynamoQuery = {
            TableName: VIOLATIONS_RESOURCES_TABLE_NAME,
            IndexName: 'violation_id-aws_account_id-index',
            KeyConditionExpression: 'violation_id = :violation_id',
            FilterExpression: '#resource_id = :resource',
            ExpressionAttributeValues: {
                ':violation_id': violationId,
                ':resource': resourceId
            },
            ExpressionAttributeNames: {
                '#resource_id': 'resource'
            }
        };

        return this.dynamoDB.query(dynamoQuery).promise()
            .then((res) => res.Items.map((item) => ViolationResource.fromDynamoDbItem(item)))
            .catch((err) => {
                console.error(err);
                return null;
            });
    }

    async getResources(awsAccountId, violationId, query) {
        let filterExpression; let expressionAttributeNames;
        const expressionAttributeValues = { ':violation_id': violationId };
        const dynamoQuery = {
            TableName: VIOLATIONS_RESOURCES_TABLE_NAME,
            IndexName: 'violation_id-aws_account_id-index',
            KeyConditionExpression: 'violation_id = :violation_id',
            ExpressionAttributeValues: expressionAttributeValues
        };

        if (awsAccountId) {
            expressionAttributeValues[':aws_account_id'] = awsAccountId;
            dynamoQuery.KeyConditionExpression = 'violation_id = :violation_id AND aws_account_id = :aws_account_id';
        }

        if (query && Object.keys(query).length > 0) {
            const queryKeys = Object.keys(query);
            filterExpression = queryKeys.map((key) => (EXPRESSIONS_FIELDS[key] ? `#${EXPRESSIONS_FIELDS[key]} = :value_${key}` : null))
            // Filter out any key which is not in EXPRESSIONS_FIELDS
                .filter((exp) => exp)
                .join(' and ');

            expressionAttributeNames = {};
            queryKeys.forEach((key) => {
                expressionAttributeNames[`#${EXPRESSIONS_FIELDS[key]}`] = EXPRESSIONS_FIELDS[key];
                expressionAttributeValues[`:value_${key}`] = query[key];
            });
            dynamoQuery.FilterExpression = filterExpression;
            dynamoQuery.ExpressionAttributeValues = expressionAttributeValues;
            dynamoQuery.ExpressionAttributeNames = expressionAttributeNames;
        }

        let results = [];
        let lastScannedKey;
        do {
            const response = await this.dynamoDB.query(dynamoQuery).promise();
            results = results.concat(response.Items.map((item) => ViolationResource.fromDynamoDbItem(item)));
            results.sort((x, y) => {
                if (x.firstDetectionDate < y.firstDetectionDate) return 1;
                if (x.firstDetectionDate > y.firstDetectionDate) return -1;
                return 0;
            });
            lastScannedKey = response.LastEvaluatedKey;
            dynamoQuery.ExclusiveStartKey = lastScannedKey;
        } while (lastScannedKey);

        return results;
    }

    async getResource(id) {
        return this.dynamoDB.get({
            TableName: VIOLATIONS_RESOURCES_TABLE_NAME,
            Key: { id, violation_id: id.split(':')[0] }
        }).promise()
            .then((resp) => (resp.Item ? ViolationResource.fromDynamoDbItem(resp.Item) : null));
    }

    async getViolationForResourcesByAccount(awsAccountId) {
        let response;
        let resources = [];
        const params = {
            TableName: VIOLATIONS_RESOURCES_TABLE_NAME,
            FilterExpression: 'aws_account_id = :accountId AND #status <> :statusDeleted',
            ExpressionAttributeValues: {
                ':accountId': awsAccountId,
                ':statusDeleted': 'DELETED'
            },
            ExpressionAttributeNames: {
                '#status': 'status'
            }
        };
        do {
            response = await this.dynamoDB.scan(params).promise();
            resources = resources.concat(response.Items);
            params.ExclusiveStartKey = response.LastEvaluatedKey;
        } while (response.LastEvaluatedKey);
        return resources;
    }

    async getNewViolationResources({ awsAccountIds, violationId, date }) {
        let response;
        let resources = [];
        const filterParams = {};
        awsAccountIds.forEach((accountId) => {
            filterParams[`:accountId${awsAccountIds.indexOf(accountId)}`] = accountId;
        });
        const params = {
            TableName: VIOLATIONS_RESOURCES_TABLE_NAME,
            IndexName: 'violation_id-aws_account_id-index',
            KeyConditionExpression: 'violationId = :violationId',
            FilterExpression: 'aws_account_id IN :filterParams AND first_detection_date > :lastSeen',
            ExpressionAttributeValues: {
                ':violationId': violationId,
                ':filterParams': Object.keys(filterParams).toString(),
                ':lastSeen': date
            }
        };
        do {
            response = await this.dynamoDB.query(params).promise();
            resources = resources.concat(response.Items);
            params.ExclusiveStartKey = response.LastEvaluatedKey;
        } while (response.LastEvaluatedKey);
        return resources;
    }
}

module.exports = new ViolationResourcesDbMgr();
