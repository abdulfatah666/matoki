const AWS = require('aws-sdk');

class BenchmarksDbMgr {
    constructor() {
        this.tableName = process.env.BENCHMARKS_TABLE_NAME;
        this.dynamoDb = new AWS.DynamoDB.DocumentClient();
    }

    async getById(benchmarkId) {
        return await this.dynamoDb.query({
            TableName: this.tableName,
            KeyConditionExpression: 'id = :key',
            ExpressionAttributeValues: {
                ':key': benchmarkId
            },
            ScanIndexForward: false
        }).promise().catch((e) => {
            console.error(e);
            return null;
        });
    }

    async getAll() {
        return await this.dynamoDb.scan({ TableName: this.tableName })
            .promise()
            .catch((e) => {
                console.error(e);
                return null;
            });
    }
}

let instance;

function getInstance(tableName) {
    if (!instance) {
        instance = new BenchmarksDbMgr(tableName);
    }

    return instance;
}

module.exports = { getInstance };