const AWS = require('aws-sdk');

const ddb = new AWS.DynamoDB.DocumentClient();

class EtlDbMgr {
    constructor() {
        this.tableName = process.env.ETL_TABLE;
    }

    getLastQueried(customerName, integrationName) {
        return ddb.get({ TableName: this.tableName, Key: { customer_name: customerName } }).promise()
            .then(response => response.Item ? response.Item[integrationName] : null);
    }

    setLastQueried(customerName, integrationName, endDate) {
        return ddb.update({
            TableName: this.tableName,
            Key: { customer_name: customerName },
            UpdateExpression: `set ${integrationName} = :enddate`,
            ExpressionAttributeValues: {
                ':enddate': endDate
            }
        }).promise().then(() => ({ customerName, integrationName, endDate }));
    }
}

module.exports = new EtlDbMgr();
