const AWS = require('aws-sdk');

const { SUPPRESSIONS_JUSTIFICATIONS_TABLE } = process.env;

class SuppressionsJustificationsDbMgr {
    constructor() {
        this.tableName = SUPPRESSIONS_JUSTIFICATIONS_TABLE;
        this.dynamoDb = new AWS.DynamoDB.DocumentClient();
    }

    async updateJustificationsTable(suppressionId, updateExpression, expressionValues) {
        return this.dynamoDb.update({
            TableName: this.tableName,
            Key: { suppression_id: suppressionId },
            UpdateExpression: updateExpression,
            ExpressionAttributeValues: expressionValues,
            ReturnValues: 'UPDATED_NEW'
        }).promise().catch((e) => {
            console.error(e);
            return null;
        });
    }

    async putSuppressionJustification(suppressionId, violationId, comment, resources, resourcesCount, customerName, userMail, startTime) {
        return this.dynamoDb.put({
            TableName: this.tableName,
            Key: { suppression_id: suppressionId },
            Item: {
                suppression_id: suppressionId,
                violation_id: violationId,
                justification_comment: comment,
                resources: this.dynamoDb.createSet(resources),
                resources_count: resourcesCount,
                owner: userMail,
                customer: customerName,
                start_time: startTime
            }
        }).promise().catch((e) => {
            console.error(e);
            return null;
        });
    }

    async fetchSuppressionsJustifications(violationId, customer) {
        return this.dynamoDb.query({
            TableName: this.tableName,
            IndexName: 'violation_id-customer-index',
            KeyConditionExpression: 'violation_id = :key AND customer = :customer',
            ExpressionAttributeValues: {
                ':key': violationId,
                ':customer': customer
            },
            ScanIndexForward: false
        }).promise().catch((e) => {
            console.error(e);
            return null;
        });
    }
}

module.exports = new SuppressionsJustificationsDbMgr();
