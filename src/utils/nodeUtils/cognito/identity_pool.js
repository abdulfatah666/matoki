const AWS = require('aws-sdk');

class IdentityPool {
    constructor() {
        this.cognitoidentity = new AWS.CognitoIdentity();
    }

    // eslint-disable-next-line class-methods-use-this
    async _getIdentityPoolByName(name) {
        let token = '';
        while (token !== undefined) {
            const res = await this.cognitoidentity.listIdentityPools({
                MaxResults: 60,
                NextToken: token !== '' ? token : undefined
            })
                .promise();
            token = res.NextToken;
            const identityPool = res.IdentityPools.find((findIdentityPool) => findIdentityPool.IdentityPoolName === name);

            if (identityPool) {
                return await this.cognitoidentity.describeIdentityPool({ IdentityPoolId: identityPool.IdentityPoolId })
                    .promise();
            }
        }

        return null;
    }

    async getCognitoProvider(identityPoolName, ProviderName, ClientId) {
        const identityPool = await this._getIdentityPoolByName(identityPoolName);

        if (!identityPool.CognitoIdentityProviders) identityPool.CognitoIdentityProviders = [];

        return identityPool.CognitoIdentityProviders.find((findCognitoIdentityProvider) => findCognitoIdentityProvider.ClientId === ClientId
            && findCognitoIdentityProvider.ProviderName === ProviderName);
    }

    async addCognitoProvider(identityPoolName, { ProviderName, ClientId, ServerSideTokenCheck }, roleMapping) {
        const identityPool = await this._getIdentityPoolByName(identityPoolName);

        if (!identityPool.CognitoIdentityProviders) identityPool.CognitoIdentityProviders = [];

        const cognitoIdentityProvider = identityPool.CognitoIdentityProviders.find((findCognitoIdentityProvider) => findCognitoIdentityProvider.ClientId === ClientId
            && findCognitoIdentityProvider.ProviderName === ProviderName);

        if (cognitoIdentityProvider) {
            throw new Error('The provider already exist');
        } else {
            await this.cognitoidentity.updateIdentityPool({
                ...identityPool,
                CognitoIdentityProviders: [...identityPool.CognitoIdentityProviders, {
                    ProviderName,
                    ClientId,
                    ServerSideTokenCheck
                }]
            })
                .promise();

            if (roleMapping) {
                const identityPoolRoles = await this.cognitoidentity.getIdentityPoolRoles({ IdentityPoolId: identityPool.IdentityPoolId })
                    .promise();

                if (!identityPoolRoles.RoleMappings) identityPoolRoles.RoleMappings = {};

                identityPoolRoles.RoleMappings[`${ProviderName}:${ClientId}`] = roleMapping;

                await this.cognitoidentity.setIdentityPoolRoles(identityPoolRoles)
                    .promise();
            }
        }
    }

    async updateCognitoProvider(identityPoolName, ProviderName, ClientId, { ServerSideTokenCheck }, roleMapping) {
        const identityPool = await this._getIdentityPoolByName(identityPoolName);

        if (!identityPool.CognitoIdentityProviders) identityPool.CognitoIdentityProviders = [];

        const cognitoIdentityProvider = identityPool.CognitoIdentityProviders.find((findCognitoIdentityProvider) => findCognitoIdentityProvider.ClientId === ClientId
            && findCognitoIdentityProvider.ProviderName === ProviderName);

        if (!cognitoIdentityProvider) {
            throw new Error('The provider does not exist');
        } else {
            if (ServerSideTokenCheck !== undefined) {
                cognitoIdentityProvider.ServerSideTokenCheck = ServerSideTokenCheck;
                await this.cognitoidentity.updateIdentityPool({
                    ...identityPool
                })
                    .promise();
            }

            if (roleMapping !== undefined) {
                const identityPoolRoles = await this.cognitoidentity.getIdentityPoolRoles({ IdentityPoolId: identityPool.IdentityPoolId })
                    .promise();

                if (!identityPoolRoles.RoleMappings) identityPoolRoles.RoleMappings = {};

                identityPoolRoles.RoleMappings[`${ProviderName}:${ClientId}`] = roleMapping;

                await this.cognitoidentity.setIdentityPoolRoles(identityPoolRoles)
                    .promise();
            }
        }
    }

    async deleteCognitoProvider(identityPoolName, ProviderName, ClientId) {
        const identityPool = await this._getIdentityPoolByName(identityPoolName);

        if (!identityPool.CognitoIdentityProviders) identityPool.CognitoIdentityProviders = [];

        const cognitoIdentityProvider = identityPool.CognitoIdentityProviders.find((findCognitoIdentityProvider) => findCognitoIdentityProvider.ClientId === ClientId
            && findCognitoIdentityProvider.ProviderName === ProviderName);

        if (!cognitoIdentityProvider) {
            throw new Error('The provider does not exist');
        } else {
            await this.cognitoidentity.updateIdentityPool({
                ...identityPool,
                CognitoIdentityProviders: identityPool.CognitoIdentityProviders.filter((findCognitoIdentityProvider) => findCognitoIdentityProvider.ClientId !== ClientId
                    && findCognitoIdentityProvider.ProviderName !== ProviderName)
            })
                .promise();

            const identityPoolRoles = await this.cognitoidentity.getIdentityPoolRoles({ IdentityPoolId: identityPool.IdentityPoolId })
                .promise();

            if (!identityPoolRoles.RoleMappings) identityPoolRoles.RoleMappings = {};

            delete identityPoolRoles.RoleMappings[`${ProviderName}:${ClientId}`];

            await this.cognitoidentity.setIdentityPoolRoles(identityPoolRoles)
                .promise();
        }
    }

    async delete(name) {
        const identityPool = await this._getIdentityPoolByName(name);
        await this.cognitoidentity.deleteIdentityPool({ IdentityPoolId: identityPool.IdentityPoolId })
            .promise();
    }

    async update(name, { roles, AllowUnauthenticatedIdentities }) {
        const identityPool = await this._getIdentityPoolByName(name);
        if (AllowUnauthenticatedIdentities !== undefined) {
            await this.cognitoidentity.updateIdentityPool({
                ...identityPool,
                AllowUnauthenticatedIdentities
            })
                .promise();
        }

        if (roles) {
            const identityPoolRoles = await this.cognitoidentity.getIdentityPoolRoles({ IdentityPoolId: identityPool.IdentityPoolId })
                .promise();

            identityPoolRoles.Roles = {
                authenticated: roles.authenticated,
                unauthenticated: roles.unauthenticated
            };

            await this.cognitoidentity.setIdentityPoolRoles(identityPoolRoles)
                .promise();
        }
    }

    async create(name, { roles, AllowUnauthenticatedIdentities }) {
        // eslint-disable-next-line no-param-reassign
        if (AllowUnauthenticatedIdentities === undefined) AllowUnauthenticatedIdentities = false;

        if (await this._getIdentityPoolByName(name)) {
            throw new Error(`identity pool ${name} already exist`);
        }

        const identityPool = await this.cognitoidentity.createIdentityPool({
            AllowUnauthenticatedIdentities,
            IdentityPoolName: name
        })
            .promise();

        if (roles) {
            await this.cognitoidentity.setIdentityPoolRoles({
                IdentityPoolId: identityPool.IdentityPoolId,
                Roles: {
                    authenticated: roles.authenticated,
                    unauthenticated: roles.unauthenticated
                }
            })
                .promise();
        }

        return identityPool;
    }

    async get(name) {
        return this._getIdentityPoolByName(name);
    }
}

module.exports = IdentityPool;