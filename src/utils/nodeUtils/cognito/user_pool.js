const AWS = require('aws-sdk');

class UserPool {
    constructor() {
        this.cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    }

    async addScopeToAppClientID(scope, { appClientID, userPoolID }) {
        const appClient = await this.cognitoidentityserviceprovider.describeUserPoolClient({
            ClientId: appClientID,
            UserPoolId: userPoolID
        }).promise();

        if (appClient.UserPoolClient.SupportedIdentityProviders.indexOf(scope) === -1) {
            delete appClient.UserPoolClient.ClientSecret;
            delete appClient.UserPoolClient.LastModifiedDate;
            delete appClient.UserPoolClient.CreationDate;
            await this.cognitoidentityserviceprovider.updateUserPoolClient({
                ...appClient.UserPoolClient,
                SupportedIdentityProviders: [...appClient.UserPoolClient.SupportedIdentityProviders, scope]
            }).promise();
        }
    }

    async removeScopeToAppClientID(scope, { appClientID, userPoolID }) {
        const appClient = await this.cognitoidentityserviceprovider.describeUserPoolClient({
            ClientId: appClientID,
            UserPoolId: userPoolID
        }).promise();

        if (appClient.UserPoolClient.SupportedIdentityProviders.indexOf(scope) !== -1) {
            await this.cognitoidentityserviceprovider.updateUserPoolClient({
                ...appClient.UserPoolClient,
                SupportedIdentityProviders: appClient.UserPoolClient.SupportedIdentityProviders.filter((supportedIdentityProvider) => supportedIdentityProvider !== scope)
            }).promise();
        }
    }
}

module.exports = UserPool;