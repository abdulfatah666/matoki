const program = require('commander');
const AWS = require('aws-sdk');
const IdentityPoolCli = require('./identity_pool');

program
    .command('getId <identityPoolName>')
    .description('show logs of customer')
    .option('-p, --profile <profile>', 'aws profile')
    .option('-r, --region <region>', 'aws region', 'us-west-2')
    .action(async (identityPoolName, source) => {
        try {
            if (source.profile !== 'default') {
                AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: source.profile });
            }
            AWS.config.update({ region: source.region });
            const identityPoolCli = new IdentityPoolCli();
            const identityPool = await identityPoolCli.get(identityPoolName);

            console.log(JSON.stringify({
                IdentityPoolId: identityPool.IdentityPoolId
            }, null, 2));
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    });

program
    .command('get <identityPoolName>')
    .description('show logs of customer')
    .option('-p, --profile <profile>', 'aws profile')
    .option('-r, --region <region>', 'aws region', 'us-west-2')
    .action(async (identityPoolName, source) => {
        try {
            if (source.profile !== 'default') {
                AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: source.profile });
            }
            AWS.config.update({ region: source.region });
            const identityPoolCli = new IdentityPoolCli();
            const identityPool = await identityPoolCli.get(identityPoolName);

            console.log(JSON.stringify(identityPool, null, 2));
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    });

program
    .command('create <identityPoolName>')
    .description('show logs of customer')
    .option('-p, --profile <profile>', 'aws profile')
    .option('-r, --region <region>', 'aws region', 'us-west-2')
    .option('--roleAuthenticated <roleAuthenticated>', 'Roles Authenticated')
    .option('--roleUnauthenticated <roleUnauthenticated>', 'Roles Unauthenticated')
    .option('--allowUnauthenticatedIdentities <allowUnauthenticatedIdentities>', 'Allow Unauthenticated Identities (bool)')
    .action(async (identityPoolName, source) => {
        try {
            if (source.profile !== 'default') {
                AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: source.profile });
            }
            AWS.config.update({ region: source.region });
            const identityPoolCli = new IdentityPoolCli();
            await identityPoolCli.create(identityPoolName, {
                roles: {
                    authenticated: source.roleAuthenticated,
                    unauthenticated: source.roleUnauthenticated
                },
                AllowUnauthenticatedIdentities: source.allowUnauthenticatedIdentities
            });
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    });

program
    .command('update <identityPoolName>')
    .description('show logs of customer')
    .option('-p, --profile <profile>', 'aws profile')
    .option('-r, --region <region>', 'aws region', 'us-west-2')
    .option('--roleAuthenticated <roleAuthenticated>', 'Roles Authenticated')
    .option('--roleUnauthenticated <roleUnauthenticated>', 'Roles Unauthenticated')
    .option('--allowUnauthenticatedIdentities <allowUnauthenticatedIdentities>', 'Allow Unauthenticated Identities (bool)')
    .action(async (identityPoolName, source) => {
        try {
            if (source.profile !== 'default') {
                AWS.config.credentials = new AWS.SharedIniFileCredentials({
                    profile: source.profile
                });
            }
            AWS.config.update({ region: source.region });
            const identityPoolCli = new IdentityPoolCli();
            await identityPoolCli.update(identityPoolName, {
                roles: {
                    authenticated: source.roleAuthenticated,
                    unauthenticated: source.roleUnauthenticated
                },
                AllowUnauthenticatedIdentities: source.allowUnauthenticatedIdentities
            });
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    });

program
    .command('delete <identityPoolName>')
    .description('show logs of customer')
    .option('-p, --profile <profile>', 'aws profile')
    .option('-r, --region <region>', 'aws region', 'us-west-2')
    .action(async (identityPoolName, source) => {
        try {
            if (source.profile !== 'default') {
                AWS.config.credentials = new AWS.SharedIniFileCredentials({
                    profile: source.profile
                });
            }
            AWS.config.update({ region: source.region });
            const identityPoolCli = new IdentityPoolCli();
            await identityPoolCli.delete(identityPoolName);
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    });

program
    .command('addCognitoProvider <identityPoolName>')
    .description('show logs of customer')
    .option('-p, --profile <profile>', 'aws profile')
    .option('-r, --region <region>', 'aws region', 'us-west-2')
    .option('--providerName <providerName>', 'ProviderName')
    .option('--clientId <clientId>', 'ClientId')
    .option('--serverSideTokenCheck <serverSideTokenCheck>', 'ServerSideTokenCheck (bool)')
    .option('--roleMapping [roleMapping]', 'RoleMapping (json)')
    .action(async (identityPoolName, source) => {
        try {
            if (source.profile !== 'default') {
                AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: source.profile });
            }
            AWS.config.update({ region: source.region });
            const identityPoolCli = new IdentityPoolCli();
            await identityPoolCli.addCognitoProvider(identityPoolName, {
                ProviderName: source.providerName,
                ClientId: source.clientId,
                ServerSideTokenCheck: source.serverSideTokenCheck === true
            }, source.roleMapping ? JSON.parse(source.roleMapping) : undefined);
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    });

program
    .command('updateCognitoProvider <identityPoolName>')
    .description('show logs of customer')
    .option('-p, --profile <profile>', 'aws profile')
    .option('-r, --region <region>', 'aws region', 'us-west-2')
    .option('--providerName <providerName>', 'ProviderName')
    .option('--clientId <clientId>', 'ClientId')
    .option('--serverSideTokenCheck <serverSideTokenCheck>', 'ServerSideTokenCheck (bool)')
    .option('--roleMapping [roleMapping]', 'RoleMapping (json)')
    .action(async (identityPoolName, source) => {
        try {
            if (source.profile !== 'default') {
                AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: source.profile });
            }
            AWS.config.update({ region: source.region });
            const identityPoolCli = new IdentityPoolCli();
            await identityPoolCli.updateCognitoProvider(identityPoolName, source.providerName, source.clientId, {
                ServerSideTokenCheck: source.serverSideTokenCheck === true
            }, source.roleMapping ? JSON.parse(source.roleMapping) : undefined);
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    });

program
    .command('deleteCognitoProvider <identityPoolName>')
    .description('show logs of customer')
    .option('-p, --profile <profile>', 'aws profile')
    .option('-r, --region <region>', 'aws region', 'us-west-2')
    .option('--providerName <providerName>', 'ProviderName')
    .option('--clientId <clientId>', 'ClientId')
    .action(async (identityPoolName, source) => {
        try {
            if (source.profile !== 'default') {
                AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: source.profile });
            }
            AWS.config.update({ region: source.region });
            const identityPoolCli = new IdentityPoolCli();
            await identityPoolCli.deleteCognitoProvider(identityPoolName, source.providerName, source.clientId);
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    });

program.parse(process.argv);