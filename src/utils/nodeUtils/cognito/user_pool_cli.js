const program = require('commander');
const AWS = require('aws-sdk');
const UserPool = require('./user_pool');

program
    .command('addScope <scope> <appClientID>')
    .description('add scope to app client')
    .option('-p, --profile <profile>', 'aws profile')
    .option('-r, --region <region>', 'aws region', 'us-west-2')
    .option('--userPoolID <userPoolID>', 'user pool id')
    .action(async (scope, appClientID, source) => {
        try {
            if (source.profile !== 'default') {
                AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: source.profile });
            }
            AWS.config.update({ region: source.region });
            const userPool = new UserPool();
            await userPool.addScopeToAppClientID(scope, {
                appClientID,
                userPoolID: source.userPoolID
            });
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    });

program
    .command('removeScope <scope> <appClientID>')
    .description('add scope to app client')
    .option('-p, --profile <profile>', 'aws profile')
    .option('-r, --region <region>', 'aws region', 'us-west-2')
    .option('--userPoolID <userPoolID>', 'user pool id')
    .action(async (scope, appClientID, source) => {
        try {
            if (source.profile !== 'default') {
                AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: source.profile });
            }
            AWS.config.update({ region: source.region });
            const userPool = new UserPool();
            await userPool.removeScopeToAppClientID(scope, {
                appClientID,
                userPoolID: source.userPoolID
            });
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    });

program.parse(process.argv);