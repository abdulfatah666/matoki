COGNITO UTILS
=============

Identity pool
-------

The identity pool util help to 
create, update and delete the identity pool and the identity providers.

The identity_pool_cli.js, is nodejs CLI

APIs
-----

#### getId
`node identity_pool_cli.js getId <identityPoolName>`

Get the id by identity pool name

#### Create
`node identity_pool_cli.js create <identityPoolName>`   
`--roleAuthenticated <roleAuthenticated>` - Role authenticated arn     
`--roleUnauthenticated <roleUnauthenticated>` - Role unauthenticated arn      
`--allowUnauthenticatedIdentities <allowUnauthenticatedIdentities>` - Allow Unauthenticated Identities (bool)   

Create identity pool and update his roles

#### Update
`node identity_pool_cli.js update <identityPoolName>`  
`--roleAuthenticated <roleAuthenticated>` - Role authenticated arn  
`--roleUnauthenticated <roleUnauthenticated>` - Role unauthenticated arn   
`--allowUnauthenticatedIdentities <allowUnauthenticatedIdentities>` - Allow Unauthenticated Identities (bool)  

Update the identity pool and update his roles 

#### Delete

`node identity_pool_cli.js delete <identityPoolName>`

Delete the identity pool

#### addCognitoProvider

`node identity_pool_cli.js addCognitoProvider <identityPoolName>`    
`--providerName <providerName>` - The user pool ARN   
`--clientId <clientId>`- App client ID    
`--serverSideTokenCheck <serverSideTokenCheck>` - Server side token check   
`--roleMapping [roleMapping]` - RoleMapping (json) like aws cli 

add new cognito provider

#### updateCognitoProvider

`node identity_pool_cli.js updateCognitoProvider <identityPoolName>`    
`--providerName <providerName>` - The user pool ARN   
`--clientId <clientId>`- App client ID    
`--serverSideTokenCheck <serverSideTokenCheck>` - Server side token check   
`--roleMapping [roleMapping]` - RoleMapping (json) like aws cli 

update exist cognito provider, the key of the update is provider name and client id

#### deleteCognitoProvider

`node identity_pool_cli.js updateCognitoProvider <identityPoolName>`    
`--providerName <providerName>` - The user pool ARN   
`--clientId <clientId>`- App client ID    

delete cognito provider by providerName and client id

User Pool
-------

The user pool util help to add scope to the appclient

#### addScope
`node user_pool_cli.js addScope <scope> <appClientID>`    
`--userPoolID <userPoolID>` - user pool ID  

#### removeScope
`node user_pool_cli.js removeScope <scope> <appClientID>`    
`--userPoolID <userPoolID>` - user pool ID  