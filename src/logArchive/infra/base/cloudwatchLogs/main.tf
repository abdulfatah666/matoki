locals {
  function_name = "handle-cloudwatchLogs-${var.unique_tag}"
  unique_tag_with_dash      = var.unique_tag == "" ? var.unique_tag : format("-%s", var.unique_tag)
  unique_tag_with_underline = var.unique_tag == "" ? var.unique_tag : format("_%s", var.unique_tag)
}

data "aws_caller_identity" "current" {}

data "aws_sns_topic" "sns_topic_monitor_error" {
  count = var.monitor["enable"] ? 1 : 0
  name  = var.monitor["sns_topic_error_name"]
}

// S3 Bucket to store Cloudwatch Logs
resource "aws_s3_bucket" "bucket" {
  bucket = "bc-cwl-${data.aws_caller_identity.current.account_id}-${var.unique_tag}"
  acl    = "private"
  force_destroy = true

  versioning {
    enabled = true
  }

  lifecycle_rule {
    id      = "expire30d"
    enabled = true

    expiration {
      days = 30
    }
  }

  tags = {
    Name = "bc-cwl-${data.aws_caller_identity.current.account_id}-${var.unique_tag}"
    Environment = var.unique_tag
  }

}

resource "aws_s3_bucket_public_access_block" "bucket_block_public_access" {
  bucket = "${aws_s3_bucket.bucket.id}"

  block_public_acls   = true
  block_public_policy = true
}

// Kinesis Firehose to dump incoming logs to S3
data "aws_iam_policy_document" "firehose_assume_role_policy" {
  statement {
    sid     = "FirehoseTrustRelationship"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["firehose.amazonaws.com"]
    }

    condition {
      test     = "StringEquals"
      values   = [ data.aws_caller_identity.current.account_id ]
      variable = "sts:ExternalId"
    }
  }
}

resource "aws_iam_role" "firehose_role" {
  name = "firehose_cwl_role${local.unique_tag_with_underline}"
  assume_role_policy = data.aws_iam_policy_document.firehose_assume_role_policy.json
}

data "aws_iam_policy_document" "firehose_policy_document" {
  statement {
    sid     = "1"
    effect  = "Allow"

    actions = [
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:PutObject"
    ]

    resources = [
      aws_s3_bucket.bucket.arn,
      "${aws_s3_bucket.bucket.arn}/*"
    ]
  }

  statement {
    sid     = "2"
    effect  = "Allow"

    actions = [ "logs:PutLogEvents" ]

    resources = [
      aws_cloudwatch_log_stream.fh_delivery_log_stream.arn
    ]
  }

}

resource "aws_iam_role_policy" "firehose_policy" {
  name  = "firehose_cwl_policy${local.unique_tag_with_underline}"
  role  = aws_iam_role.firehose_role.id

  policy = data.aws_iam_policy_document.firehose_policy_document.json
}

resource "aws_cloudwatch_log_group" "fh_delivery_log_group" {
  name = "/aws/kinesisfirehose/bc-fh-cloudwatchLogs${local.unique_tag_with_dash}"
  retention_in_days = 30

  tags = {
    Environment = var.unique_tag
    Application = "serviceA"
  }
}

resource "aws_cloudwatch_log_stream" "fh_delivery_log_stream" {
  name           = "S3Delivery"
  log_group_name = aws_cloudwatch_log_group.fh_delivery_log_group.name
}

resource "aws_kinesis_firehose_delivery_stream" "extended_s3_stream" {
  name        = "bc-fh-${var.unique_tag}-cwl-${data.aws_caller_identity.current.account_id}"
  destination = "s3"

  s3_configuration {
    role_arn   = aws_iam_role.firehose_role.arn
    bucket_arn = aws_s3_bucket.bucket.arn

    cloudwatch_logging_options {
      enabled         = true
      log_group_name  = "/aws/kinesisfirehose/bc-fh-cloudwatchLogs${local.unique_tag_with_dash}"
      log_stream_name = "S3Delivery"
    }
  }
}

// CloudWatch Logs Destination
data "aws_iam_policy_document" "cwl_to_kinesis_assume_role_policy" {
  statement {
    sid     = "CWLToKinesisTrustRelationship"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = [ "logs.us-east-1.amazonaws.com",
                      "logs.us-east-2.amazonaws.com",
                      "logs.us-west-1.amazonaws.com",
                      "logs.us-west-2.amazonaws.com",
                      "logs.ap-east-1.amazonaws.com",
                      "logs.ap-south-1.amazonaws.com",
                      "logs.ap-northeast-3.amazonaws.com",
                      "logs.ap-northeast-2.amazonaws.com",
                      "logs.ap-southeast-1.amazonaws.com",
                      "logs.ap-southeast-2.amazonaws.com",
                      "logs.ap-northeast-1.amazonaws.com",
                      "logs.ca-central-1.amazonaws.com",
                      "logs.eu-central-1.amazonaws.com",
                      "logs.eu-west-1.amazonaws.com",
                      "logs.eu-west-2.amazonaws.com",
                      "logs.eu-west-3.amazonaws.com",
                      "logs.eu-north-1.amazonaws.com",
                      "logs.me-south-1.amazonaws.com",
                      "logs.sa-east-1.amazonaws.com"
       ]
    }
  }
}

resource "aws_iam_role" "cwl_to_kinesis_role" {
  name = "cwl_to_kinesis_role${local.unique_tag_with_underline}"
  assume_role_policy = data.aws_iam_policy_document.cwl_to_kinesis_assume_role_policy.json
}

data "aws_iam_policy_document" "cwl_to_kinesis_policy_document" {
  statement {
    sid     = "1"
    effect  = "Allow"

    actions = [
      "firehose:DeleteDeliveryStream",
      "firehose:PutRecord",
      "firehose:PutRecordBatch",
      "firehose:UpdateDestination"
    ]

    resources = [
      aws_kinesis_firehose_delivery_stream.extended_s3_stream.arn
    ]
  }

  statement {
    sid     = "2"
    effect  = "Allow"

    actions = [ "iam:PassRole" ]

    resources = [
      aws_iam_role.cwl_to_kinesis_role.arn
    ]
  }

}

resource "aws_iam_policy" "cwl_to_kinesis_policy" {
  name = "cwl_to_kinesis_policy${local.unique_tag_with_underline}"
  path        = "/"
  description = "CloudWatch Logs to Kinesis Policy"

  policy = data.aws_iam_policy_document.cwl_to_kinesis_policy_document.json
}

resource "aws_iam_role_policy_attachment" "cwl_to_kinesis_attach" {
  role       = aws_iam_role.cwl_to_kinesis_role.name
  policy_arn = aws_iam_policy.cwl_to_kinesis_policy.arn
}

// Multi-Region cloudwatch logs destination - No depends_on for modules
// Sleep for Kinesis to be available
resource "null_resource" "sleep4kinesis" {
  depends_on = [
    aws_kinesis_firehose_delivery_stream.extended_s3_stream,
    aws_iam_role_policy_attachment.cwl_to_kinesis_attach,
    aws_iam_role.cwl_to_kinesis_role,
  ]

  triggers = {
    build = aws_iam_role_policy_attachment.cwl_to_kinesis_attach.id
  }

  provisioner "local-exec" {
    command = "sleep 10"
  }
}

// CloudWatch Alarm for Error in Firehose Delivery to S3
resource "aws_cloudwatch_metric_alarm" "firehose_records_alarm_error" {
  count               = var.monitor["enable"] ? 1 : 0
  alarm_name          = "${aws_kinesis_firehose_delivery_stream.extended_s3_stream.name}_error"
  comparison_operator = "LessThanThreshold"

  evaluation_periods = 1
  namespace          = "AWS/Firehose"
  metric_name        = "DeliveryToS3.Success"
  statistic          = "Average"

  dimensions = {
    DeliveryStreamName = aws_kinesis_firehose_delivery_stream.extended_s3_stream.name
  }

  treat_missing_data = "notBreaching"
  period             = 3600 // One hour
  threshold          = 1
  alarm_actions      = data.aws_sns_topic.sns_topic_monitor_error.*.arn
}

