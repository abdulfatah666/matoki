output "cloudwatch_logs_bucket" {
  value = aws_s3_bucket.bucket.id
}

output "cwl_to_kinesis_role_arn" {
  value = aws_iam_role.cwl_to_kinesis_role.arn
  depends_on = [null_resource.sleep4kinesis]
}

output "cwl_firehose_stream_arn" {
  value = aws_kinesis_firehose_delivery_stream.extended_s3_stream.arn
}

