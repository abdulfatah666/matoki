variable "lacework_account_id" {
  description = "The ID of the lacework account to be attached"
  default     = "434813966438"
}

variable "lacework_rule_name" {
  description = "The name of the rule to be created in cloudwatch for lacework events"
  default     = "lacework-cloudwatch-events"
}

variable "unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
}
