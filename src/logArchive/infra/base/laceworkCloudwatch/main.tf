resource "aws_cloudwatch_event_rule" "lacework" {
  name        = "${var.lacework_rule_name}${var.unique_tag}"
  description = "Capture lacework cloudwatch events"

  event_pattern = <<PATTERN
{
  "account": [
    "${var.lacework_account_id}"
  ]
}
PATTERN
}

resource "aws_cloudwatch_event_target" "sqs" {
  rule = aws_cloudwatch_event_rule.lacework.name
  target_id = "SendToLaceworkSQS"
  arn = aws_sqs_queue.lacework.arn
}

resource "aws_sqs_queue" "lacework" {
  name = "${var.lacework_rule_name}${var.unique_tag}"
  visibility_timeout_seconds = 300
  message_retention_seconds = 172800
}

resource "aws_sqs_queue_policy" "lacework" {
  queue_url = aws_sqs_queue.lacework.id
  policy = data.aws_iam_policy_document.sqs_policy.json
}

data "aws_iam_policy_document" "sqs_policy" {
  statement {
    sid = "lacework-cloudwatch"
    effect = "Allow"

    principals {
      identifiers = ["events.amazonaws.com"]
      type = "Service"
    }

    actions = ["sqs:SendMessage"]
    resources = [aws_sqs_queue.lacework.arn]

    condition {
      test = "ArnEquals"
      values = [aws_cloudwatch_event_rule.lacework.arn]
      variable = "aws:SourceArn"
    }
  }
}
