data "aws_iam_policy_document" "cloudwatch_policy" {
  statement {
    effect = "Allow"

    principals {
      identifiers = ["es.amazonaws.com"]
      type        = "Service"
    }

    actions = [
      "logs:PutLogEvents",
      "logs:PutLogEventsBatch",
      "logs:CreateLogStream",
    ]

    resources = [
      "arn:aws:logs:*",
    ]
  }
}

resource "aws_cloudwatch_log_resource_policy" "es_log_group_policy" {
  count = var.enable ? 1 : 0
  policy_name     = "AES-es-Application-logs${var.unique_tag}"
  policy_document = data.aws_iam_policy_document.cloudwatch_policy.json
}
