output "function_name" {
  value = module.logstash_producer_lambda.function_name
}

output "accelerator_dns_name" {
  value = data.external.accelerator_dns_name
}