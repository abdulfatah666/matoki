provider "aws" {
  region  = var.region
  profile = var.aws_profile
}

module "consts" {
  source = "../../../../utils/terraform/consts"
}

locals {
  function_name = "bc-handle-logstash-${var.unique_tag}"
  logstash_name = "logstash"
}

module "logstash_producer_lambda" {
  source                 = "../../../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../logArchive/functions/kds/producers/logstash"
  variable_string        = "--lambdaName ${local.function_name}"
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.sls_bucket_name
  lambda_names = [
    local.function_name,
  ]
}

resource "aws_security_group" "allow_https" {
  name        = "allow_https_${var.unique_tag}"
  description = "Allow HTTPS inbound traffic"
  vpc_id      = module.logstash_vpc.vpc_id

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = var.turbo_mode ? 80 : 443
    to_port     = var.turbo_mode ? 80 : 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name = "bc-sg-allow-https-all"
  }
}

module "logstash_vpc" {
  source                              = "../../../../utils/terraform/vpc/publicVpc"
  aws_profile                         = var.aws_profile
  region                              = var.region
  unique_tag                          = var.unique_tag
  name                                = local.logstash_name
  cidr_block                          = "192.168.0.0/16"
  subnets_cidrs_per_availability_zone = ["192.168.0.0/19", "192.168.32.0/19"]
  module_depends_on                   = [null_resource.wait_until_eni_is_not_in_use]
}

module "logstash_ecs" {
  source      = "../../../../utils/terraform/ecs"
  aws_profile = var.aws_profile
  region      = var.region
  unique_tag  = var.unique_tag
  name        = local.logstash_name
}

resource "aws_ssm_parameter" "logstash_vpc_id" {
  name  = "/logstash/vpc/id-${var.unique_tag}"
  type  = "String"
  value = module.logstash_vpc.vpc_id
}


// *********************** ALB *****************************
resource "aws_lb" "logstash_alb" {
  name               = "bc-logstash-alb-${var.unique_tag}"
  load_balancer_type = "application"
  internal           = false
  subnets            = module.logstash_vpc.vpc_public_subnet
  security_groups    = [aws_security_group.allow_https.id]
}

resource "aws_lb_listener" "logstash_https_listener" {
  load_balancer_arn = aws_lb.logstash_alb.arn
  port              = var.turbo_mode ? 80 : 443
  protocol          = var.turbo_mode ? "HTTP" : "HTTPS"
  ssl_policy        = var.turbo_mode ? null : "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.turbo_mode ? null : var.cert_arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Healthy"
      status_code  = "200"
    }
  }
}

resource "aws_ssm_parameter" "logstash_listener_port" {
  name  = "/logstash/listener/port-${var.unique_tag}"
  type  = "String"
  value = aws_lb_listener.logstash_https_listener.port
}

resource "aws_ssm_parameter" "logstash_lb" {
  name  = "/logstash/lb/arn-${var.unique_tag}"
  type  = "String"
  value = aws_lb.logstash_alb.arn
}

// Handle default logstash
resource "aws_lb_listener_rule" "default_logstash" {
  listener_arn = aws_lb_listener.logstash_https_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.default_logstash_target_group.arn
  }

  condition {
    path_pattern {
      values = ["/logstash"]
    }
  }
}

resource "aws_lb_target_group" "default_logstash_target_group" {
  name        = "bc-logstash-tg-${var.unique_tag}"
  target_type = "lambda"
}

resource "aws_lambda_permission" "alb_sg_permission" {
  statement_id  = "AllowExecutionFromlb"
  action        = "lambda:InvokeFunction"
  function_name = concat(module.logstash_producer_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  principal     = "elasticloadbalancing.amazonaws.com"
  source_arn    = aws_lb_target_group.default_logstash_target_group.arn
}

resource "aws_lb_target_group_attachment" "default_lambda_target" {
  target_group_arn = aws_lb_target_group.default_logstash_target_group.arn
  target_id        = concat(module.logstash_producer_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  depends_on       = [aws_lambda_permission.alb_sg_permission]
}

// *********************** ACCELERATOR *****************************

resource "aws_globalaccelerator_accelerator" "logstash_accelerator" {
  name            = "bc-logstash-accelerator-${var.unique_tag}"
  ip_address_type = "IPV4"
  enabled         = true

  depends_on = [module.logstash_vpc]
}

resource "aws_globalaccelerator_listener" "logstash_accelerator_listener" {
  accelerator_arn = aws_globalaccelerator_accelerator.logstash_accelerator.id
  protocol        = "TCP"
  port_range {
    from_port = var.turbo_mode ? 80 : 443
    to_port   = var.turbo_mode ? 80 : 443
  }
}

resource "aws_globalaccelerator_endpoint_group" "logstash_accelerator_endpoint" {
  listener_arn          = aws_globalaccelerator_listener.logstash_accelerator_listener.id
  health_check_protocol = var.turbo_mode ? "HTTP" : "HTTPS"
  health_check_port     = var.turbo_mode ? 80 : 443
  health_check_path     = "/"

  endpoint_configuration {
    endpoint_id = aws_lb.logstash_alb.arn
    weight      = 100
  }
}

data "external" "accelerator_dns_name" {
  program    = ["bash", "-c", "aws globalaccelerator describe-accelerator --profile ${var.aws_profile} --region ${var.region} --accelerator-arn ${aws_globalaccelerator_accelerator.logstash_accelerator.id} | jq '.Accelerator | {dnsName: .DnsName}'"]
  depends_on = [aws_globalaccelerator_accelerator.logstash_accelerator]
}

// *********************** Destroy *****************************

resource "null_resource" "wait_until_eni_is_not_in_use" {
  triggers = {
    build = timestamp()
  }
  provisioner "local-exec" {
    when = "destroy"

    command = <<BASH
#!/usr/bin/env bash

i=0
while [ $i -lt 60 ]
do
  i=$(( $i + 1 ))
  usage=0
  for i in $(aws ec2 describe-network-interfaces --profile ${var.aws_profile} --region ${var.region} --filters Name=vpc-id,Values=${aws_ssm_parameter.logstash_vpc_id.value} | jq -r .NetworkInterfaces[].Status); do
        if [ "in-use" == $i ]
        then
          usage=1
        fi
  done

  if [ $usage -gt 0 ]
  then
      echo "In use, waiting 1 more minute"
      sleep 60
      echo "Waited $i minutes"
  else
      echo "not in use"
      aws ec2 delete-security-group --profile ${var.aws_profile} --region ${var.region} --group-id $(aws ec2 describe-security-groups --profile ${var.aws_profile} --region ${var.region} --filters Name=vpc-id,Values=${aws_ssm_parameter.logstash_vpc_id.value} Name=group-name,Values=GlobalAccelerator | jq '.SecurityGroups[] | .GroupId' | tr -d '\"')
      break
  fi
done
BASH
  }
}


resource "aws_ssm_parameter" "logstash_dns" {
  name = "/logstash/dns/url-${var.unique_tag}"
  type = "String"
  value = var.turbo_mode ? "http://${data.external.accelerator_dns_name.result.dnsName}" : "https://logs.${var.domain}"
}





