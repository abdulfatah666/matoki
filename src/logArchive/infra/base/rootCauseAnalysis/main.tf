data "aws_caller_identity" "current" {}

module "consts" {
  source = "../../../../utils/terraform/consts"
}

module "dynamo_db_tables" {
  source     = "../../../../utils/terraform/dynamoDb"
  unique_tag = var.unique_tag

  tables = [
    {
      name       = "tenants_cloudformation"
      index_name = "aws_account_id"
    },
  ]
}

resource "aws_dynamodb_table" "remediation_runs_table" {
  hash_key     = "id"
  name         = format("customers_resources_stacks%s", var.unique_tag)
  billing_mode = "PAY_PER_REQUEST"

  global_secondary_index {
    hash_key        = "aws_account_id"
    name            = "aws_account_id-index"
    projection_type = "ALL"
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "aws_account_id"
    type = "S"
  }

  point_in_time_recovery {
    enabled = true
  }
}

module "root_cause_analysis_lambda" {
  source                 = "../../../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../logArchive/functions/enrichments/rootCauseAnalysis"
  variable_string        = "--customers-api-lambda \"${var.customers_api_lambda}\""
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.sls_bucket_name

  lambda_names = [
    "${module.consts.root_cause_analysis_function_name}-${var.unique_tag}",
  ]
}

locals {
  ec2_tags_function_name                = "${module.consts.ec2_tags_function_name}-${var.unique_tag}"
  kms_alias_function_name               = "${module.consts.kms_alias_function_name}-${var.unique_tag}"
  cwl_s3_to_elasticsearch_function_name = "bc-cloudwatch-logs-${var.unique_tag}"
}

module "ec2_tagname_lambda" {
  source                 = "../../../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../logArchive/functions/enrichments/ec2Tags"
  variable_string        = "--lambdaName ${local.ec2_tags_function_name} --customers-api-lambda \"${var.customers_api_lambda}\""
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.sls_bucket_name

  lambda_names = [
    local.ec2_tags_function_name,
  ]
}

module "kms_aliases_lambda" {
  source                 = "../../../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../logArchive/"
  variable_string        = "--customers-api-lambda \"${var.customers_api_lambda}\" --violations-api-lambda \"${var.violations_api_lambda}\" --cloudwatch-logs-bucket \"${var.cloudwatch_logs_bucket}\" --etl-table \"${var.etl_table}\""
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.sls_bucket_name

  lambda_names = [
    local.kms_alias_function_name,
    local.cwl_s3_to_elasticsearch_function_name
  ]
}
