variable "customer_assume_role_arn" {
  type = "string"
}

variable "monitor" {
  type = "map"

  default = {
    enable               = false
    sns_topic_error_name = ""
  }
}

variable "base_stack_unique_tag" {
  type = "string"
}

variable "customer_aws_account_id" {
  type = "string"
}

variable "customer_name" {
  type = "string"
}
