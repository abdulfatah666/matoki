data "aws_caller_identity" "current" {}

module "consts" {
  source = "../../../../utils/terraform/consts"
}

locals {
  ec2_tags_function_name            = "${module.consts.ec2_tags_function_name}-${var.base_stack_unique_tag}"
  root_cause_analysis_function_name = "${module.consts.root_cause_analysis_function_name}-${var.base_stack_unique_tag}"
  kms_alias_function_name           = "${module.consts.kms_alias_function_name}-${var.base_stack_unique_tag}"
  vpc_scanner_function_name         = "${module.consts.vpc_scanner_function_name}-${var.base_stack_unique_tag}"
  account_admins_function_name         = "${module.consts.enrichment_account_admins_function_name}-${var.base_stack_unique_tag}"
}

module "scheduleEventRuleRootCauseAnalysis" {
  source                   = "../../../../utils/terraform/scheduleEventRule"
  prefix_name              = "root-cause-analysis"
  customer_assume_role_arn = var.customer_assume_role_arn
  customer_aws_account_id  = var.customer_aws_account_id
  customer_name            = var.customer_name
  function_name            = local.root_cause_analysis_function_name
  schedule_expression      = "rate(12 hours)"
}

module "scheduleEventRuleEC2Tags" {
  source                   = "../../../../utils/terraform/scheduleEventRule"
  prefix_name              = "ec2-tags"
  customer_assume_role_arn = var.customer_assume_role_arn
  customer_aws_account_id  = var.customer_aws_account_id
  customer_name            = var.customer_name
  function_name            = local.ec2_tags_function_name
  schedule_expression      = "rate(12 hours)"
}

module "scheduleEventRuleKMSAlias" {
  source                   = "../../../../utils/terraform/scheduleEventRule"
  prefix_name              = "kms-alias"
  customer_assume_role_arn = var.customer_assume_role_arn
  customer_aws_account_id  = var.customer_aws_account_id
  customer_name            = var.customer_name
  function_name            = local.kms_alias_function_name
  schedule_expression      = "rate(12 hours)"
}
module "scheduleEventRuleVpcScannerAlias" {
  source                   = "../../../../utils/terraform/scheduleEventRule"
  prefix_name              = "vpc-scanner"
  customer_assume_role_arn = var.customer_assume_role_arn
  customer_aws_account_id  = var.customer_aws_account_id
  customer_name            = var.customer_name
  function_name            = local.vpc_scanner_function_name
  schedule_expression      = "rate(60 minutes)"
}

module "scheduleEventRuleAccountAdmins" {
  source                   = "../../../../utils/terraform/scheduleEventRule"
  prefix_name              = "account-admins"
  customer_assume_role_arn = var.customer_assume_role_arn
  customer_aws_account_id  = var.customer_aws_account_id
  customer_name            = var.customer_name
  function_name            = local.account_admins_function_name
  schedule_expression      = "rate(1 day)"
}