output "kinesis_arns" {
  value = "${aws_kinesis_stream.kinesis_stream.*.arn}"
}

output "firehose_arns" {
  value = "${aws_kinesis_firehose_delivery_stream.firehose_stream.*.arn}"
}