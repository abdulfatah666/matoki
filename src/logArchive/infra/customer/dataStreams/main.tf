data "aws_caller_identity" "current" {}

module "consts" {
  source = "../../../../utils/terraform/consts"
}


locals {
  producers_list                 = var.producers_list
  producers_count                = length(local.producers_list)
  kinesis_streams_arn_list       = aws_kinesis_stream.kinesis_stream.*.arn
  unique_tag_with_dash           = format("-%s", var.base_stack_unique_tag)
  bucket_arn                     = "arn:aws:s3:::bc-backup-${data.aws_caller_identity.current.account_id}-${var.base_stack_unique_tag}"
  suffix_name_with_dash          = var.suffix_name == "" ? "" : "-${var.suffix_name}"
  suffix_name_with_underline     = var.suffix_name == "" ? "" : "_${var.suffix_name}"
  suffix_name_with_slash         = var.suffix_name == "" ? "" : "/${var.suffix_name}"
  access_policy_list             = aws_iam_policy.access_policy.*.arn
  firehose_roles_names_list      = aws_iam_role.firehose_role.*.name
  firehose_roles_arn_list        = aws_iam_role.firehose_role.*.arn
  access_policies_documents_list = data.aws_iam_policy_document.firehose_access_policy.*.json
}

// Creating kinesis stream
resource "aws_kinesis_stream" "kinesis_stream" {
  count            = length(local.producers_list)
  name             = "bc-kds-${var.customer_name}-${element(local.producers_list, count.index)}${local.suffix_name_with_dash}"
  shard_count      = lookup(var.kinesis_info, "shards_count", 4)
  retention_period = lookup(var.kinesis_info, "retention_period", 24)
  encryption_type  = lookup(var.kinesis_info, "encryption_type", "NONE")
}

// Creating the firehose access policy document
data "aws_iam_policy_document" "firehose_access_policy" {
  count   = length(local.producers_list)
  version = "2012-10-17"

  // Grant access to the backup bucket
  statement {
    effect = "Allow"

    actions = [
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:PutObject",
    ]

    resources = [
      local.bucket_arn,
      "${local.bucket_arn}/*"
    ]
  }

  // Grant access to customer ES
  dynamic statement {
    for_each = var.es_domain_arn != "" ? [var.es_domain_arn] : []
    content {
      effect = "Allow"

      actions = [
        "es:DescribeElasticsearchDomain",
        "es:DescribeElasticsearchDomains",
        "es:DescribeElasticsearchDomainConfig",
        "es:ESHttpPost",
        "es:ESHttpPut",
      ]

      resources = [
        statement.value,
        "${statement.value}/*",
      ]
    }
  }

  dynamic statement {
    for_each = var.es_domain_arn != "" ? [var.es_domain_arn] : []
    content {
      effect = "Allow"

      actions = [
        "es:ESHttpGet",
      ]

      resources = [
        "${statement.value}/_all/_settings",
        "${statement.value}/_cluster/stats",
        "${statement.value}/test_index*/_mapping/test_index",
        "${statement.value}/es-acme-dev/_nodes",
        "${statement.value}/_nodes/stats",
        "${statement.value}/_nodes/*/stats",
        "${statement.value}/_stats",
        "${statement.value}/test_index*/_stats",
      ]
    }
  }

  // Grant access to write logs
  statement {
    effect = "Allow"

    actions = [
      "logs:PutLogEvents",
    ]

    resources = [
      "arn:aws:logs:${var.region}:${data.aws_caller_identity.current.account_id}:log-group:/aws/kinesisfirehose/fh-${var.customer_name}-${element(local.producers_list, count.index)}_generated_iam:log-stream:*",
    ]
  }

  // Grant access to kinesis
  statement {
    effect = "Allow"

    actions = [
      "kinesis:DescribeStream",
      "kinesis:GetShardIterator",
      "kinesis:GetRecords",
    ]

    resources = [
      local.kinesis_streams_arn_list[count.index],
    ]
  }
}

// Creating firehose assume role policy document
data "aws_iam_policy_document" "firehose_assume_role_policy" {
  version = "2012-10-17"

  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      identifiers = [
        "firehose.amazonaws.com",
      ]

      type = "Service"
    }

    condition {
      test = "StringEquals"

      values = [
        data.aws_caller_identity.current.account_id,
      ]

      variable = "sts:ExternalId"
    }
  }
}

// Crearing firhose IAM role
resource "aws_iam_role" "firehose_role" {
  count              = length(local.producers_list)
  name               = "bc_${var.customer_name}_${local.producers_list[count.index]}_firehose_role${local.suffix_name_with_underline}"
  assume_role_policy = data.aws_iam_policy_document.firehose_assume_role_policy.json
}

// Creating a policy resource from policy document
resource "aws_iam_policy" "access_policy" {
  count       = length(local.producers_list)
  name        = "bc_${var.customer_name}_${local.producers_list[count.index]}_firehose_access_policy${local.suffix_name_with_underline}"
  description = "The permissions bc-fh-${var.customer_name}-${local.producers_list[count.index]} gets on other environment resources"
  policy      = count.index >= length(local.access_policies_documents_list) ? "{}" : local.access_policies_documents_list[count.index]

  depends_on = [
    data.aws_iam_policy_document.firehose_access_policy
  ]
}


// Attach the firehose access policy to firehose IAM role
resource "aws_iam_role_policy_attachment" "firehose_access_attachment" {
  count      = length(local.producers_list)
  policy_arn = local.access_policy_list[count.index]
  role       = local.firehose_roles_names_list[count.index]

  depends_on = [
    aws_iam_role.firehose_role
  ]
}

// Creating firehose stream
resource "aws_kinesis_firehose_delivery_stream" "firehose_stream" {
  count = length(local.producers_list)
  name  = "bc-fh-${var.customer_name}-${local.producers_list[count.index]}${local.suffix_name_with_dash}"

  kinesis_source_configuration {
    kinesis_stream_arn = local.kinesis_streams_arn_list[count.index]
    role_arn           = local.firehose_roles_arn_list[count.index]
  }

  destination = var.es_domain_arn != "" ? "elasticsearch" : "s3"

  s3_configuration {
    role_arn    = aws_iam_role.firehose_role.*.arn[count.index]
    bucket_arn  = local.bucket_arn
    buffer_size = 10

    prefix = "fh/${var.customer_name}${local.suffix_name_with_slash}/${element(local.producers_list, count.index)}/"
  }

  dynamic elasticsearch_configuration {
    for_each = var.es_domain_arn != "" ? [var.es_domain_arn] : []
    content {
      domain_arn            = elasticsearch_configuration.value
      index_name            = "bc_${var.customer_name}_${local.producers_list[count.index]}_index"
      role_arn              = local.firehose_roles_arn_list[count.index]
      s3_backup_mode        = lookup(var.elasticsearch_configuration, "s3_backup_mode", "FailedDocumentsOnly")
      buffering_interval    = lookup(var.elasticsearch_configuration, "buffering_interval", 300)
      buffering_size        = lookup(var.elasticsearch_configuration, "buffering_size", 5)
      index_rotation_period = lookup(var.elasticsearch_configuration, "index_rotation_period", "OneWeek")
      retry_duration        = lookup(var.elasticsearch_configuration, "retry_duration", 300)
      type_name             = "bc_${var.customer_name}_${local.producers_list[count.index]}_index"
      cloudwatch_logging_options {
        enabled         = lookup(var.elasticsearch_configuration, "enable_cloudwatch_logging", false)
        log_group_name  = "/aws/kinesisfirehose/bc-fh-${var.customer_name}-${local.producers_list[count.index]}${local.suffix_name_with_dash}"
        log_stream_name = "ElasticsearchDelivery"
      }
    }
  }
}

data "aws_sns_topic" "topic_monitor_error" {
  count = var.monitor["enable"] ? 1 : 0
  name  = var.monitor["sns_topic_error_name"]
}

resource "aws_cloudwatch_metric_alarm" "app-s3-bucket-size-change" {
  count               = var.monitor["enable"] ? length(local.producers_list) : 0
  alarm_name          = "bc-app-s3-bucket-size-change-bc-fh-backup-${var.base_stack_unique_tag}/${var.customer_name}${local.suffix_name_with_slash}/${element(local.producers_list, count.index)}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "BucketSizeBytes"
  namespace           = "AWS/S3"

  dimensions = {
    StorageType = "StandardStorage"
    BucketName  = "bc-backup-${data.aws_caller_identity.current.account_id}-${var.base_stack_unique_tag}/fh/${var.customer_name}/${element(local.producers_list, count.index)}/elasticsearch-failed"
  }

  treat_missing_data = "notBreaching"
  period             = "600"
  statistic          = "Sum"
  threshold          = "100"
  alarm_description  = "This metric monitor firehose backup bucket"

  alarm_actions = data.aws_sns_topic.topic_monitor_error.*.arn
}

resource aws_cloudwatch_metric_alarm "kinesis_write_blocked" {
  count               = length(local.producers_list)
  alarm_name          = "${aws_kinesis_stream.kinesis_stream[count.index].name}_write_blocked_alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 0
  namespace           = "AWS/Kinesis"
  metric_name         = "WriteProvisionedThroughputExceeded"
  period              = "600"

  dimensions = {
    StreamName = aws_kinesis_stream.kinesis_stream[count.index].name
  }

  treat_missing_data = "notBreaching"
  statistic          = "Average"
  alarm_description  = "Stream ${aws_kinesis_stream.kinesis_stream[count.index].name} needs more shards"
  alarm_actions      = data.aws_sns_topic.topic_monitor_error.*.arn
}