variable "region" {
  type        = string
  description = "AWS provider region"
}

// Required parameters
variable "customer_name" {
  type        = "string"
  description = "customer name"
}

variable "producers_list" {
  type        = "list"
  description = "A list of the data producers: cloudtrail, lacework, etc. The producers names should be in lowercase"
}

variable "es_domain_arn" {
  type        = string
  description = "(Required) The ARN of the Amazon ES domain."
}

variable "kinesis_info" {
  type        = "map"
  description = "Map of all kinesis relevant configuration attributes"
  default     = {}
  // These are the attributes of kinesis_info object
  // All the attributes are optional.
  // Each parameter that won't be explicitly detailed in the object will be set with default value.

  // shard_count       : The number of shards that the stream will use.
  //                     Amazon has guidlines for specifying the Stream size that should be referenced when creating a
  //                     Kinesis stream. See Amazon Kinesis Streams for more.
  // retention_period  : Length of time data records are accessible after they are added to the stream.
  //                     The maximum value of a stream's retention period is 168 hours. Minimum value is 24. Default is 24.
  // encryption_type   : The encryption type to use. The only acceptable values are NONE or KMS. The default value is NONE.
  // Example:
  //   {
  //     shards_count    = 2
  //     encryption_type = "NONE"
  //   }
}

variable "elasticsearch_configuration" {
  type        = "map"
  description = "The elasticsearch configuration attributes"

  /*
  These are the elasticesearch_configuration object attributes.
  All the attributes are optional.
  Each parameter that won't be explicitly detailed in the object will be set with default value.

   buffering_interval     : Buffer incoming data for the specified period of time, in seconds between 60 to 900,
                            before delivering it to the destination. The default value is 300s.
   buffering_size         : Buffer incomin g data to the specified size, in MBs between 1 to 100,
                            before delivering it to the destination. The default value is 5MB.
   index_rotation_period  : The Elasticsearch index rotation period.
                            Index rotation appends a timestamp to the IndexName to facilitate expiration of old data.
                            Valid values are NoRotation, OneHour, OneDay, OneWeek, and OneMonth.
                            The default value is OneDay.
   retry_duration         : After an initial failure to deliver to Amazon Elasticsearch, the total amount of time,
                            in seconds between 0 to 7200, during which Firehose re-attempts delivery
                            (including the first attempt). After this time has elapsed, the failed documents are written
                            to Amazon S3. The default value is 300s. There will be no retry if the value is 0.
   s3_backup_mode         : Defines how documents should be delivered to Amazon S3.
                            Valid values are FailedDocumentsOnly and AllDocuments. Default value is FailedDocumentsOnly.
   cloudwatch_logging_options : The CloudWatch Logging Options for the delivery stream. More details are given below
  */

  default = {
    buffering_interval        = "300"
    buffering_size            = "5"
    index_rotation_period     = "OneWeek"
    retry_duration            = "7200"
    s3_backup_mode            = "AllDocuments"
    enable_cloudwatch_logging = true
  }
}

variable "monitor" {
  type = "map"

  default = {
    enable               = false
    sns_topic_error_name = ""
  }
}

variable "base_stack_unique_tag" {
}

variable "suffix_name" {
  default = ""
  description = "suffix name"
}
