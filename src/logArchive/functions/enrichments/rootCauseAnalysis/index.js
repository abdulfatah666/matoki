/* eslint-disable no-restricted-syntax,no-param-reassign */
const AWS = require('aws-sdk');
const crypto = require('crypto');
const convertCloudFormationResourceToARN = require('./convert_resources');

const BAD_REGIONS = ['ap-east-1', 'me-south-1'];

const STACK_STATUS_FILTER = [
    'CREATE_COMPLETE',
    'UPDATE_COMPLETE',
    'ROLLBACK_COMPLETE',
    'UPDATE_ROLLBACK_COMPLETE'
];

class CrossAccountRoleNotExist extends Error {
    constructor(message) {
        super(message);
        // Ensure the name of this error is the same as the class name
        this.name = this.constructor.name;
        // This clips the constructor invocation from the stack trace.
        // It's not absolutely essential, but it does make the stack trace a little nicer.
        //  @see Node.js reference (bottom)
        Error.captureStackTrace(this, this.constructor);
    }
}

function writeStackResourceToDynamoDB(key, data) {
    const docClient = new AWS.DynamoDB.DocumentClient();
    const UpdateExpression = Object.keys(data)
        .reduce((acc, curr, index) => {
            acc += ` ${curr} = :${curr}`;
            if (index !== Object.keys(data).length - 1) {
                acc += ',';
            }
            return acc;
        }, 'set');

    const ExpressionAttributeValues = Object.keys(data)
        .reduce((acc, curr) => {
            acc[`:${curr}`] = data[curr];
            return acc;
        }, {});

    return docClient.update({
        TableName: process.env.TENANTS_CLOUD_FORMATION_TABLE,
        Key: { aws_account_id: key },
        UpdateExpression,
        ExpressionAttributeValues: JSON.parse(JSON.stringify(ExpressionAttributeValues))
    })
        .promise();
}

function getSTSRoleAccessTokenByTenant(tenant) {
    if (!tenant.cross_account_cf_details.cross_account_role_arn) {
        return Promise.reject(new CrossAccountRoleNotExist('cross account role arn does not exist'));
    }

    const sts = new AWS.STS();

    // cross account role
    return sts.assumeRole({
        RoleArn: tenant.cross_account_cf_details.cross_account_role_arn,
        RoleSessionName: `readCloudFormationStacks_${tenant.customer_name}_${tenant.aws_account_id}_session`,
        ExternalId: tenant.cloud_trail_cf_details.external_id
    })
        .promise()
        .then((data) => ({
            accessKeyId: data.Credentials.AccessKeyId,
            secretAccessKey: data.Credentials.SecretAccessKey,
            sessionToken: data.Credentials.SessionToken
        }))
        .catch((err) => {
            if (err.errorType === 'AccessDenied') {
                throw new Error(`Verify the sts:AssumeRole or the cross account role arn:aws:iam::${tenant.aws_account_id}:role/${tenant.customer_name}-bc-bridgecrewcwssarole, message: ${err.message}, stack: ${err.stack}`);
            } else {
                throw err;
            }
        });
}

const getTenantEnvDetails = async (awsAccountId) => {
    const tenantEnvDetailsParams = {
        FunctionName: process.env.CUSTOMERS_API_LAMBDA,
        Payload: JSON.stringify({
            body: {
                awsAccountId
            },
            headers: {
                'Content-Type': 'application/json'
            },
            path: '/invoke/getTenantEnvironmentDetails'
        })
    };

    try {
        const tenantEnvDetailsRes = await new AWS.Lambda().invoke(tenantEnvDetailsParams)
            .promise();
        return JSON.parse(JSON.parse(tenantEnvDetailsRes.Payload).body);
    } catch (e) {
        throw new Error(e);
    }
};

const addSyncedStack = async (accountID, stack) => {
    const docClient = new AWS.DynamoDB.DocumentClient();
    const id = `${accountID}::${crypto.createHash('md5')
        .update(stack.StackId)
        .digest('hex')}`;
    return docClient.put({
        TableName: process.env.CUSTOMERS_RESOURCES_STACKS,
        Item: JSON.parse(JSON.stringify({
            aws_account_id: accountID,
            ...stack,
            id
        }))
    })
        .promise();
};

const getSyncedStacks = async (awsAccountId) => {
    const docClient = new AWS.DynamoDB.DocumentClient();

    const expressionAttributeValues = {
        ':aws_account_id': awsAccountId
    };
    const dynamoQuery = {
        TableName: process.env.CUSTOMERS_RESOURCES_STACKS,
        IndexName: 'aws_account_id-index',
        KeyConditionExpression: 'aws_account_id = :aws_account_id',
        ExpressionAttributeValues: expressionAttributeValues

    };
    return docClient.query(dynamoQuery)
        .promise()
        .then((res) => res.Items);
};

const splitToChunks = (arr, chunkSize) => {
    const ans = [];
    for (let i = 0; i < arr.length; i += chunkSize) {
        ans.push(arr.slice(i, i + chunkSize));
    }
    return ans;
};

async function getCloudFormation(stsAccessToken, region, tenant, syncedStacks) {
    const cloudFormation = new AWS.CloudFormation({
        ...stsAccessToken,
        region
    });
    // get list of stacks
    const stacks = await cloudFormation.listStacks({ StackStatusFilter: STACK_STATUS_FILTER })
        .promise();

    console.log(`received ${stacks.StackSummaries.length} stacks, region: ${region}`);

    const stackBatches = splitToChunks(stacks.StackSummaries, 2);
    for (const batch of stackBatches) {
        /* eslint-disable-next-line no-await-in-loop */
        await Promise.all(batch.map(async (stack) => {
            const findStack = syncedStacks.find((syncedStack) => syncedStack.StackId === stack.StackId);
            if (!findStack
                || (stack.LastUpdatedTime
                    && !findStack.LastUpdatedTime)
                || (stack.LastUpdatedTime
                    && findStack.LastUpdatedTime
                    && new Date(findStack.LastUpdatedTime).getTime() < stack.LastUpdatedTime.getTime())
                || (stack.CreationTime
                    && findStack.CreationTime
                    && new Date(findStack.CreationTime).getTime() !== stack.CreationTime.getTime())) {
                const resources = await cloudFormation.listStackResources({ StackName: stack.StackName })
                    .promise();

                await Promise.all(resources.StackResourceSummaries.map(async (resource) => {
                    if (resource.PhysicalResourceId) {
                        const resourceARN = convertCloudFormationResourceToARN(resource, region, tenant.aws_account_id);
                        const data = {
                            resourceArn: resourceARN,
                            resourceId: resource.PhysicalResourceId,
                            resourceDetails: resource,
                            lastUpdate: new Date(),
                            stack
                        };

                        return writeStackResourceToDynamoDB(`${tenant.aws_account_id}:${crypto.createHash('md5')
                            .update(resourceARN)
                            .digest('hex')}`, data)
                            .then(() => data);
                    }
                    return Promise.resolve({});
                }))
                    .then((data) => {
                        console.log(`Stack Name: ${stack.StackName}, ${JSON.stringify(data)}`);
                        return data;
                    });
                await new Promise((resolve) => setTimeout(resolve, 4));

                return addSyncedStack(tenant.aws_account_id, stack);
            }
            console.log(`Stack Name: ${stack.StackName}, doesn't change`);
            return Promise.resolve({});
        }));
    }
}

exports.handler = async (event) => {
    try {
        const tenant = await getTenantEnvDetails(event.accountId);
        console.log(`Account ID: ${event.accountId}, Customer Name: ${tenant.customer_name}`);
        const stsAccessToken = await getSTSRoleAccessTokenByTenant(tenant);
        const syncedStacks = await getSyncedStacks(event.accountId);
        const ec2 = new AWS.EC2(stsAccessToken);
        const regions = await ec2.describeRegions().promise()
            .then((response) => response.Regions.map((reg) => reg.RegionName).filter((regionName) => !BAD_REGIONS.includes(regionName)));

        return await regions.reduce(async (acc, curr) => {
            await acc;
            return getCloudFormation(stsAccessToken, curr, tenant, syncedStacks);
        }, Promise.resolve());
    } catch (err) {
        if (err instanceof CrossAccountRoleNotExist) {
            console.log(err.message);
            return Promise.resolve();
        }
        throw err;
    }
};