const ARN_CONVERT = {
    'AWS::EC2::Instance': ({ id, region, account }) => `arn:aws:ec2:${region}:${account}:instance/${id}`,
    'AWS::EC2::SecurityGroup': ({ id, region, account }) => `arn:aws:ec2:${region}:${account}:security-group/${id}`,
    'AWS::EC2::Subnet': ({ id, region, account }) => `arn:aws:ec2:${region}:${account}:subnet/${id}`,
    'AWS::EC2::VPC': ({ id, region, account }) => `arn:aws:ec2:${region}:${account}:vpc/${id}`,
    'AWS::EC2::Volume': ({ id, region, account }) => `arn:aws:ec2:${region}:${account}:volume/${id}`,
    'AWS::EC2::RouteTable': ({ id, region, account }) => `arn:aws:ec2:${region}:${account}:route-table/${id}`,
    'AWS::S3::Bucket': ({ id }) => `arn:aws:s3:::${id}`,
    'AWS::SNS::Topic': ({ id, region, account }) => `arn:aws:sns:${region}:${account}:${id}`,
    'AWS::SQS::Queue': ({ id, region, account }) => `arn:aws:sqs:${region}:${account}:${id.split('/').slice(-1)[0]}`,
    'AWS::DynamoDB::Table': ({ id, region, account }) => `arn:aws:dynamodb:${region}:${account}:table/${id}`,
    'AWS::IAM::Role': ({ id, account }) => `arn:aws:iam::${account}:role/${id}`
};

function convertCloudFormationResourceToARN(resource, region, account) {
    if (resource.PhysicalResourceId.startsWith('arn:aws')) return resource.PhysicalResourceId;
    return (ARN_CONVERT[resource.ResourceType] ? ARN_CONVERT[resource.ResourceType]({ id: resource.PhysicalResourceId, region, account }) : resource.PhysicalResourceId);
}

module.exports = convertCloudFormationResourceToARN;
