/* eslint-disable no-param-reassign */
const AWS = require('aws-sdk');
const crypto = require('crypto');

const BAD_REGIONS = ['ap-east-1', 'me-south-1'];

function writeStackResourceToDynamoDB(key, data) {
    const docClient = new AWS.DynamoDB.DocumentClient();
    const UpdateExpression = Object.keys(data).reduce((acc, curr, index) => {
        acc += ` ${curr} = :${curr}`;
        if (index !== Object.keys(data).length - 1) {
            acc += ',';
        }
        return acc;
    }, 'set');

    const ExpressionAttributeValues = Object.keys(data).reduce((acc, curr) => {
        acc[`:${curr}`] = data[curr];
        return acc;
    }, {});

    return docClient.update({
        TableName: process.env.TENANTS_CLOUD_FORMATION_TABLE,
        Key: { aws_account_id: key },
        UpdateExpression,
        ExpressionAttributeValues: JSON.parse(JSON.stringify(ExpressionAttributeValues))
    }).promise();
}

async function listAliasesAndUpdateDynamo(sts, tenantAccountId) {
    const regions = await new AWS.EC2(sts).describeRegions().promise()
        .then((response) => response.Regions.map((reg) => reg.RegionName).filter((regionName) => !BAD_REGIONS.includes(regionName)));
    // iterating all regions to find kms
    for (let i = 0; i < regions.length; i++) {
        const kms = new AWS.KMS({ ...sts, region: regions[i] });
        const aliasesResp = await kms.listAliases().promise();
        // filter out aws managed kms
        const filteredAliases = aliasesResp.Aliases.filter((alias) => (alias.AliasArn.split('/')[1] !== 'aws'));
        // iterating relevant aliases process objects and update db
        for (let k = 0; k < filteredAliases.length; k++) {
            const keyMetadata = await kms.describeKey({ KeyId: filteredAliases[k].TargetKeyId }).promise();
            const key = `${tenantAccountId}:${crypto.createHash('md5').update(keyMetadata.KeyMetadata.Arn).digest('hex')}`;
            const value = {
                resourceArn: keyMetadata.KeyMetadata.Arn,
                resourceId: keyMetadata.KeyMetadata.Arn.split('/').slice(-1)[0],
                tags: [{ Key: 'Name', Value: filteredAliases[k].AliasName }],
                lastUpdate: new Date()
            };
            /**
             todo: need to make it generic (to append tags and not overwrite them)
             this method overrides the tags column in the dynamo table with the new kms alias values.
             which for as for now is not that critic since this lambda working on kms's and ec2Tags lambda works on ec2,
             so we end up editing different key values in the table and won't override each other data.
             * */
            console.log('updating: ', key, ' , ', value);
            await writeStackResourceToDynamoDB(key, value);
        }
    }
}

async function getTenantEnvDetails(awsAccountId) {
    const tenantEnvDetailsParams = {
        FunctionName: process.env.CUSTOMERS_API_LAMBDA,
        Payload: JSON.stringify({
            body: { awsAccountId },
            headers: { 'Content-Type': 'application/json' },
            path: '/invoke/getTenantEnvironmentDetails'
        })
    };

    try {
        const tenantEnvDetailsRes = await new AWS.Lambda().invoke(tenantEnvDetailsParams).promise();
        return JSON.parse(JSON.parse(tenantEnvDetailsRes.Payload).body);
    } catch (e) {
        console.error(e);
        return null;
    }
}

async function getSTSRoleAccessTokenByTenant(tenant) {
    const sts = new AWS.STS();
    const role = {
        RoleArn: tenant.cross_account_cf_details.cross_account_role_arn,
        RoleSessionName: `readKMSAliases_${tenant.customer_name}_${tenant.aws_account_id}_session`,
        ExternalId: tenant.cloud_trail_cf_details.external_id
    };
    return sts.assumeRole(role).promise().then((data) => ({
        accessKeyId: data.Credentials.AccessKeyId,
        secretAccessKey: data.Credentials.SecretAccessKey,
        sessionToken: data.Credentials.SessionToken
    }));
}

module.exports.handler = async (event) => {
    const tenant = await getTenantEnvDetails(event.accountId);
    const sts = await getSTSRoleAccessTokenByTenant(tenant);
    await listAliasesAndUpdateDynamo(sts, tenant.aws_account_id);
};
