/* eslint-disable no-param-reassign,no-extend-native */
const AWS = require('aws-sdk');
const crypto = require('crypto');

const BAD_REGIONS = ['ap-east-1', 'me-south-1'];

class CrossAccountRoleNotExist extends Error {
    constructor(message) {
        super(message);
        // Ensure the name of this error is the same as the class name
        this.name = this.constructor.name;
        // This clips the constructor invocation from the stack trace.
        // It's not absolutely essential, but it does make the stack trace a little nicer.
        //  @see Node.js reference (bottom)
        Error.captureStackTrace(this, this.constructor);
    }
}

function writeStackResourceToDynamoDB(key, data) {
    const docClient = new AWS.DynamoDB.DocumentClient();
    const UpdateExpression = Object.keys(data).reduce((acc, curr, index) => {
        acc += ` ${curr} = :${curr}`;
        if (index !== Object.keys(data).length - 1) {
            acc += ',';
        }
        return acc;
    }, 'set');

    const ExpressionAttributeValues = Object.keys(data).reduce((acc, curr) => {
        // Tags sometimes have empty values, and that breaks DynamoDB.
        const value = curr !== 'tags' ? data[curr] : data[curr].map(tag => ({ Key: tag.Key, Value: tag.Value === '' ? ' ' : tag.Value }));
        acc[`:${curr}`] = value;
        return acc;
    }, {});

    return docClient.update({
        TableName: process.env.TENANTS_CLOUD_FORMATION_TABLE,
        Key: { aws_account_id: key },
        UpdateExpression,
        ExpressionAttributeValues: JSON.parse(JSON.stringify(ExpressionAttributeValues))
    }).promise();
}

function getSTSRoleAccessTokenByTenant(tenant) {
    if (!tenant.cross_account_cf_details.cross_account_role_arn) {
        return Promise.reject(new CrossAccountRoleNotExist('cross account role arn does not exist'));
    }

    const sts = new AWS.STS();

    // cross account role
    return sts.assumeRole({
        RoleArn: tenant.cross_account_cf_details.cross_account_role_arn,
        RoleSessionName: `readCloudFormationStacks_${tenant.customer_name}_${tenant.aws_account_id}_session`,
        ExternalId: tenant.cloud_trail_cf_details.external_id
    }).promise().then(data => [{
        accessKeyId: data.Credentials.AccessKeyId,
        secretAccessKey: data.Credentials.SecretAccessKey,
        sessionToken: data.Credentials.SessionToken
    }, tenant]).catch((err) => {
        if (err.errorType === 'AccessDenied') {
            throw new Error(`Verify the sts:AssumeRole or the cross account role arn:aws:iam::${tenant.aws_account_id}:role/${tenant.customer_name}-bc-bridgecrewcwssarole, message: ${err.message}, stack: ${err.stack}`);
        } else {
            throw err;
        }
    });
}

const getTenantEnvDetails = async (awsAccountId) => {
    const tenantEnvDetailsParams = {
        FunctionName: process.env.CUSTOMERS_API_LAMBDA,
        Payload: JSON.stringify({
            body: {
                awsAccountId
            },
            headers: {
                'Content-Type': 'application/json'
            },
            path: '/invoke/getTenantEnvironmentDetails'
        })
    };

    try {
        const tenantEnvDetailsRes = await new AWS.Lambda().invoke(tenantEnvDetailsParams).promise();
        return JSON.parse(JSON.parse(tenantEnvDetailsRes.Payload).body);
    } catch (e) {
        throw new Error(e);
    }
};

function describeSecurityGroups(stsAccessToken, region, tenant) {
    const ec2 = new AWS.EC2({ ...stsAccessToken, region });
    return ec2.describeSecurityGroups().promise().then((res) => Promise.all(res.SecurityGroups.map((securityGroup) => {
        securityGroup.arn = `arn:aws:ec2:${region}:${tenant.aws_account_id}:security-group/${securityGroup.GroupId}`;
        const key = `${tenant.aws_account_id}:${crypto.createHash('md5').update(securityGroup.arn).digest('hex')}`;

        const resource = {
            resourceArn: securityGroup.arn,
            resourceId: securityGroup.GroupId,
            tags: securityGroup.Tags,
            lastUpdate: new Date()
        };

        return writeStackResourceToDynamoDB(key, resource).then(() => {
            console.log(resource);
            return {
                ...resource,
                key
            };
        });
    })));
}

function describeInstances(stsAccessToken, region, tenant) {
    const ec2 = new AWS.EC2({ ...stsAccessToken, region });
    return ec2.describeInstances().promise().then((res) => Promise.all(res.Reservations.map((reservation) => Promise.all(reservation.Instances.map((instance) => {
        // TODO: multi-region
        instance.arn = `arn:aws:ec2:${region}:${tenant.aws_account_id}:instance/${instance.InstanceId}`;
        const key = `${tenant.aws_account_id}:${crypto.createHash('md5').update(instance.arn).digest('hex')}`;
        const resource = {
            resourceArn: instance.arn,
            resourceId: instance.InstanceId,
            tags: instance.Tags,
            reservationGroups: reservation.Groups
        };

        return writeStackResourceToDynamoDB(key, resource).then(() => {
            console.log(resource);
            return {
                ...resource,
                key
            };
        });
    })))));
}

function flatten(arr) {
    return arr.reduce((flat, toFlatten) => flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten), []);
}

exports.handler = async (event) => getTenantEnvDetails(event.accountId).then(getSTSRoleAccessTokenByTenant).then(async ([stsAccessToken, tenant]) => {
    const ec2 = new AWS.EC2(stsAccessToken);
    // No customer uses this region, and it requires a different set of credentials
    const regions = await ec2.describeRegions().promise()
        .then(response => response.Regions.map(reg => reg.RegionName).filter((regionName) => !BAD_REGIONS.includes(regionName)));
    let responseSG = [];
    let responseInstance = [];
    const res = await regions.reduce(async (acc, curr) => {
        const resp = await acc;
        if (resp) {
            responseSG = responseSG.concat(flatten(resp[0]));
            responseInstance = responseInstance.concat(flatten(resp[1]));
        }
        return Promise.all([
            describeSecurityGroups(stsAccessToken, curr, tenant),
            describeInstances(stsAccessToken, curr, tenant)
        ]);
    }, Promise.resolve());

    if (res) {
        responseSG = responseSG.concat(flatten(res[0]));
        responseInstance = responseInstance.concat(flatten(res[1]));
    }

    return { responseSG, responseInstance };
}).catch(err => {
    if (err instanceof CrossAccountRoleNotExist) {
        console.log(err.message);
        return Promise.resolve();
    }
    throw err;
});

Array.prototype.splitToChunks = function (chunkSize) {
    const ans = [];
    for (let i = 0; i < this.length; i += chunkSize) {
        ans.push(this.slice(i, i + chunkSize));
    }
    return ans;
};
