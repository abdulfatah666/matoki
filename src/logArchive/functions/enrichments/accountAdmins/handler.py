import json
import logging
import os

import boto3
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth

from utilsPython.es.domains import get_elastic_endpoint
from utilsPython.iam.account_admins import get_all_account_admins
from utilsPython.remote_lambda.invoke import lambda_invoke


customers_api_lambda_name = os.environ.get('CUSTOMERS_API_LAMBDA_NAME', "")
CLOUDTRAIL_INDEX = "5cb87170-6f27-11e9-beee-e3bd9c9d6b0a"

SEARCH_NAME = "aws-account-admins"

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))

session = boto3.session.Session()


def get_kibana_search_object(account_admins):
    """
    :param account_admins: list of user identity arn's
    :return:  json of Kibana search object, with updated query that filters userIdentity.arn.keyword by account ids
    :rtype: object
    """
    account_admins = ["userIdentity.arn.keyword: " + "\"" + admin + "\"" for admin in account_admins]
    search_query = " OR ".join(account_admins)
    search_query = search_query + " OR userIdentity.arn.keyword: *Admin*"
    searchSourceJSON = json.dumps(
        {"index": CLOUDTRAIL_INDEX, "highlightAll": True, "version": True,
         "query": {"language": "lucene", "query": search_query}, "filter": []})
    return {
        "type": "search",
        "updated_at": "2019-05-27T21:02:51.430Z",
        "search": {
            "title": SEARCH_NAME,
            "description": "",
            "hits": 0,
            "columns": [
                "_source"
            ],
            "sort": [
                "eventTime",
                "desc"
            ],
            "version": 1,
            "kibanaSavedObjectMeta": {
                "searchSourceJSON": searchSourceJSON
            }
        }
    }


def handle(event, context):
    """

    :param event: a scheduled cloudwatch event rule with constant JSON text i.e. {"accountId": "11111"}
    where "11111" is a customer aws account id :param context:
    """
    if event:
        logger.debug('got event={}'.format(event))
        try:
            account_id = event["accountId"]

            tenant_details = lambda_invoke(lambda_function_name = customers_api_lambda_name, name = "getTenantEnvironmentDetails", body = json.dumps({"awsAccountId": account_id}))
            customer_name = tenant_details["customer_name"]

            account_admins = get_customer_account_admins_arns(account_id, customer_name, tenant_details)
            update_customer_account_admins_in_es(account_admins, customer_name)

        except Exception as e:
            logger.error("An error occurred for event={} ".format(event), event)
            raise e


def update_customer_account_admins_in_es(account_admins, customer_name):
    data = get_kibana_search_object(account_admins)
    credentials = session.get_credentials()
    awsauth = AWS4Auth(credentials.access_key,
                       credentials.secret_key,
                       session.region_name, 'es',
                       session_token=credentials.token)
    endpoint = get_elastic_endpoint(customer_name)
    if not endpoint:
        # no elasticsearch endpoint exists for customer
        return
    es = Elasticsearch(
        hosts=[{'host': endpoint, 'port': 443}],
        http_auth=awsauth,
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection
    )
    es.index(index='.kibana', id="search:{}".format(SEARCH_NAME), doc_type='doc', body=data)


def get_customer_account_admins_arns(account_id, customer_name, tenant_details):
    """

    :param account_id: aws account id (i.e. 1111)
    :param customer_name: customer name (i.e. acme) - used to identify elasticsearch endpoint
    :param tenant_details: object containing details on customer environment resources. - used to get cross account role
            identifier
    :return: list of user identities ARN's of customer environment aws administrators
    """
    sts_client = boto3.client('sts')
    assumed_role_object = sts_client.assume_role(
        # todo: instead of string replace, refactor table to have persistence of cross account role arn
        RoleArn=tenant_details["cross_account_cf_details"]["cross_account_role_arn"],
        ExternalId=tenant_details["cloud_trail_cf_details"]["external_id"],
        RoleSessionName="{}_session".format(customer_name)
    )
    credentials = assumed_role_object['Credentials']
    iam_client = boto3.client('iam', aws_access_key_id=credentials['AccessKeyId'],
                              aws_secret_access_key=credentials['SecretAccessKey'],
                              aws_session_token=credentials['SessionToken']
                              )
    account_admins = get_all_account_admins(account_id, iam_client)
    return account_admins
