const AWS = require('aws-sdk');
const VpcScanner = require('./vpcScanner');

const BAD_REGIONS = ['ap-east-1', 'me-south-1'];

// invoke lambdas
async function getTenantEnvDetails(awsAccountId) {
    const tenantEnvDetailsParams = {
        FunctionName: process.env.CUSTOMERS_API_LAMBDA,
        Payload: JSON.stringify({
            body: {
                awsAccountId
            },
            headers: {
                'Content-Type': 'application/json',
                'throw-error': true
            },
            path: '/invoke/getTenantEnvironmentDetails'
        })
    };

    try {
        const tenantEnvDetailsRes = await new AWS.Lambda().invoke(tenantEnvDetailsParams).promise();
        return JSON.parse(JSON.parse(tenantEnvDetailsRes.Payload).body);
    } catch (e) {
        throw new Error(e);
    }
}

async function saveViolation(violationId, violationDetails, createdBy) {
    const createViolationParams = {
        FunctionName: process.env.VIOLATIONS_API_LAMBDA,
        Payload: JSON.stringify({
            body: {
                violationId,
                violationDetails,
                createdBy
            },
            headers: {
                'Content-Type': 'application/json'
            },
            path: '/invoke/violations/createViolation'
        })
    };
    console.log(JSON.stringify(createViolationParams));
    const response = await new AWS.Lambda().invoke(createViolationParams).promise();
    console.log(response);
}

class CrossAccountRoleNotExist extends Error {
    constructor(message) {
        super(message);
        // Ensure the name of this error is the same as the class name
        this.name = this.constructor.name;
        // This clips the constructor invocation from the stack trace.
        // It's not absolutely essential, but it does make the stack trace a little nicer.
        //  @see Node.js reference (bottom)
        Error.captureStackTrace(this, this.constructor);
    }
}

async function getSTSRoleAccessTokenByTenant(tenant) {
    if (!tenant.cross_account_cf_details.cross_account_role_arn) {
        return Promise.reject(new CrossAccountRoleNotExist('cross account role arn does not exist'));
    }

    const sts = new AWS.STS();

    // cross account role
    return sts.assumeRole({
        RoleArn: tenant.cross_account_cf_details.cross_account_role_arn,
        RoleSessionName: `readVpc_${tenant.customer_name}_${tenant.aws_account_id}_session`,
        ExternalId: tenant.cloud_trail_cf_details.external_id
    }).promise().then((data) => ({
        accessKeyId: data.Credentials.AccessKeyId,
        secretAccessKey: data.Credentials.SecretAccessKey,
        sessionToken: data.Credentials.SessionToken
    })).catch((err) => {
        if (err.errorType === 'AccessDenied') {
            throw new Error(`Verify the sts:AssumeRole or the cross account role arn:aws:iam::${tenant.aws_account_id}:role/${tenant.customer_name}-bc-bridgecrewcwssarole, message: ${err.message}, stack: ${err.stack}`);
        } else {
            throw err;
        }
    });
}

// todo : move to auth part
async function getCustomerStsAccessToken(event) {
    const tenant = await getTenantEnvDetails(event.accountId);
    console.log(`Account ID: ${event.accountId}, Customer Name: ${tenant.customer_name}`);
    return await getSTSRoleAccessTokenByTenant(tenant);
}

async function getAllRegions(stsAccessToken) {
    try {
        const ec2 = new AWS.EC2({
            ...stsAccessToken
        });
        const resp = await ec2.describeRegions().promise();
        const regions = resp.Regions;
        return regions.map((region) => region.RegionName).filter((regionName) => !BAD_REGIONS.includes(regionName));
    } catch (err) {
        console.error(`Failed to describe regions - ${err.message}`);
        throw err;
    }
}

module.exports.handler = async (event) => {
    const stsAccessToken = await getCustomerStsAccessToken(event);
    const regions = await getAllRegions(stsAccessToken);

    const unusedDefaultVpcs = await Promise.all(regions.map(async (region) => {
        const ec2 = new AWS.EC2({ ...stsAccessToken, region });
        const vpcScanner = new VpcScanner(ec2);
        const unusedDefaultVpc = await vpcScanner.findUnusedDefaultVpc().catch(e => {
            console.error(`Error for region ${region}`, e);
            throw e;
        });
        if (unusedDefaultVpc) {
            unusedDefaultVpc.region = region;
        }
        return unusedDefaultVpc;
    }));
    const filterUnusedDefaultVpcs = unusedDefaultVpcs.filter((vpc) => vpc);
    console.log(`The unused default vpcs are: ${JSON.stringify(filterUnusedDefaultVpcs)}`);

    const violationId = 'BC_AWS_NETWORKING_27';
    const violationDetails = {
        aws_account_id: event.accountId,
        resources: filterUnusedDefaultVpcs.map((vpc) => ({ arn: `arn:aws:ec2:${vpc.region}:${event.accountId}:vpc/${vpc.VpcId}` }))
    };

    await saveViolation(violationId, violationDetails, 'BC-AWS-SCANNER-3');
    return filterUnusedDefaultVpcs;
};