class VpcScanner {
    constructor(ec2) {
        this.ec2 = ec2;
    }

    async scanDefaultVpc() {
        try {
            const resp = await this.ec2.describeVpcs().promise();
            const vpcs = resp.Vpcs;
            return vpcs.filter((vpc) => vpc.IsDefault === true);
        } catch (err) {
            console.error(`Failed to describe VPC ${err.message}`);
            throw err;
        }
    }

    async isVpcUnused(vpcId) {
        try {
            const params = {
                Filters: [
                    {
                        Name: 'vpc-id',
                        Values: [vpcId]
                    }
                ]
            };
            const resp = await this.ec2.describeNetworkInterfaces(params).promise();
            const enis = resp.NetworkInterfaces;
            return (enis.length === 0);
        } catch (err) {
            console.error(`Failed to describe network interfaces (ENI) for vpc ${vpcId} ${err.message}`);
            throw err;
        }
    }

    async findUnusedDefaultVpc() {
        const defaultVpcs = await this.scanDefaultVpc();
        const defaultUnusedVpc = defaultVpcs.length > 0 ? await this.isVpcUnused(defaultVpcs[0].VpcId) : false;
        if (defaultUnusedVpc) {
            return defaultVpcs[0];
        }
        return null;
    }
}

module.exports = VpcScanner;
