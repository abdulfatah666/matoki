const AWS = require('aws-sdk');
const RemoteLambda = require('@bridgecrew/nodeUtils/remoteLambda/invoke');

const customerApiRemoteLambda = new RemoteLambda(process.env.CUSTOMERS_API_LAMBDA);
const { NotFoundError, UnauthorizedError } = require('@bridgecrew/nodeUtils/errors/http/index');

const { RESULT_BUCKET } = process.env;
const s3 = new AWS.S3();

// These AWS accounts are used by AWS, for logging purposes. They do not pose a security threat, and so are filtered out
const AWS_ACCOUNTS = [
    // ELB logs
    '127311923021', '033677994240', '027434742980', '797873946194', '985666609251', '054676820928', '156460612806', '652711504416', '009996457667', '582318560864',
    '600734575887', '383597477331', '114774131450', '783225319266', '718504428378', '507241528517', '048591011584', '638102146993', '037604701340',
    // Redshift logs
    '193672423079', '391106570357', '262260360010', '902366379725', '865932855811', '760740231472', '361669875840', '762762565011', '404641285394', '660998842044',
    '907379612154', '053454850223', '210876761215', '307160386991', '915173422425', '075028567923',
    // Cloudfront logs
    'cloudfront',
    // Billing
    '386209384616',
    // CloudTrail
    '475085895292', '086441151436', '388731089494', '113285607260', '119688915426', '819402241893', '977081816279', '765225791966', '492519147666', '903692715234',
    '284668455005', '216624486486', '193415116832', '681348832753', '035351147821', '829690693026', '859597730677', '282025262664', '262312530599', '034638983726',
    '886388586500', '608710470296', '814480443879'
];

const getGraphData = async (awsAccountId, userDetails) => {
    const customerName = await customerApiRemoteLambda.invoke('getCustomerName', { awsAccountId });

    if (!userDetails.customers.includes(customerName)) {
        throw new UnauthorizedError('Unauthorized');
    }

    return s3.getObject({ Bucket: `${RESULT_BUCKET}`, Key: `cloudmapper/${customerName}/${awsAccountId}/networkGraph.json` }).promise()
        .then((resp) => resp.Body.toString())
        .catch((e) => {
            console.error(e);
            throw new NotFoundError(`No Graph data found for ${awsAccountId}`);
        });
};

/**
 * Removes nodes which do not add more information
 *
 * 'cloudfront' nodes - are set to the cloudfront from the same account as the bucket owner, which is the account being scanned.
 *
 * @param {object} node - the node to be potentially added
 * @return {boolean} - whether the node should be added
 */
function removeUnnecessaryNodes(node) {
    return !node.data.source || !AWS_ACCOUNTS.includes(node.data.source);
}

const getCrossAccountAccessData = async (awsAccountId, userDetails) => {
    const customerName = await customerApiRemoteLambda.invoke('getCustomerName', { awsAccountId });

    if (!userDetails.customers.includes(customerName)) {
        throw new UnauthorizedError('Unauthorized');
    }

    return s3.getObject({ Bucket: `${RESULT_BUCKET}`, Key: `cloudmapper/${customerName}/${awsAccountId}/wot.json` }).promise()
        .then((resp) => JSON.stringify(JSON.parse(resp.Body.toString()).filter(removeUnnecessaryNodes)))
        .catch((e) => {
            console.error(e);
            throw new NotFoundError(`No Cross-Account access data found for ${awsAccountId}`);
        });
};

const isDataAvailableForAccount = async (account, userDetails) => {
    const crossAccountGraphResult = await getCrossAccountAccessData(account, userDetails).catch((e) => {
        console.log('no data for account ', account);
        console.error(e);
        return null;
    });

    if (crossAccountGraphResult) {
        return true;
    }

    // Try to fetch the network graph only if no results fount for the cross account graph,
    // in order to prevent unnecessary calls
    const networkGraphResult = await getGraphData(account, userDetails).catch((e) => {
        console.log('no data for acccount ', account);
        console.error(e);
        return null;
    });

    return !!networkGraphResult;
};

const isGraphDataAvailable = async (userDetails) => {
    const customerAccounts = await customerApiRemoteLambda.invoke('getCustomerAccountIdsByName', { name: userDetails.customers[0] });
    console.log('this is the customers accounts result ', customerAccounts);

    if (customerAccounts && customerAccounts.length > 0) {
        const resultArray = await Promise.all(customerAccounts.map(account => isDataAvailableForAccount(account, userDetails)));
        return (resultArray.includes(true));
    }

    console.log('No aws accounts are integrated for customer ', userDetails.customers[0]);
    return false;
};

module.exports = {
    getGraphData,
    getCrossAccountAccessData,
    isGraphDataAvailable
};
