const serverless = require('serverless-http');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');

const app = express();
const { HttpError, InternalServerError } = require('@bridgecrew/nodeUtils/errors/http');
const CloudmapperController = require('./CloudmapperController');

app.use(compression());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    if (req.method === 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
    console.error(err.message);
    if (!(err instanceof HttpError)) {
        /* eslint-disable-next-line no-param-reassign */
        err = new InternalServerError(err.message);
    }
    res.status(err.statusCode).send(err.format);
});

app.get('/api/v1/networkGraphData/:awsAccountId', async (req, res, next) => {
    try {
        const graphData = await CloudmapperController.getGraphData(req.params.awsAccountId, req.userDetails);
        return res.json(graphData);
    } catch (err) {
        console.error(err.message);
        next(err);
    }
});

app.get('/api/v1/crossAccountAccess/:awsAccountId', async (req, res, next) => {
    try {
        const graphData = await CloudmapperController.getCrossAccountAccessData(req.params.awsAccountId, req.userDetails);
        return res.json(graphData);
    } catch (err) {
        console.error(err.message);
        next(err);
    }
});

app.get('/api/v1/graphs/isDataAvailable', async (req, res, next) => {
    try {
        const result = await CloudmapperController.isGraphDataAvailable(req.userDetails);
        return res.status(200).send(result);
    } catch (err) {
        console.error('Check for available data failed for customer ', req.userDetails.customers[0]);
        console.error(err.message);
        next(err);
    }
});

module.exports.handler = serverless(app, {
    request(req, event, context) {
        req.event = event;
        req.context = context;

        if (req.event.requestContext.authorizer) {
            req.userDetails = req.event.requestContext.authorizer;
            req.userDetails.customers = req.event.requestContext.authorizer.customers.split(',');
        }
    }
});
