# Log Archive
This project handle all the products and actions that Bridgecrew support for collect data 

## Bridgecrew support the following data source:
* Cloudtrail
* Loggly
* [Lacework](kds/producers/lacework/docs/design.md)

## API
* [/customer/signup](../src/api/customer/signup_api.md)

## Testing
```bash
pipenv shell
python -m pytest tests

```