import gzip
import json
import logging
import os
import sys
import uuid
from io import BytesIO

import boto3
from utilsPython.arn import arnparse
from utilsPython.remote_lambda.invoke import lambda_invoke

customers_api_lambda_name = os.environ.get('CUSTOMERS_API_LAMBDA_NAME', "")

CLOUDTRAIL_VALIDATION_MESSAGE = 'CloudTrail validation message.'

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))

max_records_in_chunk = int(os.environ["MAX_RECORDS_IN_CHUNK"])

region = "us-west-2"
session = boto3.session.Session(region_name=region)
kinesis = boto3.client('kinesis', region_name=region)

tenant_details_cache = {}
customer_accounts_cache = {}

# Split the events array to chunks
def split_to_chunks(events, records_in_chunk):
    for i in range(0, len(events), records_in_chunk):
        yield events[i:i + records_in_chunk]


def convert_to_kinesis_record_format(records):
    formatted_records = []
    for currRecord in records:
        new_rec = {}

        if 'requestParameters' in currRecord:
            currRecord['requestParametersStr'] = json.dumps(currRecord['requestParameters'])
            del currRecord['requestParameters']
        if 'responseElements' in currRecord:
            currRecord['responseElementsStr'] = json.dumps(currRecord['responseElements'])
            del currRecord['responseElements']
        if 'additionalEventData' in currRecord:
            currRecord['additionalEventDataStr'] = json.dumps(currRecord['additionalEventData'])
            del currRecord['additionalEventData']

        new_rec["Data"] = json.dumps(currRecord)
        new_rec["PartitionKey"] = str(uuid.uuid4())
        formatted_records.append(new_rec)

    return formatted_records


def put_records_in_kinesis(stream_name, data):
    kinesis.put_records(Records=data, StreamName=stream_name)


def handle(event, context):
    # Get event from the customer SQS
    if event:
        logger.debug('got event={}'.format(json.dumps(event)))
        try:
            for record in event["Records"]:
                file_obj = record['body']
                try:
                    body = json.loads(file_obj)
                except Exception as e:
                    logger.error('record body: {} does not contain json'.format(file_obj))
                    raise e
                arn_string = record["eventSourceARN"]

                account_id = arnparse(arn_string).account
                tenant_details = get_tenant_details(account_id)

                if 'security_account_id' in tenant_details['cloud_trail_cf_details'] and tenant_details['cloud_trail_cf_details']['security_account_id'] != account_id:
                    # Logs are being saved to a central bucket, get the log file through the central logging account
                    get_tenant_details(account_id=tenant_details['cloud_trail_cf_details']['security_account_id'])

                customer_name = tenant_details["customer_name"]

                customer_accounts = get_customer_accounts(customer_name)
                sts_client = boto3.client('sts')
                assumed_role_object = sts_client.assume_role(
                    RoleArn=tenant_details["cross_account_cf_details"]["cross_account_role_arn"],
                    ExternalId=tenant_details["cloud_trail_cf_details"]["external_id"],
                    RoleSessionName="{}_session".format(customer_name)
                )
                credentials = assumed_role_object['Credentials']

                s3_resource = boto3.resource(
                    's3',
                    aws_access_key_id=credentials['AccessKeyId'],
                    aws_secret_access_key=credentials['SecretAccessKey'],
                    aws_session_token=credentials['SessionToken']
                )

                if body['Message'] != CLOUDTRAIL_VALIDATION_MESSAGE:
                    # Extract the S3 bucket and key from the SQS event
                    message = json.loads(body['Message'])
                    if 's3Bucket' in message:
                        file_obj = get_s3_file_from_cloudtrail(message, s3_resource)

                        # Read the file cloudtrails event from the S3 key
                        file_content = read_s3_file(file_obj)
                        logger.debug('The object {} has been read successfully from S3'.format(file_obj))

                        # Handle cloudtrails logs
                        records = json.loads(file_content)['Records']
                    else:
                        file_objs = get_s3_file_from_s3_event(message, s3_resource)
                        records = []
                        for file in file_objs:
                            file_content = read_s3_file(file)
                            records += json.loads(file_content)['Records']
                    filtered_records = list(filter(lambda rec: rec['recipientAccountId'] in customer_accounts, records))
                    if len(filtered_records) > 0:
                        send_cloudtrails_to_kinesis(cloudtrails={'Records': filtered_records}, customer_name=customer_name, account_id=account_id)
                    if len(filtered_records) < len(records):
                        logger.info(f"Received events from an account not connected to us by {customer_name}, throwing them away")
                else:
                    logger.info('Ignoring Cloudtrail validation message')

        except Exception as e:
            logger.error("An error occured for event={} ".format(json.dumps(event)))
            raise e

    return "success"


def get_customer_accounts(customer_name):
    if customer_name not in customer_accounts_cache:
        customer_accounts_cache[customer_name] = lambda_invoke(
            lambda_function_name=customers_api_lambda_name,
            name="getCustomerAccountIdsByName",
            body=json.dumps({"name": customer_name})
        )
    return customer_accounts_cache[customer_name]


def get_tenant_details(account_id):
    if account_id not in tenant_details_cache:
        tenant_details_cache[account_id] = lambda_invoke(
            lambda_function_name=customers_api_lambda_name,
            name="getTenantEnvironmentDetails",
            body=json.dumps({"awsAccountId": account_id})
        )
    return tenant_details_cache[account_id]


def send_cloudtrails_to_kinesis(cloudtrails, customer_name, account_id):
    """
    sends cloudtrail audit to kinesis data stream in optimal chunks
    :param customer_name: customer identifier
    :param cloudtrails: aws api actions log
    """
    cloudtrails_chunks = split_to_chunks(cloudtrails["Records"], max_records_in_chunk)
    tenant_kds = "bc-kds-{}-{}-{}".format(customer_name, "cloudtrail", account_id)
    for current_cloudtails_chunk in cloudtrails_chunks:

        converted_records = convert_to_kinesis_record_format(current_cloudtails_chunk)
        try:
            put_records_in_kinesis(tenant_kds, converted_records)
            logger.debug(
                "written to Kinesis Data Stream {} successfully {} records. The size of the records is: {} KB".format(
                    tenant_kds,
                    len(converted_records),
                    (sys.getsizeof(converted_records) / 1024)
                ))
        except Exception as e:
            logger.error(
                "failed to write {} records to kinesis data stream {}. The size of the failed records is: {} KB".format(
                    len(converted_records),
                    tenant_kds,
                    (sys.getsizeof(converted_records) / 1024)))
            raise e


def read_s3_file(file_obj):
    """

    :param file_obj: s3 file object
    :return: unzipped file
    """

    return gzip.GzipFile(fileobj=BytesIO(file_obj.get()["Body"].read())).read()


def get_s3_file_from_cloudtrail(message, s3_resource):
    """
    reads s3 resource into file
    :param message:
    :param s3_resource:
    :return:
    """
    s3_bucket = message['s3Bucket']
    s3_key = message['s3ObjectKey'][0]
    logger.debug('The s3 bucket is {} and the key of cloudtrail object is: {}'.format(s3_bucket, s3_key))
    file_obj = s3_resource.Object(s3_bucket, s3_key)
    return file_obj


def get_s3_file_from_s3_event(message, s3):
    files = []
    for record in message['Records']:
        s3_bucket = record['s3']['bucket']['name']
        s3_key = record['s3']['object']['key']
        s3_size = record['s3']['object']['size']

        if 'CloudTrail-Digest' in s3_key or s3_size == 0:
            continue
        files.append(s3.Object(s3_bucket, s3_key))
        print(record)
    return files
