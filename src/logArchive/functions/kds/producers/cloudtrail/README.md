# Deploy lambda get logs from cloudtrail
This lambda function is designed to read from s3 bucket upon notification and move to Kinesis Data Stream.
Notification arrives upon s3 update via sns-sqs triggering.

This function is useful for reading AWS Cloudtrail logs from customer without agent

# Pre-development
Add sls plugin to dev env
```bash
sls plugin install -n serverless-python-requirements
```

# Environment variables
| Variable          | Require   |    Type   | Default   |Example Value          | Description  |
|---|---|---|---|---|---|
|customer_name      |    YES    |   String  |           | Bridgecrew            | The id or name of the customer
|sqs_arn            |    YES    |   String  |           | arn:aws:sqs:VALUE2    | The SQS ARN of the customer that the lambda read from him   
|stage              |    No     |   String  |   dev     |        prod           | The stage of the lambda  
|region             |    No     |   String  | us-east-2 |   us-east-1           | The region of the lambda

# Deployment
```bash
sls deploy --customer_name VALUE1 --sqs_arn arn:aws:sqs:VALUE2

```

# Notes
* The lambda don't handle history data from Cloudtrail
* Temporary until deployment manager will develop after create the lambda you should add and change few things:
    * Change the STS ARN - according to the ARN that created in the customer
    * Add to the lambda a role for access to the customer bucket 
    * Add to the lambda a STS ARN as environment variable 
    * Add to the lambda a external ID as environment variable
    * Add to the lambda a Kinesis stream name as environment variable
    * Confirm the region of the customer! If it is not us-east-2 change in the lambda environment variable
    