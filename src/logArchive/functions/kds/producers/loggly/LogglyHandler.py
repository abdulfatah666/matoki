import json
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import os
import boto3
import logging
from datetime import datetime, timezone, timedelta
import uuid
import dateutil.parser
import sys
import time

import MetadataDynamoHandler
from utilsPython.TimeDecorator import timeit
from utilsPython.es.domains import get_elastic_endpoint

MAX_RECORD_SIZE = 1048500

current_time = datetime.utcnow()

session = boto3.session.Session()
region = session.region_name
kinesis = boto3.client('kinesis', region_name=region)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()
logger.setLevel(logging.INFO)


# Split the events array to chunks
def split_to_chunks(events, records_in_chunk):
    for i in range(0, len(events), records_in_chunk):
        yield events[i:i + records_in_chunk]


@timeit
def put_records_in_kinesis(stream_name, data):
    for obj in data:
        json_len = len(json.dumps(obj, separators=(',', ':')))
        if json_len > MAX_RECORD_SIZE:
            print('WARNING: object exceeded MAX_RECORD_SIZE, remove from chunk. object size: ',json_len)
            data.remove(obj)
    kinesis.put_records(Records=data, StreamName=stream_name)


def convert_to_kinesis_record_format(records, query_timestamp):
    formatted_records = []

    for curr_record in records:
        new_rec = {}
        curr_record["queryTime"] = query_timestamp
        new_rec["Data"] = json.dumps(curr_record)
        new_rec["PartitionKey"] = str(uuid.uuid4())
        formatted_records.append(new_rec)

    return formatted_records


# Write the loggly query response to kinesis
def write_query_response(stream_name, response, query_timestamp):
    events = response["events"]
    records_in_chunk = int(os.environ["MAX_RECORDS_IN_CHUNK"])

    # split the response events array into chunks
    events_chunks = split_to_chunks(events, records_in_chunk)
    chunk_index = 0

    # Writing the events to kinesis in chunks, so the kinesis buffer limit win't be exceeded
    for current_chunk in events_chunks:
        converted_records = convert_to_kinesis_record_format(current_chunk, query_timestamp)

        try:
            # Writing data to kinesis
            put_records_in_kinesis(stream_name, converted_records)
            logger.debug("written successfully: {}".format(current_chunk))

            chunk_index += 1
        except Exception as e:
            logger.error("failed to write message: {}".format(current_chunk))
            raise e


def requests_retry_session(
        retries=int(os.environ["MAX_RETRIES"]),
        backoff_factor=0.3,
        status_forcelist=(500, 502, 504),
        session=None
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


# Execute the query by sending GET rest call.
# In case of error, the function will try to re-query the server for maxRetries times
def query_loggly_server(url, query_params, headers):
    # Sending the query GET request
    query_time = datetime.utcnow().replace(tzinfo=timezone.utc).isoformat()

    total_polling_tries = 5
    polling_time_period = 60

    polling = True
    counter = 1
    while polling:
        try:
            response = requests_retry_session().get(url, headers=headers, params=query_params)
            polling = False
        except:
            print("got exception while get request, num of tries: {} at: {}".format(counter, total_polling_tries))
        finally:
            if(polling is True):
                if (counter < total_polling_tries):
                   print("waiting {} seconds until next try".format(polling_time_period))
                   time.sleep(polling_time_period)
                   counter += 1
                else:
                    print("total trying: {}, aborted.".format(counter))
                    polling = False

    if response.ok:
        json_size = sys.getsizeof(response)

        # logging debug information
        logger.debug("object length: {}".format(json_size))
        logger.debug("request duration: {}".format(response.elapsed))
        return {'response': response.json(), 'queryTime': query_time}
    else:
        return None


# Set the dates parameters values
# In case the from, until values exist in the dynamo, we add them the time slot.
# Otherwise, we calculate these vaules according to the current time, and at the end of the process,
# put them in the dynamo
def calculate_query_dates_params(customer_name):
    last_query_time = MetadataDynamoHandler.get_last_query_time(customer_name)

    time_confidence_factor_minutes = int(os.environ["TIME_CONFIDENCE_FACTOR_MINUTES"])
    time_slot_minutes = int(os.environ["TIME_SLOT_MINUTES"])

    if last_query_time is None:
        difference_in_minutes = time_confidence_factor_minutes + time_slot_minutes
        from_time = current_time - timedelta(minutes=difference_in_minutes)
        until = current_time - timedelta(minutes=time_confidence_factor_minutes)

        MetadataDynamoHandler.insert_last_query_time(customer_name, from_time, until)
    else:
        response_from_time = last_query_time["from"]
        from_time = dateutil.parser.parse(response_from_time) + timedelta(minutes=time_slot_minutes)
        until = from_time + timedelta(minutes=time_slot_minutes)

    return {'fromTime': from_time, 'until': until}



# Main lambda function
def handle(event, context):
    type = os.environ["AUTH_TYPE"]

    customer_name = event["customer_name"]
    server_url = event["server_url"]
    api_key = event["api_key"]

    # Calculating the time parameters
    date_params = calculate_query_dates_params(customer_name)
    from_time = date_params["fromTime"]
    until = date_params["until"]

    # Building the query params
    query_params = {"q": os.environ["QUERY_STRING"],
                    "from": from_time.replace(tzinfo=timezone.utc).isoformat(),
                    "until": until.replace(tzinfo=timezone.utc).isoformat(),
                    "size": int(os.environ["PAGE_SIZE"])}

    # Building the headers
    headers = {"authorization": "{0} {1}".format(type, api_key)}

    # Query the loggly server (GET request)
    result = query_loggly_server(server_url, query_params, headers)

    if result is None:
        logger.error("failed to handle request")
    else:
        # Writing the response to Kinesis
        response = result["response"]
        query_time = result["queryTime"]
        stream_name = "bc-kds-{}-loggly".format(customer_name)

        write_query_response(stream_name, response, query_time)

        # Qurey for next page
        while "next" in response:
            next_url = response["next"]
            result = query_loggly_server(next_url, query_params, headers)

            if result is None:
                logger.error("failed to handle request")
                break
            else:
                response = result["response"]
                query_time = result["queryTime"]
                write_query_response(stream_name, response, query_time)

        logger.debug("All messages were written successfully")

    # updating the dynamo table with the last query time
    MetadataDynamoHandler.update_last_query_time(customer_name, from_time, until)

    return "success"