import boto3
from boto3.dynamodb.types import TypeDeserializer
import os

dynamo = boto3.client('dynamodb')
deserializer = TypeDeserializer()


def get_last_query_time(customer_name):
    table_name = os.environ["METADATA_TABLE_NAME"]
    key = os.environ["TABLE_PARTITION_KEY"]

    result = dynamo.get_item(TableName=table_name, Key={key: {'S': customer_name}})
    if "Item" not in result:
        return None
    else:
        # Convert the dynamoDB json format to regular json format
        json_data = {k: deserializer.deserialize(v) for k, v in result["Item"].items()}
        return json_data


def insert_last_query_time(customer_name, from_time, until):
    table_name = os.environ["METADATA_TABLE_NAME"]

    result = dynamo.put_item(TableName=table_name, Item={
        'customer-name': {'S': customer_name},
        'from': {'S': str(from_time)},
        'until': {'S': str(until)}
    })

    return result


def update_last_query_time(customer_name, from_time, until):
    table_name = os.environ["METADATA_TABLE_NAME"]
    key = os.environ["TABLE_PARTITION_KEY"]

    result = dynamo.update_item(TableName=table_name,
                                Key={key: {'S': customer_name}},
                                UpdateExpression="set #fm = :f, #ul = :u",
                                ExpressionAttributeValues={
                                    ':f': {'S': str(from_time)},
                                    ':u': {'S': str(until)}
                                },
                                ExpressionAttributeNames={
                                    "#fm": "from",
                                    "#ul": "until"
                                },
                                ReturnValues="UPDATED_NEW")

    json_data = {k: deserializer.deserialize(v) for k,v in result["Attributes"].items()}
    return json_data