const AbsApiLogIntegration = require('./AbsApiLogIntegration');

class OneLoginIntegration extends AbsApiLogIntegration {
    constructor() {
        super('onelogin');
    }

    static cleanEvent(event) {
        const eventCopy = JSON.parse(JSON.stringify(event));
        Object.keys(event).forEach(key => {
            if (event[key] === null || event[key] === undefined) {
                delete eventCopy[key];
            }
        });
        return eventCopy;
    }

    async getToken({ endpointRegion, clientId, clientSecret }) {
        const headers = { Authorization: `client_id:${clientId}, client_secret:${clientSecret}`, 'Content-Type': 'application/json' };
        const response = await this.promisifyRequest({ baseUrl: `api.${endpointRegion}.onelogin.com`, method: 'POST', path: '/auth/oauth2/v2/token', headers, body: '{"grant_type":"client_credentials"}' });
        return response.access_token;
    }

    async getEvents({ customerName, integrationParams, startDate }) {
        console.log(`Getting OneLogin token for ${customerName}`);
        const token = await this.getToken(integrationParams);
        const headers = { Authorization: `bearer:${token}` };
        const endDate = AbsApiLogIntegration.getEndDate();
        console.log(`Getting events for ${customerName}, for ${startDate}-${endDate}`);
        let nextLink = null;
        const events = [];
        let path = `/api/1/events?since=${startDate}&until=${endDate}`;
        do {
            const result = await this.promisifyRequest({ baseUrl: `api.${integrationParams.endpointRegion}.onelogin.com`, method: 'GET', path, headers });
            nextLink = result.pagination.next_link;
            path = nextLink ? nextLink.substr(nextLink.indexOf('/api/1')) : null;
            events.push(...result.data.map(event => OneLoginIntegration.cleanEvent(event)));
        } while (nextLink);

        return { events, endDate };
    }
}

module.exports = new OneLoginIntegration();
