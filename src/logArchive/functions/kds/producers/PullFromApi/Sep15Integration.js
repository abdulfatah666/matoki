const AbsApiLogIntegration = require('./AbsApiLogIntegration');

const EVENT_TYPES = ['App Isolation', 'Exploit Protection', 'Firewall', 'Malware Protection', 'Network IPS', 'Behavioral Analysis', 'App Control', 'Deception'];
const BASE_URL = 'usea1.r3.securitycloud.symantec.com';

class Sep15Integration extends AbsApiLogIntegration {
    constructor() {
        super('sep15');
    }

    async getToken({ clientId, clientSecret }) {
        const token = Buffer.from(`${clientId}:${clientSecret}`).toString('base64');
        const headers = {
            Accept: 'application/json',
            Authorization: `Basic ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded'
        };

        const response = await this.promisifyRequest({ baseUrl: BASE_URL, method: 'POST', path: '/r3_epmp_i/oauth2/tokens', headers, body: 'grant_type=client_credentials' });
        return response.access_token;
    }

    /**
     *
     * @param token
     * @param startDate
     * @param endDate
     * @return {Promise<Object>}
     */
    async getSep15Events({ token, startDate, endDate }) {
        const headers = {
            Accept: 'application/json',
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
            'x-epmp-product': 'SAEP',
            'cache-control': 'no-cache'
        };

        return Promise.all(EVENT_TYPES.map(eventType => {
            const payload = {
                startDate,
                endDate,
                batchSize: 500,
                type: eventType
            };
            return this.promisifyRequest({ baseUrl: BASE_URL, method: 'POST', path: '/r3_epmp_i/sccs/v1/events/export', headers, body: JSON.stringify(payload) });
        })).then(responses => responses.reduce((consolidated, response) => consolidated.concat(response), []));
    }

    async getEvents({ customerName, integrationParams, startDate }) {
        const token = await this.getToken(integrationParams);
        const endDate = AbsApiLogIntegration.getEndDate();
        console.log(`Getting events for ${customerName}, for ${startDate}-${endDate}`);
        const events = await this.getSep15Events({ token, startDate, endDate });
        return { events, endDate };
    }
}

module.exports = new Sep15Integration();