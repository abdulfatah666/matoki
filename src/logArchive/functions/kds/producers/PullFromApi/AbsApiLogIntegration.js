const https = require('https');

class AbsApiLogIntegration {
    constructor(type) {
        this.type = type;
    }

    /**
     * @param baseUrl - The base URL of the API
     * @param method  - The HTTP method to use (GET / POST etc)
     * @param path    - The path after the BASE_URL
     * @param headers
     * @param body
     * @return {Promise<object>} - The response object
     */
    // eslint-disable-next-line class-methods-use-this
    async promisifyRequest({ baseUrl, method, path, headers, body }) {
        return new Promise((resolve, reject) => {
            const req = https.request({ host: baseUrl, port: 443, path, method, headers }, (res) => {
                let result = '';
                res.on('data', chunk => {
                    result += chunk;
                });
                res.on('end', () => resolve(result !== '' ? JSON.parse(result) : []));
            });
            req.on('error', (e) => reject(e));
            req.write(body || '');
            req.end();
        });
    }

    /**
     * @param {string} customerName
     * @param {object<string, object>} integrationParams
     * @param {string} startDate - the date the scan should begin from
     * @return {Promise<Object<Object[], string>>}
     */
    // eslint-disable-next-line class-methods-use-this, no-unused-vars
    async getEvents({ customerName, integrationParams, startDate }) {
        throw new Error('Reached abstract method "getEvents", please implement this method!');
    }

    static getEndDate() {
        return new Date().toISOString();
    }
}

module.exports = AbsApiLogIntegration;