const AWS = require('aws-sdk');

const ONE_HOUR_IN_MS = 60 * 60 * 1000;
const EtlDbMgr = require('@bridgecrew/nodeUtils/dbMgrs/EtlDbMgr');
const RemoteLambda = require('@bridgecrew/nodeUtils/remoteLambda/invoke');

const Sep15 = require('./Sep15Integration');
const OneLogin = require('./OneLoginIntegration');

const integrationsApiRemoteLambda = new RemoteLambda(process.env.INTEGRATIONS_API_LAMBDA);

const es = new AWS.ES();

const typeToIntegration = {
    sep15: Sep15,
    onelogin: OneLogin
};

/**
 *
 * @param {string} customerName     - Name of the customer the events belong to
 * @param {string} type            - Type of integration
 * @param {object[]} events         - List of events to be sent to ES
 * @return {Promise<string[]>} result - The result of the write operations
 */
const sendToEs = async (customerName, type, events) => {
    const results = [];
    const esDomain = await es.describeElasticsearchDomain({ DomainName: `bc-es-${customerName}` }).promise()
        .then(resp => resp.DomainStatus.Endpoint);
    const esEndpoint = new AWS.Endpoint(esDomain);
    const credentials = new AWS.EnvironmentCredentials('AWS');
    const client = new AWS.HttpClient();
    /* eslint-disable-next-line no-restricted-syntax */
    for (const event of events) {
        const request = new AWS.HttpRequest(esEndpoint, process.env.AWS_REGION);
        request.method = 'POST';
        request.path += `${customerName}-${type}-${new Date().getFullYear()}-${new Date().getUTCMonth()}/_doc`;
        request.body = JSON.stringify(event);
        request.headers.host = esDomain;
        request.headers['Content-Type'] = 'application/json';

        const signer = new AWS.Signers.V4(request, 'es');
        signer.addAuthorization(credentials, new Date());

        const result = await new Promise(((resolve, reject) => {
            client.handleRequest(request, null, response => {
                let responseBody = '';
                response.on('data', chunk => {
                    responseBody += chunk;
                });
                response.on('end', () => setTimeout(() => resolve(JSON.parse(responseBody)), 100));
            }, error => {
                reject(error);
            });
        }));
        results.push(result);
    }
    return results;
};

exports.handler = async (event) => {
    const { customerName, type } = event;
    console.info(`Pulling ${type} for ${customerName}`);
    const integrationHandler = typeToIntegration[type];
    const integrations = await integrationsApiRemoteLambda.invoke('getByType', { customerName, type });

    if (!integrations || integrations.length === 0) {
        console.error(`Couldn't find ${type} integration for ${customerName}`);
        throw new Error(`Couldn't find ${type} integration for ${customerName}`);
    }

    const integrationObj = integrations[0];

    const lastScanTime = await EtlDbMgr.getLastQueried(customerName, type);
    const startDate = lastScanTime || new Date(new Date() - ONE_HOUR_IN_MS).toISOString();
    const { events, endDate } = await integrationHandler.getEvents({ customerName, integrationParams: integrationObj.params, startDate });

    console.log(`Found ${events.length} ${type} events for ${customerName}, writing to Elasticsearch`);
    const results = await sendToEs(customerName, type, events);
    if (results.find(result => result.status >= 400)) {
        console.error(results);
        throw new Error('Failed to write to ES');
    }
    console.log(`Wrote ${events.length} ${type} events to Elasticsearch`);

    await EtlDbMgr.setLastQueried(customerName, type, endDate);
    console.info(`Finished pulling ${type} data for ${customerName}`);

    return 'Success';
};
