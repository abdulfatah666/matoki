# Lacework producer
## Overview
[Lacework](https://www.lacework.com/) is a CWPP (Cloud Workload Protection Platform) tool with the following capabilities: configuration compliance, anomaly detection and runtime protection(containers/vm's).
## Context
To enhance customer's detection capabilities over using Bridgecrew platform, we have 2 product strategy approaches:
1. Use a rule engine (like [Snowalert](https://github.com/snowflakedb/SnowAlert) & [Elastalert](https://github.com/Yelp/elastalert)) and write content (rules)
2. Use existing detection tools (with pre baked content) and integrate them into the platform

Lacework Integration stands for the second approach.

Lacework support multiple cloud vendors. Current design is focused only on lacework **AWS cloudtrail & config** integration.  
## Solution
Architecture diagram the way Lacework sees it:
![](images/architecture.png "high level diagram")
Sequence Diagram the way Lacework [sees it](https://sequencediagram.org/index.html#initialData=C4S2BsFMAIBkEMDGkDuB7ATga2gMmgEIYgAmA5pIhqtACLzDwBi4aKAUAHZrAxoBukDHCSpMWADRFSFKqgBcI5OmzQA8p2gBnbigBm4eFkjsEy8QFoAfNPKVqKRdXgloiVgFcSwDPBDhoeC8waAAKfhB4aBIGKK0AC3hqAEpTURUsAB4LC1tZBydIFzdPb19-QODgMIiomMZtRJT2dlY0AAdoPUwixHjoQ3NVSEFOYBbuXmgBISUxbCliOzlHaAAVYjIKYTRNM3mcEcgxtKGsazz7BQBhUpQGPsVOGn2M6COxgElXLh4+QWEr3EixkV1WTEgwD671G1RIkL84C0XQwaAAtnM3gBBAAKn1OB2ylxWihxagAymt5PB2iAAPT8ACMdKQyC0WjpwDQxk4WgJGRyNiW+QU0F4aPamCSAE9Aog2ciuTz+eIicKwYoAOIAUSpNPpTLpkAAHrwMJx4OAjbCOZrIdrYbQEf4tAB+bUANW1ADk1gB9T60AC8KuwguJBRhx2A32izqRLXYxxIQA
):
![](images/dataflow.png "Detailed sequence diagram")

Detailed [sequence diagram](https://sequencediagram.org/index.html#initialData=C4S2BsFMAIBkEMDGkDuB7ATga2gMmgEIYgAmA5pIhqtACLzDwBi4aKAUAA7waiIjcAdsDIY0AV07QAVNITJ02WVx58B8YXCSpMWdpEEkVvEPyEixkmdKKkKVVMu4mzG4IWLlK1Ds7XnCAGF5HWwAJUh4EkgMfUN2dkE0YBg0ADcYrQVdABpbLwcUAC4s0Jw0QWgAZySUADNweCxIdhDFLABaAD58+x8S6ijoRFZxEmAMeBBwaHgxsGgACjSQeGgSBjWqgAseSABKVu12gB4Ojt7vVAHIkmHR8cnp2fn3ZdX1zerd6kP2VjQUjqmEiiG20Ea2Ww0EgGWECSSKWg6UybVyl0KJQAKsQyBQMMjKmjobCDMAjlDOj1PH1roEHigGGCSoIaMScKThABJIwYnzdAjBY66CJRGJFXaGKCLTnAHmHJCgNIMGCC9mi6KxNXC8K3GICoWUjXi6AUYAAQUQiAk3JIMrhcpIh21Rr1GANAGVgCCimasQY3OzFkhreJbc7DWVjRgzhdAl6fbMrTbgOyAKoYcCJZKpDIE9l5GlXYrQJiQYBgmEO9blqbgKrQOpiAC2pXa0HNAAUuQl2C6o26Deq3SUzebxMBtli0M1BIsFYglSqgsOxbF2Wd++1oyVOwB5D1YorwTggAD0aQAjGeQ5AqlUz97Z1UKWVzj1I9uR9AUs3OJgeAATyTZB7x-GcDHYaJFRAZUkS3EU3QSBDdTXIcdSwHdTXLWha2mSASAAUQdYMJynCDBByWV5XYGC4NVT9ELXV9TjjVdNRKABxQijxPc8rzPSAAA8UgwQR4HAQSHQfTjy2IslcMYaYqgAfkIgA1QiADksQAfS5WgAF4ABIAG8AF8WN0d8UMw79qLuaIlPrdhAXcEA6irBS8PrAA6DTtL081AixPcwkMwzAj3ABZTtYC5c0tMCQi+0Y1DNW6dkuJ449Twva9hNE8TJOtP9wFWQRkBvFAZPLBAUiqYBAjQMqKuQCJ-14FTzQAdQ9XTgqitMdP0oyzPM3AmC5WBCN0pgwui80sUMgArKoKis7AbLSuy1xKUrOHKjRkBrZyX2gxdYOXWzowSAwjGzJEUQJG63ULOxiyKc0SDuMhxFIY6YA0O5qFK5t7oYEANtetCPwTahfXLTj-uiSrIEWJy6wI+ThAjdiYljQV4cgX0UfK1kXxhjK4e9BGzQiZNwcMSGKgxnzsYdPGMOjQn41pknoAAIg0KoQAAIygQWf0gMqVVS-H3Q-BWikQCTEHERoUktdXhHgCXIHNe8QDIVl0cx-CiM56R5e5wcacTRqQUIwRiDBDmyRlF3TG2d3cdoy76JXW3mKg0El3gnbboSC7w4YhWEiAA)
![alt text](images/detailedDataflow.png "Detailed sequence diagram")
#### Important notes:
* in the future we might like to write all of lacework the data into a db. This will require architecture change to enable immutable writes.
* cloudwatch triggers does not support recovery. queue is required for stability.
#### Additional mapping actions should be done on lacework data:
* map aws account to virtue customer(multitenancy, multi account)

### Additional resources:
* [Lacework API](https://www.lacework.com/wp-content/uploads/2019/01/Lacework_API_Server_Documentation.pdf)
* [Lacework API access keys and tokens](https://support.lacework.com/hc/en-us/articles/360011403853-API-Access-Keys-and-Tokens)
### Output
#### Mapping Table:
| Source field       | Target Field          | Example Value  | Comments      |
| ------------------ |:---------------------:| --------------:|--------------:|
| cloudwatch.version       | version       | 0 | | 
| cloudwatch.detail.SEVERITY      | severity      |   low | map number to enum |
| DB | status      |    in progress |jira-like enum|
| cloudwatch.detail.SUMMARY | description      |   For account: 090772183824  : DescribeElasticsearchDomain  failed with error code  for user IAMUser/090772183824:suli@bridgecrew.io  using es.amazonaws.com  in region us-east-2  | |
| DB | guidelinesLink      |   360004031099-AWS-Risk-and-Compliance-Whitepaper| https://bridgecrew.zendesk.com/api/v2/help_center/en-us/articles/360004031099-AWS-Risk-and-Compliance-Whitepaper.json["body"] contains html of guidelines|
| DB | businessImpact      |    low/medium/high | enum mapping by alert type|
| DB | sensitivity      |    NULL/phi/pii/pci | enum|
| cloudtrail.data.ENTITY_MAP.CT_User.USERNAME | assignee      |    IAMUser/090772183824:suli@bridgecrew.io |email address|
| const | source      |    lacework |string, identifying bridgecrew's source|
| cloudwatch.detail.LINK | laceworkLink      | https://virtue2.lacework.net/ui/investigate/Event/51 | link|
| DB | recommendation      |    code markdown |code markdown|
| DB | alertId      |  1  |sequenced number|
| DB | tennant      |  Globality  | bridgecrew customer identifier|
| cloudwatch.detail.EVENT_TYPE | eventType      |  ApiFailedWithError  | lacework event's type|
| cloudwatch.detail.EVENT_MODEL | eventModel      |  AwsApiTracker  | lacework event's model |
| cloudtrail.data.START_TIME | startTime      |  2019-04-01T11:29:46Z  | time in ISO format|
| cloudtrail.data.END_TIME | endTime      |  2019-04-01T11:29:46Z  | time in ISO format |




### Input Tests
Mock lacework events by the following samples:
`TODO`: verify all different event models from lacework
#### Cloudwatch notification
```json
{
	"version": "0",
	"id": "0742a5bf-2ced-5417-2810-ad22c55b5563",
	"detail-type": "LaceworkEvents",
	"source": "VIRTUE28_3EA32B166BDA49DC2038FDD9D34EB6B61165A6EEA2A26BB",
	"account": "434813966438",
	"time": "2019-04-01T11:29:46Z",
	"region": "us-east-2",
	"resources": [],
	"detail": {
		"EVENT_TYPE": "ApiFailedWithError",
		"EVENT_NAME": "API Failed with Error",
		"EVENT_CATEGORY": "Aws",
		"EVENT_ID": 51,
		"SEVERITY": "4",
		"START_TIME": "01 Apr 2019 10:00 GMT",
		"SUMMARY": " For account: 090772183824  : DescribeElasticsearchDomain  failed with error code  for user IAMUser/090772183824:suli@bridgecrew.io  using es.amazonaws.com  in region us-east-2 ",
		"LINK": "https://virtue2.lacework.net/ui/investigate/Event/51?startTime=1554112800000&endTime=1554116400000",
		"THREAT_TAGS": null,
		"ACCOUNT": "VIRTUE2",
		"SOURCE": "CloudTrail"
	}

}
```
```json
{
	"version": "0",
	"id": "0742a5bf-2ced-5417-2810-ad22c55b5563",
	"detail-type": "LaceworkEvents",
	"source": "VIRTUE28_3EA32B166BDA49DC2038FDD9D34EB6B61165A6EEA2A26BB",
	"account": "434813966438",
	"time": "2019-04-01T11:29:46Z",
	"region": "us-east-2",
	"resources": [],
	"detail": {
		"EVENT_TYPE": "ApiFailedWithError",
		"EVENT_NAME": "API Failed with Error",
		"EVENT_CATEGORY": "Aws",
		"EVENT_ID": 51,
		"SEVERITY": "4",
		"START_TIME": "01 Apr 2019 10:00 GMT",
		"SUMMARY": " For account: 090772183824  : DescribeElasticsearchDomain  failed with error code  for user IAMUser/090772183824:suli@bridgecrew.io  using es.amazonaws.com  in region us-east-2 ",
		"LINK": "https://virtue2.lacework.net/ui/investigate/Event/51?startTime=1554112800000&endTime=1554116400000",
		"THREAT_TAGS": null,
		"ACCOUNT": "VIRTUE2",
		"SOURCE": "CloudTrail"
	}
}
```
#### Cloudtrail CEP(rule) event
```bash
 curl -X GET   'https://virtue2.lacework.net/api/v1/external/events/GetEventDetails?EVENT_ID=30'   -H 'Authorization: Bearer _ecfce45f41e369f9c8c8c15504de05ab'   -H 'Content-Type: application/json'   -H 'Postman-Token: b9d5f6a1-5b18-4f55-bd1c-0474aecd95d7'   -H 'cache-control: no-cache'
```
```json
{
	"data": [{
		"START_TIME": "2019-03-31T11:00:00Z",
		"END_TIME": "2019-03-31T12:00:00Z",
		"EVENT_TYPE": "SuccessfulConsoleLoginWithoutMFA",
		"EVENT_ID": "30",
		"EVENT_ACTOR": "Aws",
		"EVENT_MODEL": "CloudTrailCep",
		"ENTITY_MAP": {
			"Region": [{
				"ACCOUNT_LIST": ["090772183824"],
				"REGION": "us-east-1"
			}],
			"API": [{
				"SERVICE": "signin.amazonaws.com",
				"API": "ConsoleLogin"
			}],
			"SourceIpAddress": [{
				"COUNTRY": "Israel",
				"CITY": "Tel Aviv",
				"IP_ADDRESS": "207.232.46.170",
				"REGION": "Tel Aviv"
			}]
		}
	}]
}
```
#### Compliance change event

```json
{
	"data": [{
		"START_TIME": "2019-04-02T13:00:00Z",
		"END_TIME": "2019-04-02T14:00:00Z",
		"EVENT_TYPE": "ComplianceChanged",
		"EVENT_ID": "114",
		"EVENT_ACTOR": "Compliance",
		"EVENT_MODEL": "AwsCompliance",
		"ENTITY_MAP": {
			"Resource": [{
				"VALUE": "arn:aws:iam::090772183824:root",
				"NAME": "iam:user"
			}],
			"RecId": [{
				"STATUS": "NonCompliant",
				"REC_ID": "AWS_CIS_1_1",
				"EVAL_TYPE": "LW_SA",
				"EVAL_GUID": "bfcd97c663734b1b85346da77b36f9a6",
				"ACCOUNT_ID": "090772183824",
				"TITLE": "Avoid the use of the \"root\" account"
			}]
		}
	}]
}
```
## Monitoring and Alerting
* Notify on lacework downtime
* Notify on inability to generate new access tokens
## Manual Deployment
Lacework producer would be deployed on a standart manner using cloud formation. 

The producer requires key configuration of lacework, and Lacework needs to be notified on a new customer on boarding.

For first customers, we would do the following actions manually(and will push to automate those as we get volume):
1. Configure new BridgeCrew customer on Lacework (`How?` `TBD`)
2. Configure lacework to read data from BridgeCrew’s snowflake (`How can we do that?`)
3. Configure lacework to send events to BridgeCrew’s customer’s cloudwatch (guide: [here](https://support.lacework.com/hc/en-us/articles/360005840174-AWS-CloudWatch))
4. Create lacework api key (guide: [here](https://support.lacework.com/hc/en-us/articles/360011403853-API-Access-Keys-and-Tokens))
5. Copy lacework api key to BridgeCrew configuration (`How? TBD`)
6. Configure support email on Lacework
