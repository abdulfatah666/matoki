import json
import logging
import os
from utilsPython.lacework.models.LaceworkEvent import LaceworkEvent
from utilsPython.lacework.api import LaceworkMgr
from utilsPython.lacework.laceworkConf import LaceworkConfManager
from utilsPython.kinesis.KinesisUtils import KinesisUtils
from enum import Enum


class LaceworkEventSource(Enum):
    COMPLIANCE = 'Compliance'
    CLOUDTRAIL = 'CloudTrail'
    LACEWORK_AGENT = 'Lacework Agent'
    GCP_COMPLIANCE = 'GcpCompliance'

lacework_conf_mgr = LaceworkConfManager()
api = LaceworkMgr()
kinesis_utils = KinesisUtils()
kinesis_stream_suffix = os.environ["KINESIS_STREAM_SUFFIX"]

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))


def build_lacework_event(detailed_event, lacework_cloudwatch_detail, aws_account_id):
    lacework_event = LaceworkEvent(start_time=detailed_event['START_TIME'], end_time=detailed_event['END_TIME'],
                                   lacework_event_type=detailed_event['EVENT_TYPE'],
                                   lacework_event_id=detailed_event["EVENT_ID"],
                                   lacework_event_actor=detailed_event["EVENT_ACTOR"],
                                   lacework_event_model=detailed_event["EVENT_MODEL"],
                                   lacework_entity_map=detailed_event["ENTITY_MAP"],
                                   lacework_event_name=lacework_cloudwatch_detail["EVENT_NAME"],
                                   lacework_severity=lacework_cloudwatch_detail["SEVERITY"],
                                   lacework_summary=lacework_cloudwatch_detail["SUMMARY"],
                                   lacework_link=lacework_cloudwatch_detail["LINK"],
                                   lacework_source=lacework_cloudwatch_detail["SOURCE"],
                                   lacework_threat_tags=lacework_cloudwatch_detail.get("THREAT_TAGS", ""),
                                   lacework_account=lacework_cloudwatch_detail["ACCOUNT"],
                                   lacework_event_category=lacework_cloudwatch_detail['EVENT_CATEGORY'],
                                   aws_account_id=aws_account_id
                                   )
    return lacework_event


def handle(event, context):
    if event:
        logger.info('got event{}'.format(event))
        try:
            for record in event["Records"]:
                json_str = record['body']
                try:
                    body = json.loads(json_str)
                except Exception as e:
                    logger.error('record body: {} does not contain json'.format(json_str))
                    raise e
                # Get lacework cloudwatch details
                lacework_cloudwatch_detail = body['detail']
                lacework_account = lacework_cloudwatch_detail['ACCOUNT']
                customer_lacework_conf = lacework_conf_mgr.get_conf_by_lacework_account(lacework_account=lacework_account)
                try:
                    token = api.get_access_token(customer_lacework_conf)
                except Exception as e:
                    logger.error('failed to get access token: {}'.format(customer_lacework_conf))
                    raise e
                # Get lacework event
                event_id = lacework_cloudwatch_detail["EVENT_ID"]
                detailed_event = api.get_detailed_event(access_token=token, event_id=event_id, lacework_conf=customer_lacework_conf)
                logger.debug('Detailed event: {}'.format(detailed_event))
                lacework_records = {}
                event_source = lacework_cloudwatch_detail['SOURCE']
                if event_source == LaceworkEventSource.COMPLIANCE.value:
                    aws_account_id = detailed_event['ENTITY_MAP']['RecId'][0]['ACCOUNT_ID']
                    logger.debug("The aws_account_id {0} and the lacework_account is: {1}".format(aws_account_id,
                                                                                                  lacework_account))
                    # Build event output
                    lacework_event = build_lacework_event(detailed_event, lacework_cloudwatch_detail, aws_account_id)
                    lacework_records = [lacework_event.__dict__]
                    # lacework_event.to_violation().save(customer_lacework_conf.customer_name)

                elif event_source == LaceworkEventSource.CLOUDTRAIL.value or event_source == LaceworkEventSource.LACEWORK_AGENT.value or event_source == LaceworkEventSource.GCP_COMPLIANCE.value:
                    lacework_event = build_lacework_event(detailed_event, lacework_cloudwatch_detail, None)
                    lacework_records = [lacework_event.__dict__]

                tenant_events_kds = "bc-kds-{}-{}-{}-{}".format(customer_lacework_conf.customer_name, kinesis_stream_suffix, "events", lacework_account)
                kinesis_utils.send_to_kinesis(lacework_records, tenant_events_kds)

        except Exception as e:
            logger.error("An error occur {} ".format(event))
            raise e
