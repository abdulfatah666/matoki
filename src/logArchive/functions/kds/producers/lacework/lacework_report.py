import json
import logging
import os

from utilsPython.lacework.api import LaceworkMgr
from utilsPython.lacework.laceworkConf import LaceworkConfManager
from utilsPython.lacework.models.LaceworkReport import LaceworkReport
from utilsPython.remote_lambda.invoke import lambda_invoke

customers_api_lambda_name = os.environ.get('CUSTOMERS_API_LAMBDA_NAME', "")

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))

lacework_conf_mgr = LaceworkConfManager()
lacework_api = LaceworkMgr()


def build_lacework_report(lacework_account, aws_account_id):
    logger.info("The lacework_account: {0} and the aws_account_id: {1} ".format(lacework_account, aws_account_id))
    customer_lacework_conf = lacework_conf_mgr.get_conf_by_lacework_account(lacework_account)
    token = lacework_api.get_access_token(customer_lacework_conf)
    lacework_api.run_aws_report(customer_lacework_conf,aws_account_id)
    lacework_report_json = lacework_api.get_aws_violation_report(access_token=token,
                                                                 lacework_conf=customer_lacework_conf,
                                                                 aws_account_id=aws_account_id)
    logger.debug("Got violation report: {0}".format(json.dumps(lacework_report_json)))
    is_report_ok = lacework_report_json["ok"]
    if is_report_ok:
        title = lacework_report_json["data"][0]["reportTitle"]
        time = lacework_report_json["data"][0]["reportTime"]
        report_type = lacework_report_json["data"][0]["reportType"]
        aws_account_id = lacework_report_json["data"][0]["accountId"]
        customer_name = lambda_invoke(lambda_function_name = customers_api_lambda_name, name = "getTenantEnvironmentDetails", body = json.dumps({"awsAccountId": aws_account_id}))["customer_name"]
        recommendations = lacework_report_json["data"][0]["recommendations"]
        print("The report time is : {}".format(time))
        lacework_report = LaceworkReport(title=title, time=time, type=report_type, aws_account_id=aws_account_id,
                                         customer_name=customer_name, recommendations=recommendations,
                                         lacework_account=lacework_account)

        return lacework_report
    return None


def report(aws_account_id, lacework_account):
    """

    :param aws_account_id: example: 1234
    :param lacework_account: example: VIRTUE2
    """
    # get lacework conf from cache
    customer_lacework_conf = lacework_conf_mgr.get_conf_by_lacework_account(
        lacework_account=lacework_account)
    lacework_report = build_lacework_report(lacework_account=customer_lacework_conf.lacework_account,
                                            aws_account_id=aws_account_id)
    if lacework_report is not None:
        violations = lacework_report.to_violations()
        for violation in violations:
            violation.save(lacework_report.customer_name)

        lacework_report.update_tenants_lacework()
    else:
        logger.debug("empty lacework report")


def handle(event, context):
    lacework_account = event["lacework_account_id"]
    aws_account_id = event["aws_account_id"]
    report(aws_account_id=aws_account_id, lacework_account=lacework_account)
