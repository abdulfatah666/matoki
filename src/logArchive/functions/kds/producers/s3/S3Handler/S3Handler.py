import base64
import json
import logging
import os

import boto3
from elasticsearch import Elasticsearch, RequestsHttpConnection, RequestError
from requests_aws4auth import AWS4Auth

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

AWS_ACCESS_KEY_ID = os.environ['ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['SECRET_ACCESS_KEY']
REGION = 'us-east-2'

CLIENT_NAME = os.environ['CLIENT_NAME']
CLIENT_BUCKET = os.environ['CLIENT_BUCKET']
CLIENT_ES_HOST = os.environ['CLIENT_ES_HOST']

es_client = boto3.client('es', region_name=REGION, aws_access_key_id=AWS_ACCESS_KEY_ID,
                         aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

s3_client = boto3.client('s3', region_name=REGION, aws_access_key_id=AWS_ACCESS_KEY_ID,
                         aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

aws_auth = AWS4Auth(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, REGION, 'es')


def resend_failed_logs():
    """
    This method scans the backup elasticsearch bucket for files documenting failures.
    For each file found, it goes through the failed "documents" detailed inside and retries to send it to the relevant
    Elasticsearch stream (only once).
    """
    logger.info("Scanning failures for {}".format(CLIENT_NAME))
    keys = get_failure_file_keys()

    es_instance = Elasticsearch(
        hosts=[{'host': CLIENT_ES_HOST, 'port': 443}],
        http_auth=aws_auth,
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection
    )

    for key in keys:
        file = s3_client.get_object(Bucket=CLIENT_BUCKET, Key=key)
        text = file['Body'].read().decode("utf-8")
        failure_cases = list(map(lambda x: json.loads(x), filter(None, text.split('\n'))))

        resend_failed_cases(es_instance, failure_cases)
        move_file_to_retried_folder(key)

    if len(keys) > 0:
        logger.info("Completed {} cases for {}".format(len(keys), CLIENT_NAME))
    else:
        logger.info("No failure cases found for {}".format(CLIENT_NAME))


def resend_failed_cases(es_instance, failure_cases):
    for case in failure_cases:
        try:
            data = base64.b64decode(case['rawData'])
            es_instance.create(index=case['esIndexName'], id=case['esDocumentId'], body=data)
            logger.debug("Successfully sent {}".format(case['esDocumentId']))
        except RequestError:
            logger.error(
                "Retry failed for Document ID {}\nReason: {}".format(case['esDocumentId'], case['errorMessage']))


def get_failure_file_keys():
    """
    Gets all the keys of the files which represent Elasticsearch failures for a specific client
    :return: Array of strings, each is a key which represents a single failure file in the bucket
    """
    last_key_received = ""
    is_truncated = True
    keys = []
    while is_truncated:
        logger.info("Requesting 1000 keys from bucket")
        # Get 1000 results starting after the last object that was already received
        list_response = s3_client.list_objects_v2(
            Bucket=CLIENT_BUCKET,
            Prefix='elasticsearch-failed/',
            StartAfter=last_key_received
        )

        is_truncated = list_response['IsTruncated']

        # When the number of files is a product of 1000, the last request will not have the 'Contents' field
        contents = list_response.get('Contents', [])
        if len(contents) > 0:
            last_key_received = contents[-1]['Key']
            keys.extend(list(map(lambda x: x['Key'], filter(lambda x: x['Size'] > 0, contents))))

    logger.info("Found {} keys".format(len(keys)))
    return keys


def move_file_to_retried_folder(key):
    """
    Move files from the failed folder to the failed-retried folder in the same bucket.
    :param key: The key of the file to be moved.
    """
    s3_client.copy_object(
        Bucket=CLIENT_BUCKET,
        CopySource={'Bucket': CLIENT_BUCKET, 'Key': key},
        Key='elasticsearch-failed-retried/' + key[key.index('/') + 1:]
    )

    s3_client.delete_object(Bucket=CLIENT_BUCKET, Key=key)


if __name__ == '__main__':
    resend_failed_logs()
