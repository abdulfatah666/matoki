# Deploy EC2 Instance to resend logs to Elasticsearch which previously failed 
This module contains 2 folders:
1. cloudformation.resources - Contains all the resources to support the deployment of the handler on an EC2 server.
2. S3Handler - Contains the code that re-sends the failed logs. 

# Pre-deployment
An updated zip file of S3Handler folder and the instance.template file should be in the bucket `instance-retry-es-failure`.

# Deploying using CloudFormation
Quick link:

https://us-east-2.console.aws.amazon.com/cloudformation/home?region=us-east-2#/stacks/quickcreate?templateUrl=https%3A%2F%2Fs3.us-east-2.amazonaws.com%2Finstance-retry-es-failure%2Finstance.template&stackName=instance-retry-es-failures-dev&param_AWSACCESSKEYID=&param_AWSSECRETACCESSKEY=&param_BUCKETNAME=&param_CLIENTNAME=&param_ESHOST=

Deploying requires an AWS Access Key ID and an AWS Access Secret Key, as well as the necessary client fields.
After connecting to the instance, logs can be seen by:
```bash
tail -f /var/log/cfn-init.log
```
    