from base64 import b64decode
import json
import os
import logging
import gzip
import urllib.parse
from datetime import datetime, timedelta

import boto3
from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth

logger = logging.getLogger()
logger.setLevel(os.environ.get('LOG_LEVEL', 'INFO'))

region = os.environ.get('awsRegion')
tag = os.environ.get('uniqueTag')

S3 = boto3.client('s3')
Lambda = boto3.client('lambda', region_name=region)
ES = boto3.client('es')

def getTenantEnvironmentDetails(awsAccountId): 
    try:
        client = boto3.client('lambda', region_name=region)

        payload = json.dumps({
            "body": json.dumps({"awsAccountId": awsAccountId}),
            "headers": {
                'Content-Type': 'application/json'
            },
            "path": "/invoke/{}".format("getTenantEnvironmentDetails"),
        })
        lambda_response = client.invoke(FunctionName='customers-api-' + tag, InvocationType="RequestResponse",
                                        Payload=payload)
        lambda_response_payload = json.loads(json.loads(lambda_response['Payload'].read())['body'])
    except ClientError as e:
        logger.error(e.response['Error']['Message'])
        raise e
    else:
        return lambda_response_payload['customer_name']

def connectES(esEndPoint, owner):
    logger.info('Connecting to the ES Endpoint (Account %s): %s' %(owner, esEndPoint))
    try:
        credentials = boto3.Session().get_credentials()
        service = 'es'
        awsauth = AWS4Auth(credentials.access_key, credentials.secret_key, region, service, session_token=credentials.token)

        esClient = Elasticsearch(
        hosts=[{'host': esEndPoint, 'port': 443}],
        http_auth = awsauth,
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection)
        return esClient
    except Exception as e:
        logger.error("Unable to connect to {0}".format(esEndPoint))
        logger.error(e)
        raise e

def indexDocElement(esClient, indexName, indexType, indexBody, event):
    try:
        retval = esClient.index(index=indexName, doc_type=indexType, body=indexBody)
    except Exception as e:
        logger.error("Error: Document not indexed")
        logger.error("Error Message: ",e)
        logger.error("Event in Error: ", event)
        raise e
    
def getElasticsearchUrl(domainName): 
    try:
        response = ES.describe_elasticsearch_domain(
            DomainName=domainName
        )
    except Exception as e:
        logger.error(e.response['Error']['Message'])
        raise e
    else:
        responseDetail = response['DomainStatus']
        return response['DomainStatus']['Endpoint']

def millis2iso(millis): 
    res = datetime.utcfromtimestamp(millis/1000.0).isoformat()
    return (res + ".000")[:23] + 'Z'

def process_cloudwatch_data(data):
    # Return results dict of customers, their ES URL, and records formatted to bulk load
    results = {}

    for line in data:
        # Skip control messages
        if line['messageType'] == 'CONTROL_MESSAGE':
            continue
        
        tenant = getTenantEnvironmentDetails(line['owner'])
        esDomainName = "bc-es-"+tenant
        esDomainUrl = getElasticsearchUrl(esDomainName)
        esClient = connectES(esDomainUrl, line['owner'])

        results.update({tenant: {'esUrl':esDomainUrl}})

        for event in line['logEvents']:
            # Convert timestamp to this format: 2019-12-23T19:13:10.000Z
            timestamp = millis2iso(event['timestamp'])
            indexName = tenant + '-cwl-' + datetime.utcfromtimestamp(event['timestamp']/1000.0).strftime("%Y-%m-%d")
            indexType = 'CloudWatch Logs'
            indexId = event['id']
            indexBody = {}
            indexBody['id'] = event['id']
            indexBody['timestamp'] = timestamp
            indexBody['message'] = event['message']
            indexBody['owner'] = line['owner']
            indexBody['log_group'] = line['logGroup']
            indexBody['log_stream'] = line['logStream']
            
            # Write to ElasticSearch
            indexDocElement(esClient, indexName, indexType, indexBody, event)

def lambda_handler(event, context):

    if event:
        logger.info('got event{}'.format(event))
        try:
            for record in event['Records']:
                bucket = record['s3']['bucket']['name']
                key = urllib.parse.unquote_plus(record['s3']['object']['key'])
                logger.info('Got bucket/key: %s / %s' %(bucket, key))

                obj = S3.get_object(Bucket=bucket, Key=key)
                
                body = obj['Body']
                
                with gzip.open(body, 'rt') as s3object:
                    for s3objectLine in s3object:
                        data = "[" + s3objectLine.replace("}{", "},{") + "]"
                        dataJSON = json.loads(data)
                        process_cloudwatch_data(dataJSON)

        except Exception as e:
            logger.error("An error occur {} ".format(event))
            raise e