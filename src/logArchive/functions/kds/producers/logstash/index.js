const AWS = require('aws-sdk');
const Joi = require('joi');

const kinesis = new AWS.Kinesis();

const uuidv4 = require('uuid/v4');

const LOGSTASH_INTEGRATION_NAME = 'logstash';

const schemaFortigate = Joi.object().keys({
    bc_customerName: Joi.string().required(),
    bc_messageType: Joi.string().required(),
    bc_apiToken: Joi.string().required(),
    '@timestamp': Joi.string(),
    '@version': Joi.string(),
    type: Joi.string(),
    host: Joi.string(),
    tags: Joi.array(),
    ftg_type: Joi.string(),
    ftg_subtype: Joi.string(),
    forti_log: Joi.string(),
    rcvdbyte: Joi.number().integer(),
    sentbyte: Joi.number().integer()
});

function createBaseSchema() {
    return Joi.object().keys({
        bc_customerName: Joi.string().required(),
        bc_messageType: Joi.string().required(),
        bc_apiToken: Joi.string().required()
    });
}

const mapMessageToSchema = {
    fortigate: schemaFortigate,
    pulse_logs: createBaseSchema(),
    pulse_transactions: createBaseSchema(),
    linuxauth: createBaseSchema(),
    openvpn: createBaseSchema(),
    vault: createBaseSchema(),
    pan: createBaseSchema(),
    meraki: createBaseSchema(),
    auth0: createBaseSchema(),
    forti_auth: createBaseSchema()
};

const cacheCustomerIntegrations = {};

exports.handler = async (event) => {
    const body = event.body ? JSON.parse(event.body) : event;
    const customerName = body.bc_customerName;
    let validationResult = {};
    let integrations;
    if (cacheCustomerIntegrations[customerName]) {
        integrations = cacheCustomerIntegrations[customerName];
    } else {
        const integrationsParams = {
            FunctionName: process.env.INTEGRATIONS_API_LAMBDA,
            Payload: JSON.stringify({
                body: {
                    customerName,
                    type: LOGSTASH_INTEGRATION_NAME
                },
                headers: {
                    'Content-Type': 'application/json'
                },
                path: '/invoke/getByType'
            })
        };
        const response = await new AWS.Lambda().invoke(integrationsParams).promise();

        integrations = JSON.parse(response.Payload);
        cacheCustomerIntegrations[customerName] = integrations;
    }

    if (integrations) {
        const logstashIntegration = integrations.find((integration) => integration.type === LOGSTASH_INTEGRATION_NAME && integration.params.type.includes(body.bc_messageType));
        console.log(`Got event for ${customerName}'s ${body.bc_messageType} integration`);
        if (!logstashIntegration) {
            console.error('Logstash integration is not exist');
            throw new Error('Logstash integration is not exist');
        }
        if (!logstashIntegration.enable) {
            console.error('Logstash integration is disabled');
            throw new Error('Logstash integration is disabled');
        }
        if (logstashIntegration.params.api_key !== body.bc_apiToken) {
            console.error('Unauthorized');
            throw new Error('Unauthorized');
        }

        const schemaValidator = mapMessageToSchema[body.bc_messageType];
        validationResult = Joi.validate(body, schemaValidator, { allowUnknown: true });

        if (validationResult.error !== null) {
            console.error(validationResult.error);
            throw Error(validationResult.error);
        } else {
            return kinesis.putRecord({
                Data: JSON.stringify(body) /* Strings will be Base-64 encoded on your behalf */, /* required */
                PartitionKey: uuidv4(), /* required */
                StreamName: `bc-kds-${body.bc_customerName}-${body.bc_messageType}` /* required */
            }).promise().then(() => ({
                statusCode: 200,
                body: 'Success'
            })).catch((err) => {
                console.error(err.message);
                throw new Error(err.message);
            });
        }
    }
};
