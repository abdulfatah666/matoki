const serverless = require('serverless-http');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { HttpError, InternalServerError, BadRequestError } = require('@bridgecrew/nodeUtils/errors/http');
const RemoteLambda = require('@bridgecrew/nodeUtils/remoteLambda/invoke');

const customerApiRemoteLambda = new RemoteLambda(process.env.CUSTOMERS_API_LAMBDA_NAME);
const snapshotsService = require('./snapshotsService');

const snapshotServiceInstance = snapshotsService.getInstance();

const reportsLambdaName = process.env.REPORTS_LAMBDA_NAME;

const app = express();
const ALL = 'ALL';

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    if (req.method === 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});

app.get('/api/v1/snapshots/:awsAccountId/summary', async (req, res, next) => {
    try {
        const decodeAccountId = decodeURIComponent(req.params.awsAccountId);
        const { date } = req.event.queryStringParameters;
        let customerAccounts = [];
        if (decodeAccountId === ALL) customerAccounts = await customerApiRemoteLambda.invoke('getCustomerAccountIdsByName', { name: req.userDetails.customers[0] });
        else customerAccounts.push(decodeAccountId);
        const summary = await snapshotServiceInstance.getSummaryFromDate({ customerAccounts, date });
        res.status(200).json({
            data: summary
        });
    } catch (err) {
        console.error(err);
        next(new BadRequestError('Failed to fetch summary'));
    }
});

app.get('/api/v1/snapshots/:awsAccountId/delta', async (req, res, next) => {
    let delta;
    try {
        const decodeAccountId = decodeURIComponent(req.params.awsAccountId);
        const snapshotDate = req.event.queryStringParameters.snapshot1_date;
        let customerAccounts = [];
        if (decodeAccountId === ALL) customerAccounts = await customerApiRemoteLambda.invoke('getCustomerAccountIdsByName', { name: req.userDetails.customers[0] });
        else customerAccounts.push(decodeAccountId);
        delta = await snapshotServiceInstance.getDeltaFromSnapshots({ snapshotDate, customerAccounts });
        res.status(200).json({
            data: delta
        });
    } catch (err) {
        console.error(err);
        next(new BadRequestError('Failed to fetch delta'));
    }
});

app.get('/api/v1/snapshots/:awsAccountId/period', async (req, res, next) => {
    let period = [];
    try {
        const decodeAccountId = decodeURIComponent(req.params.awsAccountId);
        const { startDate, endDate, interval } = req.event.queryStringParameters;
        let customerAccounts = [];
        if (decodeAccountId === ALL) customerAccounts = await customerApiRemoteLambda.invoke('getCustomerAccountIdsByName', { name: req.userDetails.customers[0] });
        else customerAccounts.push(decodeAccountId);
        if (customerAccounts.length > 0) {
            period = await snapshotServiceInstance.getPeriodFromDate({
                startDate, endDate, interval, customerAccounts
            });
        }
        res.status(200).json({
            data: period
        });
    } catch (err) {
        console.error(err);
        next(new BadRequestError('Failed to fetch period'));
    }
});

app.post('/api/v1/snapshots', async (req, res, next) => {
    try {
        if (req.body.accountId) {
            const { accountId } = req.params;
            const result = await snapshotServiceInstance.create({ accountId, customers: req.userDetails.customers });
            res.status(201);
            res.json({
                data: {
                    count: result.length,
                    objects: result
                }
            });
        } else {
            next(new BadRequestError('Params or type parameter does not exist'));
        }
    } catch (err) {
        next(err);
    }
});

app.get('/api/v1/snapshots/:awsAccountId/dashboardExport', async (req, res, next) => {
    const reportsRemoteLambda = new RemoteLambda(reportsLambdaName);
    try {
        const decodeAccountId = decodeURIComponent(req.params.awsAccountId);
        const { interval } = req.event.queryStringParameters;
        const token = req.headers.authorization;
        const pdf = await reportsRemoteLambda.invoke('getDashboard', { accountId: decodeAccountId, interval, token });
        res.status(200);
        res.json({ data: pdf });
    } catch (e) {
        next(e);
    }
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
    if (!(err instanceof HttpError)) {
        /* eslint-disable-next-line no-param-reassign */
        err = new InternalServerError(err.message);
    }
    console.error(err.message, err.stack);

    res.status(err.statusCode).json({
        message: err.message
    });
});

function invokeService(funcName, body) {
    if (!(snapshotServiceInstance[funcName] instanceof Function)) {
        throw new BadRequestError('Function does not exist');
    } else {
        return snapshotServiceInstance[funcName](body);
    }
}
/* eslint-disable-next-line consistent-return */
exports.handler = (event, context) => {
    if (event.path.startsWith('/invoke')) {
        const matchPattern = event.path.match(/^\/invoke\/([^/]+)(?:\/([^/]+))?$/);
        if (matchPattern.length > 2 && matchPattern[2] !== undefined) {
            if (matchPattern[1] === 'service') {
                return invokeService(matchPattern[2], event.body);
            }
        } else if (matchPattern.length > 2 && matchPattern[2] === undefined) {
            return invokeService(matchPattern[1], event.body);
        } else {
            throw new Error('The invoke not valid');
        }
    } else {
        return serverless(app, {
            request(req, e, ctx) {
                req.event = e;
                req.context = ctx;

                if (req.event.requestContext.authorizer) {
                    req.userDetails = req.event.requestContext.authorizer;
                    req.userDetails.customers = req.event.requestContext.authorizer.customers.split(',');
                }
            },
            binary: false
        })(event, context);
    }
};
