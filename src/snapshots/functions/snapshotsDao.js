const AWS = require('aws-sdk/index');

const SNAPSHOTS_TABLE = process.env.SNAPSHOTS_TABLE_NAME;
const ComplianceSnapshot = require('../models/ComplianceSnapshot');

const INDEX_NAME_ACCOUNT_ID_SCAN_DATE = 'account_id-scan_date-index';

class SnapshotsDao {
    constructor() {
        this.documentClient = new AWS.DynamoDB.DocumentClient();
    }

    async getLatestScanTimeForDay(accountId, date) {
        const startDate = new Date(Number(date)).setHours(0, 0, 0, 0).toString();
        const endDate = new Date(Number(date)).setHours(23, 59, 59, 999).toString();
        console.log('startDate:', new Date(Number(startDate)).toISOString());
        console.log('endDate:', new Date(Number(endDate)).toISOString());

        const dbParams = {
            TableName: SNAPSHOTS_TABLE,
            IndexName: INDEX_NAME_ACCOUNT_ID_SCAN_DATE,
            KeyConditionExpression: 'account_id = :accountId AND scan_date BETWEEN :startDate AND :endDate',
            ExpressionAttributeValues: {
                ':accountId': accountId,
                ':startDate': startDate,
                ':endDate': endDate
            },
            ProjectionExpression: 'scan_date',
            ScanIndexForward: false,
            Limit: 1
        };

        const maxScanDate = await this.documentClient.query(dbParams).promise().then((data) => {
            const result = data.Items.length === 0 ? undefined : data.Items[0].scan_date;
            console.log('Max scan date:', result ? new Date(Number(result)).toISOString() : result);
            return result;
        }).catch((err) => {
            console.error(err);
            throw new Error('Failed to get date time for latest scan of day');
        });

        return maxScanDate;
    }

    async getLatestSummaryOfDate(accountId, date) {
        try {
            // getting the scan date that is the closest to the received date.
            const maxScanDate = await this.getLatestScanTimeForDay(accountId, date);

            if (maxScanDate) {
                // Fetching the snapshot records of the latest scan date
                const dbParams = {
                    TableName: SNAPSHOTS_TABLE,
                    IndexName: INDEX_NAME_ACCOUNT_ID_SCAN_DATE,
                    KeyConditionExpression: 'account_id = :accountId AND scan_date = :maxScanDate',
                    ExpressionAttributeValues: {
                        ':accountId': accountId,
                        ':maxScanDate': maxScanDate
                    }
                };

                return (await this.documentClient.query(dbParams).promise().then((data) => data.Items.map((obj) => ComplianceSnapshot.fromDynamoDbItem(obj))).catch((error) => {
                    console.error(error);
                    throw new Error('Failed to extract scan data');
                }));
            }
            return [];
        } catch (exception) {
            console.error(exception);
            throw new Error('Failed to extract scan data');
        }
    }

    async getLatestScanTime(accountId) {
        const endDate = Date.now().toString();
        const dbParams = {
            TableName: SNAPSHOTS_TABLE,
            IndexName: INDEX_NAME_ACCOUNT_ID_SCAN_DATE,
            KeyConditionExpression: 'account_id = :accountId AND scan_date <= :endDate',
            ExpressionAttributeValues: {
                ':accountId': accountId,
                ':endDate': endDate
            },
            ProjectionExpression: 'scan_date',
            ScanIndexForward: false,
            Limit: 1
        };

        const maxScanDate = await this.documentClient.query(dbParams).promise().then((data) => {
            const result = data.Items.length === 0 ? undefined : data.Items[0].scan_date;
            console.log(result);
            return result;
        }).catch((err) => {
            console.error(err);
            throw new Error('Failed to get date time for latest scan of day');
        });

        return maxScanDate;
    }

    async getLatestScan(accountId) {
        const latestScanTime = await this.getLatestScanTime(accountId);

        if (latestScanTime) {
            // Fetching the snapshot records of the latest scan date
            const dbParams = {
                TableName: SNAPSHOTS_TABLE,
                IndexName: INDEX_NAME_ACCOUNT_ID_SCAN_DATE,
                KeyConditionExpression: 'account_id = :accountId AND scan_date = :maxScanDate',
                ExpressionAttributeValues: {
                    ':accountId': accountId,
                    ':maxScanDate': latestScanTime
                }
            };

            return this.documentClient.query(dbParams).promise().then((data) => data.Items.map((obj) => ComplianceSnapshot.fromDynamoDbItem(obj))).catch((error) => {
                console.error(error);
                throw new Error('Failed to extract scan data');
            });
        }
        return ([]);
    }

    async create(snapshotModel) {
        try {
            return await this.documentClient.put({
                TableName: SNAPSHOTS_TABLE,
                IndexName: INDEX_NAME_ACCOUNT_ID_SCAN_DATE,
                Item: snapshotModel.toDynamoDbItem()
            }).promise();
        } catch (e) {
            console.error(`Failed to save snapshot to the db with object ${JSON.stringify(snapshotModel)}`);
            throw e;
        }
    }
}

let instance;

const getInstance = () => {
    if (!instance) {
        instance = new SnapshotsDao();
    }
    return instance;
};

module.exports = { getInstance };
