/* eslint-disable class-methods-use-this,no-restricted-syntax */
const RemoteLambda = require('@bridgecrew/nodeUtils/remoteLambda/invoke');
const { VIOLATION_STATUSES } = require('@bridgecrew/nodeUtils/models/Enums');
const snapshotsDao = require('./snapshotsDao');
const ComplianceSnapshot = require('../models/ComplianceSnapshot');

const compliancesApiRemoteLambda = new RemoteLambda(process.env.COMPLIANCES_API_LAMBDA_NAME);
const customerApiLambda = new RemoteLambda(process.env.CUSTOMERS_API_LAMBDA_NAME);

const snapshotDaoInstance = snapshotsDao.getInstance();

const skip = ['snapshotId', 'category', 'scanDate', 'accountId'];
const violationsStatusesToFetch = [VIOLATION_STATUSES.COMPLIANT, VIOLATION_STATUSES.REMEDIATED, VIOLATION_STATUSES.SUPPRESSED, VIOLATION_STATUSES.CLOSED, VIOLATION_STATUSES.OPEN];
const NO_RESULT = undefined;
const TOTAL = 'total';
const categoriesToFilter = ['GUARDRAIL', 'undefined'];

class SnapshotsService {
    _sumAccountTotal(accountData) {
        const total = { category: TOTAL };
        const keys = Object.keys(accountData[0]);
        for (const item of accountData) {
            for (const key of keys) {
                if (!skip.includes(key)) total[key] = total[key] ? total[key] + item[key] : item[key];
            }
        }
        // total object is first in array for ui purposes
        accountData.unshift(total);
        return accountData;
    }

    // in case of request for summary of all aws accounts, each account will end with a snapshot array [{category:'Serverless' ....}, ...]
    // in order to return one summed answer we should sum up all the accounts arrays into one array of data objects
    _sumAllAccountsData(summary) {
        const mergedData = {};
        for (const accountData of summary) {
            for (let i = 0; i < accountData.length; i++) {
                const { category } = accountData[i];
                if (!mergedData[category]) mergedData[category] = { ...accountData[i] };
                else {
                    const keys = Object.keys(mergedData[category]);
                    for (const key of keys) {
                        if (!skip.includes(key)) mergedData[category][key] += accountData[i][key];
                    }
                }
            }
        }
        return Object.values(mergedData);
    }

    async getSummaryFromDate({ customerAccounts, date }) {
        const data = [];
        for (const account of customerAccounts) {
            let latestSummary = await snapshotDaoInstance.getLatestSummaryOfDate(account, date);
            if (latestSummary.length === 0) latestSummary = await snapshotDaoInstance.getLatestScan(account);
            if (latestSummary.length > 0) data.push(this._sumAccountTotal(latestSummary));
        }
        return this._sumAllAccountsData(data);
    }

    _convertToLabeledObject(snapshot) {
        const snap = {};
        if (snapshot) snapshot.forEach((a) => { snap[a.category] = a.toDynamoDbItem(); });
        return snap;
    }

    _calcDelta(snap1, snap2) {
        const ans = {};
        const categories = Object.keys(snap1);
        for (const category of categories) {
            if (snap1[category] && snap2[category]) {
                if (snap1[category].open === 0) ans[category] = NO_RESULT;
                else ans[category] = ((snap2[category].open - snap1[category].open) / snap1[category].open) * 100;
            }
        }
        return ans;
    }

    _sumAccountsSnapshots(arr) {
        const sum = {};
        for (const snapshot of arr) {
            const categories = Object.keys(snapshot);
            for (const category of categories) {
                if (!sum[category]) sum[category] = { open: snapshot[category].open };
                else sum[category].open += snapshot[category].open;
            }
        }
        return sum;
    }

    async getDeltaFromSnapshots({ snapshotDate, customerAccounts }) {
        const snap1arr = []; const snap2arr = [];
        for (const account of customerAccounts) {
            const [snapshot1, snapshot2] = await Promise.all([snapshotDaoInstance.getLatestSummaryOfDate(account, snapshotDate), snapshotDaoInstance.getLatestScan(account)]);
            snap1arr.push(this._convertToLabeledObject(snapshot1));
            snap2arr.push(this._convertToLabeledObject(snapshot2));
        }
        return this._calcDelta(this._sumAccountsSnapshots(snap1arr), this._sumAccountsSnapshots(snap2arr));
    }

    _createDatesArray(startDate, endDate, interval) {
        let current = startDate;
        const periods = [];
        const period = Math.floor((endDate - startDate) / interval);
        while (current + period <= endDate) {
            current += period;
            periods.push(current);
        }
        return periods;
    }

    _sumGraphResults(periods, periodData) {
        const PASSED = ['remediated', 'compliant', 'prevented'];
        const FAILED = ['open', 'acknowledged'];
        const summedData = [];
        for (const period of periods) {
            const allAccountsSnapshots = periodData[period];
            const tempSummedData = { [period]: { total: { passed: 0, failed: 0 } } };
            for (const accountSnapshot of allAccountsSnapshots) {
                for (const categoryObject of accountSnapshot) {
                    const currentCategory = categoryObject.category;
                    if (!tempSummedData[period][currentCategory]) tempSummedData[period][currentCategory] = { passed: 0, failed: 0 };
                    const keys = Object.keys(categoryObject);
                    for (const key of keys) {
                        if (PASSED.includes(key)) {
                            tempSummedData[period][currentCategory].passed += categoryObject[key];
                            tempSummedData[period].total.passed += categoryObject[key];
                        }
                        if (FAILED.includes(key)) {
                            tempSummedData[period][currentCategory].failed += categoryObject[key];
                            tempSummedData[period].total.failed += categoryObject[key];
                        }
                    }
                }
            }
            summedData.push(tempSummedData);
        }
        return summedData;
    }

    async getPeriodFromDate({ startDate, endDate, interval, customerAccounts }) {
        // check the last scan date for the first account of the customer
        // (there is no difference between accounts since all scans for all accounts running in parallel)
        const lastScanDate = await snapshotDaoInstance.getLatestScanTime(customerAccounts[0]);
        const tmpEndDate = Math.min(endDate, lastScanDate);
        const periods = this._createDatesArray(Number(startDate), Number(tmpEndDate), interval);
        const periodsToShow = [];
        const periodData = {};

        for (const period of periods) {
            const result = await Promise.all(customerAccounts.map((account) => snapshotDaoInstance.getLatestSummaryOfDate(account, period)));
            console.log('this is the promise result for current period: ', result);

            if (!result.includes(null)) {
                periodData[period] = result;
                periodsToShow.push(period);
            }
        }

        console.log('This is the period list to show: ', periodsToShow);
        return this._sumGraphResults(periodsToShow, periodData);
    }

    async duplicateLastSnapshot({ customerName }) {
        const accountsDetails = await customerApiLambda.invoke('getTenantEnvironmentDetailByCustomerName', { name: customerName });
        const accountIds = accountsDetails.map(accountDetails => accountDetails.aws_account_id);
        const latestScans = await Promise.all(accountIds.map(accountId => snapshotDaoInstance.getLatestScan(accountId)));
        const scanDate = new Date().getTime();
        await Promise.all(latestScans.map((scan, i) => Promise.all(scan.map(category => snapshotDaoInstance.create(Object.assign(category, {
            scanDate: scanDate.toString(),
            snapshotId: `${accountIds[i]}::${category.category}::${new Date(scanDate).toISOString()}`
        }))))));
    }

    async create({ accountId }) {
        try {
            const categoryToViolations = await compliancesApiRemoteLambda.invoke('violations/categorytoviolation');
            const filteredCategoryToViolation = Object.keys(categoryToViolations).filter((category) => !categoriesToFilter.includes(category)).reduce((obj, key) => {
                /* eslint-disable-next-line no-param-reassign */
                obj[key] = categoryToViolations[key];
                return obj;
            }, {});
            const scanDate = new Date().getTime();
            const complianceSnapshots = await Promise.all(Object.keys(filteredCategoryToViolation).map(async (category) => {
                const complianceSnapshot = new ComplianceSnapshot({ accountId, category, scanDate });
                const violationByCategory = filteredCategoryToViolation[category];
                // Loop over all the violations and get the resources for each violation
                await Promise.all(violationByCategory.map(async (violation) => {
                    for (const status of violationsStatusesToFetch) {
                        const resources = await compliancesApiRemoteLambda.invoke('violations/getResourcesByAccountId', {
                            violationId: violation,
                            violationDetails: accountId,
                            query: { status }
                        });
                        // For each resource update the status for compliance snapshot
                        resources.filter((resource) => resource.awsAccountId === accountId).forEach((resource) => {
                            const resourceStatus = resource.status.toString().toUpperCase();
                            complianceSnapshot.updateStatus(resourceStatus);
                        });
                    }
                }));
                return complianceSnapshot;
            }));
            // Save to DB
            return await Promise.all(complianceSnapshots.map(async (snapshot) => {
                await snapshotDaoInstance.create(snapshot);
                return snapshot;
            }));
        } catch (e) {
            console.log(e.message);
            throw e;
        }
    }
}

let instance;

const getInstance = () => {
    if (!instance) {
        instance = new SnapshotsService();
    }
    return instance;
};

module.exports = { getInstance };
