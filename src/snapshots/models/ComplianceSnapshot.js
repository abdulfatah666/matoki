class ComplianceSnapshot {
    constructor({
        accountId, category, scanDate, open = 0, acknowledged = 0, compliant = 0, prevented = 0, remediated = 0, suppressed = 0
    }) {
        this.accountId = accountId;
        this.category = category;
        this.scanDate = scanDate;
        this.open = open;
        this.acknowledged = acknowledged;
        this.compliant = compliant;
        this.prevented = prevented;
        this.remediated = remediated;
        this.suppressed = suppressed;
        this.snapshotId = `${accountId}::${category}::${new Date(Number(this.scanDate)).toISOString()}`;
    }

    static fromDynamoDbItem(item) {
        return new ComplianceSnapshot({
            accountId: item.account_id,
            category: item.category,
            scanDate: item.scan_date,
            open: item.open,
            acknowledged: item.acknowledged,
            compliant: item.compliant,
            prevented: item.prevented,
            remediated: item.remediated,
            suppressed: item.suppressed
        });
    }

    toDynamoDbItem() {
        return {
            snapshot_id: this.snapshotId,
            account_id: this.accountId,
            category: this.category,
            scan_date: this.scanDate.toString(),
            open: this.open,
            acknowledged: this.acknowledged,
            compliant: this.compliant,
            prevented: this.prevented,
            remediated: this.remediated,
            suppressed: this.suppressed
        };
    }

    updateStatus(status) {
        switch (status.toUpperCase()) {
            case 'OPEN':
                this.open += 1;
                break;
            case 'REMEDIATED':
                this.remediated += 1;
                break;
            case 'SUPPRESSED':
                this.suppressed += 1;
                break;
            case 'COMPLIANT':
                this.compliant += 1;
                break;
            case 'PREVENTED':
                this.prevented += 1;
                break;
            case 'ACKNOWLEDGED':
                this.acknowledged += 1;
                break;
            default:
                console.warn(`status ${status} is not define for snapshot`);
        }
    }
}
module.exports = ComplianceSnapshot;
