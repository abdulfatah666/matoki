Monitoring
======

Overview
------
Alarm and monitoring systems are a key part of mature products and applications.    
If you worry about your customers, it’s better to be notified when something goes wrong
  
The resources to monitor: 
* elasticsearch service
* api gateway
* lambda error logs
* sqs audit
* sns audit
* cognito audit


Context
----
Most of the logs pass to cloudwatch, as a result choose this service to manage the logs. 

short description about cloudwatch:

cloudwatch split to 4 parts:
1. log groups
2. metrics
3. alarms
4. dashboards

##### log groups  
The Lambda and Elasticsearch write the logs to the log groups,
log groups include raw text, in order to track the data we must to 
filter the data and create metrics.
Example filter: 
"[ERROR]" filter the error logs
  
#### metrics
A metric represents a time-ordered set of data points that are published to CloudWatch

#### alarms
An alarm watches a single metric over a specified time period.
in addition, alarm can send notification about changes to the SNS Message


Flow
-----
1. Data captured by the cloudwatch metric
2. Cloudwatch metric is monitor by the alarm
3. when the alarm change the status
4. trigger the sns topic
5. the slack lambda was subscription to the sns topic 
6. trigger lambda
7. lambda send to the slack channel message
 

Critical Metrics
------
* lambda error logs                                                         // Base Stack
* api gateway -> By Stage -> bridgecrew-api -> 5XXError 4XXError (Sum 2XX)  // Base Stack
* SNS -> Topic Metrics -> NumberOfNotificationsFailed                       // Base + Customer Stack
* Elasticserach error logs                                                  // Customer Stack
* Elasticsearch recommended                                                 // Customer Stack https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/cloudwatch-alarms.html 
* s3 public                                                                 // Base + Customer Stack

Diagram
-----

![alt text](monitor_architecture.png "Monitor Architecture")


Slack integration
-------
The slack integration based on the article:    
[how to send aws cloudwatch alarms to slack](https://dev.to/alex_barashkov/how-to-send-aws-cloudwatch-alarms-to-slack-596e)

The git project:
[https://github.com/assertible/lambda-cloudwatch-slack](https://github.com/assertible/lambda-cloudwatch-slack)


The slack plugin: 
[https://api.slack.com/incoming-webhooks](https://api.slack.com/incoming-webhooks)

Slack message example:   
<img src="./cloudwatch-slack-example.png" width="700">

Example
------------
```
// ------ Monitor ------
data "aws_sns_topic" "sns_topic_monitor_error" {
  name = "${var.sns_monitor_topic_error_name}"
}

resource "aws_cloudwatch_metric_alarm" "create_alarm_bucket_changes" {
  alarm_name                = "app_s3_bucket_size_changes_${local.bucket_name}"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "20000000"
  metric_name               = "BucketSizeBytes"
  namespace                 = "AWS/S3"
  dimensions                = {
    StorageType = "StandardStorage"
    BucketName = "${local.bucket_name}"
  }
  period                    = "86400"
  statistic                 = "Sum"
  threshold                 = "10"
  alarm_description         = "This metric monitors s3 bucket size changes"
  alarm_actions     = [
    "${data.aws_sns_topic.sns_topic_monitor_error.arn}"
  ]
}
// -------------------
```