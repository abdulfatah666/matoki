data "aws_caller_identity" "current" {}

module "cloudwatch_slack_sns_topic_error" {
  source                               = "../../utils/terraform/sns"
  aws_profile                          = var.aws_profile
  region                               = var.region
  sns_topic_name                       = var.sns_monitor_topic_name_error
  unique_tag                           = var.unique_tag
  subscribed_lambda_functions_arn_list = [var.slack_alarm_lambda_arn]
}

resource "aws_lambda_permission" "slack_function_with_sns" {
  statement_id  = "AllowExecutionFromSNS_${var.unique_tag}_${var.region}"
  action        = "lambda:InvokeFunction"
  function_name = var.slack_alarm_lambda_arn
  principal     = "sns.amazonaws.com"
  source_arn    = module.cloudwatch_slack_sns_topic_error.sns_topic_arn
}

data "aws_arn" "parse_sns_topic_arn_error" {
  arn = module.cloudwatch_slack_sns_topic_error.sns_topic_arn
}
