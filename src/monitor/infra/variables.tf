variable "region" {
  type        = "string"
  description = "the aws region"
}

variable "aws_profile" {
  type = "string"
}

variable "slack_hook_url" {
  type = "string"
}

variable "sns_monitor_topic_name_error" {
  type = "string"
}

variable "slack_alarm_lambda_arn" {
  type = string
}

variable "unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
}

variable "sls_bucket_name" {
  description = "A name for deployment bucket that all serverless function deployment state is saved on"
  type        = "string"
}
