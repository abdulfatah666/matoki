variable "aws_profile" {
  type        = string
  description = "The profile used to deploy the scanner lambdas"
}

variable "region" {
  type        = string
  description = "The base stack's main region"
}

variable "unique_tag" {
  type        = string
  description = "The base stack's unique tag"
}

variable "artifact_bucket" {
  type        = string
  description = "The bucket which stores the state files"
}

variable "github_lambda_arn" {
  type        = string
  description = "The arn of the GitHub manager lambda"
}

variable "customers_api_lambda_arn" {
  type        = string
  description = "The ARN of the customers-api lambda"
}

variable "incidents_api_lambda_arn" {
  type        = string
  description = "The ARN of the incidents-api lambda"
}

variable "suppressions_api_lambda_arn" {
  type        = string
  description = "The ARN of the suppress-api function"
}

variable "path_to_sls_file" {
  type        = string
  description = "The path to the scanner's sls file"
}

variable "monitor" {
  type = "map"

  default = {
    enable               = false
    sns_topic_error_name = ""
  }
}
