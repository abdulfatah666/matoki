output "scanners_step_function_arn" {
  value = aws_sfn_state_machine.scanners_sfn.id
}

output "scanners_bucket_name" {
  value = aws_s3_bucket.scanner_results.bucket
}

output "scanner_kms_key" {
  value = aws_kms_key.scanner_key.arn
}