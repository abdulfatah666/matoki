/* eslint-disable import/no-dynamic-require,global-require */
const fs = require('fs');

const BUILDTIME_SCANNERS_DIR = './buildtimeScanners/stepFunctions';
const RUNTIME_SCANNERS_DIR = './runtimeScanners/stepFunctions';

const MAIN_TEMPLATE_FILE_NAME = './scannerSfDefinitionTemplate.json';
const BUILDTIME_TEMPLATE_FILE_NAME = './buildtimeScanners/scannersTemplate.json';
const RUNTIME_TEMPLATE_FILE_NAME = './runtimeScanners/scannersTemplate.json';

const MAIN_SF_TEMPLATE = JSON.parse(fs.readFileSync(MAIN_TEMPLATE_FILE_NAME, 'utf8'));
const BUILDTIME_SF_TEMPLATE = JSON.parse(fs.readFileSync(BUILDTIME_TEMPLATE_FILE_NAME, 'utf8'));
const RUNTIME_SF_TEMPLATE = JSON.parse(fs.readFileSync(RUNTIME_TEMPLATE_FILE_NAME, 'utf8'));

RUNTIME_SF_TEMPLATE.States.runtimeScanners.Iterator.States.runtimeScannersParallel.Branches = fs.readdirSync(RUNTIME_SCANNERS_DIR).map(scanner => (require(`${RUNTIME_SCANNERS_DIR}/${scanner}`)));
BUILDTIME_SF_TEMPLATE.States.buildtimeScannersParallel.Branches = fs.readdirSync(BUILDTIME_SCANNERS_DIR).map(scanner => (require(`${BUILDTIME_SCANNERS_DIR}/${scanner}`)));
MAIN_SF_TEMPLATE.States.ParallelScanTypes.Branches = [BUILDTIME_SF_TEMPLATE, RUNTIME_SF_TEMPLATE];
console.log(JSON.stringify({ template: JSON.stringify(MAIN_SF_TEMPLATE) }));