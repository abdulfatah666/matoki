# Scanners Runner module
## Introduction
This step function intended to consolidate all of our scanners processes, 
in order to achieve the goal of sync the data that comes from the scanners 
to create snapshots for dashboard with consistent data.
By syncing all the scanners and parallelize them we achieve 
normalize and consistent result from all scanners at a specific time 
so the snapshot can relay on a consistent set of data.

## Scanner Type
Scanners are divided first by their type, which is either Runtime or Buildtime.
For example, Checkov is a buildtime scanner because it scans IaC while Prowler is a 
runtime scanner because it scans already-deployed AWS infrastructure.

## Scanner Step Function Structure
The main scanner step function built out of 3 parts:
1. Prepare - consists of the steps:
    * getAWSAccounts - takes the customerName from the execution input and returns an array with all the accounts ids of the customer
    * getRoleDetails - for each account id of the customer returns and object with the details needed to launch the scanners (cross account role, etc...)
2. Main Scanners Run - this is the part where scanners actually run in parallel for each account id of the customer.
    * setup - prepare the environment for the specific scanner to run
    * run - running the scanner and saving results to S3
    * parse - reading the results from the s3 bucket and parse it to bc-violations and persist 
3. Finish - consist of the step:
    * createSnapshotForAccount - running parallel for each account id of the customer and creates snapshot for the details just gathered with the scanners 

## Adding a New Scanner's Infra
In order to add new scanner to the step function 
and sync it with all of our existing scanners, do the following:
1. Identify the scanner's type (denote `scanner_type`).
2. Create a JSON file (denote `scanners.json`) defining the scanner as an AWS Step Function (steps - setup-run-parse).
3. Place the above file in `scanners/infra/base/<scanner_type>Scanners/stepFunctions/scanners.json`
4. Create in `scanners/infra/base/<scanner_type>` a directory containing all of the Terraform files needed for your scanner's deployment.
5. Create in `scanners/functions` the directory of the scanner's implementation, and attach it to a function of the scanners `serverless.yml`
6. If the scanner is of runtime type, design your input to be formatted as follows:    
    ```
    {
      "customerName": "orsf3",
      "accounts": [ "809694787632", "090772183824" ],
      "details": [
        {
          "AccountId": "090772183824",
          "RoleArn": "arn:aws:iam::090772183824:role/orsf3-bc-bridgecrewcwssarole",
          "ExternalId": "123456"        
        },
        {
          "AccountId": "809694787632",
          "RoleArn": "arn:aws:iam::809694787632:role/orsf3-bc-bridgecrewcwssarole",
          "ExternalId": "61ME8K"
        }
        ... (object for each account id)
      ]
    }
    ``` 
    The output of the scanner function is not required since the results 
    should be written to the s3 bucket (scanner-results bucket).
6. If the scanner is of buildtime type, provide the customer's name (like a runtime scanner),
and any additional required data.
### Infrastructure Folder Structure 
![FileStructure](FileStructure.png)
1. scanners:
    * create new directory with the name of the scanner you're creating
    * put in the new directory all the terraform and docker files related to your scanner 
2. stepFunctionTemplates:
    * create new json file with the name of the scanner you're creating 
    * this json should be valid aws step function json of kind:
        ```
            {
              "StartAt": "setupCloudmapper",
              "States": {
                    "setup<SCANNER_NAME>: {...},
                    "run<SCANNER_NAME>: {...},
                    "parse<SCANNER_NAME>: {...}
              }
            } 
        ```  
3. taskDefinitions:
    * create new json file with the name of the scanner you're creating
    * this json should be valid aws task definition json with the required parameters for you task to run 

## Main Step Function Diagram: 
![Architecture](ScannerSfImg.png)