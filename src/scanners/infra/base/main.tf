locals {
  scanner_lambda_names = [
    "${module.consts.scanners_module_name}-customer-role-${var.unique_tag}",
    "${module.consts.scanners_module_name}-get-prowler-checks-${var.unique_tag}",
    "${module.consts.scanners_module_name}-prowler-parser-${var.unique_tag}",
    "${module.consts.scanners_module_name}-cloudmapper-${var.unique_tag}",
    "${module.consts.scanners_module_name}-cloudmapper-parser-${var.unique_tag}",
    "${module.consts.scanners_module_name}-checkov-scanner-${var.unique_tag}",
    "${module.consts.scanners_module_name}-checkov-parser-${var.unique_tag}",

  ]
  prepare_lambda_names = [
    "customers-api-${var.unique_tag}",
    "${module.consts.github_module_name}-mgr-api-${var.unique_tag}"
  ]

  process_scans_data_lambda_names = [
    "bc-snapshots-${var.unique_tag}",
    "bc-reports-${var.unique_tag}",
    "bc-compliances-benchmarks-api-${var.unique_tag}",
    "bc-authorization-authorizer-${var.unique_tag}",
    "bc-compliances-remediations-buildtime-${var.unique_tag}"

  ]
}

data "aws_caller_identity" "current" {}

module "consts" {
  source = "../../../utils/terraform/consts"
}

module "scanner_lambdas" {
  source                 = "../../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  path_to_serverless_yml = var.path_to_sls_file
  region                 = var.region
  sls_bucket_name        = var.artifact_bucket
  unique_tag             = var.unique_tag
  variable_string        = "--customers-api-lambda ${var.customers_api_lambda_arn} --result-bucket ${aws_s3_bucket.scanner_results.bucket} --incident-api-lambda ${var.incidents_api_lambda_arn} --github-manager-lambda ${var.github_lambda_arn} --suppression-api-lambda ${var.suppressions_api_lambda_arn}"
  lambda_names           = local.scanner_lambda_names

}

resource "aws_ecs_cluster" "scanner_cluster" {
  name = "bc-scanners-${var.unique_tag}"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "null_resource" "stop_running_tasks" {
  provisioner "local-exec" {
    when    = "destroy"
    command = "aws --profile ${var.aws_profile} --region ${var.region} ecs list-tasks --cluster ${aws_ecs_cluster.scanner_cluster.name} --desired-status RUNNING | jq '.taskArns[]' -r | awk '{print \"aws --profile ${var.aws_profile} --region ${var.region} ecs stop-task --cluster ${aws_ecs_cluster.scanner_cluster.name} --task \" $0}' | sh"
  }

  depends_on = [aws_ecs_cluster.scanner_cluster]
}

resource "aws_vpc" "scanner_task_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "bc-scanner-vpc-${var.unique_tag}"
  }
}

resource "aws_internet_gateway" "scanner_vpc_igw" {
  vpc_id = aws_vpc.scanner_task_vpc.id

  tags = {
    Name = "bc-scanner-igw-${var.unique_tag}"
  }
}

resource "aws_security_group" "scanner_tasks_sg" {
  name   = "bc-scanner-tasks-${var.unique_tag}"
  vpc_id = aws_vpc.scanner_task_vpc.id

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "bc-scanner-tasks-${var.unique_tag}"
  }
}

resource "aws_route_table" "scanner_rtb" {
  vpc_id = aws_vpc.scanner_task_vpc.id

  route {
    gateway_id = aws_internet_gateway.scanner_vpc_igw.id
    cidr_block = "0.0.0.0/0"
  }

  tags = {
    Name = "bc-scanners-route-table-${var.unique_tag}"
  }
}

resource "aws_route_table_association" "scanner_rtb_subnet_association" {
  route_table_id = aws_route_table.scanner_rtb.id
  subnet_id      = aws_subnet.scanner_tasks_subnet.id
}

resource "aws_subnet" "scanner_tasks_subnet" {
  cidr_block              = "10.0.1.0/24"
  vpc_id                  = aws_vpc.scanner_task_vpc.id
  map_public_ip_on_launch = true

  tags = {
    Name = "bc-scanner-public-${var.unique_tag}"
  }
}

resource "aws_s3_bucket" "scanner_results" {
  bucket        = "bc-scanner-results-${data.aws_caller_identity.current.account_id}-${var.unique_tag}"
  force_destroy = "true"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = aws_kms_alias.scanner_key_alias.id
      }
    }
  }

  lifecycle_rule {
    id      = "CheckovDeleteRule"
    enabled = true
    prefix  = module.consts.checkov_scanner_prefix_name

    tags = {
      rule = "CheckovDeleteRule"
    }
    expiration {
      days = 2
    }
  }
}

resource "aws_s3_bucket_policy" "scanner_results_bucket_policy" {
  bucket = aws_s3_bucket.scanner_results.bucket
  policy = data.aws_iam_policy_document.bucket_policy.json
}

module "prowler_task" {
  source         = "./runtimeScanners/prowler"
  kms_key_arn    = aws_kms_alias.scanner_key_alias.arn
  region         = var.region
  results_bucket = aws_s3_bucket.scanner_results.bucket
  unique_tag     = var.unique_tag
}

module "cloudmapper_task" {
  source         = "./runtimeScanners/cloudmapper"
  kms_key_arn    = aws_kms_alias.scanner_key_alias.arn
  region         = var.region
  results_bucket = aws_s3_bucket.scanner_results.bucket
  unique_tag     = var.unique_tag
}

module "compliance_report_task" {
  source         = "../../../reports/infra/compliance-report"
  kms_key_arn    = aws_kms_alias.scanner_key_alias.arn
  region         = var.region
  results_bucket = aws_s3_bucket.scanner_results.bucket
  unique_tag     = var.unique_tag
}

// waiting for role policy to be created and tale effect
resource "null_resource" "SF_policy_delay" {
  triggers = {
    build         = aws_iam_role_policy.scanner_step_function_execution_policy.id
    step_function = "bc-scanners-sfn-${var.unique_tag}"
  }

  provisioner "local-exec" {
    command = "sleep 10"
  }

}

resource "aws_sfn_state_machine" "scanners_sfn" {
  name       = "${module.consts.bc_scanners_step_function_name}-${var.unique_tag}"
  definition = data.template_file.scanner_step_function_definition.rendered
  role_arn   = aws_iam_role.scanner_step_function_role.arn
  depends_on = [null_resource.SF_policy_delay]
}

data "template_file" "scanner_step_function_definition" {
  template = data.external.scanners_unified_sfn_definition.result.template
  vars = {
    cloudmapper_task_execution_role       = module.cloudmapper_task.cloudmapper_task_role_arn
    cloudmapper_task_definition           = module.cloudmapper_task.cloudmapper_task_definition_arn
    compliance_report_task_execution_role = module.compliance_report_task.compliance_report_task_role_arn
    compliance_report_task_definition     = module.compliance_report_task.compliance_report_task_definition_arn
    prowler_task_execution_role_arn       = module.prowler_task.prowler_task_role_arn
    prowler_task_definition_arn           = module.prowler_task.prowler_task_definition_arn
    ecs_cluster_arn                       = aws_ecs_cluster.scanner_cluster.arn
    subnet_id                             = aws_subnet.scanner_tasks_subnet.id
    security_group_id                     = aws_security_group.scanner_tasks_sg.id
    role_details_with_config_lambda_arn   = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-scanners-cloudmapper-${var.unique_tag}"
    cloudmapper_parser_lambda_arn         = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-scanners-cloudmapper-parser-${var.unique_tag}"
    role_details_lambda_arn               = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-scanners-customer-role-${var.unique_tag}"
    prowler_parser_lambda_arn             = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-scanners-prowler-parser-${var.unique_tag}"
    prowler_checks_lambda_arn             = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-scanners-get-prowler-checks-${var.unique_tag}"
    snapshot_api_lambda_arn               = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-snapshots-${var.unique_tag}"
    customers_api_lambda_arn              = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:customers-api-${var.unique_tag}"
    checkov_parser_lambda_arn             = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-scanners-checkov-parser-${var.unique_tag}"
    checkov_scanner_lambda_arn            = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-scanners-checkov-scanner-${var.unique_tag}"
    github_api_lambda_arn                 = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:${module.consts.github_module_name}-mgr-api-${var.unique_tag}"
    scanners_module_name                  = module.consts.scanners_module_name
    reports_lambda_arn                    = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-reports-${var.unique_tag}"
    benchmarks_api_lambda_arn             = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-compliances-benchmarks-api-${var.unique_tag}"
    authorizer_lambda_arn                 = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-authorization-authorizer-${var.unique_tag}"
    customer_api_lambda_arn               = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:customers-api-${var.unique_tag}"
    remediation_buildtime_lambda_arn      = "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:bc-compliances-remediations-buildtime-${var.unique_tag}"
    scanners_s3_result_bucket             = "bc-scanner-results-${data.aws_caller_identity.current.account_id}-${var.unique_tag}"
  }
}


data "aws_iam_policy_document" "bucket_policy" {
  statement {
    sid       = "Deny unsecure connections"
    actions   = ["s3:*"]
    effect    = "Deny"
    resources = ["${aws_s3_bucket.scanner_results.arn}/*", aws_s3_bucket.scanner_results.arn]
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
    condition {
      test     = "Bool"
      values   = [false]
      variable = "aws:SecureTransport"
    }
  }
}

data "aws_iam_policy_document" "kms_policy_document" {
  statement {
    sid    = "EnableOwnerAccountPermissions"
    effect = "Allow"
    principals {
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
      type        = "AWS"
    }
    actions   = ["kms:*"]
    resources = ["*"]
  }

  statement {
    sid    = "AllowScannerLambdaDecryption"
    effect = "Allow"
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    resources = ["*"]
    condition {
      test     = "StringEquals"
      values   = [data.aws_caller_identity.current.account_id]
      variable = "kms:CallerAccount"
    }
    condition {
      test     = "StringLike"
      values   = ["*bc-scanners*", "*execution-role*", "*bc-github-mgr*", "*bc-compliances-benchmarks*", "*bc-reports*"]
      variable = "aws:PrincipalArn"
    }
  }
}

resource "aws_kms_key" "scanner_key" {
  enable_key_rotation     = true
  description             = "Key for scanner results usage"
  deletion_window_in_days = 7
  policy                  = data.aws_iam_policy_document.kms_policy_document.json
}

resource "aws_kms_alias" "scanner_key_alias" {
  target_key_id = aws_kms_key.scanner_key.id
  name          = "alias/ScannerResultsKey-${var.unique_tag}"
}

data "external" "hash" {
  program     = ["node", "index.js", "${abspath(path.module)}/runtimeScanners/stepFunctions"]
  working_dir = "../../../devTools/hash-dir"
}

data "external" "scanners_unified_sfn_definition" {
  program     = ["node", "index.js"]
  working_dir = path.module
}

resource "aws_iam_role" "scanner_step_function_role" {
  name               = "bc-scanner-step-function-role-${var.unique_tag}"
  assume_role_policy = data.aws_iam_policy_document.scanner_step_function_role_assume_policy.json
}

data "aws_iam_policy_document" "scanner_step_function_role_assume_policy" {
  statement {
    effect = "Allow"
    principals {
      identifiers = ["states.amazonaws.com"]
      type        = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role_policy" "scanner_step_function_execution_policy" {
  name   = "step-function-execution-policy"
  policy = data.aws_iam_policy_document.scanner_step_function_execution_policy.json
  role   = aws_iam_role.scanner_step_function_role.id
}

data "aws_iam_policy_document" "scanner_step_function_execution_policy" {
  statement {
    effect  = "Allow"
    actions = ["ecs:RunTask", "ecs:StopTask", "ecs:DescribeTasks"]
    resources = [
      "*"
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "events:PutTargets",
      "events:PutRule",
      "events:DescribeRule"
    ]
    resources = [
      "arn:aws:events:${var.region}:${data.aws_caller_identity.current.account_id}:rule/StepFunctionsGetEventsForECSTaskRule"
    ]
  }
  statement {
    effect    = "Allow"
    actions   = ["iam:PassRole"]
    resources = [module.cloudmapper_task.cloudmapper_task_role_arn, module.prowler_task.prowler_task_role_arn, module.compliance_report_task.compliance_report_task_role_arn]
  }

  statement {
    effect = "Allow"
    actions = [
      "lambda:InvokeFunction"
    ]
    resources = formatlist("%s%s", "arn:aws:lambda:${var.region}:${data.aws_caller_identity.current.account_id}:function:", concat(local.scanner_lambda_names, local.prepare_lambda_names, local.process_scans_data_lambda_names))
  }
}

data "aws_iam_policy_document" "sfn_cloudwatch_trigger_assume_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["events.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role_policy" "sfn_cloudwatch_trigger_execution_policy" {
  name   = "step-function-trigger-execution"
  policy = data.aws_iam_policy_document.trigger_scanner_step_function.json
  role   = aws_iam_role.step_function_cloudwatch_trigger_role.id
}

data "aws_iam_policy_document" "trigger_scanner_step_function" {
  statement {
    sid       = "AllowStepFunctionTrigger"
    effect    = "Allow"
    actions   = ["states:StartExecution"]
    resources = [aws_sfn_state_machine.scanners_sfn.id]
  }
}

resource "aws_iam_role" "step_function_cloudwatch_trigger_role" {
  name               = "${module.consts.scanners_cloudwatch_stepfunction_trigger_role_name}-${var.unique_tag}"
  assume_role_policy = data.aws_iam_policy_document.sfn_cloudwatch_trigger_assume_policy.json
}

data "aws_sns_topic" "sns_topic_monitor_error" {
  count = var.monitor["enable"] ? 1 : 0
  name  = var.monitor["sns_topic_error_name"]
}

resource "aws_cloudwatch_metric_alarm" "scanner_sfn_alarm" {
  count               = var.monitor["enable"] ? 1 : 0
  alarm_name          = "scanner_step_function_failed_${var.unique_tag}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "ExecutionsFailed"
  namespace           = "AWS/States"

  dimensions = {
    StateMachineArn = aws_sfn_state_machine.scanners_sfn.id
  }

  threshold          = 1
  period             = 300
  statistic          = "Sum"
  alarm_description  = "Identify scanner SF failures"
  alarm_actions      = data.aws_sns_topic.sns_topic_monitor_error.*.arn
  treat_missing_data = "notBreaching"
}
