output "prowler_task_definition_arn" {
  value = "arn:aws:ecs:${var.region}:${data.aws_caller_identity.current.account_id}:task-definition/${aws_ecs_task_definition.prowler_task_definition.family}"

  depends_on = [aws_ecs_task_definition.prowler_task_definition]
}

output "prowler_task_role_arn" {
  value = aws_iam_role.prowler_task_execution_role.arn
}