data "aws_caller_identity" "current" {}

module "consts" {
  source = "../../../../../utils/terraform/consts"
}

resource "aws_cloudwatch_log_group" "prowler_log_group" {
  name = "ecs/prowler-scans-${var.unique_tag}"
}

data "template_file" "prowler_task_definition" {
  template = file("${path.module}/../taskDefinitions/prowler.json")
  vars = {
    log_group = aws_cloudwatch_log_group.prowler_log_group.name
    bucket    = var.results_bucket
  }
}

resource "aws_ecs_task_definition" "prowler_task_definition" {
  container_definitions    = data.template_file.prowler_task_definition.rendered
  family                   = "bc-ecs-prowler-scanner-td-${var.unique_tag}"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.prowler_task_execution_role.arn
  cpu                      = var.unique_tag == "prod" ? 4096 : 1024
  memory                   = var.unique_tag == "prod" ? 8192 : 2048
}

resource "aws_iam_role" "prowler_task_execution_role" {
  name               = "bc-prowler-execution-role-${var.unique_tag}"
  assume_role_policy = data.aws_iam_policy_document.prowler_task_definition_assume_policy.json
}

data "aws_iam_policy_document" "prowler_task_definition_assume_policy" {
  statement {
    effect = "Allow"
    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role_policy" "prowler_task_execution_policy" {
  name   = "prowler-task-execution-policy"
  policy = data.aws_iam_policy_document.prowler_task_write_to_CW.json
  role   = aws_iam_role.prowler_task_execution_role.id
}

data "aws_iam_policy_document" "prowler_task_write_to_CW" {
  statement {
    sid    = "CloudWatchLogsWrite"
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:CreateLogGroup",
      "logs:PutLogEvents"
    ]
    resources = ["arn:aws:logs:us-west-2:${data.aws_caller_identity.current.account_id}:log-group:ecs/prowler-scans*"]
  }
  statement {
    sid       = "CustomerAssumeRole"
    effect    = "Allow"
    actions   = ["sts:AssumeRole"]
    resources = ["arn:aws:iam::*:role/*bridgecrew*"]
  }
  statement {
    sid       = "S3UploadResult"
    effect    = "Allow"
    actions   = ["s3:PutObject"]
    resources = ["arn:aws:s3:::${var.results_bucket}/*"]
  }
  statement {
    sid       = "AllowKmsAccess"
    effect    = "Allow"
    actions   = ["kms:Encrypt", "kms:Decrypt"]
    resources = [var.kms_key_arn]
  }
}
