data "aws_caller_identity" "current" {}

module "consts" {
  source = "../../../../../utils/terraform/consts"
}

resource "aws_cloudwatch_log_group" "cloudmapper_log_group" {
  name = "ecs/cloudmapper-scans-${var.unique_tag}"
}

data "template_file" "cloudmapper_task_definition" {
  template = file("${path.module}/../taskDefinitions/cloudmapper.json")
  vars = {
    log_group = aws_cloudwatch_log_group.cloudmapper_log_group.name
    bucket    = var.results_bucket
  }
}


resource "aws_ecs_task_definition" "cloudmapper_task_definition" {
  container_definitions    = data.template_file.cloudmapper_task_definition.rendered
  family                   = "bc-ecs-cloudmapper-scanner-td-${var.unique_tag}"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.cloudmapper_task_execution_role.arn
  memory                   = 2048
  cpu                      = 1024
}

resource "aws_iam_role" "cloudmapper_task_execution_role" {
  name               = "bc-cloudmapper-execution-role-${var.unique_tag}"
  assume_role_policy = data.aws_iam_policy_document.cloudmapper_task_definition_assume_policy.json
}

data "aws_iam_policy_document" "cloudmapper_task_definition_assume_policy" {
  statement {
    effect = "Allow"
    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role_policy" "cloudmapper_task_execution_policy" {
  name   = "cloudmapper-task-execution-policy"
  policy = data.aws_iam_policy_document.cloudmapper_task_write_to_CW.json
  role   = aws_iam_role.cloudmapper_task_execution_role.id
}

data "aws_iam_policy_document" "cloudmapper_task_write_to_CW" {
  statement {
    sid    = "CloudWatchLogsWrite"
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:CreateLogGroup",
      "logs:PutLogEvents"
    ]
    resources = ["arn:aws:logs:us-west-2:${data.aws_caller_identity.current.account_id}:log-group:ecs/cloudmapper-scans*"]
  }
  statement {
    sid       = "CustomerAssumeRole"
    effect    = "Allow"
    actions   = ["sts:AssumeRole"]
    resources = ["arn:aws:iam::*:role/*bridgecrew*"]
  }
  statement {
    sid       = "S3UploadResult"
    effect    = "Allow"
    actions   = ["s3:PutObject", "s3:GetObject", "s3:ListBucket", "s3:HeadBucket", "s3:DeleteObject"]
    resources = ["arn:aws:s3:::${var.results_bucket}/*", "arn:aws:s3:::${var.results_bucket}"]
  }
  statement {
    sid       = "AllowKmsAccess"
    effect    = "Allow"
    actions   = ["kms:Encrypt", "kms:Decrypt"]
    resources = [var.kms_key_arn]
  }
}