variable "unique_tag" {
  type        = string
  description = "The base stack's unique tag"
}

variable "region" {
  type        = string
  description = "The base stack's main region"
}

variable "results_bucket" {
  type        = string
  description = "The name of the bucket where the scanner results will be stored in"
}

variable "kms_key_arn" {
  type        = string
  description = "The ID of the KMS key used to encrypt the data"
}
