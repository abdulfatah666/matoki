#!/bin/bash

# get config.json file
aws s3 cp s3://${BUCKET}/cloudmapper/${CUSTOMER_NAME}/config.json .
if [ $? -ne 0 ]; then
    echo "config.json not found for ${CUSTOMER_NAME}"
    exit 1
fi

mkdir ~/.aws

cat << AWS_CREDS > ~/.aws/credentials
[${ACCOUNT_ID}]
credential_source = EcsContainer
role_arn = ${ROLE_ARN}
external_id = ${EXTERNAL_ID}

AWS_CREDS

cloudmapper_account=${CUSTOMER_NAME}-${ACCOUNT_ID}

# run collect
python cloudmapper.py collect --account ${cloudmapper_account} --profile ${ACCOUNT_ID}

mkdir results
mkdir results/${ACCOUNT_ID}

# prepare command
python cloudmapper.py prepare --account ${cloudmapper_account} --no-node-data
mv web/data.json results/${ACCOUNT_ID}/networkGraph.json

#wot command
python cloudmapper.py weboftrust --account ${cloudmapper_account}
mv web/data.json results/${ACCOUNT_ID}/wot.json

python cloudmapper.py find_admins --account ${cloudmapper_account} > results/${ACCOUNT_ID}/admins.txt
python cloudmapper.py find_unused --account ${cloudmapper_account} > results/${ACCOUNT_ID}/unused.json
python cloudmapper.py public --account ${cloudmapper_account} > results/${ACCOUNT_ID}/public.txt
python cloudmapper.py iam_report --account ${cloudmapper_account} --output json

cp web/account-data/iam_report.json results/${ACCOUNT_ID}/iam_report.json

cp -r account-data/${cloudmapper_account}/* results/${ACCOUNT_ID}/

unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
unset AWS_SESSION_TOKEN

# sync files to bucket
aws s3 sync --delete results/${ACCOUNT_ID}/ s3://${BUCKET}/cloudmapper/${CUSTOMER_NAME}/${ACCOUNT_ID}/