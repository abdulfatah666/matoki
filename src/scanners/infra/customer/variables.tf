variable customer_name {
  type        = string
  description = "The name of the customer"
}

variable customer_aws_account_id {
  type        = string
  description = "The AWS account of the customer"
}

variable "region" {
  type        = string
  description = "The base stack's main region"
}

variable "unique_tag" {
  type        = string
  description = "The base stack unique tag"
}
