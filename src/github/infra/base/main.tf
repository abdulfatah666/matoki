
locals {
  github_lambda_names = [
    format("%s-mgr-api-%s", var.module_name, var.unique_tag),
    format("%s-webhook-%s", var.module_name, var.unique_tag)
  ]
  variable_string = "--github-app-pem ${aws_ssm_parameter.github_app_pem.name} --github-app-client-secret ${aws_ssm_parameter.github_app_client_secret.name} --github-webhook-secret ${aws_ssm_parameter.github_app_webhook_secret.name} --github-app-client-id ${aws_ssm_parameter.github_app_client_id.name} --result-bucket ${var.result_bucket} --github-app-id ${var.github_app_id}  --module-name ${var.module_name} --integration-api-lambda ${var.integration_api_lambda}"
}

module "consts" {
  source = "../../../utils/terraform/consts"
}


module "github_lambdas" {
  source                 = "../../../utils/terraform/setupGroupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = var.path_to_serverless_yml
  variable_string        = local.variable_string
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.sls_bucket_name
  module_name            = var.module_name
  lambda_names           = local.github_lambda_names
}

// This gets the lambda that was created in the sls-deploy resource
data "aws_lambda_function" "existing_github_lambdas" {
  count         = length(local.github_lambda_names)
  function_name = local.github_lambda_names[count.index]
  depends_on    = ["module.github_lambdas"]
}

resource "aws_ssm_parameter" "github_app_id" {
  name  = "/github/app/id-${var.unique_tag}"
  type  = "String"
  value = var.github_app_id
}
resource "aws_ssm_parameter" "github_app_pem" {
  name  = "/github/app/pem-${var.unique_tag}"
  type  = "SecureString"
  value = file(var.github_app_pem)
}
resource "aws_ssm_parameter" "github_app_client_id" {
  name  = "/github/app/client/id-${var.unique_tag}"
  type  = "String"
  value = var.github_app_client_id
}
resource "aws_ssm_parameter" "github_app_client_secret" {
  name  = "/github/app/client/secret-${var.unique_tag}"
  type  = "SecureString"
  value = var.github_app_client_secret
}

resource "random_password" "password" {
  length = 20
  special = true
}

resource "aws_ssm_parameter" "github_app_webhook_secret" {
  name  = "/github/app/webhook/secret-${var.unique_tag}"
  type  = "SecureString"
  value = random_password.password.result
}



