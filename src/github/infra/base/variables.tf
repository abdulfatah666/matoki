variable "region" {
  type        = "string"
  description = "the aws region"
}

variable "aws_profile" {
  type = "string"
}

variable "github_app_id" {
  description = "The GitHub app id of bridgcrew app"
  type        = "string"
}

variable "github_app_client_id" {
  description = "The Github app client ID"
  type        = "string"
}
variable "github_app_client_secret" {
  description = "The Github app client secret"
  type        = "string"
}
variable "github_app_pem" {
  description = "The Github app private key"
  type        = "string"
}

variable "monitor" {
  type = "map"

  default = {
    enable               = false
    sns_topic_error_name = ""
  }
}

variable "path_to_serverless_yml" {
  description = "path to serverless yml file"
  type        = "string"
}

variable "module_name" {
  description = "name of deployed module the contains all lambdas (e.g. bc-compliances)"
  type        = "string"
}

variable "integration_api_lambda" {
  description = "The integration API lambda namee"
  type        = "string"
}

variable "result_bucket" {
  description = "The bucket name of the result scanner bucket"
  type        = "string"
}

variable "unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
}

variable "sls_bucket_name" {
  description = "A name for deployment bucket that all serverless function deployment state is saved on"
  type        = "string"
}