output "github_function_arns" {
  value = data.aws_lambda_function.existing_github_lambdas.*.arn
}

output "github_function_names" {
  value = data.aws_lambda_function.existing_github_lambdas.*.function_name
}
