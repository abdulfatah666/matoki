const GithubApp = require('./githubApi');
const Installation = require('../../models/installation');

class GithubUserApi extends GithubApp {
    constructor({ token }) {
        super({ token });
    }

    async getInstallations() {
        try {
            const installationsResponse = await this.axiosInstance.request({
                url: 'user/installations',
                method: 'get'
            });
            return installationsResponse.data.installations.map((installation) => new Installation(
                {
                    id: installation.id,
                    events: installation.events,
                    permissions: installation.permissions,
                    account: installation.account
                }
            ));
        } catch (e) {
            console.error(`Failed while executing getInstallations with the ${this.token} the error is ${e.message}`);
            throw e;
        }
    }
}

module.exports = GithubUserApi;
