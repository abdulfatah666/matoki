const axios = require('axios');
const configuration = require('../conf/config');

class GithubApi {
    constructor({ token }) {
        this.axiosInstance = axios.create({
            baseURL: configuration.GITHUB_API_URL,
            headers: {
                Accept: configuration.GITHUB_MEDIA_TYPE,
                Authorization: `Bearer ${token}`
            }
        });
    }

    async paginationRequest({ config }) {
        let response = await this.axiosInstance.request(config);
        const result = response;
        while (response.headers.link) {
            const pages = response.headers.link.split(',');
            const nextPage = pages.filter((item) => item.includes('rel="next"'))[0];
            if (typeof nextPage !== 'undefined') {
                const startPos = nextPage.indexOf('<') + 1;
                const endPos = nextPage.indexOf('>', startPos);
                // eslint-disable-next-line no-param-reassign
                config.url = nextPage.substring(startPos, endPos);
                // eslint-disable-next-line no-await-in-loop
                response = await this.axiosInstance.request(config);
                result.data.items = result.data.items.concat(response.data.items);
            } else {
                break;
            }
        }
        return result;
    }
}

module.exports = GithubApi;
