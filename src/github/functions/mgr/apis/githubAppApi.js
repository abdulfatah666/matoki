const jwt = require('jsonwebtoken');
const GithubApi = require('./githubApi');
const ssmMgr = require('../../ssm/ssmMgr');
const config = require('../conf/config');

// Const
const ssmMgrInstance = ssmMgr.getInstance();
const SECONDS_IN_MIN = 60;

class GithubAppApi {
    constructor({ appId }) {
        this.appId = appId;
        this.githubApi = null;
    }

    static async readAppPrivateKey() {
        try {
            const data = await ssmMgrInstance.getGithubAppPem();
            return data.Parameter.Value;
        } catch (e) {
            console.error(`Failed to read Github App PEM ${e.message}`);
            throw e;
        }
    }

    static async getAppAccessToken(appId) {
        const payload = {
            iat: Math.floor(Date.now() / 1000), // issued at time
            exp: Math.floor(Date.now() / 1000) + (10 * SECONDS_IN_MIN), // JWT expiration time (10 minute maximum)
            iss: appId // GitHub App's identifier
        };
        const data = await GithubAppApi.readAppPrivateKey();
        return jwt.sign(payload, data, { algorithm: 'RS256' });
    }

    async init() {
        const token = await GithubAppApi.getAppAccessToken(this.appId);
        this.githubApi = new GithubApi({ token });
    }

    async getInstallationAccessToken({ installationId }) {
        const installationAccessTokenResponse = await this.githubApi.axiosInstance.request({
            url: `/app/installations/${installationId}/access_tokens`,
            method: 'post',
            data: {
                repository_ids: [],
                permissions: config.PERMISSIONS
            }
        });
        console.info(`created token for installation id: ${installationId} with the following permissions: ${JSON.stringify(installationAccessTokenResponse.data.permissions)}`);
        return installationAccessTokenResponse.data;
    }
}

module.exports = GithubAppApi;
