const axios = require('axios');
const ssmMgr = require('../../ssm/ssmMgr');

// Const
const ssmMgrInstance = ssmMgr.getInstance();
const config = require('../conf/config');

class GithubOauthApi {
    constructor() {
        this.axiosInstance = axios.create({
            baseURL: config.GITHUB_URL
        });
    }

    async getAccessToken({ code }) {
        try {
            const [clientId, clientSecret] = await Promise.all([
                ssmMgrInstance.getGithubClientId(),
                ssmMgrInstance.getGithubClientSecret()
            ]);
            const accessTokenResponse = await this.axiosInstance.request({
                url: 'login/oauth/access_token',
                method: 'post',
                data: {
                    client_id: clientId.Parameter.Value,
                    client_secret: clientSecret.Parameter.Value,
                    code
                }
            });
            return accessTokenResponse.data;
        } catch (e) {
            console.error(`Failed while executing get access token for code ${code} with error ${e.message}`);
            throw e;
        }
    }
}

module.exports = GithubOauthApi;
