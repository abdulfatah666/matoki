const { v4 } = require('uuid');
const GithubApp = require('./githubApi');
const Repository = require('../../models/repository');
const File = require('../../models/file');
const { PR } = require('../conf/config');

// Const
const SUPPORTED_FILE_TYPES = ['tf', 'json', 'yml', 'yaml', 'template'];
const DEFAULT_BRANCH = 'master';
const BLOB_TYPE = 'blob';

class GithubInstallationApi extends GithubApp {
    constructor({ installationId, token }) {
        super({ token });
        this.installationId = installationId;
    }

    async getRepositories() {
        try {
            const repositoriesResponse = await this.axiosInstance.request({
                url: 'installation/repositories',
                method: 'get'
            });
            return repositoriesResponse.data.repositories.map((repo) => new Repository(
                {
                    id: repo.id,
                    owner: repo.owner.login,
                    name: repo.name,
                    defaultBranch: repo.default_branch,
                    fork: repo.fork
                }
            ));
        } catch (e) {
            console.error(`Failed while executing getRepositories - installation ID: ${this.installationId} error: ${e.message}`);
            GithubInstallationApi._handleGitHubExceptions(e);
            throw e;
        }
    }

    async getFiles({ repo }) {
        try {
            const { owner } = repo;
            const repoName = repo.name;

            // get reference of specific branch:
            const referenceSha = await this._getLatestReferenceSha({ owner, repoName, sourceBranchName: repo.defaultBranch || DEFAULT_BRANCH });
            // get a single tree using the SHA1 value of the tree node:
            const treeArr = await this._getTree({ owner, repoName, treeSha: referenceSha });
            // filter the relevant file:
            const filterFiles = treeArr.filter(node => node.type === BLOB_TYPE && SUPPORTED_FILE_TYPES.some(fileType => node.path.endsWith(`.${fileType}`))); // BLOB_TYPE means it's a file (and not directory)
            console.info(`filter ${filterFiles.length} at total ${treeArr.length} nodes`);
            return filterFiles.map((file) => new File(
                {
                    sha: file.sha,
                    path: file.path,
                    repo
                }
            ));
        } catch (e) {
            console.error(`Failed executing get files on repository: ${JSON.stringify(repo)}, with error: ${e.message}`);
            throw e;
        }
    }

    /**
     *
     * @param owner - String e.g: bridgecrew
     * @param repoName - String e.g: platform
     * @param treeSha - String e.g: 173d9bc485b3433354a4b1dbc7d956e99cf3d99d (the sha of the father node) - use as the branch name
     * @returns {Promise<*>} Array of tree child's
     * Limits: we use recursive flag as true, it means we get all the repository files and directories - if the array exceeded github maximum limit
     * the flag 'truncated' returns as true (it occurs for directory with ~50000 files) - the result is partial.
     * @private
     */
    async _getTree({ owner, repoName, treeSha }) {
        console.info(`getting single tree of owner: ${owner} with repo name: ${repoName} of tree sha: ${treeSha}`);
        try {
            const url = `repos/${owner}/${repoName}/git/trees/${treeSha}`;
            const method = 'get';
            this._printHttpRequest({ url, method });
            const treeResponse = await this.axiosInstance.request({
                url,
                method,
                params: { recursive: true }
            });
            if (!treeResponse.data || !treeResponse.data.tree) {
                throw new Error(`bad response while getting url: ${url}, response data: ${treeResponse.data || treeResponse}`);
            }
            const { truncated, tree } = treeResponse.data;
            const msg = `got tree with ${tree.length} child's, truncated: ${truncated}`;
            // eslint-disable-next-line no-unused-expressions
            truncated ? console.error(msg) : console.info(msg);

            return tree;
        } catch (e) {
            GithubInstallationApi._handleGitHubExceptions(e);
            console.error(`got error while getting tree: ${e}`);
            throw e;
        }
    }

    async getFileContent({ file }) {
        try {
            const fileContentResponse = await this.axiosInstance.request({
                url: `/repos/${file.repo.owner}/${file.repo.name}/contents/${file.path}`,
                method: 'get'
            });
            file.setContent(fileContentResponse.data.content);
            return file;
        } catch (e) {
            if (e.response && e.response.data && e.response.data.message && e.response.data.message.includes('The requested blob is too large')) {
                const msg = e.response.data.message;
                console.info(`got error msg for file:${file.path} - ${msg}`);
                console.info(e.response.data);
                return null;
            }
            console.error(`Failed while executing get content of file ${file.path} with error ${e.message}`);
            GithubInstallationApi._handleGitHubExceptions(e);
            throw e;
        }
    }

    async createIssue({ data, owner, repo }) {
        try {
            const createIssueResponse = await this.axiosInstance.request({
                url: `/repos/${owner}/${repo}/issues`,
                method: 'post',
                data
            });
            return createIssueResponse.data;
        } catch (e) {
            console.error(`Failed while executing createIssue: ${e.message}`);
            GithubInstallationApi._handleGitHubExceptions(e);
            throw e;
        }
    }

    /**
     * Relevant documentation:
     * Understand Git Objects - https://git-scm.com/book/en/v2/Git-Internals-Git-Objects
     * Create BRANCH using Git references - https://stackoverflow.com/questions/9506181/github-api-create-branch
     * Create PR using Github API - http://www.levibotelho.com/development/commit-a-file-with-the-github-api/
     * @param owner - String e.g: livnoni
     * @param repoName - String e.g: my-repo-name
     * @param sourceBranchName - String e.g: master
     * @param commitMessage - String e.g: Fix bucket encryption on app-bucket
     * @param prTitle - String e.g: [BC] - terraform security bug
     * @param prBody - String e.g: BridgeCrew has created this PR to tix one or more vulnerable lines in the terraform file
     * @param newFiles - Array of objects e.g: {path: 'src/yudaTestFile.tf', content: BASE_64}
     * @returns reference object of the pull request
     */
    async createPullRequest({ owner, repoName, sourceBranchName, newFiles, commitMessage, prTitle, prBody }) {
        // get reference of specific branch:
        const referenceSha = await this._getLatestReferenceSha({ owner, repoName, sourceBranchName });
        // create new branch:
        const newBranchName = `${PR.newBranchPrefix}-${v4()}`;
        const createNewBranchResponse = await this._createNewBranch({ owner, repoName, referenceSha, newBranchName });
        const newBranchReferenceName = createNewBranchResponse.ref; // e.g: "refs/heads/bc-fix-1b57cca9-5496-43dc-a87c-5eedc368ae16"
        const newBranchSha = createNewBranchResponse.object.sha;
        // upload new files to server (base 64 format)
        const uploadedFiles = await Promise.all(newFiles.map(newFile => this._uploadFileToGitHubServer({ owner, repoName, path: newFile.path, content: newFile.content })));
        // create a tree containing the new upload files
        const newTreeResponse = await this._createNewGitTree({ owner,
            repoName,
            baseTreeSha: newBranchSha,
            tree: uploadedFiles.map(newFile => ({
                path: newFile.path,
                mode: PR.fileMode,
                type: PR.fileType,
                sha: newFile.fileSha
            })) });
        const newTreeSha = newTreeResponse.sha;
        // create a new commit:
        const newCommitResponse = await this._createNewCommit({ owner, repoName, message: commitMessage, parentTreeSha: newBranchSha, newTreeSha });
        const newCommmitSha = newCommitResponse.sha;
        // Update HEAD (to the new commit):
        await this._updateHead({ owner, repoName, currentBranchReferenceName: newBranchReferenceName, newReferenceSha: newCommmitSha });
        // create pull request
        const pullRequestObj = await this._makePullRequest({ owner, repoName, title: prTitle, body: prBody, fromBranch: newBranchName, toBranch: sourceBranchName });
        console.info(`successfully created pull request (${pullRequestObj.number}):\n`
            + `id: ${pullRequestObj.id}\n`
            + `state: ${pullRequestObj.state}\n`
            + `by: ${pullRequestObj.user.login}\n`
            + `commits: ${pullRequestObj.commits}\n`
            + `file changes: ${pullRequestObj.changed_files}\n`
            + `from branch: ${pullRequestObj.head.ref}\n`
            + `to branch: ${pullRequestObj.base.ref}`);
        return pullRequestObj;
    }

    static _handleGitHubExceptions(error) {
        if (!error) return;
        const { response } = error;
        if (!response) return;
        const { status, statusText, data } = response;
        if (status || statusText || data) {
            console.error('got error from github API:\n'
                + `status: ${status}\n`
                + `statusText: ${statusText}\n`
                + `data: ${data ? JSON.stringify(data) : null}`);
        }
    }

    _printHttpRequest({ url, data, method }) {
        console.info(`[http request] - ${method ? method.toUpperCase() : ''} ${this.axiosInstance.defaults.baseURL}/${url || ''} ${data ? JSON.stringify(data) : ''}`);
    }

    async _getLatestReferenceSha({ owner, repoName, sourceBranchName }) {
        console.info(`getting latest reference sha of owner: ${owner} with repo name: ${repoName} of branch: ${sourceBranchName}`);
        try {
            const url = `/repos/${owner}/${repoName}/git/ref/heads/${sourceBranchName}`;
            const method = 'get';
            this._printHttpRequest({ url, method });
            const latestReferenceResponse = await this.axiosInstance.request({
                url,
                method
            });
            if (!latestReferenceResponse.data || !latestReferenceResponse.data.object || !latestReferenceResponse.data.object.sha) {
                throw new Error(`bad response while getting url: ${url}, response data: ${latestReferenceResponse.data || latestReferenceResponse}`);
            }
            return latestReferenceResponse.data.object.sha;
        } catch (e) {
            GithubInstallationApi._handleGitHubExceptions(e);
            console.error(`got error while getting latest reference sha: ${e}`);
            throw e;
        }
    }

    async _createNewBranch({ owner, repoName, referenceSha, newBranchName }) {
        console.info(`creating new branch (${newBranchName}) of owner: ${owner} with repo name: ${repoName} of reference sha: ${referenceSha}`);
        try {
            const url = `/repos/${owner}/${repoName}/git/refs`;
            const data = {
                ref: `refs/heads/${newBranchName}`,
                sha: referenceSha
            };
            const method = 'post';
            this._printHttpRequest({ url, data, method });
            const createNewBranch = await this.axiosInstance.request({
                url,
                method,
                data
            });
            const createNewBranchResponse = createNewBranch.data;
            console.info('successfully created new branch: ', createNewBranchResponse);
            return createNewBranchResponse;
        } catch (e) {
            GithubInstallationApi._handleGitHubExceptions(e);
            console.error(`got error while creating new branch: ${newBranchName}, error: ${e}`);
            throw e;
        }
    }

    async _uploadFileToGitHubServer({ owner, repoName, path, content }) {
        console.info(`uploading file (${path}) to github server, owner: ${owner} repository name: ${repoName}`);
        try {
            const url = `/repos/${owner}/${repoName}/git/blobs`;
            const method = 'post';
            const data = {
                content,
                encoding: PR.uploadFileEncodingType
            };
            this._printHttpRequest({ url, data, method });
            const uploadBlobFileResponse = await this.axiosInstance.request({
                url,
                method,
                data
            });
            const uploadFile = uploadBlobFileResponse.data;
            console.info('successfully uploaded new file: ', uploadFile);
            return { path, fileSha: uploadFile.sha };
        } catch (e) {
            GithubInstallationApi._handleGitHubExceptions(e);
            console.error(`got error while uploading new file (blob) to github server, error: ${e}`);
            throw e;
        }
    }

    async _createNewGitTree({ owner, repoName, baseTreeSha, tree }) {
        console.info(`creating new git tree for owner: ${owner} with repository name: ${repoName} baseTreeSha: ${baseTreeSha} tree: ${tree}`);
        try {
            const url = `/repos/${owner}/${repoName}/git/trees`;
            const method = 'post';
            const data = {
                base_tree: baseTreeSha,
                tree
            };
            this._printHttpRequest({ url, data, method });
            const createNewTreeResponse = await this.axiosInstance.request({
                url,
                method,
                data
            });
            const newTreeResponse = createNewTreeResponse.data;
            console.info('successfully created new tree. tree sha:', newTreeResponse.sha);
            return newTreeResponse;
        } catch (e) {
            GithubInstallationApi._handleGitHubExceptions(e);
            console.error(`got error while creating new tree, error: ${e}`);
            throw e;
        }
    }

    async _createNewCommit({ owner, repoName, message, parentTreeSha, newTreeSha }) {
        console.info(`creating new commit for owner: ${owner} with repository name: ${repoName} commit message: ${message} parent tree sha: ${parentTreeSha} new tree sha: ${newTreeSha}`);
        try {
            const url = `/repos/${owner}/${repoName}/git/commits`;
            const method = 'post';
            const data = {
                message,
                parents: [parentTreeSha],
                tree: newTreeSha
            };
            this._printHttpRequest({ url, data, method });
            const createNewCommitResponse = await this.axiosInstance.request({
                url,
                method,
                data
            });
            const newCommitResponse = createNewCommitResponse.data;
            console.info(`successfully created new commit. sha: ${newCommitResponse.sha} author: ${newCommitResponse.author} committer: ${newCommitResponse.committer}`);
            return newCommitResponse;
        } catch (e) {
            GithubInstallationApi._handleGitHubExceptions(e);
            console.error(`got error while creating new commit, error: ${e}`);
            throw e;
        }
    }

    async _updateHead({ owner, repoName, currentBranchReferenceName, newReferenceSha }) {
        console.info(`updating head for owner: ${owner} with repository name: ${repoName} current branch reference name: ${currentBranchReferenceName} new reference sha: ${newReferenceSha}`);
        try {
            const url = `/repos/${owner}/${repoName}/git/${currentBranchReferenceName}`;
            const method = 'patch';
            const data = {
                sha: newReferenceSha,
                force: true
            };
            this._printHttpRequest({ url, data, method });
            const updateBranchHeadResponse = await this.axiosInstance.request({
                url,
                method,
                data
            });
            const branchHeadResponse = updateBranchHeadResponse.data;
            console.info('successfully updated branch head: ', branchHeadResponse);
            return branchHeadResponse;
        } catch (e) {
            GithubInstallationApi._handleGitHubExceptions(e);
            console.error(`got error while updating the head, error: ${e}`);
            throw e;
        }
    }

    async _makePullRequest({ owner, repoName, title, body, fromBranch, toBranch }) {
        console.info(`making pull request for owner: ${owner} with repository name: ${repoName} from branch: ${fromBranch} to branch: ${toBranch}`);
        try {
            const url = `/repos/${owner}/${repoName}/pulls`;
            const method = 'post';
            const data = {
                title,
                body,
                head: fromBranch,
                base: toBranch
            };
            this._printHttpRequest({ url, data, method });
            const makePullRequestResponse = await this.axiosInstance.request({
                url,
                method,
                data
            });
            const pullRequestResponse = makePullRequestResponse.data;
            // console.info('successfully made pull request, pull request response:', pullRequestResponse);
            return pullRequestResponse;
        } catch (e) {
            GithubInstallationApi._handleGitHubExceptions(e);
            console.error(`got error while creating pull request, error: ${e}`);
            throw e;
        }
    }
}
module.exports = GithubInstallationApi;