const RemoteLambda = require('@bridgecrew/nodeUtils/remoteLambda/invoke');
const GithubUserApi = require('./apis/githubUserApi');
const GithubOauthApi = require('./apis/githubOauthApi');
const GithubAppApi = require('./apis/githubAppApi');
const GithubInstallationApi = require('./apis/githubInstallationApi');
const { PR } = require('./conf/config');

const remoteLambda = new RemoteLambda(process.env.INTEGRATION_API_LAMBDA);
const { SCAN_RESULTS_BUCKET } = process.env;

const promiseAllWithRateLimit = async ({ arr, maxConcurrent, callback }) => new Promise((resolve, reject) => {
    let count = arr.length;
    const results = [];
    if (!count) return resolve(results);
    let index;
    async function next() {
        count--;
        if (index < arr.length) {
            index++;
            try {
                results[index - 1] = await callback(arr[index - 1]);
            } catch (e) {
                reject(e);
            }
            next();
        } else if (count === 0) {
            resolve(results);
        }
    }
    const startParallelPromises = Math.min(count, maxConcurrent);

    for (index = 0; index < startParallelPromises; index++) {
        const j = index;
        callback(arr[index]).then(result => {
            results[j] = result;
            next();
        }, e => reject(e));
    }
});

class GithubServiceMgr {
    constructor(githubAppId) {
        this.githubAppId = githubAppId;
    }

    // eslint-disable-next-line class-methods-use-this
    async getToken({ code }) {
        const githubOauthApi = new GithubOauthApi();
        const response = await githubOauthApi.getAccessToken({ code });
        console.log(JSON.stringify(response));
        const items = response.split('&');
        const accessToken = items.filter((s) => s.includes('access_token'));
        const tokenType = items.filter((s) => s.includes('token_type'));
        let token;
        if (accessToken.length !== 0) {
            token = {
                ok: true,
                accessToken: accessToken[0].split('=')[1],
                tokenType: tokenType[0].split('=')[1]
            };
        } else {
            token = {
                ok: false
            };
        }
        return token;
    }

    // eslint-disable-next-line class-methods-use-this
    async getInstallations({ customerName, owner }) {
        try {
            const params = {
                customerName,
                type: 'Github' // todo: change to enum after hadar merge
            };
            const integration = await remoteLambda.invoke('getByType', params);
            console.log(`Function: getInstallations - The GitHub integration for ${customerName} is ${JSON.stringify(integration)}`);
            let installationIds = [];
            if (integration && integration.length > 0) {
                const { token } = integration[0].params;
                const githubUserApi = new GithubUserApi({ token });
                installationIds = await githubUserApi.getInstallations();
            }
            if (owner && installationIds.length > 0) {
                console.info(`Filter the installation id by owner: ${owner}`);
                installationIds = installationIds.filter(installationId => installationId.account.login === owner);
            }
            console.debug(`Customer name: ${customerName} installations ids are ${JSON.stringify(installationIds)}`);
            return installationIds;
        } catch (e) {
            console.error(`Failed to get installations id for ${customerName} with error ${e.message}`);
            throw e;
        }
    }

    async getRepositories({ customerName }) {
        const githubAppApi = new GithubAppApi({ appId: this.githubAppId });
        await githubAppApi.init();
        const installations = await this.getInstallations({ customerName });
        console.log(`Function getRepositories - The installations id for ${customerName} are: ${JSON.stringify(installations)}`);
        const data = await Promise.all(installations.map(async (installation) => {
            const installationAccessTokenObject = await githubAppApi.getInstallationAccessToken({ installationId: installation.id });
            const installationAccessToken = installationAccessTokenObject.token;
            const githubInstallationApi = new GithubInstallationApi({ installationId: installation.id, token: installationAccessToken });
            const results = await githubInstallationApi.getRepositories();
            if (results.length > 0) {
                return { orgName: results[0].owner, installationId: installation.id, repositories: results };
            }

            return { installationId: installation.id, repositories: results };
        }));
        return { totalCount: data.length, data };
    }

    /**
     * @param [required] installationId - String e.g: 123456
     * @param [required] owner - String e.g: livnoni
     * @param [required] repoName - String e.g: my-repo-name
     * @param [required] sourceBranchName - String e.g: master
     * @param [required] newFiles - Array of objects e.g: {path: 'src/yudaTestFile.tf', content: BASE_64}
     * @param [optional] commitMessage - String e.g: Fix bucket encryption on app-bucket
     * @param [optional] prTitle
     * @param [optional] prBody
     * @returns reference object of the pull request
     */
    async makePullRequest({ installationId, owner, repoName, sourceBranchName, newFiles, commitMessage, prTitle, prBody }) {
        const githubAppApi = new GithubAppApi({ appId: this.githubAppId });
        await githubAppApi.init();
        const installationAccessTokenObject = await githubAppApi.getInstallationAccessToken({ installationId });
        const installationAccessToken = installationAccessTokenObject.token;
        const githubInstallationApi = new GithubInstallationApi({ installationId, token: installationAccessToken });
        return githubInstallationApi.createPullRequest({
            owner,
            repoName,
            sourceBranchName,
            newFiles,
            commitMessage: commitMessage || PR.commitDefaultMessage,
            prTitle: prTitle || PR.prDefaultBody,
            prBody: prBody || PR.prDefaultTitle
        });
    }

    // eslint-disable-next-line class-methods-use-this
    async getFiles({ githubAppApi, repositories }) {
        const mapRepoToContent = {};
        await Promise.all(repositories.map(async (result) => {
            const data = await githubAppApi.getInstallationAccessToken({ installationId: result.installationId });
            const installationAccessToken = data.token;
            const githubInstallationApi = new GithubInstallationApi({ installationId: result.installationId, token: installationAccessToken });
            // eslint-disable-next-line no-restricted-syntax
            for (const repo of result.repositories) {
                // eslint-disable-next-line no-await-in-loop
                const files = await githubInstallationApi.getFiles({ repo });
                const contents = await promiseAllWithRateLimit({
                    arr: files,
                    callback: (file) => githubInstallationApi.getFileContent({ file }),
                    maxConcurrent: 250
                });
                const id = `${repo.owner}/${repo.name}`;
                console.log('contents.length=', contents.length);
                mapRepoToContent[id] = contents.filter(c => c); // filter nulls (files that can't be fetched due to large size)
            }
        }));
        return mapRepoToContent;
    }

    async setupScan({ scannerType, customerName }) {
        const timestamp = new Date().getTime();
        const githubAppApi = new GithubAppApi({ appId: this.githubAppId });
        await githubAppApi.init();
        // eslint-disable-next-line no-unused-vars
        const params = {
            customerName,
            type: 'Github'
        };
        const repositories = await this.getRepositories({ customerName });
        console.info(`Function setupScan -  The Github repositories for ${customerName} are: ${JSON.stringify(repositories)}`);
        const response = await remoteLambda.invoke('getByType', params);
        console.info(`Function setupScan - The data from integration db is : ${JSON.stringify(response)}`);
        if (response !== null && response && response.length > 0) {
            const repositoriesToScan = response[0].params.repositories;
            // eslint-disable-next-line no-restricted-syntax
            for (const org of repositories.data) {
                const newRepositories = [];
                // eslint-disable-next-line no-restricted-syntax
                for (const repo of org.repositories) {
                    // eslint-disable-next-line no-restricted-syntax
                    for (const repoToScan of repositoriesToScan) {
                        const [owner, name] = repoToScan.split('/');
                        if (repo.owner === owner && repo.name === name) {
                            console.log(`The repo to push ${JSON.stringify(repo)}`);
                            newRepositories.push(repo);
                        }
                    }
                }
                console.log(`The repos to org ${JSON.stringify(org)} are : ${JSON.stringify(newRepositories)}`);
                org.repositories = newRepositories;
            }
        }
        console.info(`The repos to scan are: ${JSON.stringify(repositories)}`);

        const data = await this.getFiles({ githubAppApi, repositories: repositories.data });
        console.log(`The files that return are: ${JSON.stringify(data)}`);
        const allFiles = [].concat(...Object.values(data));
        const flattenRepos = {};
        console.log(`Total files that are going to be saved to s3: ${allFiles.length}`);
        if (allFiles.length > 0) {
            await Promise.all(allFiles.map(async (file) => {
                const prefix = `${scannerType}/${customerName}/${file.repo.owner}/${file.repo.name}/${timestamp}`;
                await file.save({ prefix, bucket: SCAN_RESULTS_BUCKET });
                const repoId = file.repo.id;
                if (!flattenRepos[repoId]) flattenRepos[repoId] = file.repo;
            }));
        }

        return {
            timestamp,
            repositories: Object.values(flattenRepos)
        };
    }
}
let instance;

const getInstance = (githubAppId) => {
    if (!instance) {
        instance = new GithubServiceMgr(githubAppId);
    }
    return instance;
};

module.exports = { getInstance };
