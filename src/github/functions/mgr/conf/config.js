const githubConfig = {
    GITHUB_API_URL: 'https://api.github.com',
    GITHUB_URL: 'https://github.com',
    GITHUB_MEDIA_TYPE: 'application/vnd.github.machine-man-preview+json',
    // Before changing permission check if it is necessary, it will ask all the customer for new permissions
    PERMISSIONS: {
        contents: 'write',
        metadata: 'read',
        pull_requests: 'write'
    },
    PR: {
        newBranchPrefix: 'bc-fix',
        fileMode: '100644',
        fileType: 'blob',
        uploadFileEncodingType: 'base64',
        commitDefaultMessage: 'fix: bug',
        prDefaultTitle: '[BC] - terraform security bug',
        prDefaultBody: 'Bridgecrew has created this PR to fix vulnerable lines in the terraform file'
    }
};

module.exports = githubConfig;
