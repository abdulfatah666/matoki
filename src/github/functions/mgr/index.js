const serverless = require('serverless-http');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { HttpError, InternalServerError, BadRequestError } = require('@bridgecrew/nodeUtils/errors/http');
const GithubServiceMgr = require('./githubServiceMgr');

const { GITHUB_APP_ID } = process.env;

const githubServiceMgrInstance = GithubServiceMgr.getInstance(GITHUB_APP_ID);

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    if (req.method === 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});

app.get('/api/v1/github/repositories', async (req, res, next) => {
    try {
        const customerName = req.userDetails.customers[0];
        const data = await githubServiceMgrInstance.getRepositories({ customerName });
        res.status(200).json(data);
    } catch (err) {
        console.error(err);
        next(new BadRequestError('Failed to fetch repositories'));
    }
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
    if (!(err instanceof HttpError)) {
        // eslint-disable-next-line no-param-reassign
        err = new InternalServerError(err.message);
    }
    console.error(err.message, err.stack);
    res.status(err.statusCode).json({
        message: err.message
    });
});

function invokeService(funcName, body) {
    if (!(githubServiceMgrInstance[funcName] instanceof Function)) {
        throw new BadRequestError('Function does not exist');
    } else {
        return githubServiceMgrInstance[funcName](body);
    }
}

// eslint-disable-next-line consistent-return
exports.handler = (event, context) => {
    if (event.path.startsWith('/invoke')) {
        const matchPattern = event.path.match(/^\/invoke\/([^/]+)(?:\/([^/]+))?$/);
        if (matchPattern.length > 2 && matchPattern[2] !== undefined) {
            if (matchPattern[1] === 'service') {
                return invokeService(matchPattern[2], event.body);
            }
        } else if (matchPattern.length > 2 && matchPattern[2] === undefined) {
            return invokeService(matchPattern[1], event.body);
        } else {
            throw new Error('The invoke not valid');
        }
    } else {
        return serverless(app, {
            request(req, e, ctx) {
                req.event = e;
                req.context = ctx;

                if (req.event.requestContext.authorizer) {
                    req.userDetails = req.event.requestContext.authorizer;
                    req.userDetails.customers = req.event.requestContext.authorizer.customers.split(',');
                }
            },
            binary: false
        })(event, context);
    }
};
