### GitHub Webhook server 

#### Info
Lambda that simulates webhook server.
The lambda listen to POST events on the route: *API_GATEWAY/api/v1/github/webhook*

List of available event types: [here](https://developer.github.com/webhooks/#events "here").

#### Event headers
every event has 3 main headers:
- **x-github-event** - the type of the event, e.g: push, fork, etc...
- **x-github-delivery** - the event id
- **x-hub-signature** - a hash signature that calculated using the payload and secret key (by using [HMAC](https://en.wikipedia.org/wiki/HMAC "HMAC") authentication code)

The endpoint is public (it gets events directly from GitHub server)

#### Security
In order to verify that the events are real (occurs by our GitHub app installations ) - we use the **x-hub-signature**, we define a secret key, on our GitHub app setting page, before GitHub send us the event, they make a hash of the payload data (the body of the request) using the secret key that we set.
We calculate hash signature - on the same way GitHub does it, using HMAC protocol and verify that the hash that we got is the same as GitHub sent us.

#### How to set webhook on GitHub aplication ?
- Go to https://github.com/settings/apps/YOUR_APP_NAME
- Set the Webhook URL to API_GATEWAY/api/v1/github/webhook
- Set the Webhook secret to your secret key (you can view it on the SSM under the key: /github/app/webhook/secret-TAG) 