const serverless = require('serverless-http');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { HttpError, InternalServerError } = require('@bridgecrew/nodeUtils/errors/http');
const { verifySignature } = require('@bridgecrew/nodeUtils/webhook/utils');
const ssmMgr = require('../ssm/ssmMgr').getInstance();

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    if (req.method === 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
    if (!(err instanceof HttpError)) {
        // eslint-disable-next-line no-param-reassign
        err = new InternalServerError(err.message);
    }
    console.error(err.message, err.stack);
    res.status(err.statusCode).json({
        message: err.message
    });
});

/**
 * public end-point to handle the github web hook post events
 * list of events types: https://developer.github.com/webhooks/
 * right now, it just listen and validate the events, in the future it will also react to some of them
 */
app.post('/api/v1/github/webhook', async (req, res) => {
    console.info('got webhook request: ', req);
    const { headers } = req;
    const eventType = headers['x-github-event'];
    const eventId = headers['x-github-delivery'];
    const signature = headers['x-hub-signature'];
    console.info(`got web hook event id: ${eventId}, type: ${eventType}, signature: ${signature}`);
    const { body } = req;
    const secretToken = await ssmMgr.getGithubWebHookSecret();
    verifySignature({ payload: body, digest: signature.split('sha1=')[1], secretToken: secretToken.Parameter.Value });
    console.log(`payload is:\n${JSON.stringify(body)}`);
    // todo: handle the event by it's type...
    return res.json({ success: true });
});

// eslint-disable-next-line consistent-return
exports.handler = (event, context) => serverless(app, {
    request(req, e, ctx) {
        req.event = e;
        req.context = ctx;

        if (req.event.requestContext.authorizer) {
            req.userDetails = req.event.requestContext.authorizer;
            req.userDetails.customers = req.event.requestContext.authorizer.customers.split(',');
        }
    },
    binary: false
})(event, context);