class Installation {
    constructor({
        id, events, permissions, account
    }) {
        this.id = id;
        this.events = events;
        this.permissions = permissions;
        this.account = account;
    }
}
module.exports = Installation;