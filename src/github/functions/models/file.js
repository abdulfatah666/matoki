const AWS = require('aws-sdk');

const S3 = new AWS.S3();

class File {
    constructor({
        sha, path, repo, content = '', encoding = 'base64'
    }) {
        this.sha = sha;
        this.path = path;
        this.repo = repo;
        this.content = content;
        this.encoding = encoding;
    }

    setContent(content) {
        this.content = content;
    }

    save({ prefix, bucket }) {
        try {
            return S3.putObject({
                Bucket: bucket,
                Key: `${prefix}/${this.path}`,
                Body: Buffer.from(this.content, this.encoding)
            }).promise();
        } catch (e) {
            console.log(`Failed to save file ${prefix}/${this.path} with content ${this.content} and encoding ${this.encoding} to S3: ${e.message}`);
            throw e;
        }
    }
}
module.exports = File;