class Repository {
    constructor({
        id, owner, name, defaultBranch, fork
    }) {
        this.id = id;
        this.owner = owner;
        this.name = name;
        this.defaultBranch = defaultBranch;
        this.fork = fork;
    }
}
module.exports = Repository;