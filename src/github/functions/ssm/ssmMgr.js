const AWS = require('aws-sdk');

class SsmMgr {
    constructor() {
        this.SSM = new AWS.SSM();
    }

    async getGithubAppPem() {
        return this.SSM.getParameter({ Name: process.env.GITHUB_APP_PEM, WithDecryption: true }).promise();
    }

    async getGithubClientId() {
        return this.SSM.getParameter({ Name: process.env.GITHUB_CLIENT_ID }).promise();
    }

    async getGithubClientSecret() {
        return this.SSM.getParameter({ Name: process.env.GITHUB_CLIENT_SECRET, WithDecryption: true }).promise();
    }

    async getGithubAppId() {
        return this.SSM.getParameter({ Name: process.env.GITHUB_APP_ID }).promise();
    }

    async getGithubWebHookSecret() {
        return this.SSM.getParameter({ Name: process.env.GITHUB_WEBHOOK_SECRET, WithDecryption: true }).promise();
    }
}
let instance;

const getInstance = () => {
    if (!instance) {
        instance = new SsmMgr();
    }
    return instance;
};

module.exports = { getInstance };
