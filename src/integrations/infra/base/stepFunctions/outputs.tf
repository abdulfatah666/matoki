output "integrations_step_function_arn" {
  value = aws_sfn_state_machine.bc-create-integration-sf.id
}

output "integrations_destroy_step_function_arn" {
  value = aws_sfn_state_machine.bc-destroy-integration-sf.id
}