data "aws_sns_topic" "topic_monitor_error" {
  count = var.monitor["enable"] ? 1 : 0
  name  = var.monitor["sns_topic_error_name"]
}

resource "aws_cloudwatch_metric_alarm" "create_integration_sfn_alarm" {
  count               = var.monitor["enable"] ? 1 : 0
  alarm_name          = "create_integration_step_function_failed_${var.unique_tag}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "ExecutionsFailed"
  namespace           = "AWS/States"

  dimensions = {
    StateMachineArn = aws_sfn_state_machine.bc-create-integration-sf.id
  }

  threshold          = 1
  period             = 300
  statistic          = "Sum"
  alarm_description  = "Identify CreateIntegration SF failures"
  alarm_actions      = data.aws_sns_topic.topic_monitor_error.*.arn
  treat_missing_data = "notBreaching"
}

resource "aws_cloudwatch_metric_alarm" "destroy_integration_sfn_alarm" {
  count               = var.monitor["enable"] ? 1 : 0
  alarm_name          = "destroy_integration_step_function_failed_${var.unique_tag}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "ExecutionsFailed"
  namespace           = "AWS/States"

  dimensions = {
    StateMachineArn = aws_sfn_state_machine.bc-destroy-integration-sf.id
  }

  threshold          = 1
  period             = 300
  statistic          = "Sum"
  alarm_description  = "Identify DestroyIntegration SF failures"
  alarm_actions      = data.aws_sns_topic.topic_monitor_error.*.arn
  treat_missing_data = "notBreaching"
}