module "consts" {
  source = "../../../../utils/terraform/consts"
}

// state machine definitions
data "template_file" "create_integration_sf_definition" {
  template = file("${path.module}/templates/createIntegration.json")
  vars = {
    customer_api_lambda_arn = "arn:aws:lambda:${var.region}:${var.aws_account_id}:function:customers-api-${var.unique_tag}"
    integrations_api_lambda_arn = "arn:aws:lambda:${var.region}:${var.aws_account_id}:function:bc-integrations-api-${var.unique_tag}"
    task_definition =var.aws_ecs_task_definition_arn_integration
    cluster = var.cluster_arn
    subnet = var.subnet
    container = var.container_name
  }
}

data "template_file" "destroy_integration_sf_definition" {
  template = file("${path.module}/templates/destroyIntegration.json")
  vars = {
    integrations_api_lambda_arn = "arn:aws:lambda:${var.region}:${var.aws_account_id}:function:bc-integrations-api-${var.unique_tag}"
    task_definition =var.aws_ecs_task_definition_arn_integration
    cluster = var.cluster_arn
    subnet = var.subnet
    container = var.container_name
  }
}

// Assume role policy document
data "aws_iam_policy_document" "integration_sf_assume_role_policy_document" {

  statement {
    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type = "Service"
      identifiers = [
        "states.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role" "integration_sf_role" {
  name = "bc-sf-integrations-role-${var.unique_tag}"
  depends_on = [data.aws_iam_policy_document.integration_sf_assume_role_policy_document]
  assume_role_policy = data.aws_iam_policy_document.integration_sf_assume_role_policy_document.json
}

data "aws_iam_policy_document" "integration_sf_policy_document" {
  statement {
    effect = "Allow"
    actions = [
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:UpdateItem",
      "dynamodb:DeleteItem"
    ]

    resources = ["arn:aws:dynamodb:${var.region}:${var.aws_account_id}:table/tenant*"]
  }

  statement {
    effect    = "Allow"
    actions   = [
      "ecs:RunTask",
      "ecs:StopTask",
      "ecs:DescribeTasks"]
    resources = ["arn:aws:ecs:${var.region}:${var.aws_account_id}:task-definition*"]
  }

  statement {
    effect    = "Allow"
    actions   = [
      "events:PutTargets",
      "events:PutRule",
      "events:DescribeRule"
    ]
    resources = [
      "arn:aws:events:${var.region}:${var.aws_account_id}:rule/StepFunctionsGetEventsForECSTaskRule",
      "arn:aws:events:${var.region}:${var.aws_account_id}:rule/StepFunctionsGetEventsForStepFunctionsExecutionRule"
    ]
  }

  statement {
    effect    = "Allow"
    actions   = [
      "iam:*"
    ]
    resources = ["*"]
  }

  statement {
    effect    = "Allow"
    actions   = [
      "lambda:InvokeFunction"
    ]
    resources = [
      "arn:aws:lambda:${var.region}:${var.aws_account_id}:function:customers-api-${var.unique_tag}",
      "arn:aws:lambda:${var.region}:${var.aws_account_id}:function:bc-integrations-api-${var.unique_tag}"
    ]
  }

  statement {
    effect    = "Allow"
    actions   = [
      "states:StartExecution",
      "states:DescribeExecution",
      "states:StopExecution"
    ]
    resources = ["arn:aws:states:${var.region}:${var.aws_account_id}:stateMachine:*"]
  }
}

resource "aws_iam_role_policy" "sfn_integrations_policy" {
  name = "sfn_policy_${var.unique_tag}"
  role = aws_iam_role.integration_sf_role.id

  policy = data.aws_iam_policy_document.integration_sf_policy_document.json
}

resource "null_resource" "pool_policy_delay_integration" {
  triggers = {
    build = timestamp()
  }

  provisioner "local-exec" {
    command = "sleep 10"
  }

  depends_on = ["aws_iam_role_policy.sfn_integrations_policy"]
}

resource "aws_sfn_state_machine" "bc-create-integration-sf" {
  depends_on = [null_resource.pool_policy_delay_integration]
  name = "bc-sf-integration-apply-${var.unique_tag}"
  role_arn = aws_iam_role.integration_sf_role.arn
  definition = data.template_file.create_integration_sf_definition.rendered
}

resource "aws_sfn_state_machine" "bc-destroy-integration-sf" {
  depends_on = [null_resource.pool_policy_delay_integration]
  name = "bc-sf-integration-destroy-${var.unique_tag}"
  role_arn = aws_iam_role.integration_sf_role.arn
  definition = data.template_file.destroy_integration_sf_definition.rendered
}