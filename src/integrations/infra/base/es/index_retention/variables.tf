variable "region" {
  type        = "string"
  description = "The region in which the stack will be deployed"
}

variable "index_retention_lambda_arn" {
  type        = string
  description = "The ARN of the index retention lambda function"
}

variable "unique_tag" {
  type        = string
  description = "Basestack's unique tag"
}