data "aws_caller_identity" "current" {}

locals {
  base_stack_aws_account_id    = data.aws_caller_identity.current.account_id
  index_retention_cw_rule_name = "${module.consts.es_index_retention_cw_rule}-${var.unique_tag}"
}

module "consts" {
  source = "../../../../../utils/terraform/consts"
}


resource "aws_lambda_permission" "cw_event_lambda_permission" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = var.index_retention_lambda_arn
  principal     = "events.amazonaws.com"
  source_arn    = "arn:aws:events:${var.region}:${local.base_stack_aws_account_id}:rule/${module.consts.es_index_retention_cw_rule}-${var.unique_tag}-*"
}