/* eslint no-multiple-empty-lines:0 */
const serverless = require('serverless-http');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const RemoteLambda = require('@bridgecrew/nodeUtils/remoteLambda/invoke');

const githubAppName = process.env.TAG === 'prod' ? 'bridgecrew' : 'Bridgecrew-develop';

const app = express();
const { HttpError, InternalServerError, BadRequestError } = require('@bridgecrew/nodeUtils/errors/http');
const integrationService = require('./integrationsService');
const integrationDao = require('./integrationsDao');

const integrationServiceInstance = integrationService.getInstance(process.env.INTEGRATIONS_TABLE_NAME, process.env.STEP_FUNCTION_INTEGRATIONS_APPLY, process.env.STEP_FUNCTION_INTEGRATIONS_DESTROY);
const integrationDAOInstance = integrationDao.getInstance(process.env.INTEGRATIONS_TABLE_NAME);
const customersRemoteLambda = new RemoteLambda(process.env.CUSTOMERS_API_LAMBDA);

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    if (req.method === 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});

app.get('/api/v1/integrations', async (req, res, next) => {
    try {
        const integrations = await integrationServiceInstance.getAll({ customerName: req.userDetails.customers[0] });

        res.json({
            data: integrations
        });
    } catch (err) {
        next(err);
    }
});

app.get('/api/v1/integrations/types/:integrationType', async (req, res, next) => {
    try {
        const integrations = await integrationServiceInstance.getClientIntegration({
            customerName: req.userDetails.customers[0],
            email: req.userDetails.email,
            integrationType: req.params.integrationType
        });
        res.json({
            integrations
        });
    } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
        next(new BadRequestError());
    }
});

// eslint-disable-next-line consistent-return
app.get('/api/v1/integrations/github', async (req, res, next) => {
    try {
        /* eslint prefer-destructuring:0 */
        const customerName = req.query.state;
        /* eslint prefer-destructuring:0 */
        const code = req.query.code;
        const installationId = req.query.installation_id;
        console.info(`Github integration was called with the following params: customerName=${customerName} code=${code} installationId=${installationId}`);
        // callback from Github repositories
        if (installationId) {
            if (customerName && code) {
                const tenant = await customersRemoteLambda.invoke('getCustomerByName', { name: customerName });
                if (!tenant) {
                    next(new BadRequestError(`Customer ${customerName} does not exist`));
                }
                // generate token from code
                const token = await integrationServiceInstance.generateGithubTokenFromCode(code);
                const existingIntegrations = await integrationServiceInstance.getByType({ customerName, type: 'Github' });
                if (existingIntegrations && existingIntegrations.length > 0) {
                    const existingIntegration = existingIntegrations[0];
                    const newParams = Object.assign(existingIntegration.params, { token });
                    await integrationServiceInstance.update({
                        customerName,
                        id: existingIntegration.id,
                        params: newParams,
                        updatedBy: tenant.owner_email,
                        lastStatusUpdate: (new Date()).getTime(),
                        alias: 'default'
                    });
                    return res.redirect(`${process.env.DOMAIN_NAME}/integrations/Github/edit/${existingIntegration.id}`);
                }
                const result = await integrationServiceInstance.create({
                    customerName,
                    updatedBy: tenant.owner_email,
                    params: { token, repositories: [] },
                    type: 'Github',
                    dateCreated: (new Date()).getTime(),
                    lastStatusUpdate: (new Date()).getTime(),
                    alias: 'default'
                });
                // callback to Bridgecrew app
                return res.redirect(`${process.env.DOMAIN_NAME}/integrations/Github/edit/${result.integration.id}`);
            }
            next(new BadRequestError('Missing Customer name or Code in query parameters'));
        } else {
            console.log('Github callback from OAUTH will be redirected');
            return res.redirect(`https://github.com/apps/${githubAppName}/installations/new?state=${customerName}`);
        }
    } catch (err) {
        next(err);
    }
});

app.get('/api/v1/integrations/:id', async (req, res, next) => {
    try {
        const integration = await integrationServiceInstance.get({
            customerName: req.userDetails.customers[0],
            id: req.params.id
        });

        res.json({
            data: integration
        });
    } catch (err) {
        next(err);
    }
});

app.post('/api/v1/integrations', async (req, res, next) => {
    try {
        if (!req.body.alias) {
            next(new BadRequestError('alias parameter does not exist'));
            return;
        }

        if (req.body.params && req.body.type) {
            const date = new Date().getTime();
            const { params, type, alias } = req.body;
            const result = await integrationServiceInstance.create({
                customerName: req.userDetails.customers[0],
                updatedBy: req.userDetails.email,
                params,
                type,
                dateCreated: date,
                lastStatusUpdate: date,
                alias
            });

            res.status(201);
            res.json({
                res: result.res,
                data: { integrationID: result.integration.id }
            });
        } else {
            next(new BadRequestError('Params or type parameter does not exist'));
        }
    } catch (err) {
        next(err);
    }
});

app.put('/api/v1/integrations/:id', async (req, res, next) => {
    try {
        if (Object.keys(req.body).length > 0) {
            const result = await integrationServiceInstance.update({
                customerName: req.userDetails.customers[0],
                id: req.params.id,
                updatedBy: req.userDetails.email,
                lastStatusUpdate: new Date().getTime(),
                ...req.body
            });

            res.json({
                res: result.res,
                data: result.integration
            });
        } else {
            next(new BadRequestError('Empty request body is not allowed'));
        }
    } catch (err) {
        next(err);
    }
});

app.delete('/api/v1/integrations/:id', async (req, res, next) => {
    try {
        if (req.params.id) {
            const result = await integrationServiceInstance.delete({
                customerName: req.userDetails.customers[0],
                id: req.params.id
            });

            res.send({
                res: result.res,
                data: result.integration
            });
        } else {
            next(new BadRequestError('Params or type parameter does not exist'));
        }
    } catch (err) {
        next(err);
    }
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
    if (!(err instanceof HttpError)) {
        /* eslint-disable-next-line no-param-reassign */
        err = new InternalServerError(err.message);
    }
    // eslint-disable-next-line no-console
    console.error(err.message, err.stack);

    res.status(err.statusCode).json({ message: err.message });
});

function invokeService(funcName, body) {
    if (!(integrationServiceInstance[funcName] instanceof Function)) {
        throw new BadRequestError('Function does not exist');
    } else {
        return integrationServiceInstance[funcName](body);
    }
}

function invokeDao(funcName, body) {
    if (!(integrationDAOInstance[funcName] instanceof Function)) {
        throw new BadRequestError('Function do not exist');
    } else {
        return integrationDAOInstance[funcName](body);
    }
}

/* eslint-disable-next-line consistent-return */
exports.handler = (event, context) => {
    if (event.path.startsWith('/invoke')) {
        const matchPattern = event.path.match(/^\/invoke\/([^/]+)(?:\/([^/]+))?$/);
        if (matchPattern.length > 2 && matchPattern[2] !== undefined) {
            if (matchPattern[1] === 'dao') {
                return invokeDao(matchPattern[2], event.body);
            }
            if (matchPattern[1] === 'service') {
                return invokeService(matchPattern[2], event.body);
            }
        } else if (matchPattern.length > 2 && matchPattern[2] === undefined) {
            return invokeService(matchPattern[1], event.body);
        } else {
            throw new Error('The invoke not valid');
        }
    } else {
        return serverless(app, {
            request(req, ev, ctx) {
                req.event = ev;
                req.context = ctx;

                if (req.event.requestContext.authorizer) {
                    req.userDetails = req.event.requestContext.authorizer;
                    req.userDetails.customers = req.event.requestContext.authorizer.customers.split(',');
                }
            },
            binary: false
        })(event, context);
    }
};

