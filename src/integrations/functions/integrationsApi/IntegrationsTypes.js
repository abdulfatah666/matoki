const TRIGGERS = {
    STEP_FUNCTION: 'step_function',
    ECS: 'ecs',
    LAMBDA: 'lambda',
    NONE: 'none'
};

const EMAIL_TRIGGER = {
    CREATION: 'creation',
    UPDATE: ' update'
};

const IntegrationsType = {
    cloudtrail: {
        trigger: TRIGGERS.ECS,
        requireES: false,
        email: {
            trigger: EMAIL_TRIGGER.CREATION,
            name: 'AWS CloudTrail',
            template: 'newIntegrationAWSConfig'
        }
    },
    logstash: {
        trigger: TRIGGERS.ECS,
        requireES: true
    },
    AllowRemoteRemediation: {
        trigger: TRIGGERS.NONE,
        requireES: false,
        email: {
            trigger: EMAIL_TRIGGER.CREATION,
            name: 'AWS Write Access',
            template: 'newIntegrationAWSConfig'
        }
    },
    Github: {
        trigger: TRIGGERS.STEP_FUNCTION,
        triggerData: process.env.STEP_FUNCTIONS_SCANNERS,
        email: {
            trigger: EMAIL_TRIGGER.UPDATE,
            name: 'GitHub',
            template: 'newIntegrationGithub'
        },
        // integration per customer
        customerAuthorization: true,
        requireES: false
    },
    'aws-api-access': {
        trigger: TRIGGERS.ECS,
        email: {
            trigger: EMAIL_TRIGGER.CREATION,
            name: 'AWS Read Access',
            template: 'newIntegrationAWSConfig'
        },
        requireES: false
    },
    elasticsearch: {
        trigger: TRIGGERS.ECS,
        requireES: false
    },
    awss3: {
        trigger: TRIGGERS.NONE,
        requireES: false
    },
    loggly: {
        trigger: TRIGGERS.ECS,
        requireES: true
    },
    lacework: {
        trigger: TRIGGERS.ECS,
        requireES: true
    },
    saml: {
        trigger: TRIGGERS.ECS,
        requireES: false
    },
    sep15: {
        trigger: TRIGGERS.ECS,
        requireES: true
    },
    onelogin: {
        trigger: TRIGGERS.ECS,
        requireES: true
    }
};

module.exports = { IntegrationsType, EMAIL_TRIGGER };
