const AWS = require('aws-sdk');

const STATUS = {
    created: 'CREATED',
    updating: 'UPDATING',
    failed: 'FAILED',
    succeeded: 'SUCCEEDED',
    inProgress: 'IN_PROGRESS'
};

class IntegrationDao {
    constructor(tableName) {
        this.tableName = tableName;
        this.docClient = new AWS.DynamoDB.DocumentClient();
    }

    async getAll({ customerName }) {
        return this.docClient.get({
            TableName: this.tableName,
            Key: {
                'customer-name': customerName
            }
        })
            .promise()
            .then((data) => (data.Item ? data.Item.integrations : []));
    }

    async get({ customerName, id }) {
        return this.docClient.get({
            TableName: this.tableName,
            Key: {
                'customer-name': customerName
            }
        })
            .promise()
            .then((data) => (data.Item ? data.Item.integrations.find((integration) => integration.id === id) : null));
    }

    async getByType({ customerName, type }) {
        const data = await this.docClient.get({
            TableName: this.tableName,
            Key: {
                'customer-name': customerName
            }
        }).promise();
        console.log(`Function getByType - For customer ${customerName} and type: ${type} the integrations are: ${JSON.stringify(data)}`);
        // eslint-disable-next-line no-unused-vars
        let returnValue = [];
        if (data.Item) {
            const integrationObject = data.Item;
            if (integrationObject.integrations) {
                returnValue = integrationObject.integrations.filter((integration) => (Array.isArray(integration.type) ? integration.type.indexOf(type) : integration.type === type));
            }
        }
        console.log(`Function getByType - For customer ${customerName} and type: ${type} the return value is: ${JSON.stringify(data)}`);
        return returnValue;
    }

    async update({ customerName, integration }) {
        let integrations = await this.getAll({ customerName });
        let integrationIndex = 0;

        if (integrations) {
            integrationIndex = integrations.map((integrationMap) => integrationMap.id)
                .indexOf(integration.id);

            if (integrationIndex === -1) {
                integrations.push(integration);
                integrationIndex = integrations.indexOf(integration);
            } else {
                integrations[integrationIndex] = Object.assign(integrations[integrationIndex], integration);
            }
        } else {
            integrations = [
                integration
            ];
        }

        if (!integrations[integrationIndex].status) {
            integrations[integrationIndex].status = STATUS.created;
        }

        if (integrations[integrationIndex].enable === undefined) {
            integrations[integrationIndex].enable = true;
        }

        if (typeof (integrations[integrationIndex].params) !== 'object') {
            integrations[integrationIndex].params = JSON.parse(integrations[integrationIndex].params);
        }

        if (typeof (integrations[integrationIndex].integration_details) !== 'object') {
            integrations[integrationIndex].integration_details = JSON.parse(integrations[integrationIndex].integration_details);
        }

        if (!integrations[integrationIndex].id || !integrations[integrationIndex].type || !integrations[integrationIndex].params) {
            throw new Error('The integration object does not valid');
        }

        delete integrations[integrationIndex].params.enable;

        return this.docClient.put({
            TableName: this.tableName,
            Item: JSON.parse(JSON.stringify({
                'customer-name': customerName,
                integrations
            }))
        })
            .promise()
            .then((res) => ({
                res,
                integration: integrations[integrationIndex]
            }));
    }

    async delete({ customerName, id }) {
        const integrations = await this.getAll({ customerName });
        return this.docClient.put({
            TableName: this.tableName,
            Item: JSON.parse(JSON.stringify({
                'customer-name': customerName,
                integrations: integrations.filter((integration) => integration.id !== id)
            }))
        }).promise();
    }

    async setStatus({ customerName, id, status }) {
        const integration = await this.get({ customerName, id });
        if (integration) {
            integration.status = status;

            return this.update({ customerName, integration });
        }
        throw new Error(`Integration ID ${id} does not exist, customer name: ${customerName}`);
    }
}

let instance;

const getInstance = (tableName) => {
    if (!instance) {
        instance = new IntegrationDao(tableName);
    }

    return instance;
};

module.exports = { getInstance, STATUS };
