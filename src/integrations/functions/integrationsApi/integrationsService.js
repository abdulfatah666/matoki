/* eslint-disable no-restricted-syntax,no-param-reassign,camelcase,class-methods-use-this */
const AWS = require('aws-sdk');
const uuidv4 = require('uuid/v4');
const RemoteLambda = require('@bridgecrew/nodeUtils/remoteLambda/invoke');
const { BadRequestError, NotFoundError } = require('@bridgecrew/nodeUtils/errors/http');
const integrationsDAO = require('./integrationsDao');
const INTEGRATIONS_TYPES = require('./IntegrationsTypes').IntegrationsType;
const { EMAIL_TRIGGER } = require('./IntegrationsTypes');

const githubRemoteLambda = new RemoteLambda(process.env.GITHUB_API_LAMBDA);
const customersRemoteLambda = new RemoteLambda(process.env.CUSTOMERS_API_LAMBDA);
const ssoManagerRemoteLambda = new RemoteLambda(process.env.SSO_MANAGER_API_LAMBDA);

function removeUndefined(obj) {
    for (const k in obj) if (obj[k] === undefined) delete obj[k];
    return obj;
}

class IntegrationService {
    constructor(tableName, stateMachineApplyArn, stateMachineDestroyArn) {
        this.tableName = tableName; // process.env.INTEGRATIONS_TABLE_NAME
        this.stateMachineApplyArn = stateMachineApplyArn;
        this.stateMachineDestroyArn = stateMachineDestroyArn;
        this.integrationsDAO = integrationsDAO.getInstance(this.tableName);
    }

    async getAll({ customerName, format = 'rest', action = 'create' }) {
        const result = await this.integrationsDAO.getAll({ customerName });

        if (format === 'stepFunction' && result) {
            return result.filter((integration) => integration.status !== 'DESTROYING')
                .map((integration) => {
                    const shouldSkipECSExecution = INTEGRATIONS_TYPES[integration.type].trigger !== 'ecs';
                    const esRequired = INTEGRATIONS_TYPES[integration.type].requireES;
                    return {
                        customer_details: {
                            name: customerName,
                            aws_account_id: integration.params.customer_aws_account_id,
                            cf_required: !!integration.params.customer_aws_account_id,
                            es_required: esRequired
                        },
                        integration_id: integration.id,
                        integration_details: JSON.stringify(integration.integration_details),
                        integration_execution_name: action === 'create'
                            ? `bc-apply-${customerName}-${integration.type}-${integration.id.split('-')
                                .slice(-1)[0]}-${new Date().getTime()}`
                            : `bc-destroy-${customerName}-${integration.type}-${integration.id.split('-')
                                .slice(-1)[0]}-${new Date().getTime()}`,
                        skip_ecs_execution: shouldSkipECSExecution,
                        integration_params: JSON.stringify({
                            ...integration.params,
                            enable: integration.enable
                        }),
                        trigger: INTEGRATIONS_TYPES[integration.type].trigger,
                        triggerData: INTEGRATIONS_TYPES[integration.type].triggerData,
                        integration_type: integration.type,
                        command: action === 'destroy' ? [
                            'integration',
                            'destroy',
                            integration.type
                        ] : [
                            'integration',
                            'apply',
                            integration.type
                        ]

                    };
                });
        }
        return result;
    }

    async getClientIntegration({ customerName, email, integrationType }) {
        const chosenIntegrations = (await this.integrationsDAO.getAll({ customerName })).filter((integration) => (integration.type === integrationType));
        const integrations = [];
        for (const integration of chosenIntegrations) {
            try {
                if (integrationType === 'AllowRemoteRemediation') {
                    const tenantDetails = await customersRemoteLambda.invoke('getTenantEnvironmentDetails', { awsAccountId: integration.params.customer_aws_account_id });
                    integration.params.cross_account_role_arn = tenantDetails.remote_remediation_cf_details.role_arn;
                } else if (integrationType === 'cloudtrail' || integrationType === 'aws-api-access') {
                    const tenantDetails = await customersRemoteLambda.invoke('getTenantEnvironmentDetails', { awsAccountId: integration.params.customer_aws_account_id });
                    integration.params.cross_account_role_arn = tenantDetails.cross_account_cf_details.cross_account_role_arn;
                } else if (integrationType === 'Github') {
                    integration.params.customer_name = customerName;
                } else if (integrationType === 'saml') {
                    if (integration.status !== 'IN_PROGRESS') {
                        const appClientIDDetails = await ssoManagerRemoteLambda.invoke('service/getAppClientID', {
                            username: email,
                            provider: 'saml'
                        });
                        const scope = appClientIDDetails.scopes.find((findScope) => findScope.includes('saml'));
                        integration.params.loginURL = `identity_provider=${scope}&response_type=token&client_id=${appClientIDDetails.clientID}&scopes=email%2Cprofile%2Copenid`;
                        integration.params.scope = scope;
                        integration.params.clientId = appClientIDDetails.clientID;
                    }
                }
                integrations.push(integration);
            } catch (e) {
                // eslint-disable-next-line no-console
                console.error(e);
            }
        }
        return integrations;
    }

    async get({ customerName, id }) {
        return this.integrationsDAO.get({
            customerName,
            id
        });
    }

    async getByType({ customerName, type, format = 'rest', filter }) {
        let result = await this.integrationsDAO.getByType({
            customerName,
            type
        });

        if (filter && filter.completed && result) {
            result = result.filter((integration) => integration.status === 'COMPLETED');
        }

        if (format === 'stepFunction' && result) {
            return {
                integrations: result,
                count: result.length
            };
        }
        return result;
    }

    async create({ customerName, type, params, dateCreated, updatedBy, lastStatusUpdate, alias }) {
        console.log(`Creating integration: ${type} for customer: ${customerName} with params: ${JSON.stringify(params)}`);
        if (!type) {
            throw new BadRequestError('Type does not set');
        }

        if (!INTEGRATIONS_TYPES[type]) throw new BadRequestError(`Integration ${type} does not exist`);

        const newIntegrationId = uuidv4();
        const sfExecutionName = `bc-apply-${customerName}-${type}-${newIntegrationId.split('-').slice(-1)[0]}-${new Date().getTime()}`;

        const integrationTrigger = (type === 'Github') ? 'none' : INTEGRATIONS_TYPES[type].trigger;
        const integrationTriggerData = (type === 'Github') ? undefined : INTEGRATIONS_TYPES[type].triggerData;

        const esRequired = INTEGRATIONS_TYPES[type].requireES;

        const triggerStepFunctionRes = await this.triggerStepFunction({ name: sfExecutionName,
            params: {
                customer_details: {
                    name: customerName,
                    aws_account_id: params.customer_aws_account_id,
                    cf_required: !!params.customer_aws_account_id,
                    es_required: esRequired
                },
                trigger: integrationTrigger,
                triggerData: integrationTriggerData,
                integration_type: type,
                integration_id: newIntegrationId,
                integration_details: JSON.stringify({
                    dateCreated,
                    updatedBy,
                    lastStatusUpdate,
                    alias
                }),
                integration_params: JSON.stringify({
                    ...params,
                    enable: true
                }),
                command: [
                    'integration',
                    'apply',
                    type
                ]
            },
            stateMachineArn: this.stateMachineApplyArn });

        if (INTEGRATIONS_TYPES[type].email && INTEGRATIONS_TYPES[type].email.trigger === EMAIL_TRIGGER.CREATION) {
            await new AWS.Lambda().invoke({
                FunctionName: process.env.EMAIL_API_LAMBDA,
                Payload: JSON.stringify({
                    body: {
                        name: INTEGRATIONS_TYPES[type].email.name,
                        type,
                        params,
                        integrationDetails: {
                            updatedBy
                        },
                        customerName
                    },
                    path: `/invoke/${INTEGRATIONS_TYPES[type].email.template}`
                })
            }).promise();
        }

        return {
            integration: {
                id: newIntegrationId,
                params,
                type,
                enable: true,
                status: integrationsDAO.STATUS.created,
                sfExecutionName
            },
            res: {
                triggerStepFunction: triggerStepFunctionRes
            }
        };
    }

    async update({ customerName, id, params, enable, updatedBy, lastStatusUpdate, alias }) {
        console.log(`Updating integration: ${id} for customer: ${customerName}`);
        const integration = await this.get({
            customerName,
            id
        });

        if (!integration) {
            throw new NotFoundError(`Integration ID ${id} does not exist`);
        }

        if (integration.status === 'IN_PROGRESS') {
            throw new BadRequestError(`The integration ID ${id} still in progress`);
        }

        if (integration.type === 'Github') {
            const tenantDetails = await customersRemoteLambda.invoke('getTenantEnvironmentDetailByCustomerName', { name: customerName });
            let existingRepos = [];
            if (tenantDetails.ok) {
                // eslint-disable-next-line no-restricted-globals
                existingRepos = tenantDetails.map(details => details.aws_account_id).filter(accountId => isNaN(accountId));
            }
            await this.persistRepositories(existingRepos, params.repositories, customerName);
        }

        const newParams = Object.assign(integration.params, params);

        const newIntegration = Object.assign(integration, removeUndefined({
            newParams,
            enable,
            sf_execution_name: `bc-update-${customerName}-${integration.type}-${integration.id.split('-')
                .slice(-1)[0]}-${new Date().getTime()}`,
            updatedBy,
            lastStatusUpdate,
            alias
        }));

        if (!newIntegration.dateCreated) newIntegration.dateCreated = new Date().getTime();
        if (!newIntegration.lastStatusUpdate) newIntegration.lastStatusUpdate = new Date().getTime();
        if (!newIntegration.alias) newIntegration.alias = 'default';
        const shouldSkipECSExecution = INTEGRATIONS_TYPES[integration.type].trigger !== 'ecs';
        const esRequired = INTEGRATIONS_TYPES[integration.type].requireES;

        console.log(`Updated Integration details: ${JSON.stringify(newIntegration)}`);
        const triggerStepFunctionRes = await this.triggerStepFunction({ name: newIntegration.sf_execution_name,
            params: {
                customer_details: {
                    name: customerName,
                    aws_account_id: newIntegration.params.customer_aws_account_id,
                    cf_required: !!newIntegration.params.customer_aws_account_id,
                    es_required: esRequired
                },
                integration_id: newIntegration.id,
                integration_type: newIntegration.type,
                skip_ecs_execution: shouldSkipECSExecution,
                integration_details: {
                    updatedBy: newIntegration.updatedBy,
                    lastStatusUpdate: newIntegration.lastStatusUpdate,
                    alias: newIntegration.alias,
                    dateCreated: newIntegration.dateCreated
                },
                trigger: INTEGRATIONS_TYPES[newIntegration.type].trigger,
                triggerData: INTEGRATIONS_TYPES[newIntegration.type].triggerData,
                integration_params: JSON.stringify({ enable: newIntegration.enable, ...newIntegration.params }),
                command: [
                    'integration',
                    'apply',
                    newIntegration.type
                ]
            },
            stateMachineArn: this.stateMachineApplyArn });

        if (INTEGRATIONS_TYPES[newIntegration.type].email && INTEGRATIONS_TYPES[newIntegration.type].email.trigger === EMAIL_TRIGGER.UPDATE) {
            await new AWS.Lambda().invoke({
                FunctionName: process.env.EMAIL_API_LAMBDA,
                Payload: JSON.stringify({
                    body: {
                        name: INTEGRATIONS_TYPES[newIntegration.type].email.name,
                        type: newIntegration.type,
                        params,
                        integrationDetails: {
                            updatedBy
                        },
                        customerName
                    },
                    path: `/invoke/${INTEGRATIONS_TYPES[newIntegration.type].email.template}`
                })
            }).promise();
        }

        return {
            integration: newIntegration,
            res: {
                triggerStepFunction: triggerStepFunctionRes
            }
        };
    }

    async setEnable({ customerName, id, enable }) {
        await this.update({
            customerName,
            id,
            enable
        });
    }

    triggerStepFunction({ name, params, stateMachineArn }) {
        console.log(`Trigger SF: ${name} with params: ${JSON.stringify(params)}`);
        const stepfunctions = new AWS.StepFunctions();
        const executionParams = {
            stateMachineArn,
            name,
            input: JSON.stringify(params)
        };
        return stepfunctions.startExecution(executionParams)
            .promise();
    }

    async delete({ customerName, id }) {
        console.log(`Deleting integration: ${id} for customer: ${customerName}`);
        const integration = await this.get({
            customerName,
            id
        });

        if (!integration) {
            throw new NotFoundError(`Integration ID ${id} does not exist`);
        }

        if (integration.status === integrationsDAO.STATUS.inProgress) {
            throw new BadRequestError(`The integration ID ${id} still in progress`);
        }

        const shouldSkipECSExecution = INTEGRATIONS_TYPES[integration.type].trigger !== 'ecs';

        const triggerStepFunctionRes = await this.triggerStepFunction({ name: `bc-destroy-${customerName}-${integration.type}-${integration.id.split('-')
            .slice(-1)[0]}-${new Date().getTime()}`,
        params: {
            customer_details: {
                name: customerName,
                aws_account_id: integration.params.customer_aws_account_id ? integration.params.customer_aws_account_id : null
            },
            integration_id: integration.id,
            integration_type: integration.type,
            integration_details: JSON.stringify(integration.integration_details),
            integration_params: JSON.stringify({ enable: false, ...integration.params }),
            skip_ecs_execution: shouldSkipECSExecution,
            command: [
                'integration',
                'destroy',
                integration.type
            ]
        },
        stateMachineArn: this.stateMachineDestroyArn });

        return {
            integration,
            res: {
                triggerStepFunction: triggerStepFunctionRes
            }
        };
    }

    async generateGithubTokenFromCode(code) {
        console.log(`Generating Github token from code : ${code}`);
        const response = await githubRemoteLambda.invoke('getToken', { code });
        return response.accessToken;
    }

    async persistRepositories(existingRepos, newRepos, customerName) {
        console.log(`Persisting Github repositories: ${newRepos} for customer: ${customerName}`);
        await Promise.all(existingRepos.map(repo => customersRemoteLambda.invoke('deleteTenantEnvironmentDetails', { aws_account_id: repo })));
        await Promise.all(newRepos.map(repo => customersRemoteLambda.invoke('saveTenantEnvironmentDetails', { repository: repo, customer_name: customerName, activation_name: 'Repository' })));
    }
}

let instance;

const getInstance = (tableName, stateMachineApplyArn, stateMachineDestroyArn) => {
    if (!instance) {
        instance = new IntegrationService(tableName, stateMachineApplyArn, stateMachineDestroyArn);
    }

    return instance;
};

module.exports = { getInstance };
