import boto3
import logging
import curator
import os
import re
import datetime
from utilsPython.es.es_utilities import ElasticsearchUtilities
from utilsPython.remote_lambda.invoke import lambda_invoke

logging.basicConfig(level=logging.INFO)
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
# tell the handler to use this format
console.setFormatter(formatter)
region = os.environ['AWS_REGION']
DEFAULT_INDEX_AGE = os.environ['DEFAULT_INDEX_RETENTION_AGE']
es_service_name = 'es'

session = boto3.Session()
credentials = session.get_credentials()
integrations_api_lambda_arn = os.environ['INTEGRATION_API_LAMBDA']

# Based on https://docs.aws.amazon.com/firehose/latest/dev/basic-deliver.html#es-index-rotation
index_date_patterns = {
    "OneHour": {"regex": r'(\d{4}-\d{2}-\d{2}-\d{2})', "datetime": "%Y-%m-%d-%H"},
    "OneDay": {"regex": r'(\d{4}-\d{2}-\d{2})(?!-)', "datetime": "%Y-%m-%d"},
    "OneWeek": {"regex": r'(\d{4}-w\d{2})', "datetime": "%G-W%V-%u"},
    "OneMonth": {"regex": r'(\d{4}-\d{2})(?!-)', "datetime": "%Y-%m"}
}


def _parse_date_to_datetime(date_pattern_type, date):
    datetime_pattern = index_date_patterns[date_pattern_type]["datetime"]
    if date_pattern_type == "OneWeek":
        # a way to get the first day of the week of the year
        date += '-1'
    return datetime.datetime.strptime(date, datetime_pattern)


def _is_old_index(index_datetime, index_age):
    time_delta = datetime.datetime.now() - index_datetime
    return time_delta.days > index_age


def _query_customer_integration_params(customer_name):
    body = {"customerName": customer_name, "type": "elasticsearch"}
    customer_es_integration = lambda_invoke(integrations_api_lambda_arn, "dao/getByType", body, format=None)[0]
    return customer_es_integration['params']


def _filter_unwanted_indices(indices):
    return [index for index in indices if not index.startswith('.')]


def _compute_customer_index_age(customer_name):
    index_age = int(DEFAULT_INDEX_AGE)
    customer_es_integration = _query_customer_integration_params(customer_name)
    customer_index_age = customer_es_integration.get('index_age')
    if customer_index_age:
        index_age = customer_index_age
    return index_age


def collect_indices_by_date_patterns(index_list):
    # Collect indices by their date's pattern
    matched_indices = []
    for date_pattern, date_pattern_obj in index_date_patterns.items():
        pattern_regex = date_pattern_obj["regex"]
        matched_indices += [
            (index, _parse_date_to_datetime(date_pattern, re.search(pattern_regex, index).group(1))) for index in
            index_list if re.search(pattern_regex, index)]
    return matched_indices


def handler(event, context):
    customer_name = event["customer_name"]
    domain_name = f'bc-es-{customer_name}'
    index_age = _compute_customer_index_age(customer_name)
    es_util_client = ElasticsearchUtilities(domain_name=domain_name, region=region)
    es_host = es_util_client.es_host
    es_instance = es_util_client.es_instance

    es_indices = curator.IndexList(es_instance)
    index_list = es_indices.indices
    index_list = _filter_unwanted_indices(index_list)
    matched_indices = collect_indices_by_date_patterns(index_list)

    old_indices = [index for (index, index_datetime) in matched_indices if _is_old_index(index_datetime, index_age)]
    if old_indices:
        try:
            es_indices.indices = old_indices
            logging.info(f"Attempting to delete {old_indices}")
            curator.DeleteIndices(es_indices).do_action()
            logging.info(f"Successfully deleted indices for customer {customer_name} on es host: {es_host}")
        except Exception as e:
            logging.error(
                f"Failed to delete index {old_indices} for customer {customer_name} on es host: {es_host}\n{e}")
