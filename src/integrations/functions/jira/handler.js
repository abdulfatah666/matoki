const AWS = require('aws-sdk');

const ssm = new AWS.SSM();

const corsHeaders = {
    'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date',
    'Access-Control-Allow-Methods': '*',
    'Access-Control-Allow-Origin': '*'
};

module.exports.jiraConf = async (event) => {
    /* eslint-disable-next-line no-param-reassign */
    event.body = JSON.parse(event.body);

    const { customerName } = event.body;
    const { jiraEmail } = event.body;

    if (!customerName || !jiraEmail) {
        return {
            statusCode: 400,
            body: JSON.stringify('customer name or email are missing'),
            headers: corsHeaders
        };
    }

    const ssmParams = {
        Name: `/${customerName}/jira_ticket_email`,
        Type: 'String',
        Value: jiraEmail,
        Overwrite: true
    };

    return ssm.putParameter(ssmParams).promise()
        .then(() => ({
            statusCode: 200,
            body: JSON.stringify('Success'),
            headers: corsHeaders
        })).catch((err) => {
            console.log(err.message);
            return {
                statusCode: 400,
                body: JSON.stringify(err.message),
                headers: corsHeaders
            };
        });
};
