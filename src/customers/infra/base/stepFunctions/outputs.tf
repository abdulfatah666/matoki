output "customer_step_function_arn" {
  value = aws_sfn_state_machine.bc-create-customer-sf.id
}

output "update_customer_step_function_arn" {
  value = aws_sfn_state_machine.bc-update-customer-sf.id
}

output "destroy_customer_step_function_arn" {
  value = aws_sfn_state_machine.bc-destroy-customer-sf.id
}