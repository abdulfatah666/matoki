variable "region" {
  type        = "string"
  description = "AWS provider region"
}

variable "aws_profile" {
  type        = "string"
  description = "AWS provider profile"
}

variable "aws_account_id" {
  type        = "string"
  description = "AWS account id"
}
variable "unique_tag" {
  type = "string"
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
}

variable "aws_ecs_task_definition_arn_customer" {
  type = "string"
}

variable "aws_ecs_task_definition_arn_integration" {
  type = "string"
}

variable "cluster_arn" {
  type = "string"
}

variable "subnet" {
  type = "string"
}

variable "container_name" {
  type = "string"
}

variable "integrations_sf_arn" {
  type = "string"
}

variable "integrations_destroy_sf_arn" {
  type = "string"
}

variable "monitor" {
  type = "map"

  default = {
    enable               = false
    sns_topic_error_name = ""
  }
}