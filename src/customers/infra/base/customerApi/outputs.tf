output "function_name" {
  value = module.customers_api_lambda.function_name
}

output "function_arn" {
  value = module.customers_api_lambda.function_arn
}
