variable "region" {
  type        = "string"
  description = "the aws region"
}

variable "aws_profile" {
  type = "string"
}

variable "monitor" {
  type = "map"

  default = {
    enable               = false
    sns_topic_error_name = ""
  }
}

variable "unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
}

variable "sls_bucket_name" {
  description = "A name for deployment bucket that all serverless function deployment state is saved on"
  type        = "string"
}

variable "template_url" {
  description = "Sensor Template Path"
  type        = "string"
}

variable "config_template_url" {
  description = "Config Template Path"
  type        = "string"
}

variable "customer_step_function_arn" {
  type = "string"
}

variable "update_customer_step_function_arn" {
  type = "string"
}

variable "destroy_customer_step_function_arn" {
  type = "string"
}

variable "scanners_module_name" {
  type = "string"
  description = "Scanners Lambdas module name"
}