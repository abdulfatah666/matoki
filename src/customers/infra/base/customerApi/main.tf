locals {
  function_name    = "customers-api-${var.unique_tag}"
  bridgecrew_utils = "bridgecrew-utils-${var.unique_tag}"
  slack_hook_path  = var.aws_profile == "prod" ? module.consts.prod_new_customer_slack_hook_paths : module.consts.default_new_customer_default_slack_hook_path
}

module consts {
  source = "../../../../utils/terraform/consts"
}

module "customers_api_lambda" {
  source                 = "../../../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../customers/functions"
  variable_string        = "--stepFunctionCustomerArn ${var.customer_step_function_arn} --stepFunctionUpdateCustomerArn ${var.update_customer_step_function_arn} --stepFunctionDestroyCustomerArn ${var.destroy_customer_step_function_arn} --lambdaName ${local.function_name} --config_template_s3_bucket ${var.config_template_url} --template_s3_bucket ${var.template_url} --slack_hook_path ${local.slack_hook_path} --scanners-module-name ${var.scanners_module_name}"
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.sls_bucket_name
  lambda_names = [
    local.function_name,
  ]
}
