/* eslint-disable no-restricted-syntax,no-param-reassign,class-methods-use-this */
const AWS = require('aws-sdk');
const https = require('https');
const { BadRequestError, UnauthorizedError } = require('@bridgecrew/nodeUtils/errors/http');
const tenantDao = require('./tenantDao');

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const ssm = new AWS.SSM();

const customerStackStatuses = {
    CREATING: 'CREATING',
    UPDATING: 'UPDATING',
    DESTROYING: 'DESTROYING',
    COMPLETED: 'COMPLETED',
    FAILED: 'FAILED'
};

const INITAL_PASSWORD_LENGTH = 8;

function removeUndefined(obj) {
    for (const k in obj) if (obj[k] === undefined) delete obj[k];
    return obj;
}

class TenantController {
    constructor(tableName, stateMachineCreateArn, stateMachineUpdateArn, stateMachineDestroyArn, ssmUserPoolName) {
        this.tableName = tableName; // process.env.TENANTS_TABLE_NAME
        this.stateMachineCreateArn = stateMachineCreateArn; // process.env.TENANTS_TABLE_NAME
        this.stateMachineUpdateArn = stateMachineUpdateArn; // process.env.TENANTS_TABLE_NAME
        this.stateMachineDestroyArn = stateMachineDestroyArn; // process.env.TENANTS_TABLE_NAME
        this.ssmUserPoolName = ssmUserPoolName;
    }

    async sendSlackNotification(messageStr) {
        // Notify customer creation via slack
        const options = {
            hostname: 'hooks.slack.com',
            method: 'POST',
            path: `/services/${process.env.SLACK_HOOK_PATH}`
        };
        return new Promise((resolve) => {
            const req = https.request(options, () => resolve('Sent a slack notification'));
            req.write(JSON.stringify({ text: messageStr }));
            req.end();
            resolve('Sent slack update');
        });
    }

    async listInvitation({ userDetails }) {
        if (userDetails && userDetails.customers && userDetails.customers[0]) {
            const customerName = userDetails.customers[0];
            const userpoolId = await ssm.getParameter(
                {
                    Name: this.ssmUserPoolName,
                    WithDecryption: true

                }
            )
                .promise();

            let users = [];
            let token = '';

            while (token !== undefined) {
                const { Users, PaginationToken } = await cognitoidentityserviceprovider.listUsers({
                    UserPoolId: userpoolId.Parameter.Value,
                    Limit: 60,
                    PaginationToken: token !== '' ? token : undefined
                }).promise();

                token = PaginationToken;
                users = users.concat(Users.filter((User) => User.Attributes
                    .find((attribute) => attribute.Name === 'custom:org' && attribute.Value === customerName)));
            }

            return users.map((user) => ({
                username: user.Username,
                ...user.Attributes.reduce((accu, curr) => {
                    const attribute = {};
                    attribute[curr.Name] = curr.Value;
                    return { ...accu, ...attribute };
                }, {})
            }));
        }
    }

    async invitation({ email, userDetails }) {
        if (userDetails && userDetails.customers && userDetails.customers[0]) {
            await this.createCognitoUser({
                customer: {
                    owner_email: email,
                    customer_name: userDetails.customers[0]
                },
                welcomeEmail: false
            });

            const emailApiParams = {
                FunctionName: process.env.EMAIL_API_LAMBDA,
                Payload: JSON.stringify({
                    body: {
                        inviter: {
                            mail: userDetails.email,
                            name: userDetails.name && userDetails.family_name ? `${userDetails.name} ${userDetails.family_name}` : null
                        },
                        dest: {
                            mail: email
                        }
                    },
                    path: '/invoke/invitation'
                })
            };

            await new AWS.Lambda().invoke(emailApiParams).promise();

            return {
                email,
                customerName: userDetails.customers[0]
            };
        }
        throw new UnauthorizedError('Unauthorized');
    }

    async deleteInvitation({ email, userDetails }) {
        if (email !== userDetails.email) {
            await this.deleteCognitoUser({ customer: { owner_email: email } });
            return null;
        }
        throw new BadRequestError("can\\'t delete yourself'");
    }

    async createCognitoUser({ customer, welcomeEmail = true }) {
        const userpoolId = await ssm.getParameter(
            {
                Name: this.ssmUserPoolName,
                WithDecryption: true

            }
        )
            .promise();

        if (!customer.customer_name) {
            throw new BadRequestError('Customer name does not set');
        }

        if (!customer.owner_email) {
            throw new BadRequestError('owner_email field is required');
        }

        const users = await cognitoidentityserviceprovider.listUsers({
            UserPoolId: userpoolId.Parameter.Value
        })
            .promise();

        const user = users.Users.find((findUser) => findUser.Username === customer.owner_email);
        if (user) {
            throw new BadRequestError('user already exist');
        } else {
            const UserAttributes = [];
            if (customer.owner_first_name) {
                UserAttributes.push({
                    Name: 'name',
                    Value: customer.owner_first_name
                });
            }

            if (customer.owner_last_name) {
                UserAttributes.push({
                    Name: 'family_name',
                    Value: customer.owner_last_name
                });
            }

            if (customer.owner_phone) {
                UserAttributes.push({
                    Name: 'phone_number',
                    Value: customer.owner_phone
                });
            }

            const temporaryPassword = this.generatePassword();
            return await cognitoidentityserviceprovider.adminCreateUser({
                UserPoolId: userpoolId.Parameter.Value,
                Username: customer.owner_email,
                DesiredDeliveryMediums: [],
                TemporaryPassword: temporaryPassword,
                MessageAction: 'SUPPRESS',
                UserAttributes: [
                    ...UserAttributes,
                    {
                        Name: 'email',
                        Value: customer.owner_email
                    },
                    {
                        Name: 'custom:org',
                        Value: customer.customer_name
                    }
                ]
            })
                .promise()
                .then(async newUser => {
                    if (welcomeEmail) {
                        console.log('send welcome email with password');
                        const emailApiParams = {
                            FunctionName: process.env.EMAIL_API_LAMBDA,
                            Payload: JSON.stringify({
                                body: {
                                    dest: {
                                        mail: customer.owner_email
                                    },
                                    temporaryPassword
                                },
                                path: '/invoke/welcome'
                            })
                        };

                        await new AWS.Lambda().invoke(emailApiParams).promise();
                        return newUser;
                    }
                });
        }
    }

    async deleteCognitoUser({ customer }) {
        const userpoolId = await ssm.getParameter(
            {
                Name: this.ssmUserPoolName,
                WithDecryption: true

            }
        )
            .promise();

        const users = await cognitoidentityserviceprovider.listUsers({
            UserPoolId: userpoolId.Parameter.Value
        })
            .promise();

        const user = users.Users.find((findUser) => findUser.Username === customer.owner_email);
        if (!user) {
            throw new BadRequestError('user does not exist');
        } else {
            return await cognitoidentityserviceprovider.adminDeleteUser({
                UserPoolId: userpoolId.Parameter.Value,
                Username: customer.owner_email
            })
                .promise();
        }
    }

    async create(customer) {
        if (!customer.customer_name) {
            throw new BadRequestError('Customer name does not set');
        }
        let customerExists;
        try {
            customerExists = await tenantDao._getCustomerByName(customer.customer_name);
        } catch (err) {
            console.log(err);
        }

        if (customerExists) {
            throw new BadRequestError('The account name is already in use. Please choose another name.');
        }

        const message = `Creating a new customer - ${customer.customer_name} (${customer.owner_email})`;
        await this.sendSlackNotification(message)
            .catch((err) => {
                console.error(`Couldn't send message to slack for customer ${customer.customer_name}. Proceeding with the setup nonetheless`);
                console.error(err.message);
                return 'Ignoring slack error';
            });

        // trigger signup step function
        const csExecutionName = `bc-apply-${customer.customer_name}-${new Date().getTime()}`;
        const data = {
            Customer: {
                customer_name: customer.customer_name,
                owner_first_name: customer.owner_first_name,
                owner_last_name: customer.owner_last_name,
                owner_phone: customer.owner_phone,
                owner_email: customer.owner_email,
                cs_execution: {
                    sf_execution_name: csExecutionName,
                    status: customerStackStatuses.CREATING
                }
            }
        };

        const triggerStepFunctionRes = await this.triggerStepFunction(csExecutionName, data, this.stateMachineCreateArn);

        return {
            customer: data,
            res: {
                triggerStepFunction: triggerStepFunctionRes
            }
        };
    }

    async delete(customerName) {
        if (!customerName) {
            throw new BadRequestError('Customer name does not set');
        }
        let customer;
        try {
            customer = await tenantDao.getCustomerByName({ name: customerName });
        } catch (err) {
            console.log(err);
        }

        if (!customer) {
            throw new BadRequestError(`Customer ${customerName} does not exist`);
        }

        if (customer.status === customerStackStatuses.CREATING || customer.status === customerStackStatuses.DESTROYING || customer.status === customerStackStatuses.UPDATING) {
            throw new BadRequestError(`Customer ${customerName} is in status IN_PROGRESS`);
        }

        const message = `Destroying the customer ${customerName}`;
        await this.sendSlackNotification(message);

        // trigger destroy step function
        const csExecutionName = `bc-destroy-${customer.customer_name}-${new Date().getTime()}`;
        const data = {
            Customer: {
                customer_name: customer.customer_name,
                owner_first_name: customer.owner_first_name,
                owner_last_name: customer.owner_last_name,
                owner_phone: customer.owner_phone,
                owner_email: customer.owner_email,
                cs_execution: {
                    sf_execution_name: customer.cs_execution.sf_execution_name,
                    sf_execution_name_destroy: csExecutionName,
                    status: customerStackStatuses.DESTROYING
                }
            }
        };

        const triggerStepFunctionRes = await this.triggerStepFunction(csExecutionName, data, this.stateMachineDestroyArn);

        return {
            customer: data,
            res: {
                triggerStepFunction: triggerStepFunctionRes
            }
        };
    }

    async update({
        customerName, firstName, lastName, email, phone
    }) {
        if (!customerName) {
            throw new BadRequestError('Customer name does not set');
        }
        let customer;
        try {
            customer = await tenantDao.getCustomerByName({ name: customerName });
        } catch (err) {
            console.log(err);
        }

        if (!customer) {
            throw new BadRequestError(`Customer ${customerName} does not exist`);
        }

        if (customer.status === customerStackStatuses.CREATING || customer.status === customerStackStatuses.DESTROYING || customer.status === customerStackStatuses.UPDATING) {
            throw new BadRequestError(`The customer ${customerName} still in progress`);
        }

        const newCustomer = Object.assign(customer, removeUndefined({
            firstName,
            lastName,
            phone,
            email
        }));

        // trigger signup step function
        const csExecutionName = `bc-update-${customer.customer_name}-${new Date().getTime()}`;
        const data = {
            Customer: {
                customer_name: customerName,
                owner_first_name: newCustomer.owner_first_name,
                owner_last_name: newCustomer.owner_last_name,
                owner_phone: newCustomer.owner_phone,
                owner_email: newCustomer.owner_email,
                cs_execution: {
                    sf_execution_name: csExecutionName,
                    status: customerStackStatuses.UPDATING
                }
            }
        };

        const triggerStepFunctionRes = await this.triggerStepFunction(csExecutionName, data, this.stateMachineUpdateArn);

        return {
            customer: data,
            res: {
                triggerStepFunction: triggerStepFunctionRes
            }
        };
    }

    triggerStepFunction(name, params, stateMachineArn) {
        const stepfunctions = new AWS.StepFunctions();
        const executionParams = {
            stateMachineArn,
            name,
            input: JSON.stringify(params)
        };
        return stepfunctions.startExecution(executionParams)
            .promise();
    }

    getCloudFormationUrl(params) {
        return tenantDao.getCloudFormationUrl(params);
    }

    generatePassword() {
        const upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        const lower = 'abcdefghijklmnopqrstuvwxyz';
        const digit = '0123456789';
        const all = upper + lower + digit;

        // generate random integer not greater than `max`
        function rand(max) {
            return Math.floor(Math.random() * max);
        }

        // generate random character of the given `set`
        function random(set) {
            return set[rand(set.length - 1)];
        }

        // generate an array with the given `length`of characters of the given `set`
        function generate(length, set) {
            const result = [];
            while (length--) result.push(random(set));
            return result;
        }

        // shuffle an array randomly
        function shuffle(arr) {
            let result = [];

            while (arr.length) {
                result = result.concat(arr.splice(rand[arr.length - 1]));
            }

            return result;
        }

        let result = [];
        result = result.concat(generate(1, upper)); // 1 upper case
        result = result.concat(generate(1, lower)); // 1 lower case
        result = result.concat(generate(1, digit)); // 1 digit
        result = result.concat(generate(INITAL_PASSWORD_LENGTH - 3, all)); // remaining - whatever

        return shuffle(result).join(''); // shuffle and make a string
    }

    async signup(customer) {
        // Check if a customer with this name is already exists
        const customerName = customer.customer_name;
        if (!customerName) {
            throw new BadRequestError('Customer name does not set');
        }
        const existingCustomer = await tenantDao.getCustomerByName({ name: customerName });

        if (existingCustomer) throw new Error('Customer name already exists');

        await this.createCognitoUser({
            customer
        });

        const cloudformationParams = {
            customerName,
            stackName: `${customerName}-bridgecrew`,
            param_ResourceNamePrefix: `${customerName}-bc`,
            param_CustomerName: customerName
        };
        // generate cloud formation url:
        const cloudFormation = this.getCloudFormationUrl(cloudformationParams);
        return cloudFormation;
    }
}

let instance;

const getInstance = (tableName, stateMachineCreateArn, stateMachineUpdateArn, stateMachineDestroyArn, ssmUserPoolName) => {
    if (!instance) {
        instance = new TenantController(tableName, stateMachineCreateArn, stateMachineUpdateArn, stateMachineDestroyArn, ssmUserPoolName);
    }

    return instance;
};

module.exports = { getInstance };
