/* eslint-disable camelcase */
const AWS = require('aws-sdk/index');
const querystring = require('querystring');
const IdentityPoolCli = require('@bridgecrew/nodeUtils/cognito/identity_pool');
const tenantEnvDao = require('../tenantEnv/tenantEnvDao');

const { SENSOR_TEMPLATE_PATH, CONFIG_TEMPLATE_PATH, SSM_USER_POOL_ID, SSM_APP_CLIENT_ID, REGION, IDENTITY_POOL_ROLE, BASE_STACK_UNIQUE_TAG } = process.env;
const CONSOLE_CLOUDFORMATION_URL = 'https://us-west-2.console.aws.amazon.com/cloudformation/home?#/stacks/create/review?';
const TENANTS_TABLE = process.env.TENANTS_TABLE_NAME;

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const USERPOOL_NAME = process.env.SSM_USER_POOL_ID;
const ssm = new AWS.SSM();
const iam = new AWS.IAM();

const EXPRESSIONS_FIELDS = {
    customerName: 'customer_name',
    email: 'owner_email',
    first_name: 'owner_first_name',
    last_name: 'owner_last_name',
    phone: 'owner_phone'
};

class TenantDao {
    constructor() {
        this.documentClient = new AWS.DynamoDB.DocumentClient();
    }

    // private:
    async _getCustomerByName(name) {
        const params = {
            TableName: TENANTS_TABLE,
            Key: {
                customer_name: name
            }
        };

        const result = await this.documentClient.get(params).promise();

        // In case there are no results, return empty object.
        if (!result.Item) {
            console.log('No Item found for customer: ', name);
            return null;
        }

        return result.Item;
    }

    async _getCustomerByNameWithQuery(customerName, query) {
        const queryKeys = Object.keys(query);
        const keyConditionExpression = '#customerName = :value_customerName';
        const filterExpression = queryKeys.map((key) => (EXPRESSIONS_FIELDS[key] ? `#${EXPRESSIONS_FIELDS[key]} = :value_${key}` : null))
        // Filter out any key which is not in EXPRESSIONS_FIELDS
            .filter((exp) => exp)
            .join(' and ');

        const expressionAttributeValues = {};
        const expressionAttributeNames = {};
        queryKeys.forEach((key) => {
            expressionAttributeNames[`#${EXPRESSIONS_FIELDS[key]}`] = EXPRESSIONS_FIELDS[key];
            expressionAttributeValues[`:value_${key}`] = query[key];
        });
        expressionAttributeNames['#customerName'] = 'customer_name';
        expressionAttributeValues[':value_customerName'] = customerName;

        console.debug(`KeyConditionExpression is: ${keyConditionExpression}`);
        console.debug(`filterExpression is: ${JSON.stringify(filterExpression)}`);
        console.debug(`ExpressionAttributeValues is: ${JSON.stringify(expressionAttributeValues)}`);
        console.debug(`ExpressionAttributeNames is: ${JSON.stringify(expressionAttributeNames)}`);

        const result = await this.documentClient.query({
            TableName: TENANTS_TABLE,
            KeyConditionExpression: keyConditionExpression,
            ExpressionAttributeNames: expressionAttributeNames,
            ExpressionAttributeValues: expressionAttributeValues,
            FilterExpression: filterExpression || undefined
        }).promise();

        if (!result.Items || result.Items.length === 0) {
            console.error('No Items found');
            return null;
        }
        return result.Items[0];
    }

    _saveTenantItem(itemFields) {
        const {
            customer_name, owner_email, owner_first_name, owner_last_name, owner_phone, cs_execution
        } = itemFields;
        const params = {
            TableName: TENANTS_TABLE,
            Item: {
                customer_name, owner_email, owner_first_name, owner_last_name, owner_phone, cs_execution
            }
        };
        return this.documentClient.put(params).promise();
    }

    async _deleteTenantItem({ customerName }) {
        try {
            // delete tenant env details
            await tenantEnvDao.deleteTenantEnvironmentDetailsByCustomerName(customerName);
            const params = {
                TableName: TENANTS_TABLE,
                Key: {
                    customer_name: customerName
                }
            };
            return this.documentClient.delete(params).promise();
        } catch (err) {
            throw new Error(`Can't delete tenant item for: ${customerName} due to error: ${err}`);
        }
    }

    async _getCustomerByAccountId(awsAccountId) {
        const tenantEnvItem = await tenantEnvDao.getTenantEnvironmentDetails({ awsAccountId });
        if (!tenantEnvItem) throw new Error(`Can't get tenantEnv item for: ${awsAccountId}`);
        const customerName = tenantEnvItem.customer_name;
        return this.getCustomerByName({ name: customerName });
    }

    // eslint-disable-next-line class-methods-use-this
    async _createIdentityPool(customerName) {
        const identityPoolCli = new IdentityPoolCli();
        const identityPoolName = `IP_${customerName}`;

        let identityPool = await identityPoolCli.get(identityPoolName);
        if (!identityPool) {
            identityPool = await identityPoolCli.create(identityPoolName, {
                roles: {
                    authenticated: IDENTITY_POOL_ROLE
                }
            });
        }

        const SSM = new AWS.SSM();

        const userpoolId = await SSM.getParameter(
            {
                Name: SSM_USER_POOL_ID

            }
        )
            .promise()
            .then((userpoolIdParam) => userpoolIdParam.Parameter.Value);

        const appClientId = await SSM.getParameter(
            {
                Name: SSM_APP_CLIENT_ID

            }
        )
            .promise()
            .then((appClientIdParam) => appClientIdParam.Parameter.Value);

        const ProviderName = `cognito-idp.${REGION}.amazonaws.com/${userpoolId}`;

        const cognitoProvider = await identityPoolCli.getCognitoProvider(identityPoolName, ProviderName, appClientId);
        if (!cognitoProvider) {
            await identityPoolCli.addCognitoProvider(identityPoolName, {
                ClientId: appClientId,
                ProviderName: `cognito-idp.${REGION}.amazonaws.com/${userpoolId}`,
                ServerSideTokenCheck: false
            });
        }

        const roleName = `app_users_authenticated_${BASE_STACK_UNIQUE_TAG}_${customerName}`;

        try {
            await iam.getRole({ RoleName: roleName })
                .promise();
        } catch (e) {
            const CreateRoleParams = {
                AssumeRolePolicyDocument: JSON.stringify({
                    Version: '2012-10-17',
                    Statement: [
                        {
                            Sid: '',
                            Effect: 'Allow',
                            Principal: {
                                Federated: 'cognito-identity.amazonaws.com'
                            },
                            Action: 'sts:AssumeRoleWithWebIdentity',
                            Condition: {
                                'ForAnyValue:StringLike': {
                                    'cognito-identity.amazonaws.com:amr': 'authenticated'
                                },
                                StringEquals: {
                                    'cognito-identity.amazonaws.com:aud': identityPool.IdentityPoolId
                                }
                            }
                        }
                    ]
                }),
                RoleName: roleName,
                Description: 'AWS Cognito Role Auth'
            };

            const createPolicyParams = {
                PolicyDocument: JSON.stringify({
                    Version: '2012-10-17',
                    Statement: [{
                        Effect: 'Allow',
                        Action: [
                            'mobileanalytics:PutEvents',
                            'cognito-sync:*',
                            'cognito-identity:*'
                        ],
                        Resource: '*'
                    }]
                }),
                PolicyName: 'cognito_policy',
                RoleName: CreateRoleParams.RoleName
            };

            await iam.createRole(CreateRoleParams)
                .promise();

            await iam.putRolePolicy(createPolicyParams)
                .promise();

            await identityPoolCli.update(identityPoolName, {
                roles: {
                    authenticated: `${IDENTITY_POOL_ROLE.split('/')[0]}/${CreateRoleParams.RoleName}`
                }
            });
        }
    }

    // public:
    async getCustomerByName(params) {
        if (params.query) return this._getCustomerByNameWithQuery(params.name, params.query);
        return this._getCustomerByName(params.name);
    }

    getCustomerByAccountId(params) {
        return this._getCustomerByAccountId(params.awsAccountId);
    }

    async saveTenantItem(params) {
        await this._createIdentityPool(params.customer_name);
        await this._saveTenantItem(params);
        return true;
    }

    async deleteTenantItem(params) {
        await this._deleteTenantItem(params);
        return true;
    }

    async getActiveCustomer(name) {
        const customer = await this.getCustomerByName(name);
        if (!customer || (customer.cs_execution && customer.cs_execution.status !== 'COMPLETED')) {
            throw new Error('No Item found');
        }
        return customer;
    }

    async updateCustomerStatus(customerName, status) {
        const existingCustomer = await this._getCustomerByName(customerName);

        // If the customer not exist throw an exception
        if (!existingCustomer) throw new Error('No Item found');

        if (existingCustomer.cs_execution) {
            existingCustomer.cs_execution.status = status;
        } else {
            Object.assign(existingCustomer, { cs_execution: { status } });
        }
        const params = {
            TableName: TENANTS_TABLE,
            Item: existingCustomer
        };
        try {
            return this.documentClient.put(params).promise();
        } catch (e) {
            console.error(`ERROR: ${e}`);
            return null;
        }
    }

    async setStatus({ customerName, status }) {
        return this.updateCustomerStatus(customerName, status);
    }

    /* eslint-disable-next-line class-methods-use-this */
    getCloudFormationUrl(params) {
        const { stackName, param_ResourceNamePrefix, param_CustomerName } = params;
        const externalId = Math.random().toString(36).substr(2, 6).toUpperCase(); // 6 digit random digits
        const variables = {
            templateURL: params.type === 'read' ? CONFIG_TEMPLATE_PATH : SENSOR_TEMPLATE_PATH,
            param_ExternalID: externalId,
            stackName,
            param_ResourceNamePrefix,
            param_CustomerName
        };

        return {
            variables,
            url: CONSOLE_CLOUDFORMATION_URL + querystring.stringify(variables)
        };
    }

    async getTenantCognitoSubByCustomerName({ customerName }) {
        const [customerObj, userPoolId] = await Promise.all([
            this._getCustomerByName(customerName),
            ssm.getParameter({ Name: USERPOOL_NAME }).promise().then(response => response.Parameter.Value)
        ]);

        // In case the customer not exist
        if (!customerObj) throw new Error('No customer item found');

        const userResp = await cognitoidentityserviceprovider.adminGetUser({ UserPoolId: userPoolId, Username: `${customerObj.owner_email}` }).promise();
        console.log(userResp);

        return userResp.UserAttributes.find(att => att.Name === 'sub').Value;
    }
}

TenantDao.requiredFunctionParams = {
    getCustomerByName: ['name'],
    getCustomerByAccountId: ['awsAccountId'],
    saveTenantItem: ['customer_name', 'owner_email', 'owner_first_name', 'owner_last_name', 'owner_phone'],
    getCloudFormationUrl: ['customerName'],
    setStatus: ['customerName', 'status'],
    deleteTenantItem: ['customerName'],
    getActiveCustomer: ['name'],
    getTenantCognitoSubByCustomerName: ['customerName']
};

module.exports = new TenantDao();
