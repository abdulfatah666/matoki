# CUSTOMERS API  - /invoke/:FUNC_NAME 

## Intro
Customer API contains a inside route (app.get('/invoke/:funcName')) - for internal use only.

## Info
The API that described below wrap inside calls to lambda for CRUD tenants and tenants_env_detalis tables.

## How to use
NodeJS:

```js
const RemoteLambda = require('@bridgecrew/nodeUtils/remoteLambda/invoke');
const remoteLambda = new RemoteLambda(process.env['CUSTOMERS_API_LAMBDA']);
let response  = remoteLambda.invoke("getCustomerAccountIdsByName", {name: "customer_1"});
```

Python:
```py
from utilsPython.remote_lambda.invoke import lambda_invoke
customers_api_lambda_name = os.environ.get('CUSTOMERS_API_LAMBDA_NAME', "")
response = lambda_invoke(lambdaFunctionName=customers_api_lambda_name, name = "getCustomerAccountIdsByName", body = json.dumps({"name": customer_1}))
```

## Tenants table structure
[Here](https://github.com/bridgecrewio/platform/wiki/%28MultiAccount%29-Migrate-tenants-and-tenants_env_detalis-table) you can see details of the tables.

# API:

## List of end points:

- ### /invoke/getCustomerByName
- ### /invoke/getCustomerByAccountId
- ### /invoke/saveTenantItem
- ### /invoke/getCloudFormationUrl
- ### /invoke/removeS3AccessDetails
- ### /invoke/setS3AccessDetails
- ### /invoke/removeRemoteRemediationDetails
- ### /invoke/setRemoteRemediationDetails
- ### /invoke/getCustomerAccountIdsByName
- ### /invoke/saveTenantEnvironmentDetails
- ### /invoke/getTenantEnvironmentDetails
- ### /invoke/getTenantEnvironmentDetailByCustomerName

  
**Get Customer By Name**  
----  
  Returns tenant item (json)  data about a single customer.  
  
* **URL**  - /invoke/getCustomerByName 
* **Method:** `GET`
*  **URL Params**
   **Required:**
   `name=[string]`
    **Optional:**
   `query=[object]`

* **Success Response:**

  * **Code:** 200
    **Content:** 
    ```json
    {
        "owner_first_name": "yehuda",
        "owner_last_name": "livnoni",
        "owner_phone": "+972543007218",
        "owner_email": "yehuda@bridgecrew.io",
        "customer_name": "fake1"
    }
    ```

  
**Get Customer By Account ID**  
----  
  Returns tenant item (json)  data about a single customer.      
* **URL**  - /invoke/getCustomerByAccountId 
* **Method:** `GET`
*  **URL Params**
   **Required:**
   `awsAccountId=[string]`
    **Optional:**
      None

* **Success Response:**

  * **Code:** 200
    **Content:** 
    ```json
    {
        "owner_first_name": "yehuda",
        "owner_last_name": "livnoni",
        "owner_phone": "+972543007218",
        "owner_email": "yehuda@bridgecrew.io",
        "customer_name": "fake1"
    }
    ```   
    
**Create New Tenant Row (New Customer)**  
----  
  Returns tenant item (json)  data about a single customer.      
* **URL**  - /invoke/saveTenantItem 
* **Method:** `GET`
*  **URL Params**
   **Required:**
   `customer_name=[string]`
   `owner_email=[string]`
   `owner_first_name=[string]`
   `owner_last_name=[string]`
   `owner_phone=[string]`   
    **Optional:**
      None

* **Success Response:**

  * **Code:** 200
    **Content:**
   ```json
    true
    ```
**Get Tenant Environment Details Item**  
----  
  Returns tenant environment details item (json) data about a single account.      
* **URL**  - /invoke/getTenantEnvironmentDetails 
* **Method:** `GET`
*  **URL Params**
   **Required:**
   `awsAccountId=[string]`
    **Optional:**
      None

* **Success Response:**

  * **Code:** 200
    **Content:** 
    ```json
    {
      "cloud_trail_cf_details": {
        "sqs_queue_url": "fake2-aqs-queue-url",
        "external_id": "WLHMEI",
        "ct_stack_name": "bridgecrew-bridgecrewcws"
      },
      "remote_remediation_cf_details": {
        "external_id": {
          "S": "h3PDni"
        },
        "role_arn": {
          "S": "arn:aws:iam::809694787632:role/bridgecrew-allow-remote-remediation-role"
        },
        "installed_version": {
          "S": "1.0.0"
        },
        "stack_id": {
          "S": "arn:aws:cloudformation:us-west-2:809694787632:stack/bridgecrew-remediation/96f93c10-f010-11e9-9955-06cac24c53a2"
        }
      },
      "cross_account_cf_details": {
        "cross_account_role_arn": "fake2-account-role-arn",
      },
      "customer_name": "fake1",
      "aws_account_id": "1111111111"
    }
    ```     

**Get Tenant Cloud Formation URL**  
----  
  Returns cloud formatio url.      
* **URL**  - /invoke/getCloudFormationUrl 
* **Method:** `GET`
*  **URL Params**
   **Required:**
   `customerName=[string]`
    **Optional:**
      None

* **Success Response:**

  * **Code:** 200
    **Content:** 
    ```text
        https://us-west-2.console.aws.amazon.com/cloudformation/home?#/stacks/create/review?templateURL=&param_ExternalID=&stackName=-bridgecrew&param_ResourceNamePrefix=-bc&param_CustomerName=
    ```
    
    
**Get Tenant Environment Detail By Customer Name**  
----  
  Returns tenant environment details item (json) data about a single account.      
* **URL**  - /invoke/getTenantEnvironmentDetailByCustomerName 
* **Method:** `GET`
*  **URL Params**
   **Required:**
   `name=[string]`
    **Optional:**
      None

* **Success Response:**

  * **Code:** 200
    **Content:** 
     ```json
    [{
      "cloud_trail_cf_details": {
        "sqs_queue_url": "fake2-aqs-queue-url",
        "external_id": "WLHMEI",
        "ct_stack_name": "bridgecrew-bridgecrewcws"
      },
      "remote_remediation_cf_details": {
        "external_id": {
          "S": "h3PDni"
        },
        "role_arn": {
          "S": "arn:aws:iam::809694787632:role/bridgecrew-allow-remote-remediation-role"
        },
        "installed_version": {
          "S": "1.0.0"
        },
        "stack_id": {
          "S": "arn:aws:cloudformation:us-west-2:809694787632:stack/bridgecrew-remediation/96f93c10-f010-11e9-9955-06cac24c53a2"
        }
      },
      "cross_account_cf_details": {
        "cross_account_role_arn": "fake2-account-role-arn"
      },
      "customer_name": "fake1",
      "aws_account_id": "1111111111"
    }]
    ```     

**Save New Tenant Environment Details Item**  
----  
  Returns cloud formatio url.      
* **URL**  - /invoke/saveTenantEnvironmentDetails 
* **Method:** `GET`
*  **URL Params**
   **Required:**
   `customer_name=[string], aws_account_id=[string], role_arn=[string], cross_account_role_arn=[string], sqs_queue_url=[string]`
    **Optional:**
      `external_id=[string]`

* **Success Response:**

  * **Code:** 200
    **Content:** 
     ```json
    true
    ``` 
    
 **Get Customer Account Ids ByName**  
----  
  Returns array of customer id's per customer name     
* **URL**  - /invoke/getCustomerAccountIdsByName 
* **Method:** `GET`
*  **URL Params**
   **Required:**
   `name=[string]`
    **Optional:**
      None
* **Success Response:**

  * **Code:** 200
    **Content:** 
     ```json
    [1111111,2222222,3333333]
    ```  
