/* eslint-disable class-methods-use-this */
const AWS = require('aws-sdk/index');

const TENANTS_ENV_DETAILS = process.env.TENANTS_ENV_DETAILS_TABLE_NAME;

class TenantEnvDao {
    constructor() {
        this.documentClient = new AWS.DynamoDB.DocumentClient();
    }

    // private:
    _saveTenantEnvironmentDetails(envDetails) {
        let tenantEnvObj;
        const activationName = envDetails ? envDetails.activation_name : null;
        switch (activationName) {
            case 'AWSConfigActivation':
                tenantEnvObj = {
                    aws_account_id: envDetails.aws_account_id,
                    customer_name: envDetails.customer_name,
                    cloud_trail_cf_details: { external_id: envDetails.external_id },
                    cross_account_cf_details: {
                        cross_account_role_arn: envDetails.cross_account_role_arn,
                        stack_name: envDetails.stack_name,
                        physical_resource_id: envDetails.physical_resource_id,
                        template_version: envDetails.template_version
                    }
                };
                break;
            case 'CustomerSignUp':
                tenantEnvObj = {
                    aws_account_id: envDetails.aws_account_id,
                    customer_name: envDetails.customer_name,
                    deployment_region: envDetails.deployment_region,
                    cloud_trail_cf_details: {
                        ct_stack_name: envDetails.ct_stack_name,
                        sqs_queue_url: envDetails.sqs_queue_url,
                        security_account_id: envDetails.security_account_id,
                        external_id: envDetails.external_id,
                        template_version: envDetails.template_version
                    },
                    cross_account_cf_details: { cross_account_role_arn: envDetails.cross_account_role_arn }
                };
                break;
            case 'Repository':
                tenantEnvObj = {
                    aws_account_id: envDetails.repository,
                    customer_name: envDetails.customer_name
                };
                break;
            default:
                throw new Error(`Activation ${activationName} is not supported`);
        }

        const documentClient = new AWS.DynamoDB.DocumentClient();
        if (envDetails.remote_remediation_cf_details) tenantEnvObj.remote_remediation_cf_details = envDetails.remote_remediation_cf_details;
        if (envDetails.s3_access_details) tenantEnvObj.s3_access_details = envDetails.s3_access_details;
        return documentClient.put({ TableName: TENANTS_ENV_DETAILS, Item: tenantEnvObj }).promise();
    }

    async _getItemByName(name) {
        const params = {
            TableName: TENANTS_ENV_DETAILS,
            IndexName: 'customer_name-index',
            KeyConditionExpression: 'customer_name = :value_customer_name',
            ExpressionAttributeValues: {
                ':value_customer_name': name
            }
        };

        const response = await this.documentClient.query(params).promise();
        return response.Items; // customer_name is unique
    }

    async _getCustomerAccountIdsByName(name) {
        const params = {
            TableName: TENANTS_ENV_DETAILS,
            IndexName: 'customer_name-index',
            KeyConditionExpression: 'customer_name = :value_customer_name',
            ExpressionAttributeValues: {
                ':value_customer_name': name
            },
            ProjectionExpression: ['aws_account_id']
        };

        const result = await this.documentClient.query(params).promise();
        return result.Items.map(item => item.aws_account_id);
    }

    async deleteTenantEnvironmentDetails(params) {
        return await this._deleteTenantEnvironmentDetails(params.awsAccountId);
    }

    async removeCustomerFromTenantEnvironmentDetails(params) {
        return await this._removeCustomerFromTenantEnvironmentDetails(params.awsAccountId);
    }

    // public:
    async getTenantEnvironmentDetails(params) {
        const { awsAccountId } = params;
        const dbParams = {
            TableName: TENANTS_ENV_DETAILS,
            Key: {
                aws_account_id: awsAccountId
            }
        };

        // Call DynamoDB to read the item from the table
        const result = await this.documentClient.get(dbParams).promise();
        if (!result.Item) throw new Error('No Item found');
        return result.Item;
    }

    async deleteTenantEnvironmentDetailsByCustomerName(name) {
        const accounts = await this.getCustomerAccountIdsByName({ name });
        accounts.map(async (account) => {
            await this._deleteTenantEnvironmentDetails(account);
        });
    }

    async _deleteTenantEnvironmentDetails(awsAccountId) {
        const dynamoParams = {
            TableName: TENANTS_ENV_DETAILS,
            Key: {
                aws_account_id: awsAccountId
            }
        };
        return this.documentClient.delete(dynamoParams).promise();
    }

    async _removeCustomerFromTenantEnvironmentDetails(awsAccountId) {
        const dynamoParams = {
            Key: {
                aws_account_id: awsAccountId
            },
            ReturnValues: 'UPDATED_NEW',
            TableName: TENANTS_ENV_DETAILS,
            UpdateExpression: 'set customer_name = :customerName',
            ExpressionAttributeValues: {
                ':customerName': 'null'
            }
        };

        return this.documentClient.update(dynamoParams).promise();
    }

    // todo: search the getTenantEnvByName nome for replace with this: getTenantEnvironmentDetailByCustomerName
    async getTenantEnvironmentDetailByCustomerName(params) {
        const { name } = params;
        const items = await this._getItemByName(name);
        if (!items || items.length === 0) {
            return [];
        }
        return items;
    }

    async getAccounts({ customerName }) {
        const items = await this.getTenantEnvironmentDetailByCustomerName({ name: customerName });
        if (!items || items.length === 0) {
            return [];
        }
        return items.filter(envDetails => envDetails.aws_account_id.match(/[0-9]{12}/g));
    }

    async saveTenantEnvironmentDetails(params) {
        await this._saveTenantEnvironmentDetails(params);
        return true;
    }

    async getCustomerAccountIdsByName(params) {
        return await this._getCustomerAccountIdsByName(params.name);
    }

    setRemoteRemediationDetails(params) {
        const dynamoParams = {
            ExpressionAttributeNames: {
                '#RRCF': 'remote_remediation_cf_details'
            },
            ExpressionAttributeValues: {
                ':rrcf': params.remoteRemediationDetailsObject
            },
            Key: {
                aws_account_id: params.awsAccountId
            },
            ReturnValues: 'ALL_NEW',
            TableName: TENANTS_ENV_DETAILS,
            UpdateExpression: 'SET #RRCF = :rrcf'
        };
        console.log(`setDetails params - ${dynamoParams}`);

        return this.documentClient.update(dynamoParams).promise().then((data) => {
            console.log(`setDetails transaction result - ${data}`);
        }).catch((err) => {
            console.error(err);
            throw err;
        });
    }

    removeRemoteRemediationDetails(params) {
        const dynamoParams = {
            Key: {
                aws_account_id: params.awsAccountId
            },
            ReturnValues: 'ALL_NEW',
            TableName: TENANTS_ENV_DETAILS,
            UpdateExpression: 'REMOVE remote_remediation_cf_details'
        };
        console.log(`deleteDetails params - ${dynamoParams}`);

        return this.documentClient.update(dynamoParams).promise().then((data) => {
            console.log(`deleteDetails transaction result - ${data}`);
        }).catch((err) => {
            console.error(err);
            throw err;
        });
    }

    setS3AccessDetails(params) {
        const dynamoParams = {
            ExpressionAttributeNames: {
                '#S3A': 's3_access_details'
            },
            ExpressionAttributeValues: {
                ':s3a': params.s3AccessObject
            },
            Key: {
                aws_account_id: params.awsAccountId
            },
            ReturnValues: 'ALL_NEW',
            TableName: TENANTS_ENV_DETAILS,
            UpdateExpression: 'SET #S3A = :s3a'
        };

        console.log(`Set s3 access details - ${JSON.stringify(dynamoParams)}`);

        return this.documentClient.update(dynamoParams).promise();
    }

    removeS3AccessDetails(params) {
        const dynamoParams = {
            Key: {
                aws_account_id: params.awsAccountId
            },
            ReturnValues: 'ALL_NEW',
            TableName: TENANTS_ENV_DETAILS,
            UpdateExpression: 'REMOVE s3_access_details'
        };
        console.log(`deleteS3AccessDetails params - ${dynamoParams}`);

        return this.documentClient.update(dynamoParams).promise().then((data) => {
            console.log(`deleteS3AccessDetails transaction result - ${data}`);
        }).catch((err) => {
            console.error(err);
            throw err;
        });
    }

    async getCustomerName({ awsAccountId }) {
        return await this.getTenantEnvironmentDetails({ awsAccountId }).then((envDetails) => envDetails.customer_name);
    }
}

module.exports = new TenantEnvDao();

TenantEnvDao.requiredFunctionParams = {
    removeS3AccessDetails: ['awsAccountId'],
    setS3AccessDetails: ['s3AccessObject', 'awsAccountId'],
    removeRemoteRemediationDetails: ['awsAccountId'],
    setRemoteRemediationDetails: ['remoteRemediationDetailsObject', 'awsAccountId'],
    getCustomerAccountIdsByName: ['name'],
    saveTenantEnvironmentDetails: ['customer_name'],
    getTenantEnvironmentDetails: ['awsAccountId'],
    getTenantEnvironmentDetailByCustomerName: ['name'],
    deleteTenantEnvironmentDetailsByCustomerName: ['name'],
    deleteTenantEnvironmentDetails: ['awsAccountId'],
    removeCustomerFromTenantEnvironmentDetails: ['awsAccountId'],
    getCustomerName: ['awsAccountId']
};
