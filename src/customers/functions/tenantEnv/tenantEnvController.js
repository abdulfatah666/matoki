const AWS = require('aws-sdk');
const RemoteLambda = require('@bridgecrew/nodeUtils/remoteLambda/invoke');

const integrationRemoteLambda = new RemoteLambda(process.env.INTEGRATION_API_LAMBDA);

const getSiemDashboardsDetails = async ({ customerName }) => {
    const params = {
        customerName,
        type: 'elasticsearch'
    };
    let esIntegration;
    try {
        esIntegration = await integrationRemoteLambda.invoke('getByType', params);
    } catch (e) {
        console.error(`Failed to get elasticsearch integration for customer: ${customerName} with error ${e.message}`);
        throw e;
    }
    if (esIntegration && esIntegration.length > 0) {
        const es = new AWS.ES();
        const esCustomers = `bc-es-${customerName}`;
        try {
            const esDomainsDetails = await es.describeElasticsearchDomains({ DomainNames: [esCustomers] }).promise();
            return esDomainsDetails.DomainStatusList.map((esDomainDetails) => {
                const dashboardUrl = `https://${esDomainDetails.Endpoint}/_plugin/kibana/`;
                return {
                    customerName,
                    dashboardUrl
                };
            });
        } catch (e) {
            console.error(`Failed to get elasticsearch data for customer: ${customerName} with error ${e.message}`);
            throw e;
        }
    }
    return [];
};

module.exports = {
    getSiemDashboardsDetails
};