/* eslint-disable no-restricted-syntax */
const serverless = require('serverless-http');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const {
    HttpError, InternalServerError, BadRequestError, BadParamsError
} = require('@bridgecrew/nodeUtils/errors/http');
const tenantEnvController = require('./tenantEnv/tenantEnvController');
const tenantEnvDao = require('./tenantEnv/tenantEnvDao');
const tenantDao = require('./tenant/tenantDao');
const tenantController = require('./tenant/tenantController');
const customersWebappDataController = require('./customerWebapp/customerWebAppDataController');

const tenantControllerInstance = tenantController.getInstance(process.env.TENANTS_TABLE_NAME, process.env.STEP_FUNCTION_CUSTOMER, process.env.STEP_FUNCTION_UPDATE_CUSTOMER, process.env.STEP_FUNCTION_DESTROY_CUSTOMER, process.env.SSM_USER_POOL_ID);
const customersWebappDataControllerInstance = customersWebappDataController.getInstance();

const invokedControllersInstances = {
    tenantController: tenantControllerInstance,
    customersWebAppController: customersWebappDataControllerInstance
};

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.text());

function validate(funcName, body) {
    if (!(tenantDao[funcName] instanceof Function) && !(tenantEnvDao[funcName] instanceof Function)) {
        throw new BadRequestError('Function do not exist');
    }

    const isSubArr = (arr, subArr) => {
        const arrMap = {};
        arr.forEach((element) => {
            arrMap[element] = true;
        });
        for (const e of subArr) {
            if (!arrMap[e]) return false;
        }
        return true;
    };

    const requiredFunctionParams = tenantDao.constructor.requiredFunctionParams[funcName] || tenantEnvDao.constructor.requiredFunctionParams[funcName];

    if (!isSubArr(Object.keys(body), requiredFunctionParams)) {
        throw new BadParamsError(`Bad parameters for ${funcName} - expected: ${requiredFunctionParams}, got: ${Object.keys(body)}`);
    }
}
// please invoke only with the new method
app.post('/invoke/controller/:funcName', async (req, res, next) => {
    const { funcName } = req.params;

    try {
        const controllersInstances = Object.values(invokedControllersInstances);
        for (let i = 0; i < controllersInstances.length; i++) {
            const controllerInstance = controllersInstances[i];
            if (controllerInstance[funcName]) {
                const data = await controllerInstance[funcName](req.body);
                return res.json(data);
            }
        }
        next(new BadRequestError('Function do not exist'));
    } catch (err) {
        next(new InternalServerError(err.message || err));
    }
});

app.get('/invoke/:funcName', async (req, res, next) => {
    const { funcName } = req.params;

    try {
        validate(funcName, req.body);
        console.info(`executing ${funcName}...`);
    } catch (e) {
        next(e);
        return;
    }

    try {
        const data = await (tenantEnvDao[funcName] ? tenantEnvDao[funcName](req.body) : tenantDao[funcName](req.body));
        return res.json(data);
    } catch (err) {
        next(new InternalServerError(err.message || err));
    }
});

/**
 * req.body: {customer_name: STRING, owner_email:STRING, owner_first_name:STRING, owner_last_name:STRING, owner_phone:STRING}
 * Pay attention: this is a public route - need to be declared above app.use authorization !
 */
app.post('/api/v1/signup', async (req, res, next) => {
    try {
        const cloudFormation = await tenantControllerInstance.signup(req.body);
        return (req.query.format && req.query.format === 'json')
            ? res.send({ data: { cloudFormation } }) : res.send(cloudFormation.url);
    } catch (err) {
        console.error(err);
        next(err);
    }
});

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    if (req.method === 'OPTIONS') {
        res.send(200);
    } else {
        next();
    }
});

app.get('/api/v1/invitation', async (req, res) => {
    const listInvitation = await tenantControllerInstance.listInvitation({ userDetails: req.userDetails });
    res.status(200).json(listInvitation);
});

app.post('/api/v1/invitation', async (req, res) => {
    await tenantControllerInstance.invitation({ email: req.body.email, userDetails: req.userDetails });
    res.status(201).send();
});

app.delete('/api/v1/invitation/:username', async (req, res) => {
    await tenantControllerInstance.deleteInvitation({ email: req.params.username, userDetails: req.userDetails });
    res.send();
});

app.put('/api/v1/customers/appdata', async (req, res, next) => {
    try {
        const { customers } = req.userDetails;
        const categoryData = await customersWebappDataControllerInstance.update(customers[0], req.body);
        return res.json(categoryData);
    } catch (err) {
        console.error(`Failed to update webapp data for customer ${req.userDetails} with the following object ${req.body}`);
        console.log(err);
        next(err);
    }
});

app.put('/api/v1/customers/:customerName', async (req, res, next) => {
    try {
        const result = await tenantControllerInstance.update({
            customer_name: req.params.customerName,
            owner_first_name: req.body.owner_first_name,
            owner_last_name: req.body.owner_last_name,
            owner_email: req.body.owner_email,
            owner_phone: req.body.owner_phone
        });

        return res.send({
            data: {
                sfExecutionARN: result.res.triggerStepFunction.executionArn
            }
        });
    } catch (err) {
        next(err);
    }
});

app.get('/api/v1/customers/appdata/:category', async (req, res, next) => {
    try {
        const { customers } = req.userDetails;
        const categoryData = await customersWebappDataControllerInstance.getByCategory(customers[0], req.params.category);
        return res.json(categoryData);
    } catch (err) {
        console.error(`Failed to get ${req.params.category} webapp data category for user ${req.userDetails}`);
        console.error(err);
        next(err);
    }
});

app.get('/api/v1/customers/appdata', async (req, res, next) => {
    try {
        const { customers } = req.userDetails;
        const categoryData = await customersWebappDataControllerInstance.getByCustomerName(customers[0]);
        return res.json(categoryData);
    } catch (err) {
        console.error(`Failed to get webapp data for user ${req.userDetails}`);
        console.error(err);
        next(err);
    }
});

app.get('/api/v1/customers/siem/dashboards', async (req, res, next) => {
    try {
        const customerName = req.userDetails.customers[0];
        const siemDashboardsDetails = await tenantEnvController.getSiemDashboardsDetails({ customerName });
        return res.json({ siemDashboardsDetails });
    } catch (err) {
        console.log('Failed to get SIEM dashboard data');
        next(new InternalServerError('Failed to get SIEM dashboard data'));
    }
});

app.get('/api/v1/customers/accountIds', async (req, res, next) => {
    try {
        const customer = req.userDetails.customers[0];
        const accounts = await tenantEnvDao.getCustomerAccountIdsByName({ name: customer });
        return res.json(accounts);
    } catch (err) {
        console.log(err);
        next(err);
    }
});

app.get('/api/v1/customers/cloudformation/url', async (req, res, next) => {
    try {
        const customerName = req.userDetails.customers[0];
        const cloudformationParams = {
            customerName,
            stackName: `${customerName}-bridgecrew`,
            param_ResourceNamePrefix: `${customerName}-bc`,
            param_CustomerName: customerName
        };
        const { url } = tenantControllerInstance.getCloudFormationUrl(cloudformationParams);
        return res.json({ url });
    } catch (e) {
        next(e);
    }
});

app.get('/api/v1/customers/cloudformation/readUrl', async (req, res, next) => {
    try {
        const customerName = req.userDetails.customers[0];
        const cloudformationParams = {
            type: 'read',
            customerName,
            stackName: `${customerName}-bridgecrew-read`,
            param_ResourceNamePrefix: `${customerName}-bc-read`,
            param_CustomerName: customerName
        };
        const { url } = tenantControllerInstance.getCloudFormationUrl(cloudformationParams);
        return res.json({ url });
    } catch (e) {
        next(e);
    }
});

app.get('/api/v1/customers/details/:awsAccountId', async (req, res, next) => {
    try {
        const customerDetails = tenantEnvDao.getTenantEnvironmentDetails({ awsAccountId: req.params.awsAccountId });
        return res.json({ customerDetails });
    } catch (e) {
        next(e);
    }
});

app.get('/api/v1/customers/details', async (req, res, next) => {
    try {
        const customer = req.userDetails.customers[0];
        console.log('The following user trigger the customer details api: ', req.userDetails);
        const customerDetails = await tenantDao.getCustomerByName({ name: customer });

        if (!customerDetails) {
            return res.json({});
        }

        return res.json(customerDetails);
    } catch (e) {
        next(e);
    }
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
    if (!(err instanceof HttpError)) {
        /* eslint-disable-next-line no-param-reassign */
        err = new InternalServerError(err.message);
    }

    console.error(err.message, err.stack);

    if (req.get('throw-error')) {
        process.exit(1);
    }

    res.status(err.statusCode).send(err.format);
});

function invokeDaoTenantEnv(funcName, body) {
    if (!(tenantEnvDao[funcName] instanceof Function)) {
        throw new BadRequestError('Function does not exist');
    } else {
        return tenantEnvDao[funcName](body);
    }
}

function invokeControllerTenantEnv(funcName, body) {
    if (!(tenantEnvController[funcName] instanceof Function)) {
        throw new BadRequestError('Function do not exist');
    } else {
        return tenantEnvController[funcName](body);
    }
}

function invokeDaoTenant(funcName, body) {
    if (!(tenantDao[funcName] instanceof Function)) {
        throw new BadRequestError('Function does not exist');
    } else {
        return tenantDao[funcName](body);
    }
}

function invokeControllerTenant(funcName, body) {
    if (!(tenantControllerInstance[funcName] instanceof Function)) {
        throw new BadRequestError('Function do not exist');
    } else {
        return tenantControllerInstance[funcName](body);
    }
}

module.exports.handler = (event, context) => {
    if (event.path.startsWith('/invoke') && event.newInvoke) {
        const matchPattern = event.path.match(/^\/invoke\/([^/]+)\/([^/]+)(?:\/([^/]+))?$/);
        if (matchPattern.length > 2 && matchPattern[2] !== undefined && matchPattern[3] !== undefined) {
            if (matchPattern[1] === 'tenant') {
                if (matchPattern.length > 3 && matchPattern[2] === 'dao') {
                    return invokeDaoTenant(matchPattern[3], event.body);
                }
                return invokeControllerTenant(matchPattern.length > 3 ? matchPattern[3] : matchPattern[2], event.body);
            }

            if (matchPattern[1] === 'tenantEnv') {
                if (matchPattern.length > 3 && matchPattern[2] === 'dao') {
                    return invokeDaoTenantEnv(matchPattern[3], event.body);
                }
                return invokeControllerTenantEnv(matchPattern.length > 3 ? matchPattern[3] : matchPattern[2], event.body);
            }
        } else {
            throw new Error('The invoke not valid');
        }
    } else {
        return serverless(app, {
            request(req, ev, ctx) {
                req.event = ev;
                req.context = ctx;

                if (req.event.requestContext.authorizer) {
                    req.userDetails = req.event.requestContext.authorizer;
                    req.userDetails.customers = req.event.requestContext.authorizer.customers.split(',');
                }
            },
            binary: false
        })(event, context);
    }
};
