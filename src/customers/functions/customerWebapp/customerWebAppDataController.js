const customerWebappDbMgr = require('@bridgecrew/nodeUtils/dbMgrs/CustomersWebappDataDBMgr');
const CustomerWebappData = require('@bridgecrew/nodeUtils/models/CustomerWebappData');

class CustomersWebappDataController {
    // eslint-disable-next-line class-methods-use-this
    async createNewCustomerData(params) {
        const defaultObject = customerWebappDbMgr.getInstance().categoriesDefaultValues;
        return await customerWebappDbMgr.getInstance().create(params.customerName, defaultObject);
    }

    // eslint-disable-next-line class-methods-use-this
    async getByCustomerName(customerName) {
        const result = await customerWebappDbMgr.getInstance().getByCustomerName(customerName);
        if (result) {
            return CustomerWebappData.createFromDynamoDbItem(result);
        }

        return null;
    }

    // eslint-disable-next-line class-methods-use-this
    async getByCategory(customerName, category) {
        const result = await customerWebappDbMgr.getInstance().getByCategory(customerName, CustomerWebappData.getWebappCategoryDynamoPropertyName(category));
        if (result) {
            return CustomerWebappData.createFromDynamoDbItem(result);
        }

        return null;
    }

    // eslint-disable-next-line class-methods-use-this
    isValid(object) {
        const inputObjectCategories = Object.keys(object);
        const filteredCategories = inputObjectCategories.filter((category) => {
            const dynamoPropName = CustomerWebappData.getWebappCategoryDynamoPropertyName(category);
            return customerWebappDbMgr.getInstance().categories.includes(dynamoPropName);
        });

        return filteredCategories.length === inputObjectCategories.length;
    }

    async update(customerName, objectToUpdate) {
        if (this.isValid(objectToUpdate)) {
            const item = objectToUpdate;
            item.customerName = customerName;

            const dynamoDBItem = new CustomerWebappData(item).toDynamoDbItem();
            const result = await customerWebappDbMgr.getInstance()
                .update(dynamoDBItem);

            if (result) {
                return CustomerWebappData.createFromDynamoDbItem(result);
            }
        }

        console.error('Invalid object. one of the object attributes is not supported ', objectToUpdate);
        return null;
    }
}

let instance;

function getInstance() {
    if (!instance) {
        instance = new CustomersWebappDataController();
    }

    return instance;
}

module.exports = { getInstance };