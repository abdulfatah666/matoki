provider "aws" {
  profile = var.profile
  region  = var.region
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

module "consts" {
  source = "../../utils/terraform/consts"
}

locals {
  role_name = "app_users_authenticated_${var.base_stack_unique_tag}_${var.customer_name}"
}

// Prowler cloudwatch event
data "aws_caller_identity" "current" {}

data "aws_ssm_parameter" "bucket_scanner" {
  name = "/base_stack/scanners_backet_name_${var.base_stack_unique_tag}"
}

data "aws_ssm_parameter" "bucket_scanner_kms_key" {
  name = "/base_stack/scanners_backet_kms_key_${var.base_stack_unique_tag}"
}

resource "aws_cloudwatch_event_rule" "scanners_console" {
  name                = "${module.consts.bc_scanners_step_function_name}_event_rule_${var.customer_name}"
  description         = "Scanners event rule"
  schedule_expression = "cron(0	6,14 * * ? *)"
}

resource "aws_cloudwatch_event_target" "scanners_event_target" {
  rule      = aws_cloudwatch_event_rule.scanners_console.name
  target_id = "TriggerScanners"
  arn       = "arn:aws:states:${var.region}:${data.aws_caller_identity.current.account_id}:stateMachine:${module.consts.bc_scanners_step_function_name}-${var.base_stack_unique_tag}"
  input     = "{\"customerName\":\"${var.customer_name}\"}"
  role_arn  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${module.consts.scanners_cloudwatch_stepfunction_trigger_role_name}-${var.base_stack_unique_tag}"
}

resource "null_resource" "remove_identity_pool" {
  provisioner "local-exec" {
    when = "destroy"
    working_dir = "${path.module}/../../utils/nodeUtils/cognito"
    command = "npm i --quiet --no-progress -s --loglevel error  > /dev/null&&node identity_pool_cli.js delete IP_${var.customer_name} --profile ${var.profile} --region ${var.region}"
  }
}

data "aws_iam_policy_document" "bucket_scanner_access_policy" {
  version = "2012-10-17"

  // Grant access to the scanner bucket
  statement {
    effect = "Allow"

    actions = [
      "s3:GetObject"
    ]

    resources = [
      "arn:aws:s3:::${data.aws_ssm_parameter.bucket_scanner.value}/reports/${var.customer_name}/*"
    ]
  }

  // Grant access to the scanner kms
  statement {
    effect = "Allow"

    actions = [
      "kms:Decrypt"
    ]

    resources = [
      data.aws_ssm_parameter.bucket_scanner_kms_key.value
    ]
  }
}

resource "aws_iam_policy" "access_policy" {
  name = "bc_scanner_reports_policy_${var.customer_name}"
  description = "policy for cognito to access scanner reports"
  policy = data.aws_iam_policy_document.bucket_scanner_access_policy.json
}

resource "aws_iam_role_policy_attachment" "attach_scanner_policy" {
  role       = local.role_name
  policy_arn = aws_iam_policy.access_policy.arn

  depends_on = [null_resource.remove_iam_role]
}

resource "null_resource" "remove_iam_role_policy" {
  provisioner "local-exec" {
    when = "destroy"
    command = "aws iam delete-role-policy --role-name ${local.role_name} --policy-name cognito_policy --profile ${var.profile} --region ${var.region}"
  }

  depends_on = [ null_resource.remove_iam_role]
}

resource "null_resource" "remove_iam_role" {
  provisioner "local-exec" {
    when = "destroy"
    command = "aws iam delete-role --role-name ${local.role_name} --profile ${var.profile} --region ${var.region}"
  }
}


resource "random_id" "write_access_token" {
  byte_length = 16
}

resource "aws_dynamodb_table_item" "write_access_token_item" {
  table_name = "api_tokens${var.base_stack_unique_tag}"
  hash_key   = "token"

  item = <<ITEM
{
  "token": {"S": "${uuidv5("oid", random_id.write_access_token.hex)}"},
  "user_id": {"S": "${var.customer_name}"}
}
ITEM
}

