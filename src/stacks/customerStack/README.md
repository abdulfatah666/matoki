# Kinesis and Firehose data streams module
This module creates for a new customer all necessary AWS resources under our dev environment.

The created resources are:

    Identity Pool     - For limiting the access for the customer's Kibana service to its users only.
    ES + Kibana      - An elasticsearch + Kibana service for storing and displaying log events
    Cognito user group  - To grants its users with the suitable permission to access kibana  
    Kinesis + Firehose data streams - For writing the collected log events to ES
    Cognito user - For the customer to log in to its Kibana
 
 In addition, as part of the process, we attached the customer sqs to our multi-tenant producers handlers lambda 
 functions,and add our pre-created support user to the customer's cognito user group.   

# Variables
| Name                      | Required? | Type   | Example Value                                                                                                                 | Description                                                                                                                                                                                                                                          |
|---------------------------|-----------|--------|-------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
|profile                    | YES       | String | dev                                                                                                                           | The aws credentials profile as denoted in ~/.aws/credentials.                                                        |
|region                     | YES       | String | us-east-1                                                                                                                     | The aws region.                                                                                                      |
|customer_name              | YES       | String | globality                                                                                                                     | The name of the customer. <br> **Should not contain special characters.**                                            |
|user_pool_id               | YES       | String | us-west-2_7wehduj4                                                                                                            | The user pool id.                                                                                                    |
|producers_list             | YES       | List   | ['cloudtrail','lacework']                                                                                                     | List of data producers: cloudtrail, lacework, etc. <br> Should be **lowercase only**, and **no special characters**. |
|user_attributes            | YES       | Map    | <pre>{ <br> name="ploni", <br> family_name="almoni", <br> phone_number="+1555555555",<br> email="ploni@gmail.com" <br>}</pre> | Map of cognito user attributes.                                                                                      |
|client_sqs_arn             | YES       | String | arn:aws:sqs:REGION:ACCOUNT_ID:SQS_NAME                                                                                        | The customer SQS arn.                                                                                                |
|customer_assume_role_arn   | YES       | String | arn:aws:iam::ACCOUNT_ID:role/ROLE_NAME                                                                                        | The ARN of the role in the customer environment that allow access to the lambda function.                            |
|base_stack_unique_tag   | YES       | String | tag11                                                                                        | The unique tag of the base stack                            |
|customer_assume_cross_account_role_arn   | YES       | String | arn:aws:iam::ACCOUNT_ID:role/ROLE_NAME                                                                                        | The ARN of the role in the customer environment that allow access to the lambda function.                            |
|customer_aws_account_id   | YES       | String | 1234567890                                                                                        | The account id of the customer                            |


# Outputs
None