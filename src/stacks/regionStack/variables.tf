variable "region" {
  type        = "string"
  description = "The region in which the stack will be deployed"
}

variable "aws_profile" {
  type        = "string"
  description = "The profile to be selected from the ~/.aws/credentials file to be used in the creation of the stack"
}

variable "circle_ci_key" {
  type        = "string"
  description = "The CircleCI key"
}

variable "unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
}

variable "state_bucket" {
  description = "A name to identify s3 bucket where the stack is deployed, and deployment state is saved"
  type        = "string"
}

variable "customer_actions_lambda_arn" {
  description = "The arn of handle customer actions lambda function"
  type        = "string"
}

variable "slack_alarm_lambda_arn" {
  description = "The arn of slack lambda function"
  type        = "string"
}

variable "bridgecrew_template_bucket" {
  description = "The name of the bridgecrew cloud formation template bucket"
}

variable "slack_hook_url" {
  description = "slack hook url"
  type        = "string"
}

variable "customers_api_lambda_function_name" {
  type        = "string"
}

variable "cwl_to_kinesis_role_arn" {
  description = "Kinesis Role ARN to allow CloudWatch Logs to write to Kinesis Firehose"
  type        = "string"
}

variable "cwl_firehose_stream_arn" {
  description = "ARN of the Kinesis Firehose for Cloudwatch to forward logs"
  type        = "string"
}
