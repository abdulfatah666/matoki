output "unique_tag" {
  value = var.unique_tag
}

output "sns_monitor_topic_error_name" {
  value = module.monitor.sns_monitor_topic_error_name
}

output "customer_actions_sns_topic_name" {
  value = module.customer_actions_sns_topic.sns_topic_name
}