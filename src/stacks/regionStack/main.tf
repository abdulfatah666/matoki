provider "aws" {
  alias   = "current_region"
  region  = var.region
  profile = var.aws_profile
}

provider "aws" {
  alias   = "main_region"
  region  = "us-west-2"
  profile = var.aws_profile
}

locals {
  unique_tag_with_dash       = var.unique_tag == "" ? var.unique_tag : format("-%s", var.unique_tag)
  unique_tag_with_underline  = var.unique_tag == "" ? var.unique_tag : format("_%s", var.unique_tag)
  sls_deployment_bucket_name = "bc-state-${data.aws_caller_identity.current.account_id}-${var.region}${local.unique_tag_with_dash}"
}

module "consts" {
  source = "../../utils/terraform/consts"
}

resource "aws_s3_bucket" "sls_deployment_bucket_name" {
  provider      = aws.current_region
  region        = var.region
  bucket        = local.sls_deployment_bucket_name
  force_destroy = true
  acl           = "private"
  tags = {
    Name = local.sls_deployment_bucket_name
  }
}

module "monitor" {
  source                       = "../../monitor/infra"
  region                       = var.region
  aws_profile                  = var.aws_profile
  slack_hook_url               = var.slack_hook_url
  sns_monitor_topic_name_error = module.consts.monitor_sns_topic_error
  slack_alarm_lambda_arn       = var.slack_alarm_lambda_arn
  unique_tag                   = var.unique_tag
  sls_bucket_name              = aws_s3_bucket.sls_deployment_bucket_name.bucket
}

data "aws_caller_identity" "current" {}

locals {
  unique_tag = var.unique_tag == "" ? var.unique_tag : format("-%s", var.unique_tag)
}

/*
 * Create the handle-cloudtrail lambda
 */
module "cloudtrail_lambda" {
  source                 = "../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../logArchive/functions/kds/producers/cloudtrail/"
  unique_tag             = var.unique_tag
  sls_bucket_name        = aws_s3_bucket.sls_deployment_bucket_name.bucket
  variable_string        = "--customers-api-lambda ${var.customers_api_lambda_function_name}"

  lambda_names = [
    "handle-cloudtrail-${var.unique_tag}",
  ]
}

/*
 * Create the SNS topic that will publish the customer actions
 */
module "customer_actions_sns_topic" {
  source         = "../../utils/terraform/sns"
  aws_profile    = var.aws_profile
  region         = var.region
  sns_topic_name = "handle-customer-actions"
  unique_tag     = var.aws_profile == "prod" ? "" : var.unique_tag

  monitor = {
    enable               = true
    sns_topic_error_name = module.monitor.sns_monitor_topic_error_name
  }
}

resource "aws_lambda_permission" "customers_function_with_sns" {
  provider      = aws.main_region
  statement_id  = "AllowExecutionFromSNS${local.unique_tag_with_underline}_${var.region}"
  action        = "lambda:InvokeFunction"
  function_name = var.customer_actions_lambda_arn
  principal     = "sns.amazonaws.com"
  source_arn    = module.customer_actions_sns_topic.sns_topic_arn
}

/*
 * Subscribe the customer_actions_lambda to the customer_actions_sns_topic
 */
resource "aws_sns_topic_subscription" "handle_customer_actions_subscription" {
  provider  = aws.current_region
  endpoint  = var.customer_actions_lambda_arn
  protocol  = "lambda"
  topic_arn = module.customer_actions_sns_topic.sns_topic_arn
}

/*
 * Save the CircleCI API key to SSM
 */
resource "aws_ssm_parameter" "circle_ci_key_parameter" {
  provider  = aws.current_region
  name      = "/base_stack/circle_ci_api${local.unique_tag_with_underline}"
  type      = "SecureString"
  value     = var.circle_ci_key
  overwrite = true
}

locals {
  cloudtrail_arn_ssm_parameter_name = "/base_stack/cloudtrail${var.unique_tag}/${var.region}"
}

resource "aws_ssm_parameter" "cloudtrail_lambda_arn_ssm_param" {
  provider  = aws.main_region
  name      = local.cloudtrail_arn_ssm_parameter_name
  type      = "String"
  value     = concat(module.cloudtrail_lambda.function_arn, list(""))[0]
  overwrite = true
}

resource "aws_cloudwatch_log_destination" "cwl_destination" {
  provider   = aws.current_region
  name       = "cwl_destination_${var.unique_tag}"
  role_arn   = var.cwl_to_kinesis_role_arn
  target_arn = var.cwl_firehose_stream_arn
}

data "aws_iam_policy_document" "cwl_destination_policy" {
  statement {
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions = [
      "logs:PutSubscriptionFilter",
    ]

    resources = [
      aws_cloudwatch_log_destination.cwl_destination.arn
    ]
  }
}

resource "aws_cloudwatch_log_destination_policy" "cwl_destination_policy_us-east-1" {
  provider         = aws.current_region
  destination_name = aws_cloudwatch_log_destination.cwl_destination.name
  access_policy    = data.aws_iam_policy_document.cwl_destination_policy.json
}