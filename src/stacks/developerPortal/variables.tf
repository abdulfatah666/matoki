variable "region" {
  description = "The AWS region to deploy the portal in"
}

variable "aws_profile" {
  description = "The AWS Profile to use"
}

variable "template_artifacts_bucket" {
  description = "The bucket where the deployment info will be saved. Usually - the terraform state bucket"
  default     = "developer-portal-template-artifacts"
}

variable "cognito_prefix" {
  description = "The prefix in case a user pool is not supplied."
  default     = "dev-portal"
}

variable "portal_bucket_name" {
  default = "developer-portal-site"
}

variable "swagger_artifacts_bucket_name" {
  default = "swagger-artifacts-bucket"
}

variable "stack_name" {
  default = "developer-portal"
}

variable "path_to_template_file" {
  description = "Path to the template file, which is inside the local copy of aws-api-gateway-developer-portal repo. Clone it if you hadn't already."
  type        = "string"
}

variable "user_pool_id" {
  description = "The ID of the user pool to connect to. Will create a user pool if value is \"\""
  default     = ""
}

variable "user_pool_domain" {
  description = "The User Pool domain. For example: https://auth-baraktest5.auth.us-west-2.amazoncognito.com. Unnecessary if creating new user pool."
  default     = ""
}

variable "domain" {
  description = "The domain the developers portal will be in, appended to 'developers.'"
}

variable "cloudfront_dist_domain_file" {
  description = "The name of the file which holds the cloudfront distribution arn"
}
