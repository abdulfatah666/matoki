# Developer Portal Stack
To run this stack, need to do the following steps:
1. Make sure aws-cli and aws-sam-cli tools are set up locally. https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html
2. Clone the developer portal repo (https://github.com/bridgecrewio/aws-api-gateway-developer-portal.git)
3. Set up the parameter file. Required fields:
    - region
    - aws_profile
    - *ONE* of cognito_prefix (for everything **except prod**) or (user_pool_id AND user_pool_domain) for **prod**
    - path_to_template_file - will be the cloned repo's directory, appended with /cloudformation/template.yaml
    - domain - The domain the developer portal will be registered to. 'developers.' will be appended to this value.

This will create a developers portal with either a new Cognito User Pool (for everything **except prod**)
or a developer portal which is connected to the BridgeCrew User Pool on prod.

### Creating another portal in the same account
Make sure to:
   - change the default value of the bucket names
   - change the default stack name
