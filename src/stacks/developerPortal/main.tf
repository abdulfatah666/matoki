provider "aws" {
  profile = var.aws_profile
  region  = var.region
}

provider "aws" {
  alias   = "certificate"
  profile = var.aws_profile
  region  = "us-east-1"
}

provider "aws" {
  alias   = "route53"
  profile = "route53"
  region  = "us-east-1"
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

data "aws_caller_identity" "current" {
  provider = aws
}

locals {
  portal_bucket_name            = format("%s-%s", var.portal_bucket_name, data.aws_caller_identity.current.account_id)
  swagger_artifacts_bucket_name = format("%s-%s", var.swagger_artifacts_bucket_name, data.aws_caller_identity.current.account_id)
  user_pool_id_var_string       = var.user_pool_id != "" ? "CognitoUserPoolId=\"${var.user_pool_id}\" ExistingUserPoolDomain=\"${var.user_pool_domain}\"" : ""
  domain_name                   = "developers.${var.domain}"
  domain_var_string             = "CustomDomainNameAcmCertArn=\"${aws_acm_certificate_validation.cert_validation.certificate_arn}\" CustomDomainName=\"${local.domain_name}\""
  hosted_zone_id                = "Z2CCHMURZOJZR6"
  cloudfront_dist_arn           = "cloudfront_arn.txt"
}

resource "aws_acm_certificate" "cert" {
  provider          = aws.certificate
  domain_name       = local.domain_name
  validation_method = "DNS"
}

resource "aws_route53_record" "cert_validation_record" {
  provider = aws.route53
  name     = aws_acm_certificate.cert.domain_validation_options[0].resource_record_name
  type     = aws_acm_certificate.cert.domain_validation_options[0].resource_record_type
  zone_id  = local.hosted_zone_id
  records  = [aws_acm_certificate.cert.domain_validation_options[0].resource_record_value]
  ttl      = 300
}

resource "aws_acm_certificate_validation" "cert_validation" {
  provider                = aws.certificate
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [aws_route53_record.cert_validation_record.fqdn]
}

data "local_file" "cloudfront_dist_arn" {
  filename = var.cloudfront_dist_domain_file

  depends_on = [null_resource.deploy_stack]
}

resource "aws_route53_record" "developers_record" {
  provider = aws.route53
  name     = local.domain_name
  type     = "A"
  zone_id  = local.hosted_zone_id

  alias {
    evaluate_target_health = false
    name                   = data.local_file.cloudfront_dist_arn.content

    //     The cloudfront Zone ID is constant, and therefore can be hardcoded
    zone_id = "Z2FDTNDATAQYW2"
  }
}

resource "aws_s3_bucket" "deployment_artifacts_bucket" {
  bucket        = format("%s-%s", var.template_artifacts_bucket, data.aws_caller_identity.current.account_id)
  force_destroy = true
}

resource "null_resource" "deploy_stack" {
  triggers = {
    build_number = timestamp()
  }

  provisioner "local-exec" {
    command = "sam package --template-file ${var.path_to_template_file} --output-template-file ./developer_portal_packaged.yaml --s3-bucket ${aws_s3_bucket.deployment_artifacts_bucket.bucket} --profile ${var.aws_profile} --region ${var.region}"
  }

  provisioner "local-exec" {
    command = "sam deploy --template-file ./developer_portal_packaged.yaml --stack-name \"${var.stack_name}\" --s3-bucket ${aws_s3_bucket.deployment_artifacts_bucket.bucket} --capabilities CAPABILITY_NAMED_IAM --parameter-overrides DevPortalSiteS3BucketName=\"${local.portal_bucket_name}\" ArtifactsS3BucketName=\"${local.swagger_artifacts_bucket_name}\" CognitoDomainNameOrPrefix=\"${var.cognito_prefix}\" ${local.user_pool_id_var_string} ${local.domain_var_string} --profile ${var.aws_profile} --region ${var.region} --no-fail-on-empty-changeset"
  }

  //  Get cloudfront ARN, to be able to get the cloudfront domain
  provisioner "local-exec" {
    command = "aws cloudformation describe-stack-resource --stack-name ${var.stack_name} --logical-resource-id CustomDomainCloudfrontDistribution --profile ${var.aws_profile} --region ${var.region} | printf $(jq .StackResourceDetail.PhysicalResourceId) | tr -d '\"' > ${local.cloudfront_dist_arn}"
  }

  //  Get cloudfront domain, for route53 record
  provisioner "local-exec" {
    command = "aws cloudfront get-distribution --id $( cat ./${local.cloudfront_dist_arn} ) --profile ${var.aws_profile} | printf $(jq .Distribution.DomainName) | tr -d '\"' > ${var.cloudfront_dist_domain_file} && rm ${local.cloudfront_dist_arn}"
  }
}

resource "null_resource" "remove_stack" {
  provisioner "local-exec" {
    when    = "destroy"
    command = "aws cloudformation delete-stack --stack-name '${var.stack_name}' --profile ${var.aws_profile} --region ${var.region}"
  }
}

resource "null_resource" "customize_portal" {
  triggers = {
    build_number = timestamp()
  }

  provisioner "local-exec" {
    command = "aws s3 --profile ${var.aws_profile} --region ${var.region} sync custom-content s3://${local.portal_bucket_name}/custom-content"
  }

  depends_on = [null_resource.deploy_stack]
}

// This resource creates a delay for the certificate deletion, since aws_acm_certificate has a
// non-configurable timeout of 10 minutes.
// It is needed because the the cloudformation destroy does not wait for the completion of the destruction of the stack.
resource "null_resource" "wait_until_certificate_is_not_in_use" {
  triggers = {
    build = aws_acm_certificate.cert.arn
  }

  provisioner "local-exec" {
    when = "destroy"

    command = <<BASH
#!/usr/bin/env bash

i=0
while [ $i -lt 60 ]
do
  i=$(( $i + 1 ))
  usage=$(aws acm describe-certificate --profile ${var.aws_profile} --region us-east-1 --certificate-arn ${aws_acm_certificate.cert.arn} --output json | jq -r .Certificate.InUseBy | jq length)
  if [ $usage -gt 0 ]
  then
    echo "In use by $usage, waiting 1 more minute"
	sleep 60
  	echo "Waited $i minutes"
  else
  	echo "not in use"
    break
  fi
done
BASH
  }
}

resource "null_resource" "add_users_to_pool_and_group" {
  triggers = {
    build = timestamp()
  }

  provisioner "local-exec" {
    command = "npm i aws-sdk && PROFILE=${var.aws_profile} node ./signup.js"
  }

  depends_on = [null_resource.deploy_stack]
}
