const AWS = require('aws-sdk');

AWS.config.update({ region: 'us-west-2' });
AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile: process.env.PROFILE });
const COGNITO = new AWS.CognitoIdentityServiceProvider();
const DEVELOPER_PORTAL_ADMINS_GROUP_NAME = 'developer-portalAdminsGroup';
const BRIDGECREW_DEV_TEAM = [
    'support@bridgecrew.io',
    'barak@bridgecrew.io',
    'suli@bridgecrew.io',
    'yoni@bridgecrew.io',
    'lital@bridgecrew.io',
    'niv@bridgecrew.io',
    'nimrod@bridgecrew.io',
    'naor@bridgecrew.io'
];

async function addExistingUsersToGroup(poolId) {
    return COGNITO.listUsers({
        UserPoolId: poolId
    }).promise()
        .then((usersResponse) => usersResponse.Users)
        .then((users) => users.filter((user) => user.Attributes.find((att) => att.Name === 'email').Value.includes('bridgecrew')))
        .then(async (bridgecrewUsers) => Promise.all(bridgecrewUsers.map((user) => COGNITO.adminAddUserToGroup({
            GroupName: DEVELOPER_PORTAL_ADMINS_GROUP_NAME,
            UserPoolId: poolId,
            Username: user.Username
        }).promise())));
}

async function createUsersAndAddToGroup(poolId) {
    return Promise.all(BRIDGECREW_DEV_TEAM.map(async (devMember) => COGNITO.adminCreateUser({
        UserPoolId: poolId,
        Username: devMember,
        DesiredDeliveryMediums: ['EMAIL'],
        UserAttributes: [
            { Name: 'email', Value: devMember }
        ],
        TemporaryPassword: 'Password1234'
    }).promise()
        .then(() => COGNITO.adminAddUserToGroup({
            UserPoolId: poolId,
            Username: devMember,
            GroupName: DEVELOPER_PORTAL_ADMINS_GROUP_NAME
        }).promise())
        .catch((error) => {
            if (error.code && error.code === 'UsernameExistsException') {
                console.log(`User ${devMember} exists, skipping`);
            } else {
                return Promise.reject(error);
            }
            return null;
        })));
}

COGNITO.listUserPools({ MaxResults: 60 }).promise()
    .then((pools) => pools.UserPools.find((pool) => pool.Name === (process.env.PROFILE === 'prod' ? 'bridgecrew-users' : 'DevPortalIdentityPool')))
    .then((pool) => (process.env.PROFILE === 'prod' ? addExistingUsersToGroup(pool.Id) : createUsersAndAddToGroup(pool.Id)))
    .catch((err) => Promise.reject(err));
