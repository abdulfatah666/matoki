output "app_url" {
  value = local.domain_name
}

output "api_gateway_invoke_url" {
  value = aws_api_gateway_deployment.app_api_gateway_deployment.invoke_url
}

output "api_base_url" {
  value = local.api_url
}

output "unique_tag" {
  value = var.unique_tag
}

output "user_pool" {
  value = module.user_pool.cognito_user_pool_id
}