variable "region" {
  type        = "string"
  description = "The region in which the stack will be deployed"
}

variable "aws_profile" {
  type        = "string"
  description = "The profile to be selected from the ~/.aws/credentials file to be used in the creation of the stack"
}

variable "bridgecrew_template_bucket" {
  description = "The name of the bridgecrew cloud formation template bucket"
  default     = "bc-cf-template"
}

variable "template_bucket_path" {
  description = "The s3 path to the template file"
  default     = ""
}

variable "circle_ci_key" {
  type        = "string"
  description = "The CircleCI key"
}

variable "zendesk_api_key" {
  type        = "string"
  description = "The Zendesk api key"
}

variable "domain" {
  description = "The cognito domain string. This string will come after the subdomain"
  default     = "test.bridgecrew.cloud"
}

variable "cognito_subdomain" {
  description = "The cognito subdomain string. This string will come before the domain"
  default     = "auth"
}

variable "stage" {
  description = "The stage of the API deployment"
  default     = "v1"
}

variable "app_subdomain" {
  description = "The app subdomain string. This string will come before the domain"
  default     = "www"
}

variable hosted_zone_id {
  description = "The ID of the hosted zone where the route53 records will be created"
  default     = "Z2CCHMURZOJZR6"
}

variable support_email {
  description = "The BridgeCrew support email address"
  default     = "support@bridgecrew.io"
}

variable "turbo_mode" {
  description = "A flag that indicate wether to create route53 domain and cerficate or use AWS auto-generate domains"
}

variable "unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
}

variable "slack_hook_url" {
  description = "slack hook url"
  type        = "string"
}

variable "state_bucket" {
  description = "A name to identify s3 bucket where the stack is deployed, and deployment state is saved"
  type        = "string"
}

variable "circle_job_branch" {
  description = "The branch name to deploy with him"
  type        = "string"
  default     = "master"
}

variable "google_analytics_key" {
  description = "The key of google analytics account"
  type        = "string"
}

variable "run_circleci" {
  description = "should run circle ci job"
  type        = "string"
  default     = "True"
}

variable "app_npm_token" {
  description = "token for accessing bridgecrew repository on npmjs"
  type        = "string"
}

variable "es_cloudwatch_policy_create" {
  description = "if create the elasticsearch cloudwatch policy name"
  default     = true
}

variable "token_signing_secret" {
  description = "the secert that will be used to sign BC JWT tokens"
}

variable "token_signing_algo" {
  description = "the algorithm that will be used to sign BC JWT tokens"
  default     = "HS512"
}
variable "snapshots_moduleName" {
  description = "module name for snapshots api lambda"
  type        = "string"
  default     = "bc-snapshots"
}

variable "github_app_id" {
  description = "The Github app ID"
  type        = "string"
}

variable "github_app_client_id" {
  description = "The Github app client ID"
  type        = "string"
}
variable "github_app_client_secret" {
  description = "The Github app client secret"
  type        = "string"
}
variable "github_app_pem" {
  description = "The Github app private key"
  type        = "string"
}

variable "github_oauth_client_id" {
  type        = "string"
  description = "the oauth github app client id"
}

variable "github_oauth_client_secret" {
  type        = "string"
  description = "the oauth github app client id"
}

variable "google_oauth_client_id" {
  type        = "string"
  description = "the oauth google app client id"
}

variable "google_oauth_client_secret" {
  type        = "string"
  description = "the oauth google app client id"
}