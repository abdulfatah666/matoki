provider "aws" {
  region  = var.region
  profile = var.aws_profile
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

locals {
  unique_tag_with_dash      = var.unique_tag == "" ? var.unique_tag : format("-%s", var.unique_tag)
  unique_tag_with_underline = var.unique_tag == "" ? var.unique_tag : format("_%s", var.unique_tag)
}

module "consts" {
  source = "../../utils/terraform/consts"
}

module "cloudwatch_slack_lambda" {
  source                 = "../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../monitor/functions/monitor/"

  variable_string = "--slack_hook_url \"${var.slack_hook_url}\""
  unique_tag      = var.unique_tag
  sls_bucket_name = var.state_bucket

  lambda_names = [
    "bc-cloudwatch-slack-${var.unique_tag}",
  ]
}

// Creating support in the following regions: [The main region is us-west-2]
module "create_us_east-1" {
  source                             = "../regionStack"
  region                             = "us-east-1"
  aws_profile                        = var.aws_profile
  circle_ci_key                      = var.circle_ci_key
  unique_tag                         = var.unique_tag
  state_bucket                       = var.state_bucket
  bridgecrew_template_bucket         = var.bridgecrew_template_bucket
  customer_actions_lambda_arn        = concat(module.customer_actions_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  slack_alarm_lambda_arn             = concat(module.cloudwatch_slack_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  slack_hook_url                     = var.slack_hook_url
  customers_api_lambda_function_name = concat(module.customers_api_lambda.function_name, ["DUMMY"])[0]
  cwl_firehose_stream_arn            = module.cloudwatch_logs_producer.cwl_firehose_stream_arn
  cwl_to_kinesis_role_arn            = module.cloudwatch_logs_producer.cwl_to_kinesis_role_arn
}

module "us_east_2" {
  source                             = "../regionStack"
  region                             = "us-east-2"
  aws_profile                        = var.aws_profile
  circle_ci_key                      = var.circle_ci_key
  unique_tag                         = var.unique_tag
  state_bucket                       = var.state_bucket
  bridgecrew_template_bucket         = var.bridgecrew_template_bucket
  customer_actions_lambda_arn        = concat(module.customer_actions_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  slack_alarm_lambda_arn             = concat(module.cloudwatch_slack_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  slack_hook_url                     = var.slack_hook_url
  customers_api_lambda_function_name = concat(module.customers_api_lambda.function_name, ["DUMMY"])[0]
  cwl_firehose_stream_arn            = module.cloudwatch_logs_producer.cwl_firehose_stream_arn
  cwl_to_kinesis_role_arn            = module.cloudwatch_logs_producer.cwl_to_kinesis_role_arn
}

module "us_west_1" {
  source                             = "../regionStack"
  region                             = "us-west-1"
  aws_profile                        = var.aws_profile
  circle_ci_key                      = var.circle_ci_key
  unique_tag                         = var.unique_tag
  state_bucket                       = var.state_bucket
  bridgecrew_template_bucket         = var.bridgecrew_template_bucket
  customer_actions_lambda_arn        = concat(module.customer_actions_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  slack_alarm_lambda_arn             = concat(module.cloudwatch_slack_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  slack_hook_url                     = var.slack_hook_url
  customers_api_lambda_function_name = concat(module.customers_api_lambda.function_name, ["DUMMY"])[0]
  cwl_firehose_stream_arn            = module.cloudwatch_logs_producer.cwl_firehose_stream_arn
  cwl_to_kinesis_role_arn            = module.cloudwatch_logs_producer.cwl_to_kinesis_role_arn
}

module "main_region" {
  source                             = "../regionStack"
  region                             = "us-west-2"
  aws_profile                        = var.aws_profile
  circle_ci_key                      = var.circle_ci_key
  unique_tag                         = var.unique_tag
  state_bucket                       = var.state_bucket
  bridgecrew_template_bucket         = var.bridgecrew_template_bucket
  customer_actions_lambda_arn        = concat(module.customer_actions_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  slack_alarm_lambda_arn             = concat(module.cloudwatch_slack_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  slack_hook_url                     = var.slack_hook_url
  customers_api_lambda_function_name = concat(module.customers_api_lambda.function_name, ["DUMMY"])[0]
  cwl_firehose_stream_arn            = module.cloudwatch_logs_producer.cwl_firehose_stream_arn
  cwl_to_kinesis_role_arn            = module.cloudwatch_logs_producer.cwl_to_kinesis_role_arn
}

module "eu_west_1" {
  source                             = "../regionStack"
  region                             = "eu-west-1"
  aws_profile                        = var.aws_profile
  circle_ci_key                      = var.circle_ci_key
  unique_tag                         = var.unique_tag
  state_bucket                       = var.state_bucket
  bridgecrew_template_bucket         = var.bridgecrew_template_bucket
  customer_actions_lambda_arn        = concat(module.customer_actions_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  slack_alarm_lambda_arn             = concat(module.cloudwatch_slack_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[0]
  slack_hook_url                     = var.slack_hook_url
  customers_api_lambda_function_name = concat(module.customers_api_lambda.function_name, ["DUMMY"])[0]
  cwl_firehose_stream_arn            = module.cloudwatch_logs_producer.cwl_firehose_stream_arn
  cwl_to_kinesis_role_arn            = module.cloudwatch_logs_producer.cwl_to_kinesis_role_arn
}

data "aws_caller_identity" "current" {}

locals {
  unique_tag  = var.unique_tag == "" ? var.unique_tag : format("-%s", var.unique_tag)
  bucket_name = "${var.bridgecrew_template_bucket}-${data.aws_caller_identity.current.account_id}${local.unique_tag}"
}

/*
 * Creating a backup bucket in which firehose will store records in case it fails to write them to ES
 */
resource "aws_s3_bucket" "backup_bucket" {
  bucket        = "bc-backup-${data.aws_caller_identity.current.account_id}-${var.unique_tag}"
  acl           = "private"
  region        = var.region
  force_destroy = true

  tags = {
    Name = "bc-backup-${data.aws_caller_identity.current.account_id}-${var.unique_tag}"
  }

  versioning {
    enabled = true
  }
  lifecycle_rule {
    id      = "archive_after_4_days"
    enabled = true

    transition {
      days          = 4
      storage_class = "GLACIER"
    }

    expiration {
      days = 365
    }
  }
}

/*
 * Creates the buckets for cloud formation templates that should be accessable to our customers
 */

resource "aws_s3_bucket" "template_bucket" {
  region        = var.region
  bucket        = local.bucket_name
  acl           = "public-read"
  force_destroy = true

  tags = {
    Name = "${local.bucket_name}-${data.aws_caller_identity.current.account_id}"
  }
}

/*
 * Creates the cloudformation that will be sent to clients, and the bucket which will hold it.
 */
module "create_cloudformation_stack" {
  source                   = "../../api/infra/base/uploadCFTemplateToS3"
  region                   = var.region
  sns_topic_name           = module.main_region.customer_actions_sns_topic_name
  s3_bucket_name           = aws_s3_bucket.template_bucket.id
  unique_tag               = var.unique_tag
  cloudtrail_template_path = "../../api/infra/base/uploadCFTemplateToS3/cf_cloudtrails_sensor_template.json"
  config_template_path     = "../../api/infra/base/uploadCFTemplateToS3/cf_config_template.yml"
  s3_access_template_path  = "../../api/infra/base/uploadCFTemplateToS3/cf_s3_access_template.json"

  monitor = {
    enable               = true
    sns_topic_error_name = module.main_region.sns_monitor_topic_error_name
  }
}


module "loggly_lambda" {
  source                 = "../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../logArchive/functions/kds/producers/loggly/"
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.state_bucket

  lambda_names = [
    "handle-loggly-${var.unique_tag}",
  ]
}

/*
 * Create the handle-lacework lambda
 */
module "lacework_lambda" {
  source                 = "../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../logArchive/functions/kds/producers/lacework/"
  variable_string        = "--sqs_arn \"${module.lacework_cloudwatch.sqs_arn}\" --customers-api-lambda \"${concat(module.customers_api_lambda.function_name, [""])[0]}\""
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.state_bucket

  lambda_names = [
    "handle-lacework-${var.unique_tag}",
  ]
}

/*
 * Create Lacework cloudwatch event bus that triggers lacework data handling by the lambda above (lacework_lambda)
 */
module "lacework_cloudwatch" {
  source     = "../../logArchive/infra/base/laceworkCloudwatch"
  unique_tag = var.unique_tag
}

/*
 * Create the lambda that listens to the customer actions and handle them on our side.
 */
module "customer_actions_lambda" {
  source                 = "../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../customerActions"
  variable_string        = "--aws_profile ${var.aws_profile} --guardrailApiLambda \"${concat(module.compliances.violations_function_names, list("", "", ""))[2]}\" --circle_job_branch ${var.circle_job_branch} --run_circleci ${var.run_circleci} --customers_api_lambda \"${concat(module.customers_api_lambda.function_name, [""])[0]}\" --integrations_api_lambda \"${concat(module.bc_integrations_api_lambda.function_name, [""])[0]}\" --stepFunctionScannersArn ${module.scanners.scanners_step_function_arn}"
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.state_bucket

  lambda_names = [
    "handle-customer-actions-${var.unique_tag}",
  ]
}

/*
 * Create the lambda that fetches aws account admins
 */
module "enrich_account_admins_lambda" {
  source                 = "../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../logArchive/functions/enrichments/accountAdmins"
  variable_string        = "--aws_profile ${var.aws_profile} --customers-api-lambda \"${concat(module.customers_api_lambda.function_name, [""])[0]}\""
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.state_bucket

  lambda_names = [
    "enrich-account-admins-${var.unique_tag}",
  ]
}

/*
 * Create the Jira lambda
 */
module "jira_api_lambda" {
  source                 = "../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../integrations/functions/jira"
  sls_bucket_name        = var.state_bucket
  unique_tag             = var.unique_tag

  lambda_names = ["jira-conf-${var.unique_tag}"]
}

locals {
  domain_name = var.turbo_mode ? format("http://%s", module.app_site.bucket_distribution_domain_name) : format("https://%s.%s", var.app_subdomain, var.domain)
}

/*
* Create the bc-integrations handler lambda
*/
module "bc_integrations_api_lambda" {
  source                 = "../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../integrations/functions"
  variable_string        = "--stepFunctionIntegrationArn ${module.integrations_state_machines.integrations_step_function_arn} --stepFunctionIntegrationDestroyArn ${module.integrations_state_machines.integrations_destroy_step_function_arn} --stepFunctionScannersArn ${module.scanners.scanners_step_function_arn} --bridgecrewUtilsLambdaName \"${concat(module.bridgecrew_utils_lambda.function_name, [""])[0]}\" --domainName ${local.domain_name} --defaultIndexAge ${module.consts.es_index_retention_age}"
  sls_bucket_name        = var.state_bucket

  unique_tag = var.unique_tag

  lambda_names = [
    "bc-integrations-api-${var.unique_tag}",
    "bc-integrations-es-index-retention-${var.unique_tag}"
  ]
}

module "elasticsesarch_index_retention" {
  source                     = "../../integrations/infra/base/es/index_retention"
  index_retention_lambda_arn = concat(module.bc_integrations_api_lambda.function_arn, list("arn:aws:lambda:us-west-2:111111111111:function:DUMMY", "arn:aws:lambda:us-west-2:111111111111:function:DUMMY"))[1]
  unique_tag                 = var.unique_tag
  region                     = var.region
}

/*
 * Create DDB tables
 */
module "dynamo_db_tables" {
  source     = "../../utils/terraform/dynamoDb"
  unique_tag = var.unique_tag
}

resource "aws_dynamodb_table" "violations_for_resources" {
  name         = "violations_for_resources${var.unique_tag}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "id"
  range_key    = "violation_id"

  local_secondary_index {
    name            = "violation_id_index"
    projection_type = "ALL"
    range_key       = "violation_id"
  }

  global_secondary_index {
    hash_key        = "violation_id"
    name            = "violation_id-aws_account_id-index"
    range_key       = "aws_account_id"
    projection_type = "ALL"
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "violation_id"
    type = "S"
  }

  attribute {
    name = "aws_account_id"
    type = "S"
  }

  # When restoring a table via point_in_time_recovery you must manually set up the following on the restored table:
  # - Auto scaling policies
  # - AWS Identity and Access Management (IAM) policies
  # - Amazon CloudWatch metrics and alarms
  # - Tags
  # - Stream settings
  # - Time to Live (TTL) settings

  # Currently we dont use any of these settings
  point_in_time_recovery {
    enabled = true
  }
}

resource "aws_dynamodb_table" "tenants_env_details" {
  name         = "tenants_env_details${var.unique_tag}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "aws_account_id"

  global_secondary_index {
    hash_key        = "customer_name"
    name            = "customer_name-index"
    range_key       = "aws_account_id"
    projection_type = "ALL"
  }

  attribute {
    name = "aws_account_id"
    type = "S"
  }

  attribute {
    name = "customer_name"
    type = "S"
  }

  # When restoring a table via point_in_time_recovery you must manually set up the following on the restored table:
  # - Auto scaling policies
  # - AWS Identity and Access Management (IAM) policies
  # - Amazon CloudWatch metrics and alarms
  # - Tags
  # - Stream settings
  # - Time to Live (TTL) settings

  # Currently we dont use any of these settings
  point_in_time_recovery {
    enabled = true
  }
}

resource "aws_dynamodb_table" "violations_history" {
  name         = "violations_history${var.unique_tag}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "id"
  range_key    = "violation_id"

  global_secondary_index {
    hash_key        = "violation_id"
    name            = "violation_id-aws_account_id-index"
    range_key       = "aws_account_id"
    projection_type = "ALL"
  }

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "violation_id"
    type = "S"
  }

  attribute {
    name = "aws_account_id"
    type = "S"
  }

  # When restoring a table via point_in_time_recovery you must manually set up the following on the restored table:
  # - Auto scaling policies
  # - AWS Identity and Access Management (IAM) policies
  # - Amazon CloudWatch metrics and alarms
  # - Tags
  # - Stream settings
  # - Time to Live (TTL) settings

  # Currently we dont use any of these settings
  point_in_time_recovery {
    enabled = true
  }
}

resource "aws_dynamodb_table" "resources_snapshots_summary" {
  name         = "resources_snapshots_summary${var.unique_tag}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "snapshot_id"


  global_secondary_index {
    hash_key        = "account_id"
    name            = "account_id-category-index"
    range_key       = "category"
    projection_type = "ALL"
  }

  global_secondary_index {
    hash_key        = "account_id"
    name            = "account_id-scan_date-index"
    range_key       = "scan_date"
    projection_type = "ALL"
  }

  attribute {
    name = "snapshot_id"
    type = "S"
  }

  attribute {
    name = "category"
    type = "S"
  }

  attribute {
    name = "account_id"
    type = "S"
  }

  attribute {
    name = "scan_date"
    type = "S"
  }

  # When restoring a table via point_in_time_recovery you must manually set up the following on the restored table:
  # - Auto scaling policies
  # - AWS Identity and Access Management (IAM) policies
  # - Amazon CloudWatch metrics and alarms
  # - Tags
  # - Stream settings
  # - Time to Live (TTL) settings

  # Currently we dont use any of these settings
  point_in_time_recovery {
    enabled = true
  }
}

resource "aws_dynamodb_table" "violations_users" {
  name         = "violations_users_${var.unique_tag}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "id"

  attribute {
    name = "id"
    type = "S"
  }

  # When restoring a table via point_in_time_recovery you must manually set up the following on the restored table:
  # - Auto scaling policies
  # - AWS Identity and Access Management (IAM) policies
  # - Amazon CloudWatch metrics and alarms
  # - Tags
  # - Stream settings
  # - Time to Live (TTL) settings

  # Currently we dont use any of these settings
  point_in_time_recovery {
    enabled = true
  }
}

/*
 * Create the BridgeCrew User Pool
 */
module "user_pool" {
  source                     = "../../api/infra/base/userPool"
  region                     = var.region
  aws_profile                = var.aws_profile
  user_pool_name             = module.consts.user_pool_name
  domain                     = var.turbo_mode ? var.unique_tag : module.route53.route53_domain_record
  subdomain                  = var.cognito_subdomain
  certificate_arn            = module.certificate.certificate_arn
  path_to_logo               = "../../api/infra/base/userPool/logo.png"
  support_email              = var.support_email
  unique_tag                 = local.unique_tag_with_dash
  turbo_mode                 = var.turbo_mode
  oidc_issuer                = aws_api_gateway_deployment.app_api_gateway_deployment.invoke_url
  github_oauth_client_id     = var.github_oauth_client_id
  google_oauth_client_id     = var.google_oauth_client_id
  google_oauth_client_secret = var.google_oauth_client_secret
}

module "identity_pool" {
  source         = "../../api/infra/base/identityPool"
  user_pool_id   = module.user_pool.cognito_user_pool_id
  app_client_id  = module.user_pool.app_users_client_id
  region         = var.region
  api_gateway_id = module.api_gw.api_gateway_id
  unique_tag     = var.unique_tag
  aws_profile    = var.aws_profile
}


resource "aws_ssm_parameter" "scanners_backet_kms_key_parameter" {
  name  = "/base_stack/scanners_backet_kms_key_${var.unique_tag}"
  type  = "String"
  value = module.scanners.scanner_kms_key
}


resource "aws_ssm_parameter" "scanners_backet_name_parameter" {
  name  = "/base_stack/scanners_backet_name_${var.unique_tag}"
  type  = "String"
  value = module.scanners.scanners_bucket_name
}


/*
 * Store the user_pool_id in ssm
 */
resource "aws_ssm_parameter" "user_pool_id_parameter" {
  name  = "/base_stack/user_pool_id_${var.unique_tag}"
  type  = "String"
  value = module.user_pool.cognito_user_pool_id
}

resource "aws_ssm_parameter" "app_client_id_parameter" {
  name  = "/base_stack/app_client_id_${var.unique_tag}"
  type  = "String"
  value = module.user_pool.app_client_id
}

resource "aws_ssm_parameter" "domain_parameter" {
  name  = "/base_stack/domain_${var.unique_tag}"
  type  = "String"
  value = local.domain_name
}

/*
 * Store the remote remediation CF version in ssm
 */
resource "aws_ssm_parameter" "remote_remediation_cf_version_parameter" {
  name  = "/base_stack/remote_remediation_cf_version_${var.unique_tag}"
  type  = "String"
  value = module.consts.latest_remediation_cf_version
}

/*
 * Store the jwt signing secret in ssm
 */
resource "aws_ssm_parameter" "signing_secret_parameter" {
  name  = "/base_stack/token_signing_secret_${var.unique_tag}"
  type  = "SecureString"
  value = var.token_signing_secret
}

/*
 * Store the jwt signing algo in ssm
 */
resource "aws_ssm_parameter" "signing_algo_parameter" {
  name  = "/base_stack/token_signing_algo_${var.unique_tag}"
  type  = "SecureString"
  value = var.token_signing_algo
}

/*
 * Creates a certificate to authneticate all calls to the route53 domains.
 */
module "certificate" {
  source                      = "../../api/infra/base/certificate"
  profile                     = var.aws_profile
  cert_validation_record_fqdn = module.route53.cert_validation_record_fqdn
  domain                      = var.domain
  turbo_mode                  = var.turbo_mode
}

/*
 * Creates a certificate to authneticate all calls to the route53 domains.
 */
module "certificate_us_west_2" {
  source                      = "../../api/infra/base/certificate"
  profile                     = var.aws_profile
  cert_validation_record_fqdn = module.route53.cert_validation_record_fqdn
  domain                      = var.domain
  turbo_mode                  = var.turbo_mode
  region                      = "us-west-2"
}

/*
 * This module creates 3 route53 records:
 * 1. Certificate validation record.
 * 2. Domain record, which is a static alias to an existing IP of a webserver (currently - Wix homepage)
 * 3. The domain to be used with the User Pool's Cognito sign-in. This domain will be created *after* the
 *    user pool finishes attaching its domain (~15 minutes)
 */
module "route53" {
  source                              = "../../api/infra/base/route53Records"
  cognito_cloudfront_distribution_arn = module.user_pool.cognito_cloudfront_distribution_arn
  certificate_record_name             = module.certificate.certificate_resource_record_name
  certificate_record_value            = module.certificate.certificate_resource_record_value
  certificate_record_type             = module.certificate.certificate_resource_record_type
  domain                              = var.domain
  cognito_subdomain                   = var.cognito_subdomain
  app_subdomain                       = var.app_subdomain
  app_cloudfront_dist_arn             = module.app_site.cf_distribution_domain_name
  turbo_mode                          = var.turbo_mode
  accelerator_dns_name                = module.logstash_producer.accelerator_dns_name
}

/*
* Save the Zendesk API key to SSM
*/
resource "aws_ssm_parameter" "zendesk_key_parameter" {
  name      = "/base_stack/zendesk_api_key_${var.unique_tag}"
  type      = "SecureString"
  value     = var.zendesk_api_key
  overwrite = true
}

module "app_site" {
  source                  = "../../api/infra/base/appSite"
  aws_profile             = var.aws_profile
  region                  = var.region
  domain                  = var.turbo_mode ? local.api_url : var.domain
  api_gateway_invoke_url  = aws_api_gateway_deployment.app_api_gateway_deployment.invoke_url
  api_subdomain           = var.app_subdomain
  cert_arn                = module.certificate.certificate_arn
  unique_tag              = local.unique_tag_with_dash
  turbo_mode              = var.turbo_mode
  path_to_origin_folder   = "../../../client/"
  user_pool_id            = module.user_pool.cognito_user_pool_id
  google_analytics_key    = var.google_analytics_key
  app_npm_token           = var.app_npm_token
  github_app_id           = var.aws_profile == "prod" ? "Iv1.d236fb9b2c298d7b" : "Iv1.81645100d5f6069d"
  scanners_results_bucket = module.scanners.scanners_bucket_name

  cognito_user_pool_domain = module.user_pool.cognito_user_pool_domain

  monitor = {
    enable               = true
    sns_topic_error_name = module.main_region.sns_monitor_topic_error_name
  }
}

module "api_gw" {
  source                             = "../../api/infra/base/apiGateway"
  region                             = var.region
  apigw_name                         = "bridgecrew-api"
  stage                              = var.stage
  certificate_arn                    = module.certificate.certificate_arn
  domain                             = var.turbo_mode ? var.unique_tag : var.domain
  sub_domain                         = "api"
  jira_function_names                = module.jira_api_lambda.function_name
  violations_function_names          = module.compliances.violations_function_names
  customers_function_names           = module.customers_api_lambda.function_name
  logstash_function_name             = module.logstash_producer.function_name
  cloudmapper_api_function_name      = module.cloudmapper_api_lambda.function_name
  snapshots_api_function_name        = module.snapshots_api_lambda.function_name
  integrations_handler_function_name = module.bc_integrations_api_lambda.function_name
  authorization_functions_names      = module.authorization.authorization_function_names
  turbo_mode                         = var.turbo_mode
  unique_tag                         = local.unique_tag_with_dash
  remediation_function_arns          = module.compliances.remediations_function_arns
  user_pool_arn                      = module.user_pool.cognito_user_pool_arn
  swagger_file_path                  = "../../api/swagger/swagger.yml"
  github_api_function_name           = module.github_lambdas.github_function_names
  benchmarks_function_names          = module.compliances.benchmarks_function_names
  monitor = {
    enable               = true
    sns_topic_error_name = module.main_region.sns_monitor_topic_error_name
  }
}

module "root_cause_analysis_base" {
  source                 = "../../logArchive/infra/base/rootCauseAnalysis"
  region                 = var.region
  aws_profile            = var.aws_profile
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.state_bucket
  cloudwatch_logs_bucket = module.cloudwatch_logs_producer.cloudwatch_logs_bucket
  customers_api_lambda   = concat(module.customers_api_lambda.function_name, [""])[0]
  violations_api_lambda  = concat(module.compliances.violations_function_names, [""])[0]
  etl_table              = aws_dynamodb_table.etl_events_table.name

  monitor = {
    enable               = true
    sns_topic_error_name = module.main_region.sns_monitor_topic_error_name
  }
}

module "logstash_producer" {
  source = "../../logArchive/infra/base/logstashProducer"

  region          = var.region
  aws_profile     = var.aws_profile
  unique_tag      = var.unique_tag
  sls_bucket_name = var.state_bucket
  turbo_mode      = var.turbo_mode
  domain          = var.domain
  cert_arn        = module.certificate_us_west_2.certificate_arn

  monitor = {
    enable               = true
    sns_topic_error_name = module.main_region.sns_monitor_topic_error_name
  }
}

module "cloudwatch_logs_producer" {
  source = "../../logArchive/infra/base/cloudwatchLogs"

  region          = var.region
  aws_profile     = var.aws_profile
  unique_tag      = var.unique_tag
  sls_bucket_name = var.state_bucket

  monitor = {
    enable               = true
    sns_topic_error_name = module.main_region.sns_monitor_topic_error_name
  }
}

resource "aws_api_gateway_deployment" "app_api_gateway_deployment" {
  rest_api_id = module.api_gw.api_gateway_id
  stage_name  = var.stage

  variables = {
    deployed_at = timestamp()
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = ["module.api_gw.api_gateway_rest_api_execution_arn"]
}

locals {
  api_url = aws_api_gateway_deployment.app_api_gateway_deployment.*.invoke_url[0]
}


// -------- Compliances Lambdas --------
module "compliances" {
  source                       = "../../compliances/infra/base"
  aws_profile                  = var.aws_profile
  region                       = var.region
  path_to_serverless_yml       = "../../compliances"
  unique_tag                   = var.unique_tag
  sls_bucket_name              = var.state_bucket
  zendesk_api_key_name         = aws_ssm_parameter.zendesk_key_parameter.name
  module_name                  = module.consts.compliances_module_name
  remediations_sns_topic_name  = module.consts.remediations_sns_topic_name
  public_cloudformation_bucket = aws_s3_bucket.template_bucket.bucket
  customer_action_sns_topic    = module.main_region.customer_actions_sns_topic_name
  customers_api_lambda         = concat(module.customers_api_lambda.function_name, [""])[0]
  scanners_bucket_name         = module.scanners.scanners_bucket_name
  scanner_kms_key              = module.scanners.scanner_kms_key
  monitor = {
    enable               = true
    sns_topic_error_name = module.main_region.sns_monitor_topic_error_name
  }
}


// TODO: Remove this lambda
module "bridgecrew_utils_lambda" {
  source          = "../../utils/infra/base/lambda"
  aws_profile     = var.aws_profile
  region          = var.region
  unique_tag      = var.unique_tag
  sls_bucket_name = var.state_bucket
}

// This module allows our Elasticsearch hosts to write logs to CloudWatch
// It has a limit of 10 per region!
module "elasticsearch_application_logs_policy" {
  source     = "../../logArchive/infra/base/dataStreams"
  unique_tag = local.unique_tag_with_dash
  enable     = var.es_cloudwatch_policy_create
}

// ---------- customer API ----------------

locals {
  customer_api_function_name = "customer-info-api-${var.unique_tag}"
}

module "customer_setup" {
  source      = "../../customerActions/infra/base"
  aws_profile = var.aws_profile
  region      = var.region
  unique_tag  = var.unique_tag
}

module "customers_api_lambda" {
  source                             = "../../customers/infra/base/customerApi"
  aws_profile                        = var.aws_profile
  region                             = var.region
  unique_tag                         = var.unique_tag
  sls_bucket_name                    = var.state_bucket
  template_url                       = module.create_cloudformation_stack.template_url
  config_template_url                = module.create_cloudformation_stack.config_template_url
  customer_step_function_arn         = module.customer_state_machines.customer_step_function_arn
  update_customer_step_function_arn  = module.customer_state_machines.update_customer_step_function_arn
  destroy_customer_step_function_arn = module.customer_state_machines.destroy_customer_step_function_arn
  scanners_module_name               = module.consts.scanners_module_name
}

// ------------- Step Functions -------------
module "customer_state_machines" {
  source                                  = "../../customers/infra/base/stepFunctions"
  region                                  = var.region
  unique_tag                              = var.unique_tag
  aws_account_id                          = data.aws_caller_identity.current.account_id
  aws_ecs_task_definition_arn_customer    = module.customer_setup.aws_ecs_task_definition_arn_customer
  aws_ecs_task_definition_arn_integration = module.customer_setup.aws_ecs_task_definition_arn_integration
  cluster_arn                             = module.customer_setup.cluster_arn
  subnet                                  = concat(module.customer_setup.subnets, [""])[0]
  container_name                          = module.customer_setup.container_name
  aws_profile                             = var.aws_profile
  integrations_sf_arn                     = module.integrations_state_machines.integrations_step_function_arn
  integrations_destroy_sf_arn             = module.integrations_state_machines.integrations_destroy_step_function_arn

  monitor = {
    enable               = true
    sns_topic_error_name = module.main_region.sns_monitor_topic_error_name
  }
}

module "integrations_state_machines" {
  source                                  = "../../integrations/infra/base/stepFunctions"
  region                                  = var.region
  unique_tag                              = var.unique_tag
  aws_account_id                          = data.aws_caller_identity.current.account_id
  aws_ecs_task_definition_arn_customer    = module.customer_setup.aws_ecs_task_definition_arn_customer
  aws_ecs_task_definition_arn_integration = module.customer_setup.aws_ecs_task_definition_arn_integration
  cluster_arn                             = module.customer_setup.cluster_arn
  subnet                                  = concat(module.customer_setup.subnets, [""])[0]
  container_name                          = module.customer_setup.container_name
  aws_profile                             = var.aws_profile
  customer_sf_arn                         = module.customer_state_machines.customer_step_function_arn

  monitor = {
    enable               = true
    sns_topic_error_name = module.main_region.sns_monitor_topic_error_name
  }
}

// ------ cloudmapper ------
module "cloudmapper_api_lambda" {
  source                 = "../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../logArchive/functions/enrichments/cloudmapper"
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.state_bucket

  lambda_names = [
    "bc-cloudmapper-api-${var.unique_tag}"
  ]
}

// ------ snapshots ------
module "snapshots_api_lambda" {
  source                 = "../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../snapshots"
  variable_string        = "--module-name \"${var.snapshots_moduleName}\""
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.state_bucket

  lambda_names = [
    "bc-snapshots-${var.unique_tag}"
  ]
}

module "authorization" {
  source                     = "../../authorization/infra/base"
  aws_profile                = var.aws_profile
  cognito_userpool           = module.user_pool.cognito_user_pool_id
  module_name                = module.consts.authorization_module_name
  region                     = var.region
  sls_bucket_name            = var.state_bucket
  unique_tag                 = var.unique_tag
  user_pool_arn              = module.user_pool.cognito_user_pool_arn
  github_oauth_client_id     = var.github_oauth_client_id
  github_oauth_client_secret = var.github_oauth_client_secret
  customers_function_name    = concat(module.customers_api_lambda.function_name, [""])[0]
}

module "scanners" {
  source                      = "../../scanners/infra/base"
  artifact_bucket             = var.state_bucket
  aws_profile                 = var.aws_profile
  customers_api_lambda_arn    = concat(module.customers_api_lambda.function_arn, [""])[0]
  incidents_api_lambda_arn    = concat(module.compliances.violations_function_arns, [""])[0]
  suppressions_api_lambda_arn = concat(module.compliances.violations_function_arns, ["", ""])[1]
  path_to_sls_file            = "../../scanners/"
  region                      = var.region
  unique_tag                  = var.unique_tag
  github_lambda_arn           = concat(module.github_lambdas.github_function_arns, [""])[0]
  monitor = {
    enable               = true
    sns_topic_error_name = module.main_region.sns_monitor_topic_error_name
  }
}

// -------- Github Lambdas --------
module "github_lambdas" {
  source                   = "../../github/infra/base"
  aws_profile              = var.aws_profile
  region                   = var.region
  path_to_serverless_yml   = "../../github"
  unique_tag               = var.unique_tag
  sls_bucket_name          = var.state_bucket
  module_name              = module.consts.github_module_name
  github_app_id            = var.github_app_id
  result_bucket            = "bc-scanner-results-${data.aws_caller_identity.current.account_id}-${var.unique_tag}"
  integration_api_lambda   = concat(module.bc_integrations_api_lambda.function_name, ["arn:aws:lambda:us-west-2:111111111111:function:DUMMY"])[0]
  github_app_client_id     = var.github_app_client_id
  github_app_client_secret = var.github_app_client_secret
  github_app_pem           = var.github_app_pem
}

resource "aws_dynamodb_table" "etl_events_table" {
  hash_key     = "customer_name"
  name         = "etl_events_${var.unique_tag}"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "customer_name"
    type = "S"
  }

  # When restoring a table via point_in_time_recovery you must manually set up the following on the restored table:
  # - Tags
  # - Time to Live (TTL) settings

  # Currently we dont use any of these settings
  point_in_time_recovery {
    enabled = true
  }
}

resource "aws_dynamodb_table" "customers_webapp_data_table" {
  hash_key     = "customer_name"
  name         = "customers_webapp_data_${var.unique_tag}"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "customer_name"
    type = "S"
  }

  # When restoring a table via point_in_time_recovery you must manually set up the following on the restored table:
  # - Tags
  # - Time to Live (TTL) settings

  # Currently we dont use any of these settings
  point_in_time_recovery {
    enabled = true
  }
}

// Reports lambda
module "reports_lambda" {
  source                 = "../../utils/terraform/setupLambda"
  aws_profile            = var.aws_profile
  region                 = var.region
  path_to_serverless_yml = "../../reports"
  variable_string        = "--aws_profile ${var.aws_profile}"
  unique_tag             = var.unique_tag
  sls_bucket_name        = var.state_bucket

  lambda_names = [
    "bc-reports-${var.unique_tag}",
  ]
}

module "email_service" {
  source          = "../../email/infra/base/email"
  aws_profile     = var.aws_profile
  region          = var.region
  sls_bucket_name = var.state_bucket
  unique_tag      = var.unique_tag
}