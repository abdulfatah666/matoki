# Base Stack Pre-requisition per AWS account
Installation of base stack require some configuration in a specific account for the first time before running terraform for full installation
1. Enter to AWS account to Simple Email Service (SES)
2. Click Email Addresses in the left panel
3. Click Verify a New Email Address
4. Write the email support@bridgecrew.io
5. Enter to the email account and verify it
6. After the email verified mark it and click view details
7. Under Identity Policies click Create Policy 
8. Choose Custom Policy
9. Put the following Policy Document (**Don't forget to change the Resource ARN**):
```
{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "stmnt1556611747526",
            "Effect": "Allow",
            "Principal": {
                "Service": "cognito-idp.amazonaws.com"
            },
            "Action": [
                "ses:SendEmail",
                "ses:SendRawEmail"
            ],
            "Resource": "PUT THE EMAIL ARM FOR EXAMPLE: arn:aws:ses:us-west-2:090772183824:identity/support@bridgecrew.io"
        }
    ]
}
```
10. Click Apply Policy
11. Enter to AWS System Manager (SSM)
12. In the left panel click Parameter Store
13. Create parameter and create the following parameter:
```
Name: /base_stack/ses_support_arn
Description: The ARN of the verified SES identity
Value: THE ARN YOU PUT IN RESOURCE AT LEVEL 9 FOR EXAMPLE: arn:aws:ses:us-west-2:090772183824:identity/support@bridgecrew.io
```
14. Save the parameter
# Base Stack creation module
This module creates the base stack that serves multiple clients, which includes:
1. Cloudtrail handler lambda
2. Lacework handler lambda
3. customer-actions-handler lambda
4. SNS topic dedicated to receiving customer actions
5. SNS subscription to connect the customer-actions-handler lambda (#3) to the topic (#4)
6. The DDB tables, according to the "table_names_to_create" input
7. A BridgeCrew User Pool. 
8. A support user
9. A certificate for Kibana sign in.
10. 3 route 53 records (1 for certificate validation, 1 general, 1 for sign in)
11. SSM parameters of the producer lambdas
12. SSM paraeter of Circle CIF
13. The cloudformation template used for clients, and the bucket it is uploaded to.

# Variables
| Name | Required? | Type | Default | Example Value | Description |
|---|---|---|---|---|---|
|region | YES | String |  | us-east-1 | The aws region the stack will be deployed to.
|aws_profile | YES | String | |default| dev | The name of the aws profile to be used as defined in the credentials file
|bridgecrew_template_bucket| NO | String | bridgecrew-infra-as-code| bucket-name |The name of the bridgecrew cloud formation template bucket
|template_bucket_path|NO|String| |path/to/template |The s3 path to the template file
|user_pool_name|NO|String|bridgecrew-users|user-pool-name|The name of the BridgeCrew user pool
|circle_ci_key|YES|String||key| my-key| The CircleCI key, which will be saved to SSM Parameter Store
|table_names_to_create|NO|String|["loggly_tenants", "tenants"]|[""]|The names of the DDB tables to be cretaed
|domain|NO|String|test.bridgecrew.cloud.|example.com|The cognito domain string. This string will come after the subdomain.
|cognito_subdomain|NO|String|auth|www|The cognito subdomain string. This string will come before the domain
|hosted_zone_id|NO|String|Z2CCHMURZOJZR6| | The ID of the hosted zone where the route53 records will be created
|unique_tag|NO|String| |nim|A unique tag which enables creating a base stack along side an existing base stack
|slack_hook_url|YES|String||https://hooks.slack.com/services/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 | The Slack hook url for monitoring
|base_stack_bucket|YES|String||bridgecrew-terraform-remote-state-222 | The deployment s3 bucket
# Output
This module has no output.