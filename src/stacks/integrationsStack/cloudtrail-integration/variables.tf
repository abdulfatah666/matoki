// Required parameters

variable "region" {
  type        = "string"
  description = "AWS provider region"
}

variable "profile" {
  type        = "string"
  description = "AWS provider profile"
}

variable "enable" {
  description = "enable/disable the current integration"
}

variable "customer_name" {
  description = "The name of the customer this integration is created for"
}

variable "base_stack_unique_tag" {
  default = ""
}

variable "customer_aws_account_id" {
  type = "string"
}

variable "prod" {
  description = "if the terraform run on the production mode"
  default = false
}