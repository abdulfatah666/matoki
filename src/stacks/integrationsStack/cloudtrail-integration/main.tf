provider "aws" {
  profile = var.profile
  region = var.region
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

locals {
  producer = ["cloudtrail"]
  lambda_triggers_arns_list = [data.aws_ssm_parameter.cloudtrail_lambda_arn.value]
}

module "consts" {
  source = "../../../utils/terraform/consts"
}

data "external" "client_details" {
  program = ["bash","-c","aws dynamodb get-item --region us-west-2 --profile ${var.profile} --table-name tenants_env_details${var.base_stack_unique_tag} --key '{\"aws_account_id\": {\"S\": \"${var.customer_aws_account_id}\"}}' | jq '.Item | {customer_region: .deployment_region.S, sqs_url: .cloud_trail_cf_details.M.sqs_queue_url.S, customer_assume_role_arn: .cross_account_cf_details.M.role_arn.S}'"]
}

// Retrieve the producers lambda's ARN
data "aws_ssm_parameter" "cloudtrail_lambda_arn" {
  name = "/base_stack/cloudtrail${var.base_stack_unique_tag}/${data.external.client_details.result.customer_region}"
  with_decryption = false
}

module "es_details" {
  source = "../../../utils/terraform/getESDetails"
  region = var.region
  profile = var.profile
  es_domain_name = "bc-es-${var.customer_name}"
}

// Create data streams for cloudtrail producer
module "cloudtrail_data_streams" {
  source = "../../../logArchive/infra/customer/dataStreams"
  region = var.region
  producers_list = local.producer
  customer_name = var.customer_name
  es_domain_arn = module.es_details.arn
  base_stack_unique_tag = var.base_stack_unique_tag
  suffix_name = var.customer_aws_account_id

  monitor = {
    enable = true
    sns_topic_error_name = "${module.consts.monitor_sns_topic_error}${var.base_stack_unique_tag}"
  }
}

locals{
    sqs_name = split("/", data.external.client_details.result.sqs_url)[4]
    sqs_arn = "arn:aws:sqs:${data.external.client_details.result.customer_region}:${var.customer_aws_account_id}:${local.sqs_name}"
}


module "attach_sqs_to_producers" {
  source = "../../../utils/terraform/lambdaTriggers"
  region = data.external.client_details.result.customer_region
  aws_profile = var.profile
  sqs_arn = local.sqs_arn
  customer_aws_account_id = var.customer_aws_account_id
  lambda_arn_list = local.lambda_triggers_arns_list
  customer_name = var.customer_name
  customer_assume_role_arn = data.external.client_details.result.customer_assume_role_arn
}


