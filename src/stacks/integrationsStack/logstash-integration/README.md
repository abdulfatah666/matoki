# Logstash Integration

Enables reading logstash output 

Configured by rest API call:
```POST /v1/api/v1/integrations```
```json
{
	"customer_name": "acme",
	"name": "logstash",
	"enable": true,
	"params": {
		"api_key": "${REPLACE_WITH_GUID}",
		"type": ["linuxauth|auth0|pulse|fortigate|vault|meraki|pan"]
	}
}
```

[To view logstash collectors click here](https://github.com/bridgecrewio/bridgecrew-integrations).
