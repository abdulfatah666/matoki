provider "aws" {
  profile = var.profile
  region  = var.region
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

module "consts" {
  source = "../../../utils/terraform/consts"
}
module "es_details" {
  source         = "../../../utils/terraform/getESDetails"
  region         = var.region
  profile        = var.profile
  es_domain_name = "bc-es-${var.customer_name}"
}
module "data_streams" {
  source                = "../../../logArchive/infra/customer/dataStreams"
  region                = var.region
  producers_list        = var.type
  customer_name         = var.customer_name
  es_domain_arn         = module.es_details.arn
  base_stack_unique_tag = var.base_stack_unique_tag

  monitor = {
    enable               = true
    sns_topic_error_name = "${module.consts.monitor_sns_topic_error}${var.base_stack_unique_tag}"
  }
}
//-------------------------------------------------------------

data "aws_ecs_cluster" "logstash_ecs" {
  cluster_name = "bc-logstash-cluster-${var.base_stack_unique_tag}"
}
data "aws_ssm_parameter" "logstash_vpc_id" {
  name = "/logstash/vpc/id-${var.base_stack_unique_tag}"
}
data "aws_ssm_parameter" "logstash_dns" {
  name = "/logstash/dns/url-${var.base_stack_unique_tag}"
}
data "aws_ssm_parameter" "logstash_listener_port" {
  name = "/logstash/listener/port-${var.base_stack_unique_tag}"
}
data "aws_vpc" "logstash_vpc" {
  id = data.aws_ssm_parameter.logstash_vpc_id.value
}
data "aws_lb" "logstash_alb" {
  name = "bc-logstash-alb-${var.base_stack_unique_tag}"
}
data "aws_lb_listener" "logstash_https_listener" {
  load_balancer_arn = data.aws_lb.logstash_alb.arn
  port              = data.aws_ssm_parameter.logstash_listener_port.value
}
data "aws_subnet_ids" "logstash_public_subnets" {
  vpc_id = data.aws_ssm_parameter.logstash_vpc_id.value
}


resource "aws_security_group" "allow_logstash_proxy_ports" {
  name        = "${var.customer_name}_allow_logstash_proxy_ports_${var.base_stack_unique_tag}"
  description = "Allow logstash proxy inbound traffic for the container"
  vpc_id      = data.aws_ssm_parameter.logstash_vpc_id.value

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 9911
    to_port     = 9911
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 9910
    to_port     = 9910
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name     = "${var.customer_name}_allow_logstash_proxy_ports_${var.base_stack_unique_tag}"
    Customer = var.customer_name
    Tag      = var.base_stack_unique_tag
  }
}

resource "aws_lb_target_group" "integration_lb_target_group" {
  count       = var.enable_proxy ? length(var.type) : 0
  name        = "${var.customer_name}-${var.type[count.index]}-lb-tg-${var.base_stack_unique_tag}"
  port        = 9910
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = data.aws_ssm_parameter.logstash_vpc_id.value
  health_check {
    path = "/"
    port = 9911
  }
}

resource "aws_lb_listener_rule" "integration_route" {
  count        = var.enable_proxy ? length(var.type) : 0
  listener_arn = data.aws_lb_listener.logstash_https_listener.arn
  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.integration_lb_target_group[count.index].arn
  }
  condition {
    path_pattern {
      values = ["/logstash/${var.customer_name}/${var.type[count.index]}"]
    }
  }
}

locals {

  values_family = {
    "auth0"       = var.enable_proxy ? contains(var.type, "auth0") ? aws_ecs_task_definition.integration_task_definition[index(var.type, "auth0")].family : "" : "",
    "pan-os-http" = var.enable_proxy ? contains(var.type, "pan-os-http") ? aws_ecs_task_definition.integration_task_definition[index(var.type, "pan-os-http")].family : "" : ""
  }

  values_container_definition = {
    "auth0"       = module.auth0_integration_container_definition.json_map,
    "pan-os-http" = module.pan_os_http_container_definition.json_map
  }
}

data "aws_iam_role" "logstash_task_role" {
  name = "bc-logstash-ecs-task-role-${var.base_stack_unique_tag}"
}
data "aws_iam_role" "logstash_execution_role" {
  name = "bc-logstash-ecs-task-execution-role-${var.base_stack_unique_tag}"
}

resource "aws_ecs_service" "integration_service" {
  count                              = var.enable_proxy ? length(var.type) : 0
  name                               = "${var.customer_name}-${var.type[count.index]}-service-${var.base_stack_unique_tag}"
  cluster                            = data.aws_ecs_cluster.logstash_ecs.arn
  task_definition                    = lookup(local.values_family, var.type[count.index], "")
  desired_count                      = 1
  launch_type                        = "FARGATE"
  deployment_minimum_healthy_percent = "75"
  deployment_maximum_percent         = "100"

  network_configuration {
    subnets          = data.aws_subnet_ids.logstash_public_subnets.ids.*
    assign_public_ip = true
    security_groups  = [aws_security_group.allow_logstash_proxy_ports.id]
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.integration_lb_target_group[count.index].arn
    container_name   = "${var.customer_name}-logstash-${var.type[count.index]}-container-${var.base_stack_unique_tag}"
    container_port   = 9910
  }
  lifecycle {
    create_before_destroy = true
  }

  depends_on = [
    aws_ecs_task_definition.integration_task_definition
  ]
}

resource "aws_ecs_task_definition" "integration_task_definition" {
  count                    = var.enable_proxy ? length(var.type) : 0
  family                   = "${var.customer_name}-logstash-${var.type[count.index]}-td-${var.base_stack_unique_tag}"
  container_definitions    = "[ ${lookup(local.values_container_definition, var.type[count.index], "")} ]"
  task_role_arn            = data.aws_iam_role.logstash_task_role.arn
  execution_role_arn       = data.aws_iam_role.logstash_execution_role.arn
  requires_compatibilities = ["EC2", "FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 3072
}

module "auth0_integration_container_definition" {
  source           = "cloudposse/ecs-container-definition/aws"
  version          = "0.21.0"
  container_name   = "${var.customer_name}-logstash-auth0-container-${var.base_stack_unique_tag}"
  container_image  = "bridgecrew/auth0-integration:latest"
  container_memory = 1024
  container_cpu    = 1024
  essential        = true
  port_mappings = [{
    containerPort = 9910
    hostPort      = 9910
    protocol      = "tcp"
    },
    {
      containerPort = 9911
      hostPort      = 9911
      protocol      = "tcp"
    }
  ]
  environment = [
    {
      name  = "BC_CUSTOMER_NAME"
      value = var.customer_name
    },
    {
      name  = "BC_API_TOKEN"
      value = var.api_key
    },
    {
      name  = "BC_URL"
      value = "${data.aws_ssm_parameter.logstash_dns.value}/logstash/${var.customer_name}/auth0"
    }
  ]
  log_configuration = {
    logDriver = "awslogs"
    "options" : {
      "awslogs-group" : "/ecs/bc-logstash-ecs-log-${var.base_stack_unique_tag}"
      "awslogs-region" : var.region,
      "awslogs-stream-prefix" : "auth0-integration"
    }
    secretOptions = []
  }
}
module "pan_os_http_container_definition" {
  source           = "cloudposse/ecs-container-definition/aws"
  version          = "0.21.0"
  container_name   = "${var.customer_name}-logstash-pan-os-http-container-${var.base_stack_unique_tag}"
  container_image  = "bridgecrew/pan-os-http-integration:latest"
  container_memory = 1024
  container_cpu    = 1024
  essential        = true
  port_mappings = [{
    containerPort = 9910
    hostPort      = 9910
    protocol      = "tcp"
    },
    {
      containerPort = 9911
      hostPort      = 9911
      protocol      = "tcp"
    }
  ]
  environment = [
    {
      name  = "BC_CUSTOMER_NAME"
      value = var.customer_name
    },
    {
      name  = "BC_API_TOKEN"
      value = var.api_key
    },
    {
      name  = "BC_URL"
      value = "${data.aws_ssm_parameter.logstash_dns.value}/logstash/${var.customer_name}/pan-os-http"
    }
  ]
  log_configuration = {
    logDriver = "awslogs"
    "options" : {
      "awslogs-group" : "/ecs/bc-logstash-ecs-log-${var.base_stack_unique_tag}"
      "awslogs-region" : var.region,
      "awslogs-stream-prefix" : "pan-os-http"
    }
    secretOptions = []
  }
}



