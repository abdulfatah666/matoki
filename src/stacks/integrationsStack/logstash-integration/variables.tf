variable "region" {
  type        = "string"
  description = "AWS region name"
}

variable "profile" {
  type        = "string"
  description = "AWS profile. Should be exist in aws credentials file"
}

variable "customer_name" {
  type        = "string"
  description = "Customer name"
}

variable "base_stack_unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
}

variable "type" {
  description = "A suffix of the integration producers"
  type        = "list"
}

variable "prod" {
  description = "if the terraform run on the production mode"
  default     = false
}
variable "api_key" {
  description = "API token to logstash proxy"
  type        = "string"
  default     = "None"
}

variable "enable_proxy" {
  description = "enable proxy from default logstash lambda to our docker LB"
  default     = false
}