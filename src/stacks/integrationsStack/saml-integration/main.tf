provider "aws" {
  profile = var.profile
  region  = var.region
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

module "consts" {
  source = "../../../utils/terraform/consts"
}

locals {
  user_pool_name = module.consts.user_pool_name
  identity_pool_name      = "IP_${var.customer_name}"
}

data "aws_cognito_user_pools" "selected" {
  name = "${local.user_pool_name}-${var.base_stack_unique_tag}"
}

data "aws_ssm_parameter" "base_stack_domain" {
  name = "/base_stack/domain_${var.base_stack_unique_tag}"
}

locals {
   user_pool_id = concat(data.aws_cognito_user_pools.selected.ids.*,list(""))[0]
}


resource "aws_cognito_identity_provider" "provider" {
  user_pool_id  = local.user_pool_id
  provider_name = "saml-${var.provider_type}-${var.customer_name}"
  provider_type = "SAML"
  idp_identifiers = split(",", var.domain)
  provider_details = {
    MetadataFile = var.saml_file
  }

  attribute_mapping = {
    "email" = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"
  }
}

resource "aws_cognito_user_pool_client" "app_client" {
  name                = "app_client_${var.provider_type}_${var.customer_name}"
  user_pool_id        = local.user_pool_id
  generate_secret     = false
  callback_urls       = [(var.prod) ? "${data.aws_ssm_parameter.base_stack_domain.value}/callback" : "http://localhost:8080/callback"]
  logout_urls         = [(var.prod) ? data.aws_ssm_parameter.base_stack_domain.value : "http://localhost:8080"]
  allowed_oauth_flows_user_pool_client = true
  explicit_auth_flows = ["ADMIN_NO_SRP_AUTH"]
  allowed_oauth_flows = ["implicit"]
  allowed_oauth_scopes = ["phone","email","openid","profile"]
  supported_identity_providers = [aws_cognito_identity_provider.provider.provider_name]
}

locals {
  providerName = "cognito-idp.${var.region}.amazonaws.com/${local.user_pool_id}"
}

resource "null_resource" "attach_app_client_id" {
  provisioner "local-exec" {
    working_dir = "${path.module}/../../../utils/nodeUtils/cognito"
    command = "npm i --quiet --no-progress -s --loglevel error  > /dev/null&&node identity_pool_cli.js addCognitoProvider ${local.identity_pool_name} --providerName ${local.providerName} --clientId ${aws_cognito_user_pool_client.app_client.id} --serverSideTokenCheck false --profile ${var.profile} --region ${var.region}"
  }
}