variable "region" {
  type        = "string"
  description = "AWS region name"
}

variable "profile" {
  type        = "string"
  description = "AWS profile. Should be exist in aws credentials file"
}

variable "customer_name" {
  type        = "string"
  description = "Customer name"
}

variable "base_stack_unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
  default     = ""
}

variable "saml_file" {
  description = "saml file"
  type        = "string"
}

variable "provider_type" {
  description = "provider type (onelogin, okta)"
  type        = "string"
}

variable "domain" {
  description = "domain name"
}

variable "prod" {
  description = "if the terraform run on the production mode"
  default = false
}