# Integrations
## How to configure Bridgecrew integrations:
* [Lacework](lacework-integration/README.md)
* [Logstash](logstash-integration/README.md)
* [SEP15](#integration-using-credentials-pull-from-api)
* [OneLogin](#integration-using-credentials-pull-from-api)

## Integration using credentials (Pull from API)
After getting the clientId & clientSecret / API token and so on, the following API call should be made to
```POST /api/v1/integrations```:

```json
{
    "type": "[sep15|onelogin]",
    "enable": true,
    "alias": "default",
    "params":{
        "clientId": "...",
        "clientSecret": "...",
        "endpointRegion": "[us|eu]" // For OneLogin integration only
    }
}
 ```