variable "region" {
  type        = "string"
  description = "AWS provider region"
}

variable "profile" {
  type        = "string"
  description = "AWS provider profile"
}

variable "customer_name" {
  description = "The name of the customer this integration is created for"
}

variable "base_stack_unique_tag" {
  description = "The base stack's unique tag"
  type        = string
}

variable "schedule_rate" {
  description = "The scheduling expression rate in minutes"
  default     = 5
}