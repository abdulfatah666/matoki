provider "aws" {
  profile = var.profile
  region  = var.region
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

module "consts" {
  source = "../../../utils/terraform/consts"
}

data "aws_lambda_function" "logs_handler_lambda_details" {
  function_name = "${module.consts.pull_from_api_producer_function_name}-${var.base_stack_unique_tag}"
}

resource "aws_cloudwatch_event_rule" "onelogin_logs_scheduled_rule" {
  name                = "bc-${var.customer_name}-onelogin-logs-trigger"
  description         = "Trigger the producer lambda function"
  schedule_expression = "rate(${var.schedule_rate} minutes)"
  is_enabled          = true

  tags = {
    customer_name = var.customer_name
  }

}

resource "aws_cloudwatch_event_target" "onelogin_logs_lambda_func_target" {
  rule  = aws_cloudwatch_event_rule.onelogin_logs_scheduled_rule.name
  arn   = data.aws_lambda_function.logs_handler_lambda_details.arn
  input = "{\"customerName\": \"${var.customer_name}\", \"type\": \"onelogin\"}"
}

// Add permission to allow the event rule trigger the lambda function
resource "aws_lambda_permission" "allow_rule_trigger" {
  statement_id  = "bc-${var.customer_name}-onelogin-logs-event"
  action        = "lambda:InvokeFunction"
  function_name = data.aws_lambda_function.logs_handler_lambda_details.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.onelogin_logs_scheduled_rule.arn
}