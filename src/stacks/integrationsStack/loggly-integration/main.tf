provider "aws" {
  profile = var.profile
  region  = var.region
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

locals {
  rule_name            = "bc-${var.customer_name}-loggly-trirgger"
  statement_id         = "bc-${local.rule_name}-event"
  string_list          = var.enable ? "loggly" : ""
  producers_list       = compact(split(",", local.string_list))
  lambda_function_name = "${var.target_lambda_name}-${var.base_stack_unique_tag}"
}

data "aws_lambda_function" "loggly_handler_lambda_details" {
  function_name = local.lambda_function_name
}

// Creating a scheduled event rule that will trigget the loggly multi-tenant lambda function
// Note : This resource will be created only after the loggly data streams were created
resource "aws_cloudwatch_event_rule" "loggly_scheduled_trigger" {
  count               = var.enable ? 1 : 0
  name                = local.rule_name
  description         = "Trigger the loggly-handler lambda function"
  schedule_expression = "rate(${var.schedule_rate} minutes)"
  is_enabled          = true
  tags = {
    customer_name = var.customer_name
  }

}

// Add the loggly multi-tenant lambda function as event rule target
resource "aws_cloudwatch_event_target" "loggly_lambda_func_target" {
  count = var.enable ? 1 : 0
  rule  = local.rule_name
  arn   = data.aws_lambda_function.loggly_handler_lambda_details.arn
  input = "{\"customer_name\": \"${var.customer_name}\", \"api_key\": \"${var.api_key}\", \"server_url\": \"${var.server_url}\"}"

  depends_on = ["aws_cloudwatch_event_rule.loggly_scheduled_trigger"]
}

// Add permission to allow the event rule trigger the lambda function
resource "aws_lambda_permission" "allow_rule_trigger" {
  count         = var.enable ? 1 : 0
  statement_id  = local.statement_id
  action        = "lambda:InvokeFunction"
  function_name = local.lambda_function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.loggly_scheduled_trigger[0].arn

  depends_on = ["aws_cloudwatch_event_target.loggly_lambda_func_target"]
}

module "consts" {
  source = "../../../utils/terraform/consts"
}

module "es_details" {
  source         = "../../../utils/terraform/getESDetails"
  region         = var.region
  profile        = var.profile
  es_domain_name = "bc-es-${var.customer_name}"
}

// Create data streams for loggly producer
module "loggly_data_streams" {
  source                = "../../../logArchive/infra/customer/dataStreams"
  region                = var.region
  producers_list        = local.producers_list
  customer_name         = var.customer_name
  es_domain_arn         = module.es_details.arn
  base_stack_unique_tag = var.base_stack_unique_tag

  monitor = {
    enable               = true
    sns_topic_error_name = "${module.consts.monitor_sns_topic_error}${var.base_stack_unique_tag}"
  }
}
