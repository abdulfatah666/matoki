// Required parameters

variable "region" {
  type        = "string"
  description = "AWS provider region"
}

variable "profile" {
  type        = "string"
  description = "AWS provider profile"
}

variable "enable" {
  description = "enable/disable the current integration"
}

variable "customer_name" {
  description = "The name of the customer this integration is created for"
}

variable "api_key" {
  type        = "string"
  description = "The api key to the loggly server"
}

variable "server_url" {
  type        = "string"
  description = "The url of the loggly server"
}

// Optional parameters

variable "schedule_rate" {
  description = "The scheduling expression rate in minutes"
  default     = 3
}

variable "target_lambda_name" {
  type        = "string"
  description = "The name of the lambda function that handle loggly logs"
  default     = "handle-loggly"
}

variable "base_stack_unique_tag" {
  default = ""
}

variable "working_directory_path" {
  default = "./"
}

variable "create_template_script_file_path" {
  default = "../../../utils/utilsPython/utilsPython/es/create_dynamic_template.py"
}

variable "template_name" {
  default = "loggly-template"
}

variable "mapping_path" {
  default = "./loggly_index_mapping"
}

variable "prod" {
  description = "if the terraform run on the production mode"
  default = false
}