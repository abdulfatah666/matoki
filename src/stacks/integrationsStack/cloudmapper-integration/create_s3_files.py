import os
import json
from argparse import ArgumentParser


# Parsing the command-line parameters
parser = ArgumentParser()
parser.add_argument('--func_name', required=True)
parser.add_argument('--customer_name', required=False)
parser.add_argument('--account_ids', required=False)
parser.add_argument('--bucket_name', required=False)
parser.add_argument('--cross_account_roles', required=False)
parser.add_argument('--external_ids', required=False)
parser.add_argument('--unique_tag', required=False)

# Extract the command line variables
args = vars(parser.parse_args())
func_name = args["func_name"]
customer_name = args["customer_name"]
account_ids = args["account_ids"]
bucket_name = args["bucket_name"]
cross_account_roles = args["cross_account_roles"]
external_ids = args["external_ids"]
unique_tag = args["unique_tag"]

account_ids = account_ids.split(',') if account_ids != None else []
cross_account_roles = cross_account_roles.split(',') if cross_account_roles != None else []
external_ids = external_ids.split(',') if external_ids != None else []

S3_BUCKET_FILES_PATH = "cloudmapper/auditor/s3_bucket_files/"

def create_config_json():
    try:
        os.remove("%sconfig.json" % (S3_BUCKET_FILES_PATH))
    except OSError:
        pass

    obj = {"accounts": [], "cidrs": {}}
    for idx, account_id in enumerate(account_ids):
        obj["accounts"].append({"id": account_id, "name": account_id + '-' + customer_name, "default": 'true' if idx == 0 else 'false'})

    f = open("%sconfig.json" % (S3_BUCKET_FILES_PATH), "a")
    f.write(json.dumps(obj))
    f.close()

def create_cdk_app_yaml():
    try:
        os.remove("%scdk_app.yaml" % (S3_BUCKET_FILES_PATH))
    except OSError:
        pass
    f = open("%scdk_app.yaml" % (S3_BUCKET_FILES_PATH), "a")
    f.write("s3_bucket: %s\n" % (bucket_name))
    f.write("iam_role: %s-bc-bridgecrewcwssarole\n" % (customer_name))
    f.write('minimum_alert_severity: "LOW"\n')
    f.write('customer_name: "%s"\n' % (customer_name))
    f.close()

def create_config_file():
    try:
        os.remove("%sconfig" % (S3_BUCKET_FILES_PATH))
    except OSError:
        pass
    f = open("%sconfig" % (S3_BUCKET_FILES_PATH), "a")
    for idx, account_id in enumerate(account_ids):
        f.write("[profile %s-%s]\n" % (account_id, customer_name))
        f.write("region=us-west-2\n")
        f.write("output=json\n")
        f.write("credential_source = EcsContainer\n")
        f.write("role_arn=%s\n" % (cross_account_roles[idx]))
        f.write("external_id=%s\n\n" % (external_ids[idx]))
    f.close()

def add_tag_to_slack_webhook_name():
    with open("%srun_cloudmapper.sh" % (S3_BUCKET_FILES_PATH), "r") as f:
        fileData = f.read()

    fileData = fileData.replace('--secret-id cloudmapper-slack-webhook', '--secret-id cloudmapper-slack-webhook-%s' % (unique_tag))

    with open('%srun_cloudmapper.sh' % (S3_BUCKET_FILES_PATH), 'w') as file:
      file.write(fileData)


funcDic = {
    "create_config_json": create_config_json,
    "create_cdk_app_yaml": create_cdk_app_yaml,
    "create_config_file": create_config_file,
    "add_tag_to_slack_webhook_name": add_tag_to_slack_webhook_name
}

funcDic[func_name]()
