variable "region" {
  type        = "string"
  description = "AWS provider region"
}

variable "profile" {
  type        = "string"
  description = "AWS provider profile"
}

variable "customer_name" {
  description = "The name of the customer this integration is created for"
}

variable "aws_account_ids" {
  type        = "list"
  description = "list of customers aws account ids"
}

variable "base_stack_unique_tag" {
  default = ""
}

variable "prod" {
  description = "if the terraform run on the production mode"
  default = false
}