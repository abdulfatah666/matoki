# Cloudmapper Integration

Enables reading aws account resources and create json file which represent the architecture of the account resources.

Configured by rest API call:

```POST /api/v1/integrations```

```json
 {
     "customer_name": "ACME",
     "name": "cloudmapper",
     "enable": true,
     "params":{
         "aws_account_ids" :["111"]
     }
 }
 ```
if you'd like to add more accounts to the cloudmapper, you must provide ALL the account ids that were already integrated plus the new ones.
```json
 {
     "customer_name": "ACME",
     "name": "cloudmapper",
     "enable": true,
     "params":{
         "aws_account_ids" :["111","222"]
     }
 }
 ```