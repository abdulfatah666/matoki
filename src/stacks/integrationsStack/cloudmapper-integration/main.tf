provider "aws" {
  profile = "${var.profile}"
  region  = "${var.region}"
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

data "aws_caller_identity" "current" {}

locals {
  base_stack_aws_account_id = "${data.aws_caller_identity.current.account_id}"
  bucket_name          = "bc-bridgecrew-${local.base_stack_aws_account_id}-${var.customer_name}-cloudmapper"
  unique_tag_with_dash = var.base_stack_unique_tag == "" ? var.base_stack_unique_tag : format("-%s", var.base_stack_unique_tag)
}

data "external" "is_account_bucket_exists" {
  program = ["bash", "-c", "if aws s3api head-bucket --profile ${var.profile} --bucket ${local.bucket_name} 2>/dev/null; then echo '{\"ok\":\"true\"}'; else echo '{\"ok\":\"false\"}'; fi"]
}

resource "aws_s3_bucket" "create_costumer_bucket" {
  bucket = "${local.bucket_name}"
  acl    = "private"
  force_destroy = true

  depends_on = ["data.external.is_account_bucket_exists"]
}

resource "null_resource" "install_cloudmapper" {
  triggers = {
    build = timestamp()
  }

  provisioner "local-exec" {
    command = "rm -rf cloudmapper && git clone https://github.com/bridgecrewio/cloudmapper.git && cd cloudmapper/auditor && git clone https://github.com/bridgecrewio/cloudmapper.git resources/cloudmapper && npm install && npm i aws-cdk"
  }
}

data "external" "get_cross_account_roles" {
  count   = length(var.aws_account_ids)
  program = ["bash", "-c", "aws dynamodb get-item --profile ${var.profile} --region ${var.region} --table-name tenants_env_details${var.base_stack_unique_tag} --key '{\"aws_account_id\": {\"S\": \"${var.aws_account_ids[count.index]}\"}}' | jq '.Item | {cross: .cross_account_cf_details.M.cross_account_role_arn.S, external_id: .cloud_trail_cf_details.M.external_id.S}'"]
}

// ---- Create config file -----
resource "null_resource" "create_s3_bucket_files-config" {
  triggers = {
    build = timestamp()
  }

  provisioner "local-exec" {
    command = "python create_s3_files.py --func_name create_config_file --customer_name ${var.customer_name} --account_ids ${join(",", var.aws_account_ids)} --cross_account_roles ${join(",", data.external.get_cross_account_roles.*.result.cross)} --external_ids ${join(",", data.external.get_cross_account_roles.*.result.external_id)}"
  }

  depends_on = ["data.external.get_cross_account_roles", "null_resource.install_cloudmapper"]
}

// ---- Create config.json file -----
resource "null_resource" "create_confing_json_file" {
  triggers = {
    build = timestamp()
  }

  provisioner "local-exec" {
    command = "python create_s3_files.py --func_name create_config_json --customer_name ${var.customer_name} --account_ids ${join(",", var.aws_account_ids)}"
  }

  depends_on = ["data.external.get_cross_account_roles", "null_resource.install_cloudmapper"]
}

// ---- Create cdk_app.yaml file -----
resource "null_resource" "create_cdk_app-yaml" {
  triggers = {
    build = timestamp()
  }

  provisioner "local-exec" {
    command = "python create_s3_files.py --func_name create_cdk_app_yaml --customer_name ${var.customer_name} --bucket_name ${local.bucket_name}"
  }

  depends_on = ["data.external.get_cross_account_roles", "null_resource.install_cloudmapper"]
}

// ---- Upload files to bucket -----
locals {
  files_for_upload = list("audit_config_override.yaml", "cdk_app.yaml", "config", "config.json", "run_cloudmapper.sh")
}

resource "null_resource" "upload_s3_file" {
  count  = length(local.files_for_upload)
    triggers = {
      build = timestamp()
    }

  provisioner "local-exec" {
    working_dir = "cloudmapper/auditor/s3_bucket_files"
    command     = "aws s3 --profile ${var.profile} cp ${local.files_for_upload[count.index]}  s3://${local.bucket_name}"
  }

    depends_on = [
      "null_resource.create_cdk_app-yaml",
      "null_resource.create_s3_bucket_files-config",
      "null_resource.create_confing_json_file",
      "aws_s3_bucket.create_costumer_bucket"
    ]
}

resource "null_resource" "deploy_cdk" {
  count = "${data.external.is_account_bucket_exists.result.ok == "true" ? 0 : 1}"
  triggers = {
    build = timestamp()
  }

  provisioner "local-exec" {
    working_dir = "cloudmapper/auditor"
    command     = "./node_modules/aws-cdk/bin/cdk --profile ${var.profile} bootstrap aws://${local.base_stack_aws_account_id}/${var.region} && ./node_modules/aws-cdk/bin/cdk --profile ${var.profile} deploy --require-approval never"
  }

  depends_on = ["null_resource.upload_s3_file", "null_resource.install_cloudmapper"]
}
