provider "aws" {
  profile = var.profile
  region  = var.region
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

module "consts" {
  source = "../../../utils/terraform/consts"
}

locals {
  user_pool_name = module.consts.user_pool_name
}

data "aws_cognito_user_pools" "selected" {
  name = "${local.user_pool_name}-${var.base_stack_unique_tag}"
}

data "aws_caller_identity" "current" {}

locals {
  user_pool_id               = concat(data.aws_cognito_user_pools.selected.ids.*, list(""))[0]
  base_stack_aws_account_id  = data.aws_caller_identity.current.account_id
  index_retention_lambda_arn = "arn:aws:lambda:${var.region}:${local.base_stack_aws_account_id}:function:${var.retention_lambda_name}-${var.base_stack_unique_tag}"
  index_retention_cw_rule    = "${module.consts.es_index_retention_cw_rule}-${var.base_stack_unique_tag}-${var.customer_name}"
}


module "create_ES_and_IP" {
  source                = "../../../api/infra/customer/clientIdentityPoolAndElasticsearch"
  region                = var.region
  aws_profile           = var.profile
  user_pool_id          = local.user_pool_id
  customer_name         = var.customer_name
  ebs_volume_size       = var.ebs_volume_size
  instance_type         = var.data_node_type
  instance_count        = var.data_node_count
  master_enable         = var.master_enable
  master_count          = var.master_count
  master_type           = var.master_type
  base_stack_unique_tag = var.base_stack_unique_tag

  monitor = {
    enable               = true
    sns_topic_error_name = "${module.consts.monitor_sns_topic_error}${var.base_stack_unique_tag}"
  }
}

resource "aws_cloudwatch_event_rule" "index_retention_event" {
  name                = local.index_retention_cw_rule
  description         = "Daily ElasticSearch index retention"
  schedule_expression = "rate(1 day)"
}

resource "aws_cloudwatch_event_target" "index_retention_target" {
  rule      = aws_cloudwatch_event_rule.index_retention_event.name
  input     = "{\"customer_name\": \"${var.customer_name}\"}"
  arn       = local.index_retention_lambda_arn
}