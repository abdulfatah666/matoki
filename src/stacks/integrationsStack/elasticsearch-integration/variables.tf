variable "region" {
  type        = "string"
  description = "AWS region name"
}

variable "profile" {
  type        = "string"
  description = "AWS profile. Should be exist in aws credentials file"
}

variable "customer_name" {
  type        = "string"
  description = "Customer name"
}

variable "base_stack_unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type        = "string"
  default     = ""
}

variable "total_fields_limit" {
  description = "total fields limit"
  default     = 2000
}

variable "ebs_volume_size" {
  description = "The storage size of the EBS, in GB."
  default     = 10
}

variable "data_node_type" {
  description = "The type of instances to be created in the cluster"
  default     = "t2.small.elasticsearch"
  type        = "string"
}

variable "data_node_count" {
  description = "The number of instances in the cluster"
  default     = "2"
  type        = "string"
}

variable "master_enable" {
  default = false
}

variable "master_type" {
  description = "The type of master instances to be created in the cluster"
  type        = "string"
  default     = "r5.large.elasticsearch"
}

variable "master_count" {
  description = "The number of master instances in the cluster"
  default     = 3
}

variable "prod" {
  description = "if the terraform run on the production mode"
  default     = false
}

variable "enable" {
  description = "enable/disable the current integration"
}

variable "retention_lambda_name" {
  type        = "string"
  description = "The name of the index retention lambda function"
  default     = "bc-integrations-es-index-retention"
}