# ElasticSearch (SIEM) Integration

Enables SIEM ElasticSearch DB

Configured by rest API call:

```POST /api/v1/integrations```

```json
 {
     "type": "elasticsearch",
     "enable": true,
     "params":{
         "ebs_volume_size" : 150,
         "data_node_type"  : "m5.2xlarge.elasticsearch",
         "data_node_count" : 2,
         "master_enable"   : true,
         "master_type"     : "r5.large.elasticsearch",
         "master_count"    : 3
     }
 }
 ```