provider "aws" {
  profile = var.profile
  region = var.region
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

module "aws-enrichments" {
  source = "./aws-enrichments-integration"
  customer_aws_account_id = var.customer_aws_account_id
  customer_name = var.customer_name
  profile = var.profile
  region = var.region
  base_stack_unique_tag = var.base_stack_unique_tag
}