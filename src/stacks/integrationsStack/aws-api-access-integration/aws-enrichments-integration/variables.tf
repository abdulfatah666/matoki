variable "region" {
  type = "string"
  description = "the aws region"
}

variable "profile" {
  type = "string"
  description = "AWS profile. Should be exist in aws credentials file"
}

variable "base_stack_unique_tag" {
  description = "A unique name to identify all the resources created by this run. Must be a single word, no '-' or '_'"
  type = "string"
}

variable "customer_aws_account_id" {
  type = "string"
}

variable "customer_name" {
  type = "string"
  description = "Customer name"
}
