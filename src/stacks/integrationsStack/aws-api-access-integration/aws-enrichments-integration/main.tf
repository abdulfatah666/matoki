provider "aws" {
  region  = var.region
  profile = var.profile
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

data "external" "client_details" {
  program = ["bash","-c","aws dynamodb get-item --region us-west-2 --profile ${var.profile} --table-name tenants_env_details${var.base_stack_unique_tag} --key '{\"aws_account_id\": {\"S\": \"${var.customer_aws_account_id}\"}}' | jq '.Item | {customer_region: .deployment_region.S, sqs_url: .cloud_trail_cf_details.M.sqs_queue_url.S, customer_assume_role_arn: .cross_account_cf_details.M.cross_account_role_arn.S}'"]
}

module "root_cause_analysis_customer" {
  source                   = "../../../../logArchive/infra/customer/rootCauseAnalysis"
  base_stack_unique_tag    = var.base_stack_unique_tag
  customer_assume_role_arn = data.external.client_details.result.customer_assume_role_arn
  customer_aws_account_id  = var.customer_aws_account_id
  customer_name            = var.customer_name
}