// Required parameters

variable "region" {
  type        = "string"
  description = "AWS provider region"
}

variable "profile" {
  type        = "string"
  description = "AWS provider profile"
}

variable "enable" {
  description = "enable/disable the current integration"
}

variable "customer_name" {
  description = "The name of the customer this integration is created for"
}

variable "customer_aws_account_id" {
  type        = "string"
  description = "The customers aws account id"
}

variable "lacework_account_id" {
  type        = "string"
  description = "The lacework account identifier"
}

variable "lacework_secret_access_key" {
  type        = "string"
  description = "The lacework account secret key"
}

variable "lacework_access_key_id" {
  type        = "string"
  description = "The lacework account access key"
}

// Optional parameters

variable "schedule_rate" {
  description = "The scheduling expression rate in minutes"
  default     = 480
}

variable "target_lambda_name" {
  type        = "string"
  description = "The name of the lambda function that handle lacework report"
  default     = "handle-lacework-report"
}

variable "base_stack_unique_tag" {
  default = ""
}

variable "prod" {
  description = "if the terraform run on the production mode"
  default = false
}