# Lacework Integration

Enables reading lacework report data on a scheduled manner and receiving lacework events

Configured by rest API call:

```POST /api/v1/integrations```

```json
 {
     "customer_name": "ACME",
     "name": "lacework",
     "enable": true,
     "params":{
         "aws_account_id" :"111",
         "lacework_account_id"  : "ACME",
         "lacework_secret_access_key" : "a-b-c-1", 
         "lacework_access_key_id" : "a-b-c-1"
     }
 }
 ```
if you'd like to modify the report fetch scheduling rate from the default value 
you can try something like this to fetch every 5 minutes:
```json
 {
     "customer_name": "ACME",
     "name": "lacework",
     "enable": true,
     "params":{
         "aws_account_id" :"111",
         "lacework_account_id"  : "ACME",
         "lacework_secret_access_key" : "a-b-c-1", 
         "lacework_access_key_id" : "a-b-c-1",
         "schedule_rate": 5
     }
 }
 ```