provider "aws" {
  profile = var.profile
  region  = var.region
}

terraform {
  backend "s3" {
    encrypt = true
  }
}

locals {
  rule_name            = "bc-${var.customer_name}-${var.customer_aws_account_id}-lacework-report-trirgger"
  statement_id         = "bc-${local.rule_name}-event"
  lambda_function_name = "${var.target_lambda_name}-${var.base_stack_unique_tag}"
  producers_list = ["lacework-events"]
}

module "es_details" {
  source = "../../../utils/terraform/getESDetails"
  region = var.region
  profile = var.profile
  es_domain_name = "bc-es-${var.customer_name}"
}

data "aws_lambda_function" "lacework_report_handler_lambda_details" {
  function_name = local.lambda_function_name
}

// Creating a scheduled event rule that will trigget the lacework-report multi-tenant lambda function
resource "aws_cloudwatch_event_rule" "lacework_report_scheduled_trigger" {
  count               = var.enable ? 1 : 0
  name                = local.rule_name
  description         = "Trigger the lacework-report-handler lambda function"
  schedule_expression = "rate(${var.schedule_rate} minutes)"
  is_enabled          = false
  tags = {
    customer = var.customer_name
  }
}

// Add the lacework_report multi-tenant lambda function as event rule target
resource "aws_cloudwatch_event_target" "lacework_report_lambda_func_target" {
  count = var.enable ? 1 : 0
  rule  = local.rule_name
  arn   = data.aws_lambda_function.lacework_report_handler_lambda_details.arn
  input = "{\"lacework_account_id\": \"${var.lacework_account_id}\", \"aws_account_id\": \"${var.customer_aws_account_id}\"}"

  depends_on = [
  "aws_cloudwatch_event_rule.lacework_report_scheduled_trigger"]

}

// Add permission to allow the event rule trigger the lambda function
resource "aws_lambda_permission" "allow_rule_trigger" {
  count         = var.enable ? 1 : 0
  statement_id  = local.statement_id
  action        = "lambda:InvokeFunction"
  function_name = local.lambda_function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.lacework_report_scheduled_trigger[0].arn

  depends_on = [
  "aws_cloudwatch_event_target.lacework_report_lambda_func_target"]
}

// Save lacework conf in SSM parameter store
resource "aws_ssm_parameter" "customer_name" {
  name  = "/lacework_stack_${var.base_stack_unique_tag}/${var.lacework_account_id}/customer_name"
  type  = "String"
  value = var.customer_name
  tags = {
    customer = var.customer_name
  }
  overwrite = true
}

resource "aws_ssm_parameter" "lacework_url" {
  name  = "/lacework_stack_${var.base_stack_unique_tag}/${var.lacework_account_id}/lacework_url"
  type  = "String"
  value = "https://${var.lacework_account_id}.lacework.net"
  tags = {
    customer = var.customer_name
  }
  overwrite = true
}

resource "aws_ssm_parameter" "lacework_account" {
  name  = "/lacework_stack_${var.base_stack_unique_tag}/${var.lacework_account_id}/lacework_account"
  type  = "String"
  value = var.lacework_account_id
  tags = {
    customer = var.customer_name
  }
  overwrite = true
}


resource "aws_ssm_parameter" "lacework_access_key_id" {
  name  = "/lacework_stack_${var.base_stack_unique_tag}/${var.lacework_account_id}/access_key_id"
  type  = "SecureString"
  value = var.lacework_access_key_id
  tags = {
    customer = var.customer_name
  }
  overwrite = true
}


resource "aws_ssm_parameter" "lacework_secret_access_key" {
  name  = "/lacework_stack_${var.base_stack_unique_tag}/${var.lacework_account_id}/secret_access_key"
  type  = "SecureString"
  value = var.lacework_secret_access_key
  tags = {
    customer = var.customer_name
  }
  overwrite = true
}

module "consts" {
  source = "../../../utils/terraform/consts"
}


// Create data streams for lacework producer
module "lacework_data_streams" {
  source = "../../../logArchive/infra/customer/dataStreams"
  region = var.region
  producers_list = local.producers_list
  customer_name = var.customer_name
  es_domain_arn = module.es_details.arn
  base_stack_unique_tag = var.base_stack_unique_tag
  suffix_name = var.lacework_account_id

  monitor = {
    enable = true
    sns_topic_error_name = "${module.consts.monitor_sns_topic_error}${var.base_stack_unique_tag}"
  }
}

